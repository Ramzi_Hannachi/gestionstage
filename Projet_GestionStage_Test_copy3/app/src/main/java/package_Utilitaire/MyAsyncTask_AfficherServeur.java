package package_Utilitaire;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.projet_gestionstage_test.R;

public class MyAsyncTask_AfficherServeur extends AsyncTask<Void, Integer, String> 
{
	private ProgressDialog _myProgress;
	private Context _Context;
	private String _strURL;

	public MyAsyncTask_AfficherServeur(Context _Context,String _strURL) 
	{
		this._Context=_Context;
		this._strURL=_strURL;
	}


	@Override
	protected void onPreExecute()
	{
		super.onPreExecute();

		_myProgress=new ProgressDialog(_Context);
		_myProgress.setTitle("Loading ...");
		_myProgress.setIcon(R.drawable.ic_launcher);
		_myProgress.show();
	}

	@Override
	protected String doInBackground(Void... arg0)
	{
		String strURL=ClientHTTP.ADDRESSE_IP+_strURL+".php";

		try{
			URL myUrl = new URL(strURL);
			URLConnection connection = myUrl.openConnection();
			connection.setConnectTimeout(10000);
			connection.connect();
		} catch (Exception e)
		{
			return "Erreur lors de la connexion avec le serveur";
		}

		// Créer un buffer
		StringBuilder builder = new StringBuilder();
		// Créer un client Http
		HttpClient client = new DefaultHttpClient();
		// Créer un obejet httpget pour utiliser la methode get
		HttpGet httpGet = new HttpGet(strURL);
		try {
			// récuperer la réponse
			HttpResponse Response = client.execute(httpGet);
			//
			StatusLine statusLine = Response.getStatusLine();
			// récduper le ack
			int statusCode = statusLine.getStatusCode();
			// si ack =200 cnx avec succée
			if (statusCode == 200)
			{
				HttpEntity entity = Response.getEntity();
				InputStream content = entity.getContent();
				//BufferedReader reader = new BufferedReader(new InputStreamReader(content));
				BufferedReader reader = new BufferedReader(new InputStreamReader(content,"UTF-8"), 8192);
				String line;
				while ((line = reader.readLine()) != null)
				{
					builder.append(line);
				}
			}
			else
			{
				// errur du chargelent
				Toast.makeText(_Context, "pade de cnx", Toast.LENGTH_LONG).show();
			}
		} 
		catch (ClientProtocolException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return builder.toString();
	}

	@Override
	protected void onPostExecute(String result)
	{
		if(_myProgress.isShowing()) 
		{
			_myProgress.dismiss();
		}
		super.onPostExecute(result);

	}

}
