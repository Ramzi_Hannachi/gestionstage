package package_Utilitaire;

public class DataBase_Static
{
	public static final String DB_NAME="memoire_stage.db";
	//TABLE CONDIDATER
	public static final String TABLE_CONDIDATER="condidater";
	public static final String CONDIDATER_COL_CONFIRMATION="confirmation_condidater";
	public static final String CONDIDATER_COL_DATE="date_condidater";
	public static final String CONDIDATER_COL_IdETUDIANT="id_etudiant_condidater";
	public static final String CONDIDATER_COL_IdOFFRESTAGE="id_offreStage_condidater";
	// TABLE CYCLE
	public static final String TABLE_CYCLE="cycle";
	public static final String CYCLE_COL_ID="Id_cycle";
	public static final String CYCLE_COL_NUM="num_cycle";
	public static final String CYCLE_COL_DESIGNATION="designation_cycle";
	public static final String CYCLE_COL_NBrSEM="nbrSem_cycle";
	public static final String CYCLE_COL_NBrFILIERE="nbrfiliere_cycle";
	// TABLE DEMANDE
	public static final String TABLE_DEMANDE="demande";
	public static final String DEMANDE_COL_CONFIRMATION="confirmation_demande";
	public static final String DEMANDE_COL_DATE="date_demande";
	public static final String DEMANDE_COL_IdENSEIGNANT="idensei_demande";
	public static final String DEMANDE_COL_IdETUDIANT="id_etudiant_demande";
	// TABLE DEPARTEMENT
	public static final String TABLE_DEPARTEMENT="departement";
	public static final String DEPARTEMENT_COL_ID="Id_departement";
	public static final String DEPARTEMENT_COL_LIBELLE="libelle_departement";
	public static final String DEPARTEMENT_COL_ABREVIATION="abreviation_departement";
	public static final String DEPARTEMENT_COL_ORDRE="ordre_departement";
	// TABLE DERIGER
	public static final String TABLE_DERIGER="deriger";
	public static final String DERIGER_COL_TYPE="type_deriger";
	public static final String DERIGER_COL_IdENSEIGNANT="idensei_deriger";
	public static final String DERIGER_COL_IdSOUTENANCE="id_soutenance_deriger";
	// TABLE DOCUMENT
	public static final String TABLE_DOCUMENT="document";
	public static final String DOCUMENT_COL_ID="Id_document";
	public static final String DOCUMENT_COL_CV="cv_document";
	public static final String DOCUMENT_COL_LETTReMOTIVATION="lettreM_document";
	public static final String DOCUMENT_COL_IdETUDIANT="id_etudiant_document";
	// TABLE ENCADREUR_PRO
	public static final String TABLE_ENCADREUR_PRO="encadreur_pro";
	public static final String ENCADREURPRO_COL_ID="Id_encadreurPro";//_id
	public static final String ENCADREURPRO_COL_NCIN="ncin_encadreurPro";
	public static final String ENCADREURPRO_COL_NOM="nom_encadreurPro";
	public static final String ENCADREURPRO_COL_PRENOM="prenom_encadreurPro";
	public static final String ENCADREURPRO_COL_NUmTEL="numTel_encadreurPro";
	public static final String ENCADREURPRO_COL_EMAIL="email_encadreurPro";
	// TABLE ENSEIGNANT
	public static final String TABLE_ENSEIGNANT="enseignant";
	public static final String ENSEIGNANT_COL_ID="Idensei";
	public static final String ENSEIGNANT_COL_NCIN="ncin";
	public static final String ENSEIGNANT_COL_NOM="nom";
	public static final String ENSEIGNANT_COL_PRENOM="prenom";
	public static final String ENSEIGNANT_COL_SEXE="sexe";
	public static final String ENSEIGNANT_COL_DATENAISS="dateNaiss";
	public static final String ENSEIGNANT_COL_LIEUNAISS="lieuNaiss";
	public static final String ENSEIGNANT_COL_NUmTEL="numTel";
	public static final String ENSEIGNANT_COL_EMAIL="email";
	public static final String ENSEIGNANT_COL_ADRESSE="adresse";
	public static final String ENSEIGNANT_COL_VILLE="ville";
	public static final String ENSEIGNANT_COL_CODEPOSTALE="codePost";
	public static final String ENSEIGNANT_COL_GRADE="grade";
	public static final String ENSEIGNANT_COL_DIPLOME="diplome";
	public static final String ENSEIGNANT_COL_LIEUDIPLOME="lieuDiplome";
	public static final String ENSEIGNANT_COL_SITUATIONCIVILE="situationCivil";
	public static final String ENSEIGNANT_COL_TITULAIRE="titulaire";
	public static final String ENSEIGNANT_COL_FACtADMIN="factAdmin";
	public static final String ENSEIGNANT_COL_ENCADREMENT="encadrement";
	public static final String ENSEIGNANT_COL_ANNEEDIPLOME="annee_diplome";
	public static final String ENSEIGNANT_COL_EnDETACHEMENT="enDetachement";
	public static final String ENSEIGNANT_COL_IdDepartement="id_departement_enseignant";
	public static final String ENSEIGNANT_COL_MATRICULE="matricule";
	public static final String ENSEIGNANT_COL_SPECIALITE="specialite";

	// TABLE ENTREPRISE
	public static final String TABLE_ENTREPRISE="entreprise";
	public static final String ENTREPRISE_COL_ID="Id_entreprise";//_id
	public static final String ENTREPRISE_COL_LIBELLE="libelle_entreprise";
	public static final String ENTREPRISE_COL_RAISON="raison_entreprise";
	public static final String ENTREPRISE_COL_NUmTEL="numTel_entreprise";
	public static final String ENTREPRISE_COL_EMAIL="email_entreprise";
	public static final String ENTREPRISE_COL_SITEWEB="siteWeb_entreprise";
	public static final String ENTREPRISE_COL_LOGIN="login_entreprise";
	public static final String ENTREPRISE_COL_PASSWORD="password_entreprise";
	public static final String ENTREPRISE_COL_LOGO="logo_entreprise";
	public static final String ENTREPRISE_COL_IdENSEIGNANT="idensei_entreprise";

	// TABLE ETUDIANT
	public static final String TABLE_ETUDIANT="etudiant";
	public static final String ETUDIANT_COL_ID="Id_etudiant";//_id
	public static final String ETUDIANT_COL_NUmINSCRIT="numInscrit_etudiant";
	public static final String ETUDIANT_COL_CIN="cin_etudiant";
	public static final String ETUDIANT_COL_NOM="nom_etudiant";
	public static final String ETUDIANT_COL_PRENOM="prenom_etudiant";
	public static final String ETUDIANT_COL_SEXE="sexe_etudiant";
	public static final String ETUDIANT_COL_DATENAISS="dateNaiss_etudiant";
	public static final String ETUDIANT_COL_LIEUNAISS="lieuNaiss_etudiant";
	public static final String ETUDIANT_COL_NUmTEL="numTel_etudiant";
	public static final String ETUDIANT_COL_EMAIL="email_etudiant";
	public static final String ETUDIANT_COL_ADRESSE="adresse_etudiant";
	public static final String ETUDIANT_COL_CODePOSTAL="codePostal_etudiant";
	public static final String ETUDIANT_COL_IMAGE="image_etudiant";
	public static final String ETUDIANT_COL_IdDEPARTEMENT="id_departement_etudiant";
	public static final String ETUDIANT_COL_IdTRAVAILPFE="id_travailPfe_etudiant";
	public static final String ETUDIANT_COL_IdETUDIANT1="id_etudiant_1";
	public static final String ETUDIANT_COL_IdFILIERE="id_filiere_etudiant";
	public static final String ETUDIANT_COL_IdDOCUMENT="id_document_etudiant";

	// TABLE FILIERE
	public static final String TABLE_FILIERE="filiere";
	public static final String FILIERE_COL_ID="Id_filiere";//_id
	public static final String FILIERE_COL_DESIGNATION="designation_filiere";
	public static final String FILIERE_COL_IdCYCLE="id_cycle_filiere";

	// TABLE NIVEAU
	public static final String TABLE_NIVEAU="niveau";
	public static final String NIVEAU_COL_ID="Id_niveau";//_id
	public static final String NIVEAU_COL_NUM="num_niveau";
	public static final String NIVEAU_COL_DESIGNATION="designation_niveau";
	public static final String NIVEAU_COL_NbMODULE="nbModule_niveau";
	public static final String NIVEAU_COL_NbSECTION="nbSection_niveau";
	public static final String NIVEAU_COL_IdFiliere="id_filiere_niveau";
	//TABLE OFFRE_STAGE
	public static final String TABLE_OFFRESTAGE="offre_stage";
	public static final String OFFRESTAGE_COL_ID="Id_offreStage";//_id
	public static final String OFFRESTAGE_COL_NOM="nom_offreStage";
	public static final String OFFRESTAGE_COL_DESCRIPTION="description_offreStage";
	public static final String OFFRESTAGE_COL_CONFIRMATION="confirmation_offreStage";
	public static final String OFFRESTAGE_COL_DATE="date_offreStage";
	public static final String OFFRESTAGE_COL_IdENTREPRISE="id_entreprise_offreStage";
	public static final String OFFRESTAGE_COL_IdSTAGE="id_stage_offreStage";
	public static final String OFFRESTAGE_COL_IdTRAVAILPFE="id_travailPfe_offreStage";
	public static final String OFFRESTAGE_COL_IdFILIERE="id_filiere_offreStage";
	// TABLE PLAN_AFFAIRE
	public static final String TABLE_PLANAFFAIRE="plant_affaire";
	public static final String PLANAFFAIRE_COL_ID="Id_plantAffaire";//_id
	public static final String PLANAFFAIRE_COL_IdTRAVAILPFE="id_travailPfe_plantAffaire";//_id
	// TABLE PROJET
	public static final String TABLE_PROJET="projet";
	public static final String PROJET_COL_ID="Id_projet";
	public static final String PROJET_COL_IdTRAVAILPFE="id_travailPfe_projet";
	// TABLE SALLE
	public static final String TABLE_SALLE="salle";
	public static final String SALLE_COL_ID="Id_salle";
	public static final String SALLE_COL_BLOC="bloc_salle";
	public static final String SALLE_COL_ETAGE="etage_salle";
	public static final String SALLE_COL_ABREVIATION="abreviation_salle";
	public static final String SALLE_COL_TYPE="type_salle";
	public static final String SALLE_COL_CAPACITE="capacite_salle";
	public static final String SALLE_COL_ETAT="etat_salle";
	public static final String SALLE_COL_VIDEOPRO="videoProject_salle";
	public static final String SALLE_COL_INFORMATIQUE="informatique_salle";
	// TABLE SOUTENANCE
	public static final String TABLE_SOUTENANCE="soutenance";
	public static final String SOUTENANCE_COL_ID="Id_soutenance";
	public static final String SOUTENANCE_COL_TYPE="type_soutenance";
	public static final String SOUTENANCE_COL_HEUReDEB="heureDeb_soutenance";
	public static final String SOUTENANCE_COL_HEUReFIN="heureFin_soutenance";
	public static final String SOUTENANCE_COL_JOUR="jour_soutenance";
	public static final String SOUTENANCE_COL_IdSALLE="id_salle_soutenance";
	public static final String SOUTENANCE_COL_IdTRAVAILPFE="id_travailPfe_soutenance";

	// TBLE STAGE
	public static final String TABLE_STAGE="stage";
	public static final String STAGE_COL_ID="Id_stage";
	public static final String STAGE_COL_NOM="nom_stage";
	public static final String STAGE_COL_DESCRIPTION="description_stage";
	public static final String STAGE_COL_DATE="date_stage";
	public static final String STAGE_COL_IdTRAVAILPFE="id_travailPfe_stage";
	public static final String STAGE_COL_IdENCADREUrPRO="id_encadreurPro_stage";
	public static final String STAGE_COL_IdOFFRESTAGE="id_offreStage_stage";
	public static final String STAGE_COL_IdENTREPRISE="id_entreprise_stage";
	// TABLE TRAVAIL_PFE
	public static final String TABLE_TRAVAILPFE="travail_pfe";
	public static final String TRAVAILPFE_COL_ID="Id_travailPfe";
	public static final String TRAVAILPFE_COL_LIBELLE="libelle_travailPfe";
	public static final String TRAVAILPFE_COL_IdENSEIGNANT="idensei_travailPfe";
	public static final String TRAVAILPFE_COL_IdSOUTENANCE="id_soutenance_travailPfe";






















}
