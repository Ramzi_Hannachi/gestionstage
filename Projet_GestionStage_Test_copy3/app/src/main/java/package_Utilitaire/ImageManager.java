package package_Utilitaire;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

public class ImageManager
{
	public Bitmap TelechargerImage(String url)
	{
		Bitmap _Bitmap=null;
		try 
		{
			URL _URL=new URL(url);
			URLConnection conn=_URL.openConnection();
			conn.connect();
			InputStream _InputStream=conn.getInputStream();
			BufferedInputStream _BufferedInputStream =new BufferedInputStream(_InputStream);
			_Bitmap=BitmapFactory.decodeStream(_BufferedInputStream);
			_BufferedInputStream.close();
			_InputStream.close();

		} catch (Exception e) 
		{
			Log.d("Ramzi TAG", "erreur");	
		}
		return _Bitmap;
	}


}
