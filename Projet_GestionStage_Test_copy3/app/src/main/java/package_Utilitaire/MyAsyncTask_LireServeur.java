package package_Utilitaire;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.example.projet_gestionstage_test.R;

public class MyAsyncTask_LireServeur extends AsyncTask<Void, Integer, String> 
{
	String result = null;
	InputStream is = null;
	JSONObject json_data=null;

	private ProgressDialog _myProgress;
	private Context _Context;
	private ArrayList<NameValuePair> nameValuePairs;
	private String _strURL;

	public MyAsyncTask_LireServeur(Context _Context,ArrayList<NameValuePair> nameValuePairs,String _strURL) 
	{
		this._Context=_Context;
		this.nameValuePairs=nameValuePairs;
		this._strURL=_strURL;
	}


	@Override
	protected void onPreExecute()
	{
		super.onPreExecute();

		_myProgress=new ProgressDialog(_Context);
		_myProgress.setTitle("Loading ...");
		_myProgress.setIcon(R.drawable.ic_launcher);
		_myProgress.show();
	}

	@Override
	protected String doInBackground(Void... arg0)
	{
		String strURL=ClientHTTP.ADDRESSE_IP+_strURL+".php";

		try{
			URL myUrl = new URL(strURL);
			URLConnection connection = myUrl.openConnection();
			connection.setConnectTimeout(10000);
			connection.connect();
		} catch (Exception e)
		{
			return "Erreur lors de la connexion avec le serveur";
		}

		try{
			//commandes httpClient
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(strURL);
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();
		}
		catch(Exception e)
		{
			Log.i("taghttppost",""+e.toString());
			Toast.makeText(_Context,e.toString() ,Toast.LENGTH_LONG).show();
		}
		//conversion de la réponse en chaine de caractère
		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8192);//"iso-8859-1"), 8);  
			StringBuilder sb  = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) 
			{
				sb.append(line + "\n");
			}
			is.close();
			result = sb.toString();
		}
		catch(Exception e)
		{
			Log.i("tagconvertstr",""+e.toString());
		}
		///////// compteur !
		for ( int i=0 ; i < 10; i++)
		{
			try
			{
				Thread.sleep(1000); // arret 1/2 une seconde 
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			publishProgress(i);
		}

		return result;
	}

	@Override
	protected void onProgressUpdate(Integer... values)
	{
		super.onProgressUpdate(values);
		_myProgress.setMessage("Progression : "+values[0]+"0%");
	}

	@Override
	protected void onPostExecute(String result)
	{
		if(_myProgress.isShowing()) 
		{
			_myProgress.dismiss();
		}
		super.onPostExecute(result);

	}

}
