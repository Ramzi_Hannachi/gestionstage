package package_Utilitaire;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.projet_gestionstage_test.R;

public class MyAsyncTask_EcrireServeur extends AsyncTask<Void, Integer, Boolean> 
{

	private ProgressDialog _myProgress;
	private Context _Context;
	private ArrayList<NameValuePair> nameValuePairs;
	private String _strURL;

	public MyAsyncTask_EcrireServeur(Context _Context,ArrayList<NameValuePair> nameValuePairs,String _strURL) 
	{
		this._Context=_Context;
		this.nameValuePairs=nameValuePairs;
		this._strURL=_strURL;
	}


	@Override
	protected void onPreExecute()
	{
		super.onPreExecute();

		_myProgress=new ProgressDialog(_Context);
		_myProgress.setTitle(" Loading ...");
		_myProgress.setIcon(R.drawable.ic_launcher);

		_myProgress.show();
	}

	@Override
	protected Boolean doInBackground(Void... arg0)
	{

		String strURL=ClientHTTP.ADDRESSE_IP+_strURL+".php";

		try{
			URL myUrl = new URL(strURL);
			URLConnection connection = myUrl.openConnection();
			connection.setConnectTimeout(10000);//1
			connection.connect();
		} catch (Exception e)
		{
			Toast.makeText(_Context, "Erreur lors de la connexion avec le serveur", Toast.LENGTH_LONG).show();
			return false;
		}

		// Créer un buffer
		StringBuilder builder = new StringBuilder();
		// Créer un client Http
		HttpClient client = new DefaultHttpClient();
		// Créer un obejet httpget pour utiliser la methode post
		HttpPost httppost = new HttpPost(strURL);
		try {
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			// récuperer la réponse
			HttpResponse response = client.execute(httppost);

			StatusLine statusLine = response.getStatusLine();
			// récduper le ack
			int statusCode = statusLine.getStatusCode();
			// si ack =200 cnx avec succée
			if (statusCode == 200)
			{
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(content));
				String line;
				while ((line = reader.readLine()) != null)
				{
					builder.append(line);
					//Toast.makeText(_mContext, "connxecté", Toast.LENGTH_LONG).show();
				}
			}
			else
			{
				// errur du chargelent
				Toast.makeText(_Context, " Ereur lors du chargement", Toast.LENGTH_LONG).show();
			}
		} 
		catch (ClientProtocolException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return Boolean.parseBoolean(builder.toString());  

	}

	@Override
	protected void onPostExecute(Boolean result)
	{
		if(_myProgress.isShowing()) 
		{
			_myProgress.dismiss();

		}
		super.onPostExecute(result);

	}

}
