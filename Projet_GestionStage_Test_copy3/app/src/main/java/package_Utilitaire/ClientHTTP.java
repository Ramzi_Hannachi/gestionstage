package package_Utilitaire;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

public class ClientHTTP
{
	public static String ADDRESSE_IP="http://192.168.1.3/";
	private Context _mContext;

	String result = null;
	InputStream is = null;
	JSONObject json_data=null;

	public ClientHTTP(Context pContext)
	{
		this._mContext=pContext;
	}

	///////// RAMZI: la lecture du xxxxxxx.php de la DB
	public String readFromUrl(String _strURL)
	{// ' :probleme il n'est pas reconue !!!!!!!!!!!!!!!!!!!!!
		String strURL=ADDRESSE_IP+_strURL+".php";

		// Créer un buffer
		StringBuilder builder = new StringBuilder();
		// Créer un client Http
		HttpClient client = new DefaultHttpClient();
		// Créer un obejet httpget pour utiliser la methode get
		HttpGet httpGet = new HttpGet(strURL);
		try {
			// récuperer la réponse
			HttpResponse Response = client.execute(httpGet);
			//
			StatusLine statusLine = Response.getStatusLine();
			// récduper le ack
			int statusCode = statusLine.getStatusCode();
			// si ack =200 cnx avec succée
			if (statusCode == 200)
			{
				HttpEntity entity = Response.getEntity();
				InputStream content = entity.getContent();
				//BufferedReader reader = new BufferedReader(new InputStreamReader(content));
				BufferedReader reader = new BufferedReader(new InputStreamReader(content,"UTF-8"), 8192);
				String line;
				while ((line = reader.readLine()) != null)
				{
					builder.append(line);
				}
			}
			else
			{
				// errur du chargelent
				Toast.makeText(_mContext, "pade de cnx", Toast.LENGTH_LONG).show();
			}
		} 
		catch (ClientProtocolException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return builder.toString();
	}
	///// Ramzi : Lire et Ecrire 
	public String LireEcrireUrl(String _strURL,ArrayList<NameValuePair> nameValuePairs)
	{// ' :probleme il n'est pas reconue !!!!!!!!!!!!!!!!!!!!!
		String strURL=ADDRESSE_IP+_strURL+".php";

		try{
			//commandes httpClient
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(strURL);
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();
		}
		catch(Exception e)
		{
			Log.i("taghttppost",""+e.toString());
			Toast.makeText(_mContext,e.toString() ,Toast.LENGTH_LONG).show();
		}
		//conversion de la réponse en chaine de caractère
		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8192);//"iso-8859-1"), 8);  
			StringBuilder sb  = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) 
			{
				sb.append(line + "\n");
			}
			is.close();
			result = sb.toString();
		}
		catch(Exception e)
		{
			Log.i("tagconvertstr",""+e.toString());
		}
		return result;
	}
	//Ramzi: ecrire dans la BD 
	public boolean SendToUrl(String _strURL,ArrayList<NameValuePair> nameValuePairs)
	{// ' :probleme il n'est pas reconue !!!!!!!!!!!!!!!!!!!!!
		String strURL=ADDRESSE_IP+_strURL+".php";

		// Créer un buffer
		StringBuilder builder = new StringBuilder();
		// Créer un client Http
		HttpClient client = new DefaultHttpClient();
		// Créer un obejet httpget pour utiliser la methode post
		HttpPost httppost = new HttpPost(strURL);
		try {
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			// récuperer la réponse
			HttpResponse response = client.execute(httppost);

			StatusLine statusLine = response.getStatusLine();
			// récduper le ack
			int statusCode = statusLine.getStatusCode();
			// si ack =200 cnx avec succée
			if (statusCode == 200)
			{
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(content));
				String line;
				while ((line = reader.readLine()) != null)
				{
					builder.append(line);
					//Toast.makeText(_mContext, "connxecté", Toast.LENGTH_LONG).show();
				}
			}
			else
			{
				// errur du chargelent
				Toast.makeText(_mContext, " Ereur lors du chargement", Toast.LENGTH_LONG).show();
			}
		} 
		catch (ClientProtocolException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return Boolean.parseBoolean(builder.toString());  //
	}

}