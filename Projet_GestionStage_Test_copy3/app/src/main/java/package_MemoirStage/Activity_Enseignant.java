package package_MemoirStage;

import java.util.ArrayList;
import java.util.HashMap;

import package_Foction_DataBase.MySQliteFonction_Enseignant;
import package_MyAdapter_ChefDepartement.MyDialog_ChefDepartement_Information;
import package_MyAdapter_Enseignant.MyExpandableListAdapter_Ensiegnant_DemandeEncadreument;
import package_MyAdapter_Enseignant.MyExpandableListAdapter_Ensiegnant_Encadreument;
import package_MyAdapter_Etudiant.TabHost_Personaliser;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

import com.example.projet_gestionstage_test.R;

public class Activity_Enseignant extends Activity implements OnTabChangeListener, OnClickListener
{
	public static String ID_ENSEIGNANT;

	public static ExpandableListView _ExpandableListView_DemandeEncadreument;
	public static ExpandableListView _ExpandableListView_Encadreument;

	private MySQliteFonction_Enseignant _MySQliteFonction_Enseignant;
	private MyDialog_ChefDepartement_Information _MyDialog_ChefDepartement_Information;

	private TextView _TextView_NomPrenom;
	private TextView _TextView_Matricule;
	private TextView _TextView_DateNaiss;
	private TextView _TextView_Grade;
	private ImageView _ImageView_Enseignant;

	private TabHost_Personaliser _PersonaliserTabHost;
	private TabHost _TabHost_Entreprise;
	private TabSpec _TabSpec1;
	private TabSpec _TabSpec2;
	private TabSpec _TabSpec3;

	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_enseignant);

		_TextView_NomPrenom=(TextView) findViewById(R.id.Activity_Enseignant_TextView_Nom_Prenom);
		_TextView_Matricule=(TextView) findViewById(R.id.Activity_Enseignant_TextView_Matricule);
		_TextView_DateNaiss=(TextView) findViewById(R.id.Activity_Enseignant_TextView_DateNaiss);
		_TextView_Grade=(TextView) findViewById(R.id.Activity_Enseignant_textView_Grade);
		_ImageView_Enseignant=(ImageView) findViewById(R.id.Activity_Enseignant_ImageView);

		_ExpandableListView_DemandeEncadreument=(ExpandableListView) findViewById(R.id.Activity_Enseignant_ExpandableListView_DemandeEncadreument);
		_ExpandableListView_Encadreument=(ExpandableListView) findViewById(R.id.Activity_Enseignant_ExpandableListView_Encadreument);

		_TabHost_Entreprise=(TabHost)findViewById(android.R.id.tabhost);
		//TabHost personaliser 
		_PersonaliserTabHost = new TabHost_Personaliser(Activity_Enseignant.this);
		// TabHost:
		_TabHost_Entreprise.setup();

		_TabSpec1=_TabHost_Entreprise.newTabSpec("tag1");
		_TabSpec1.setContent(R.id.Activity_Enseignant_Layout_DemandeEncadreument);
		_TabSpec1.setIndicator("Demandes d'encadreument",getResources().getDrawable(R.drawable.demandestage));
		_TabHost_Entreprise.addTab(_TabSpec1);

		_TabSpec2=_TabHost_Entreprise.newTabSpec("tag2");
		_TabSpec2.setContent(R.id.Activity_Enseignant_Layout_Encadreument);
		_TabSpec2.setIndicator("Encadreument",getResources().getDrawable(R.drawable.encadreument));
		_TabHost_Entreprise.addTab(_TabSpec2);

		_TabSpec3=_TabHost_Entreprise.newTabSpec("tag3");
		_TabSpec3.setContent(R.id.Activity_Enseignant_Layout_Calendrier);
		_TabSpec3.setIndicator("Calendrier",getResources().getDrawable(R.drawable.calendrier));
		_TabHost_Entreprise.addTab(_TabSpec3);


		_PersonaliserTabHost.TabHost_Perso(_TabHost_Entreprise);
		_TabHost_Entreprise.setOnTabChangedListener(this);
		_MySQliteFonction_Enseignant=new MySQliteFonction_Enseignant(this);
		//
		Remplir_EnseignantDepartement();
		//
		Remplir_DemandeEncadreument();
		//
		Remplir_Encadreument();
		//
		_ImageView_Enseignant.setOnClickListener(this);
	}

	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.Activity_Enseignant_ImageView:
			ArrayList<HashMap<String, String>> _ArrayList_Departement=new ArrayList<HashMap<String,String>>();
			_ArrayList_Departement=_MySQliteFonction_Enseignant.Afficher_EnseignantDepartement();
			ArrayList<HashMap<String, String>> _ArrayList_Filiere=new ArrayList<HashMap<String,String>>();
			_ArrayList_Filiere=_MySQliteFonction_Enseignant.Afficher_Filiere();
			if (_ArrayList_Departement.isEmpty()==false && _ArrayList_Filiere.isEmpty()==false) 
			{
				_MyDialog_ChefDepartement_Information=new MyDialog_ChefDepartement_Information(Activity_Enseignant.this, _ArrayList_Departement, _ArrayList_Filiere,"Enseignant");
				_MyDialog_ChefDepartement_Information.MyDialogChefDepartement_Information();
			}
			break;

		default:
			break;
		}
	}

	public void Remplir_Encadreument()
	{
		// impossible de faire clear stage travail etudiant !!
		String _URL0=getResources().getString(R.string.EnseignantActivity_import_StageTravailPfeEtudiantEntreprise);
		_MySQliteFonction_Enseignant.Synchroniser_Serveur_StagePfeEtudiantEntreprise(Activity_Enseignant.ID_ENSEIGNANT, _URL0);

		_MySQliteFonction_Enseignant.ClearAll_Encadreurpro();
		String _URL1=getResources().getString(R.string.EnseignantActivity_import_EncadreurPro);
		_MySQliteFonction_Enseignant.Synchroniser_Serveur_EncadreurPro(Activity_Enseignant.ID_ENSEIGNANT, _URL1);

		ArrayList<HashMap<String, String>> _List_EtudiantEntreprise=new ArrayList<HashMap<String,String>>();
		ArrayList<HashMap<String, String>> _List_Stage=new ArrayList<HashMap<String,String>>();

		_List_EtudiantEntreprise=_MySQliteFonction_Enseignant.Afficher_EtudiantEntreprise_ENCADREMENT();
		_List_Stage=_MySQliteFonction_Enseignant.Afficher_Stage_ENCADREMENT();

		MyExpandableListAdapter_Ensiegnant_Encadreument _MyExpandableListAdapter_Ensiegnant_Encadreument =new MyExpandableListAdapter_Ensiegnant_Encadreument(Activity_Enseignant.this, _List_Stage, _List_EtudiantEntreprise);
		_ExpandableListView_Encadreument.setAdapter(_MyExpandableListAdapter_Ensiegnant_Encadreument);
		_ExpandableListView_Encadreument.setGroupIndicator(null);
	}

	public void Remplir_DemandeEncadreument()
	{
		String _URL=getResources().getString(R.string.EnseignantActivity_import_DemandeEtudiantTravailPfeStageEntreprise);

		_MySQliteFonction_Enseignant.ClearAll_Demande();
		_MySQliteFonction_Enseignant.ClearAll_Etudiant();
		_MySQliteFonction_Enseignant.ClearAll_Entreprise();
		_MySQliteFonction_Enseignant.clearAll_TravailPfe();
		_MySQliteFonction_Enseignant.clearAll_Stage();

		boolean x=_MySQliteFonction_Enseignant.Synchroniser_Serveur_DemandeEtudiantTravailPfeStageEntreprise(ID_ENSEIGNANT, _URL);
		if (x) 
		{
			ArrayList<HashMap<String, String>> _ArrayList_Stage=new ArrayList<HashMap<String,String>>();
			_ArrayList_Stage=_MySQliteFonction_Enseignant.Afficher_Stage_DEMANDeENCADREMENT();

			ArrayList<HashMap<String, String>> _ArrayList_DemandeEtudiantEntreprise=new ArrayList<HashMap<String,String>>();
			_ArrayList_DemandeEtudiantEntreprise=_MySQliteFonction_Enseignant.Afficher_DemandeEtudiantEntreprise_DEMANDeENCADREMENT(Integer.valueOf(ID_ENSEIGNANT));

			MyExpandableListAdapter_Ensiegnant_DemandeEncadreument _MyExpandableListAdapter_Ensiegnant_DemandeEncadreument =new MyExpandableListAdapter_Ensiegnant_DemandeEncadreument(Activity_Enseignant.this, _ArrayList_Stage, _ArrayList_DemandeEtudiantEntreprise);
			_ExpandableListView_DemandeEncadreument.setAdapter(_MyExpandableListAdapter_Ensiegnant_DemandeEncadreument);
			_ExpandableListView_DemandeEncadreument.setGroupIndicator(null);
		}
	}

	public void Remplir_EnseignantDepartement()
	{
		ArrayList<HashMap<String, String>> _ArrayList_Enseignant=new ArrayList<HashMap<String,String>>();
		_ArrayList_Enseignant=_MySQliteFonction_Enseignant.Afficher_EnseignantDepartement();
		ID_ENSEIGNANT =_ArrayList_Enseignant.get(0).get("ID_Enseignant");;
		String _nom=_ArrayList_Enseignant.get(0).get("nom_Enseignant");
		String _prenom=_ArrayList_Enseignant.get(0).get("prenom_Enseignant");
		String _matricule=_ArrayList_Enseignant.get(0).get("matricule_Enseignant");
		String _dateNaiss=_ArrayList_Enseignant.get(0).get("dateNaiss_Enseignant");
		String _grade=_ArrayList_Enseignant.get(0).get("grade_Enseignant");

		_TextView_NomPrenom.setText(_nom+" "+_prenom);
		_TextView_Matricule.setText(_matricule);
		_TextView_DateNaiss.setText(_dateNaiss);
		_TextView_Grade.setText(_grade);
	}








	public void expandGroup(ExpandableListView _ExpandableListView,int groupPosition)
	{
		if(_ExpandableListView.isGroupExpanded(groupPosition))
			_ExpandableListView.collapseGroup(groupPosition);
		else
			_ExpandableListView.expandGroup(groupPosition);
	}


	public void onTabChanged(String arg0)
	{
		_PersonaliserTabHost.TabHost_Perso(_TabHost_Entreprise);
	}

}