package package_MemoirStage;
import java.util.ArrayList;
import java.util.HashMap;

import package_Foction_DataBase.MySQliteFonction_Etudiant;
import package_MyAdapter_Etudiant.MyAdapter_MonStage;
import package_MyAdapter_Etudiant.MyAdapter_OffreStage;
import package_MyAdapter_Etudiant.MyAlertDialog_Binome_InsertDelete;
import package_MyAdapter_Etudiant.MyDialog_EtudiantInformation;
import package_MyAdapter_Etudiant.TabHost_Personaliser;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TextView;

import com.example.projet_gestionstage_test.R;

public class Activity_Etudiant extends Activity implements OnTabChangeListener , android.view.View.OnClickListener, OnLongClickListener
{
	public static String  ID_ETUDIANT;
	public static String NUM_INSCRIPTION;
	public static String Id_FILIERE;
	public static String Id_CYCLE;
	public static String ID_BINOME;

	private MySQliteFonction_Etudiant _MySQliteFonction_Etudiant;
	private MyDialog_EtudiantInformation _MyDialog_EtudiantInformation;
	private MyAlertDialog_Binome_InsertDelete _MyAlertDialog_Binome_InsertDelete;

	private ArrayList<HashMap<String, String>> _List_Binome=new ArrayList<HashMap<String,String>>();
	private	ArrayList<HashMap<String, String>> _List_Etudiant = new ArrayList<HashMap<String,String>>();
	private ArrayList<HashMap<String, String>> _List_OffreStage = new ArrayList<HashMap<String,String>>();

	private ListView _ListView_OffreStage;
	private ListView _ListView_MonStage;

	private Button _Button_Binome_InsertDelete;

	private ImageView _ImageView_Etudiant;

	private TextView _TextView_NomPrenom;
	private TextView _TextView_NumInscription;
	private TextView _TextView_Cycle;
	private TextView _TextView_Filiere;

	private TabHost _TabHost_Etudiant;
	private TabHost.TabSpec _TabSpec1;
	private TabHost.TabSpec _TabSpec2;
	private TabHost.TabSpec _TabSpec3;
	private TabHost.TabSpec _TabSpec4;

	TabHost_Personaliser _PersonaliserTabHost;

	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_etudiant);
		// appelle constructeur
		_PersonaliserTabHost = new TabHost_Personaliser(Activity_Etudiant.this);
		_MySQliteFonction_Etudiant=new MySQliteFonction_Etudiant(this);
		//
		_Button_Binome_InsertDelete=(Button) findViewById(R.id.Activity_Etudiant_Button_AjouterSupprimer_Binome);
		_ImageView_Etudiant=(ImageView) findViewById(R.id.Activity_Etudiant_ImageView_Etudiant);
		_TextView_NomPrenom=(TextView) findViewById(R.id.Activity_Etudiant_TextView_Nom_Prenom);
		_TextView_NumInscription=(TextView) findViewById(R.id.Activity_Etudiant_TextView_NumInscription);
		_TextView_Cycle=(TextView) findViewById(R.id.Activity_Etudiant_TextView_Cycle);
		_TextView_Filiere=(TextView) findViewById(R.id.Activity_Etudiant_TextView_Filiaire);
		_ListView_OffreStage=(ListView) findViewById(R.id.Activity_Etudiant_ListView_OffreStage);
		_ListView_MonStage=(ListView) findViewById(R.id.Activity_Etudiant_ListView_MonStage);

		_TabHost_Etudiant=(TabHost)findViewById(android.R.id.tabhost);
		_TabHost_Etudiant.setup();

		_TabSpec1=_TabHost_Etudiant.newTabSpec("tag1");
		_TabSpec1.setContent(R.id.Activity_Etudiant_Layout_OffreStage);
		_TabSpec1.setIndicator("Entreprise", getResources().getDrawable(R.drawable.offre_stage));
		_TabHost_Etudiant.addTab(_TabSpec1);

		_TabSpec2=_TabHost_Etudiant.newTabSpec("tag2");
		_TabSpec2.setContent(R.id.Activity_Etudiant_Layout_MonStage);
		_TabSpec2.setIndicator("Mon Stage",getResources().getDrawable(R.drawable.mon_stage));
		_TabHost_Etudiant.addTab(_TabSpec2);

		_TabSpec3=_TabHost_Etudiant.newTabSpec("tag3");
		_TabSpec3.setContent(R.id.Activity_Etudiant_Layout_Calendrier);
		_TabSpec3.setIndicator("Calendrier",getResources().getDrawable(R.drawable.calendrier));
		_TabHost_Etudiant.addTab(_TabSpec3);

		_TabSpec4=_TabHost_Etudiant.newTabSpec("tag4");
		_TabSpec4.setContent(R.id.Activity_Etudiant_Layout_Historique);
		_TabSpec4.setIndicator("Historique",getResources().getDrawable(R.drawable.historique));
		_TabHost_Etudiant.addTab(_TabSpec4);
		// TabHost Théme personalier :
		_PersonaliserTabHost.TabHost_Perso(_TabHost_Etudiant);
		_TabHost_Etudiant.setOnTabChangedListener(this);
		/// Saisir Information Etudiant
		Remplir_EtuDepNivFilCyc();
		/// Import Information Binome
		Remplir_Binome();
		/// Import MonStage
		Remplir_MonStage();
		/// Insert Delete Etudiant_1 
		_Button_Binome_InsertDelete.setOnClickListener(this);
		_Button_Binome_InsertDelete.setOnLongClickListener(this);
		/// réagir click image
		_ImageView_Etudiant.setOnClickListener(this);

	}

	@Override
	public void onTabChanged(String tabId) 
	{
		_PersonaliserTabHost.TabHost_Perso(_TabHost_Etudiant);
	}
	public void onClick(View v) 
	{
		switch (v.getId())
		{
		case R.id.Activity_Etudiant_ImageView_Etudiant:
			_MyDialog_EtudiantInformation=new MyDialog_EtudiantInformation(Activity_Etudiant.this,_List_Etudiant);
			_MyDialog_EtudiantInformation.DialogEudiantInformation();			
			break;
		case R.id.Activity_Etudiant_Button_AjouterSupprimer_Binome:
			if (_List_Binome.isEmpty())
			{		
				// Insert Binome
				_MyAlertDialog_Binome_InsertDelete=new MyAlertDialog_Binome_InsertDelete(Activity_Etudiant.this,_Button_Binome_InsertDelete);
				_MyAlertDialog_Binome_InsertDelete.Insert_Binome();
			}else
			{
				// Afficher Binome
				_MyDialog_EtudiantInformation=new MyDialog_EtudiantInformation(Activity_Etudiant.this,_List_Binome);
				_MyDialog_EtudiantInformation.DialogEudiantInformation();			
			}
			break;

		default:
			break;
		}
	}
	public boolean onLongClick(View v)
	{
		switch (v.getId())
		{
		case R.id.Activity_Etudiant_Button_AjouterSupprimer_Binome :
			//// Supprimer Binome
			if (_List_Binome.isEmpty()==false)
			{
				_MyAlertDialog_Binome_InsertDelete=new MyAlertDialog_Binome_InsertDelete(Activity_Etudiant.this,_Button_Binome_InsertDelete);
				_MyAlertDialog_Binome_InsertDelete.Delete_Binome();
			}
			break;

		default:
			break;
		}
		return false;
	}
	public void Remplir_EtuDepNivFilCyc()
	{
		_List_Etudiant=_MySQliteFonction_Etudiant.Afficher_EtuDepNivFilCyc(Activity_Authentification.LOGIN, Activity_Authentification.PASSWORD);
		ID_ETUDIANT=_List_Etudiant.get(0).get("ID_Etudiant");
		NUM_INSCRIPTION=_List_Etudiant.get(0).get("numInscrit_Etudiant");
		Id_FILIERE=_List_Etudiant.get(0).get("Id_filiere_Etudiant");
		Id_CYCLE=_List_Etudiant.get(0).get("ID_cycle");
		_TextView_NomPrenom.setText(_List_Etudiant.get(0).get("nom_Etudiant")+" "+_List_Etudiant.get(0).get("prenom_Etudiant"));
		_TextView_NumInscription.setText(_List_Etudiant.get(0).get("numInscrit_Etudiant"));
		_TextView_Cycle.setText(_List_Etudiant.get(0).get("designation_Cycle"));
		_TextView_Filiere.setText(_List_Etudiant.get(0).get("designation_Filiere"));
	}
	public void Remplir_Binome()
	{
		_List_Binome=_MySQliteFonction_Etudiant.Afficher_Binome(ID_ETUDIANT);
		if (_List_Binome.isEmpty())
		{
			_Button_Binome_InsertDelete.setBackgroundResource(R.drawable.etudiant_button_ajouter_binome_noir);	
		}else 
		{
			ID_BINOME=_List_Binome.get(0).get("ID_Etudiant");
			_Button_Binome_InsertDelete.setBackgroundResource(R.drawable.etudiant_button_ajouter_binome_vert);	
		}
	}
	public void Remplir_OffreStage()
	{
		// clear offreStage Entreprise
		_MySQliteFonction_Etudiant.ClearAll_Entreprise();
		_MySQliteFonction_Etudiant.ClearAll_OffreStage();

		String _URL=getResources().getString(R.string.EtudiantActivity_import_OffreStageEntreprise);
		boolean x=_MySQliteFonction_Etudiant.Synchroniser_Serveur_OffreStageEntreprise(Id_FILIERE, _URL);
		if (x)
		{
			_List_OffreStage=_MySQliteFonction_Etudiant.Afficher_OffreStageEntreprise();
			MyAdapter_OffreStage _Adapter_OffreStage = new MyAdapter_OffreStage(Activity_Etudiant.this, _List_OffreStage);
			_ListView_OffreStage.setAdapter(_Adapter_OffreStage);
		}
	}
	public void Remplir_Condidater()
	{
		// clear Condidater
		_MySQliteFonction_Etudiant.ClearAll_Condidater();

		String _URL=getResources().getString(R.string.EtudiantActivity_import_Condidater);
		_MySQliteFonction_Etudiant.Synchroniser_Serveur_Condidater(ID_ETUDIANT,ID_BINOME,_URL);
	}
	public void Remplir_MonStage() 
	{
		// clear stage travailPfe
		_MySQliteFonction_Etudiant.ClearAll_Entreprise();// juste
		_MySQliteFonction_Etudiant.ClearAll_Stage();
		_MySQliteFonction_Etudiant.ClearAll_TravailPfe();

		boolean x=false;
		String _URL=getResources().getString(R.string.EtudiantActivity_import_StageTravailPfeEntreprise);
		x=_MySQliteFonction_Etudiant.Synchroniser_Serveur_StageTravailPfeEntreprise(ID_ETUDIANT, _URL);
		if (x==true)
		{
			// clear encadreurPro
			_MySQliteFonction_Etudiant.ClearAll_EncadreurPro();
			String _URL1=getResources().getString(R.string.EtudiantActivity_import_EncadreurPro);
			_MySQliteFonction_Etudiant.Synchroniser_Serveur_EncadreurPro(ID_ETUDIANT, _URL1);
			// clear enseignant
			_MySQliteFonction_Etudiant.ClearAll_Enseignant();
			String _URL2=getResources().getString(R.string.EtudiantActivity_import_Enseignant);
			_MySQliteFonction_Etudiant.Synchroniser_Serveur_Enseignant(ID_ETUDIANT, _URL2);

			_MySQliteFonction_Etudiant.ClearAll_Demande();
			String _URL0=getResources().getString(R.string.EtudiantActivity_import_Demande);
			_MySQliteFonction_Etudiant.Synchroniser_Serveur_Demande(Activity_Etudiant.ID_ETUDIANT, Activity_Etudiant.ID_BINOME, _URL0);

			ArrayList<HashMap<String, String>> _List_StageEntreprise=new ArrayList<HashMap<String,String>>();
			ArrayList<HashMap<String, String>> _List_Enseignant=new ArrayList<HashMap<String,String>>();
			ArrayList<HashMap<String, String>> _List_EncadreurAcad=new ArrayList<HashMap<String,String>>();
			ArrayList<HashMap<String, String>> _List_EncadreurPro=new ArrayList<HashMap<String,String>>();
			ArrayList<HashMap<String, String>> _ArrayList_Demande=new ArrayList<HashMap<String,String>>();

			_List_StageEntreprise=_MySQliteFonction_Etudiant.Afficher_StageEntreprise();
			_List_Enseignant=_MySQliteFonction_Etudiant.Afficher_Enseignant();
			_List_EncadreurAcad=_MySQliteFonction_Etudiant.Afficher_EncadreurAca(ID_ETUDIANT);
			_List_EncadreurPro=_MySQliteFonction_Etudiant.Afficher_EncadreurPro();
			_ArrayList_Demande=_MySQliteFonction_Etudiant.Afficher_Demande();

			MyAdapter_MonStage _MyAdapter_MonStage = new MyAdapter_MonStage(Activity_Etudiant.this, _List_StageEntreprise, _List_EncadreurAcad, _List_EncadreurPro,_List_Enseignant,_ArrayList_Demande);
			_ListView_MonStage.setAdapter(_MyAdapter_MonStage);
		}else 
		{
			/// Import Information OffreStage
			Remplir_OffreStage();
			/// Import Condidater
			Remplir_Condidater();	
		}

	}

}

