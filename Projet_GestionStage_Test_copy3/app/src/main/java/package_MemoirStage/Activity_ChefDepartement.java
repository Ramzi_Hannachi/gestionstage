package package_MemoirStage;

import java.util.ArrayList;
import java.util.HashMap;

import package_Foction_DataBase.MySQliteFonction_ChefDepartement;
import package_Foction_DataBase.MySQliteFonction_Entreprise;
import package_MyAdapter_ChefDepartement.MyDialog_ChefDepartement_Information;
import package_MyAdapter_ChefDepartement.MyExpandableListAdapter_ChefDepartement_AffectationEncadreur;
import package_MyAdapter_ChefDepartement.MyExpandableListAdapter_ChefDepartement_Calendrier;
import package_MyAdapter_ChefDepartement.MyExpandableListAdapter_ChefDepartement_DemandeEncadrement;
import package_MyAdapter_ChefDepartement.MyExpandableListAdapter_ChefDepartement_DemandeStage;
import package_MyAdapter_ChefDepartement.MyExpandableListAdapter_ChefDepartement_DepotStage;
import package_MyAdapter_ChefDepartement.MyExpandableListAdapter_ChefDepartement_OffreStage;
import package_MyAdapter_Entreprise.MyDialog_Entreprise_InscriptionInformation;
import package_MyAdapter_Etudiant.TabHost_Personaliser;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

import com.example.projet_gestionstage_test.R;

public class Activity_ChefDepartement extends Activity implements OnTabChangeListener, OnClickListener
{
	public static String ID_DEPARTEMENT;
	private static String ID_CHEfDEPARTEMENT;

	private MyExpandableListAdapter_ChefDepartement_OffreStage _MyExpandableListAdapter_ChefDepartement_OffreStage_ENTREPRISE;
	private MyExpandableListAdapter_ChefDepartement_OffreStage _MyExpandableListAdapter_ChefDepartement_OffreStage_DEPOT;
	private MySQliteFonction_ChefDepartement _MySQliteFonction_ChefDepartement;
	private MyDialog_ChefDepartement_Information _MyDialog_ChefDepartementInformation;

	private ImageView _ImageView_ChefDepartement;
	private TextView _TextView_NomPrenom;
	private TextView _TextView_Matricuel;
	private TextView _TextView_DateNaiss;
	private TextView _TextView_Grade;

	private Button _Button_AjouterEntreprise;

	public static ExpandableListView _ExpandableListView_OffreStage_Entreprise;
	public static ExpandableListView _ExpandableListView_OffreStage_Depot;
	public static ExpandableListView _ExpandableListView_DemandeStage;
	public static ExpandableListView _ExpandableListView_Stage_Depot;
	public static ExpandableListView _ExpandableListView_Stage_DemandeEncadrement;
	public static ExpandableListView _ExpandableListView_AffectationEncadreur;
	public static ExpandableListView _ExpandableListView_Calendrier;

	private TabHost_Personaliser _PersonaliserTabHost;
	private TabHost _TabHost_ChefDepartement;
	private TabSpec _TabSpec1;
	private TabSpec _TabSpec2;
	private TabSpec _TabSpec3;
	private TabSpec _TabSpec4;
	private TabSpec _TabSpec5;
	private TabSpec _TabSpec6;
	private TabSpec _TabSpec7;

	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_chefdepartement);

		_TabHost_ChefDepartement=(TabHost)findViewById(android.R.id.tabhost);

		_ImageView_ChefDepartement=(ImageView) findViewById(R.id.Activity_Chefdepartement_ImageView);
		_TextView_NomPrenom=(TextView) findViewById(R.id.Activity_Chefdepartement_TextView_Nom_Prenom);
		_TextView_Matricuel=(TextView) findViewById(R.id.Activity_Chefdepartement_TextView_Matricule);
		_TextView_DateNaiss=(TextView) findViewById(R.id.Activity_Chefdepartement_TextView_DateNaiss);
		_TextView_Grade=(TextView) findViewById(R.id.Activity_Chefdepartement_textView_Grade);
		_Button_AjouterEntreprise=(Button) findViewById(R.id.Activity_Chefdepartement_Button_Depot_Entreprise);

		// offre stage Entreprise
		_ExpandableListView_OffreStage_Entreprise=(ExpandableListView) findViewById(R.id.Activity_Chefdepartement_ExpandableListView_OffreStageEntreprise);
		// offre stage Depot
		_ExpandableListView_OffreStage_Depot=(ExpandableListView) findViewById(R.id.Activity_Chefdepartement_ExpandableListView_DepotOffreStage);
		// demande stage
		_ExpandableListView_DemandeStage=(ExpandableListView) findViewById(R.id.Activity_Chefdepartement_ExpandableListView_DemandeStage);
		// stage depot
		_ExpandableListView_Stage_Depot=(ExpandableListView) findViewById(R.id.Activity_Chefdepartement_ExpandableListView_DepotStage);
		// demande encadrement
		_ExpandableListView_Stage_DemandeEncadrement=(ExpandableListView) findViewById(R.id.Activity_Chefdepartement_ExpandableListView_DemandeEncadreument);
		// affectation encadreur
		_ExpandableListView_AffectationEncadreur=(ExpandableListView) findViewById(R.id.Activity_Chefdepartement_ExpandableListView_AffectationEncadreument);
		// calendrier
		_ExpandableListView_Calendrier=(ExpandableListView) findViewById(R.id.Activity_Chefdepartement_ExpandableListView_Calendrier);

		//TabHost personaliser 
		_PersonaliserTabHost = new TabHost_Personaliser(Activity_ChefDepartement.this);
		_MySQliteFonction_ChefDepartement=new MySQliteFonction_ChefDepartement(Activity_ChefDepartement.this);
		// TabHost:
		_TabHost_ChefDepartement.setup();

		_TabSpec1=_TabHost_ChefDepartement.newTabSpec("tag1");
		_TabSpec1.setContent(R.id.Activity_Chefdepartement_Layout_OffreStage_Entreprise);
		_TabSpec1.setIndicator("Offre Stage Entreprise",getResources().getDrawable(R.drawable.offre_stage));
		_TabHost_ChefDepartement.addTab(_TabSpec1);

		_TabSpec2=_TabHost_ChefDepartement.newTabSpec("tag2");
		_TabSpec2.setContent(R.id.Activity_Chefdepartement_Layout_OffreStage_Depot);
		_TabSpec2.setIndicator("Dépôt Offre Stage",getResources().getDrawable(R.drawable.depotoffrestage));
		_TabHost_ChefDepartement.addTab(_TabSpec2);

		_TabSpec3=_TabHost_ChefDepartement.newTabSpec("tag3");
		_TabSpec3.setContent(R.id.Activity_Chefdepartement_Layout_DemandeStage);
		_TabSpec3.setIndicator("Demande De Stage",getResources().getDrawable(R.drawable.demandestage));
		_TabHost_ChefDepartement.addTab(_TabSpec3);

		_TabSpec4=_TabHost_ChefDepartement.newTabSpec("tag4");
		_TabSpec4.setContent(R.id.Activity_Chefdepartement_Layout_DepotStage);
		_TabSpec4.setIndicator("Dépôt Stage" ,getResources().getDrawable(R.drawable.depotstage));
		_TabHost_ChefDepartement.addTab(_TabSpec4);

		_TabSpec5=_TabHost_ChefDepartement.newTabSpec("tag5");
		_TabSpec5.setContent(R.id.Activity_Chefdepartement_Layout_DemandeEncadreument);
		_TabSpec5.setIndicator("Demande Encadrement", getResources().getDrawable(R.drawable.boitereception));
		_TabHost_ChefDepartement.addTab(_TabSpec5);

		_TabSpec6=_TabHost_ChefDepartement.newTabSpec("tag6");
		_TabSpec6.setContent(R.id.Activity_Chefdepartement_Layout_AffectationEncadreument);
		_TabSpec6.setIndicator("Affectation Des Encadreurs Académique", getResources().getDrawable(R.drawable.affectation_encadreur));
		_TabHost_ChefDepartement.addTab(_TabSpec6);

		_TabSpec7=_TabHost_ChefDepartement.newTabSpec("tag7");
		_TabSpec7.setContent(R.id.Activity_Chefdepartement_Layout_Calendrier);
		_TabSpec7.setIndicator("Calendrier", getResources().getDrawable(R.drawable.calendrier));
		_TabHost_ChefDepartement.addTab(_TabSpec7);

		_PersonaliserTabHost.TabHost_Perso(_TabHost_ChefDepartement);
		_TabHost_ChefDepartement.setOnTabChangedListener(this);

		// Import ChefDepartement Filiere Departement :
		Remplir_ChefDepartementFiliereDepartement();
		// Import OffreStage Entreprise : 
		Remplir_OffreStageEntreprise_ENTREPRISE();
		//   Depot OffreStage
		Remplir_OffreStageEntreprise_DEPOT();
		/// Import Condidater Etudiant
		Remplir_DemandeStage();
		// Depot Stage
		Remplire_DepotStage();
		// Demande Encadrement
		DemandeEncadrement();
		// Affectation Encadreur
		AffectationEncadreur();  
		// Calendrier
		Calendrier();

		_ImageView_ChefDepartement.setOnClickListener(this);
		_Button_AjouterEntreprise.setOnClickListener(this);
	}

	public void onClick(View v)
	{
		switch (v.getId()) 
		{
		case R.id.Activity_Chefdepartement_Button_Depot_Entreprise:
			/// insertion Entreprise 
			Inscription_Entreprise();
			break;

		case R.id.Activity_Chefdepartement_ImageView:
			ArrayList<HashMap<String, String>> _ArrayList_DepartementChef=new ArrayList<HashMap<String,String>>();
			_ArrayList_DepartementChef=_MySQliteFonction_ChefDepartement.Afficher_EnseignantDepartement();
			ArrayList<HashMap<String, String>> _ArrayList_Filiere=new ArrayList<HashMap<String,String>>();
			_ArrayList_Filiere=_MySQliteFonction_ChefDepartement.Afficher_Filiere();
			if (_ArrayList_DepartementChef.isEmpty()==false && _ArrayList_Filiere.isEmpty()==false) 
			{
				_MyDialog_ChefDepartementInformation=new MyDialog_ChefDepartement_Information(Activity_ChefDepartement.this, _ArrayList_DepartementChef, _ArrayList_Filiere,"ChefDepartement");
				_MyDialog_ChefDepartementInformation.MyDialogChefDepartement_Information();
			}
			break;

		default:
			break;
		}
	}
	public void Remplir_ChefDepartementFiliereDepartement()
	{
		ArrayList<HashMap<String, String>> _ArrayList_DepartementChef=new ArrayList<HashMap<String,String>>();
		_ArrayList_DepartementChef=_MySQliteFonction_ChefDepartement.Afficher_EnseignantDepartement();
		if (!_ArrayList_DepartementChef.isEmpty())
		{
			ID_DEPARTEMENT=_ArrayList_DepartementChef.get(0).get("ID_Departement");
			ID_CHEfDEPARTEMENT = _ArrayList_DepartementChef.get(0).get("ID_Enseignant");;
			String _nom=_ArrayList_DepartementChef.get(0).get("nom_Enseignant");
			String _prenom=_ArrayList_DepartementChef.get(0).get("prenom_Enseignant");
			String _matricule=_ArrayList_DepartementChef.get(0).get("matricule_Enseignant");
			String _dateNaiss=_ArrayList_DepartementChef.get(0).get("dateNaiss_Enseignant");
			String _grade=_ArrayList_DepartementChef.get(0).get("grade_Enseignant");

			_TextView_NomPrenom.setText(_nom+" "+_prenom);
			_TextView_Matricuel.setText(_matricule);
			_TextView_DateNaiss.setText(_dateNaiss);
			_TextView_Grade.setText(_grade);
		}
	}

	public void Remplir_OffreStageEntreprise_ENTREPRISE()
	{
		_MySQliteFonction_ChefDepartement.ClearAll_Entreprise();
		_MySQliteFonction_ChefDepartement.ClearAll_OffreStage();
		_MySQliteFonction_ChefDepartement.Synchroniser_Serveur_OffreStageEntreprise(ID_DEPARTEMENT, getResources().getString(R.string.ChefDepartementActivity_import_OffreStageEntreprise));

		ArrayList<HashMap<String, String>> _List_OffreStage_ENTREPRISE=new ArrayList<HashMap<String,String>>();
		ArrayList<HashMap<String, String>> _List_Entreprise_ENTREPRISE=new ArrayList<HashMap<String,String>>();
		String TYPE_OFFRESTAGE="ENTREPRISE";

		_List_OffreStage_ENTREPRISE=_MySQliteFonction_ChefDepartement.Afficher_OffreStage_ENTREPRISE();
		_List_Entreprise_ENTREPRISE=_MySQliteFonction_ChefDepartement.Afficher_Entreprise_ENTREPRISE();

		if (!_List_Entreprise_ENTREPRISE.isEmpty() && !_List_OffreStage_ENTREPRISE.isEmpty()) 
		{
			_MyExpandableListAdapter_ChefDepartement_OffreStage_ENTREPRISE=new MyExpandableListAdapter_ChefDepartement_OffreStage(Activity_ChefDepartement.this, _List_OffreStage_ENTREPRISE, _List_Entreprise_ENTREPRISE,TYPE_OFFRESTAGE);
			_ExpandableListView_OffreStage_Entreprise.setAdapter(_MyExpandableListAdapter_ChefDepartement_OffreStage_ENTREPRISE);
			_ExpandableListView_OffreStage_Entreprise.setGroupIndicator(null);
		}
	}
	public void Remplir_OffreStageEntreprise_DEPOT()
	{  // importer les entreprise qui non pas d'offre de stage mais qui ont été introduite par le chef de département
		MySQliteFonction_Entreprise _MySQliteFonction_Entreprise=new MySQliteFonction_Entreprise(Activity_ChefDepartement.this);
		_MySQliteFonction_Entreprise.Synchroniser_Serveur_Entreprise(null, null, ID_CHEfDEPARTEMENT, getResources().getString(R.string.EntrepriseActivity_import_Entreprise));

		ArrayList<HashMap<String, String>> _List_OffreStage_DEPOT=new ArrayList<HashMap<String,String>>();
		ArrayList<HashMap<String, String>> _List_Entreprise_DOPOT=new ArrayList<HashMap<String,String>>();
		String TYPE_OFFRESTAGE="DEPOT";

		_List_OffreStage_DEPOT=_MySQliteFonction_ChefDepartement.Afficher_OffreStage_DEPOT();
		_List_Entreprise_DOPOT=_MySQliteFonction_ChefDepartement.Afficher_Entreprise_DEPOT();
		if (!_List_Entreprise_DOPOT.isEmpty() && !_List_OffreStage_DEPOT.isEmpty())
		{
			_MyExpandableListAdapter_ChefDepartement_OffreStage_DEPOT=new MyExpandableListAdapter_ChefDepartement_OffreStage(Activity_ChefDepartement.this, _List_OffreStage_DEPOT, _List_Entreprise_DOPOT,TYPE_OFFRESTAGE);
			_ExpandableListView_OffreStage_Depot.setAdapter(_MyExpandableListAdapter_ChefDepartement_OffreStage_DEPOT);
			_ExpandableListView_OffreStage_Depot.setGroupIndicator(null);
		}
	}
	public void Remplir_DemandeStage()
	{
		_MySQliteFonction_ChefDepartement.ClearAll_Condidater();
		_MySQliteFonction_ChefDepartement.ClearAll_Etudiant();

		_MySQliteFonction_ChefDepartement.Synchroniser_Serveur_CondidaterEtudiant(ID_DEPARTEMENT, getResources().getString(R.string.ChefDepartementActivity_import_CondidaterEtudiant));
		ArrayList<HashMap<String, String>> _List_OffreStage_DEMANDESTAGE=new ArrayList<HashMap<String,String>>();
		_List_OffreStage_DEMANDESTAGE=_MySQliteFonction_ChefDepartement.Afficher_OffreStage_DEMANDESTAGE();
		if (!_List_OffreStage_DEMANDESTAGE.isEmpty())
		{
			MyExpandableListAdapter_ChefDepartement_DemandeStage _MyExpandableListAdapter_ChefDepartement_DemandeStage = new MyExpandableListAdapter_ChefDepartement_DemandeStage(Activity_ChefDepartement.this, _List_OffreStage_DEMANDESTAGE);
			_ExpandableListView_DemandeStage.setAdapter(_MyExpandableListAdapter_ChefDepartement_DemandeStage);
			_ExpandableListView_DemandeStage.setGroupIndicator(null);
		}
	}

	public void Remplire_DepotStage()
	{
		// on ne peut pas faire clearAllEtudiant
		_MySQliteFonction_ChefDepartement.clearAll_Stage(); 
		_MySQliteFonction_ChefDepartement.clearAll_TravailPfe(); 
		_MySQliteFonction_ChefDepartement.clearAll_EncadreurPro();

		String _URL0=getResources().getString(R.string.ChefDepartementActivity_import_StageTravailPfeEtudiant);
		_MySQliteFonction_ChefDepartement.Synchroniser_Serveur_StageTravailPfeEtudiant(ID_DEPARTEMENT, _URL0);

		String _URL1=getResources().getString(R.string.ChefDepartementActivity_import_EncadreurPro);
		_MySQliteFonction_ChefDepartement.Synchroniser_Serveur_EncadreurPro(_URL1);

		ArrayList<HashMap<String, String>> _List_Entreprise_DEPOT=new ArrayList<HashMap<String,String>>();
		ArrayList<HashMap<String, String>> _List_Stage=new ArrayList<HashMap<String,String>>();

		_List_Stage=_MySQliteFonction_ChefDepartement.Afficher_Stage_DEPOTSTAGE();
		_List_Entreprise_DEPOT=_MySQliteFonction_ChefDepartement.Afficher_Entreprise_DEPOT();
		if (!_List_Stage.isEmpty() && !_List_Entreprise_DEPOT.isEmpty())
		{
			MyExpandableListAdapter_ChefDepartement_DepotStage _MyExpandableListAdapter_ChefDepartement_DepotStage =new MyExpandableListAdapter_ChefDepartement_DepotStage(Activity_ChefDepartement.this, _List_Stage, _List_Entreprise_DEPOT);
			_ExpandableListView_Stage_Depot.setAdapter(_MyExpandableListAdapter_ChefDepartement_DepotStage);
			_ExpandableListView_Stage_Depot.setGroupIndicator(null);
		}
	}

	public void DemandeEncadrement()
	{  
		// imporssible de faire clear !!
		String _URL0=getResources().getString(R.string.ChefDepartementActivity_import_EnseignatDepartementFiliereNiveauCycle_ENCADREMENT);
		boolean x=_MySQliteFonction_ChefDepartement.Synchroniser_Serveur_EnseignantDepartementFiliereNiveauCycle_ENCADREMENT(ID_DEPARTEMENT, _URL0);

		_MySQliteFonction_ChefDepartement.clearAll_Demande();
		String _URL1=getResources().getString(R.string.ChefDepartementActivity_import_Demande);
		boolean y=_MySQliteFonction_ChefDepartement.Synchroniser_Serveur_Demande(ID_DEPARTEMENT, _URL1);
		if (x && y)
		{
			ArrayList<HashMap<String, String>> _List_EnseignantDepartement=new ArrayList<HashMap<String,String>>();
			_List_EnseignantDepartement=_MySQliteFonction_ChefDepartement.Afficher_EnseignantDepartement_DEMANDeENCADREMENT();

			if (_List_EnseignantDepartement.isEmpty()==false) 
			{
				MyExpandableListAdapter_ChefDepartement_DemandeEncadrement _MyExpandableListAdapter_ChefDepartement_DemandeEncadrement=new MyExpandableListAdapter_ChefDepartement_DemandeEncadrement(Activity_ChefDepartement.this, _List_EnseignantDepartement);
				_ExpandableListView_Stage_DemandeEncadrement.setAdapter(_MyExpandableListAdapter_ChefDepartement_DemandeEncadrement);
				_ExpandableListView_Stage_DemandeEncadrement.setGroupIndicator(null);
			}
		}
	}

	public void AffectationEncadreur()
	{
		ArrayList<HashMap<String, String>> _List_EnseignantDepartement=new ArrayList<HashMap<String,String>>();
		ArrayList<HashMap<String, String>> _List_TravailStageEntreprise = new ArrayList<HashMap<String,String>>();

		_List_EnseignantDepartement=_MySQliteFonction_ChefDepartement.Afficher_EnseignantDepartement_AFFECTATIOnENCADREUR();
		_List_TravailStageEntreprise=_MySQliteFonction_ChefDepartement.Afficher_TravailStageEntreprise_AFFECTATIOnENCADREUR();

		MyExpandableListAdapter_ChefDepartement_AffectationEncadreur _MyExpandableListAdapter_ChefDepartement_AffectationEncadreur =new MyExpandableListAdapter_ChefDepartement_AffectationEncadreur(Activity_ChefDepartement.this, _List_EnseignantDepartement,_List_TravailStageEntreprise);
		_ExpandableListView_AffectationEncadreur.setAdapter(_MyExpandableListAdapter_ChefDepartement_AffectationEncadreur);
		_ExpandableListView_AffectationEncadreur.setGroupIndicator(null);
	}

	public void Calendrier()
	{
		_MySQliteFonction_ChefDepartement.clearAll_Soutenance();
		_MySQliteFonction_ChefDepartement.clearAll_Deriger();
		_MySQliteFonction_ChefDepartement.clearAll_Salle();

		String _URL0=getResources().getString(R.string.ChefDepartementActivity_import_Soutenance);
		_MySQliteFonction_ChefDepartement.Synchroniser_Serveur_Soutenance(_URL0);

		String _URL1=getResources().getString(R.string.ChefDepartementActivity_import_Deriger);
		_MySQliteFonction_ChefDepartement.Synchroniser_Serveur_Deriger(_URL1);

		String _URL=getResources().getString(R.string.ChefDepartementActivity_import_Salle);
		_MySQliteFonction_ChefDepartement.Synchroniser_Serveur_Salle(_URL);

		ArrayList<HashMap<String, String>> _List_Stage=new ArrayList<HashMap<String,String>>();
		_List_Stage=_MySQliteFonction_ChefDepartement.Stage_CALENDRIER();

		MyExpandableListAdapter_ChefDepartement_Calendrier _MyExpandableListAdapter_ChefDepartement_Calendrier = new MyExpandableListAdapter_ChefDepartement_Calendrier(Activity_ChefDepartement.this, _List_Stage);
		_ExpandableListView_Calendrier.setAdapter(_MyExpandableListAdapter_ChefDepartement_Calendrier);
		_ExpandableListView_Calendrier.setGroupIndicator(null);
	}






	public void Inscription_Entreprise()
	{
		ArrayList<HashMap<String, String>> _list_Entreprise_vide=new ArrayList<HashMap<String,String>>();
		MyDialog_Entreprise_InscriptionInformation _MyDialog_Entreprise_InscriptionInformation = new MyDialog_Entreprise_InscriptionInformation(Activity_ChefDepartement.this, _list_Entreprise_vide, ID_CHEfDEPARTEMENT);
		_MyDialog_Entreprise_InscriptionInformation.DialogEntrepriseInscription();
	}







	public void expandGroup(ExpandableListView _ExpandableListView,int groupPosition)
	{
		if(_ExpandableListView.isGroupExpanded(groupPosition))
			_ExpandableListView.collapseGroup(groupPosition);
		else
			_ExpandableListView.expandGroup(groupPosition);
	}

	@Override
	public void onTabChanged(String arg0)
	{
		_PersonaliserTabHost.TabHost_Perso(_TabHost_ChefDepartement);
	}


}