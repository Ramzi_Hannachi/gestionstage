package package_MemoirStage;

import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;

import package_Foction_DataBase.MySQliteFonction_ChefDepartement;
import package_Foction_DataBase.MySQliteFonction_Enseignant;
import package_Foction_DataBase.MySQliteFonction_Entreprise;
import package_Foction_DataBase.MySQliteFonction_Etudiant;
import package_MyAdapter_Entreprise.MyDialog_Entreprise_InscriptionInformation;
import package_Utilitaire.ClientHTTP;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.example.projet_gestionstage_test.R;

public class Activity_Authentification extends Activity implements OnClickListener 
{
	private static MySQliteFonction_Etudiant _MySQliteFonction_Etudiant;
	private static MySQliteFonction_Entreprise _MySQliteFonction_Entreprise;
	private static MySQliteFonction_ChefDepartement _MySQliteFonction_ChefDepartement;
	private static MySQliteFonction_Enseignant _MySQliteFonction_Enseignant;
	private MyDialog_Entreprise_InscriptionInformation _MyDialog_Entreprise_InscriptionInformation;

	private  boolean sms_connexion=false;
	private RadioGroup _RadioGroup;
	private RadioButton _RadioButton;
	private Button _Button_InscriptionEntreprise;
	private Button _Button_Valider_Authentification;
	private Button _Button_Annuler_Authentification;
	private EditText _EditText_Login_Authentification;
	private EditText _EditText_Password_Authentification;

	public static String LOGIN;
	public static String PASSWORD;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_authentification);
		//
		_MySQliteFonction_Etudiant=new MySQliteFonction_Etudiant(this);
		_MySQliteFonction_Entreprise = new MySQliteFonction_Entreprise(this);
		_MySQliteFonction_ChefDepartement=new MySQliteFonction_ChefDepartement(this);
		_MySQliteFonction_Enseignant=new MySQliteFonction_Enseignant(this);
		/// clear
		Entreprise_ClearAll_BD();
		Etudiant_ClearAll_BD();
		ChefDepartement_ClearAll_BD();
		Enseignant_ClearAll_BD();        

		_Button_InscriptionEntreprise =(Button) findViewById(R.id.Activity_Authentification_Button_InscriptionEntreprise);
		_RadioGroup=(RadioGroup) findViewById(R.id.Activity_Authentification_RadioGroup);
		_RadioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() 
		{
			public void onCheckedChanged(RadioGroup group, int checkedId)
			{
				_Button_Valider_Authentification.setEnabled(true);
				if (_RadioButton!=null)
				{
					RadioButtonBold();	
				}
				int id=_RadioGroup.getCheckedRadioButtonId();
				_RadioButton=(RadioButton) findViewById(id);
				ChangerLogoInscriptionEntreprise();
				RadioButtonAnimation(_RadioButton);
			}
		});
		_Button_Annuler_Authentification=(Button) findViewById(R.id.Activity_Authentification_Button_Annuler);
		_Button_Valider_Authentification=(Button) findViewById(R.id.Activity_Authentification_Button_Valider);
		_Button_Valider_Authentification.setEnabled(false);
		_EditText_Login_Authentification=(EditText) findViewById(R.id.Activity_Authentification_EditText_Login);
		_EditText_Password_Authentification=(EditText) findViewById(R.id.Activity_Authentification_EditText_Password);

		_Button_Annuler_Authentification.setOnClickListener(this);
		_Button_Valider_Authentification.setOnClickListener(this);
		_Button_InscriptionEntreprise.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) 
	{
		switch (v.getId())
		{
		case R.id.Activity_Authentification_Button_InscriptionEntreprise:
			Entreprise_ClearAll_BD();
			ArrayList<HashMap<String, String>> _List_Entreprise_vide=new ArrayList<HashMap<String,String>>();
			_MyDialog_Entreprise_InscriptionInformation = new MyDialog_Entreprise_InscriptionInformation(Activity_Authentification.this, _List_Entreprise_vide,"null");
			_MyDialog_Entreprise_InscriptionInformation.DialogEntrepriseInscription();
			break;
		case R.id.Activity_Authentification_Button_Valider:
			if (_RadioButton.getText().toString().equalsIgnoreCase("Etudiant")) 
			{
				Etudiant_ClearAll_BD();
				LOGIN=_EditText_Login_Authentification.getText().toString();
				PASSWORD=_EditText_Password_Authentification.getText().toString();

				sms_connexion=false;
				sms_connexion=Verifier_Connexion();
				if (sms_connexion)
				{
					sms_connexion=_MySQliteFonction_Etudiant.Synchroniser_Serveur_EtuDepNivFilCyc(LOGIN, PASSWORD, getResources().getString(R.string.EtudiantActivity_import_EtuDepNivFilCyc));
					_MySQliteFonction_Etudiant.Synchroniser_Serveur_Binome(Activity_Authentification.LOGIN,Activity_Authentification.PASSWORD, getResources().getString(R.string.EtudiantActivity_import_Binome));
					if (sms_connexion)
					{
						Intent lIntent_To_ActivityEtudiant = new Intent(Activity_Authentification.this,Activity_Etudiant.class);
						this.startActivity(lIntent_To_ActivityEtudiant);
					}	
				}
			}

			if (_RadioButton.getText().toString().equalsIgnoreCase("Entreprise")) 
			{
				Entreprise_ClearAll_BD();
				LOGIN=_EditText_Login_Authentification.getText().toString();
				PASSWORD=_EditText_Password_Authentification.getText().toString();

				sms_connexion=false; 
				sms_connexion=Verifier_Connexion();
				if (sms_connexion)
				{
					sms_connexion=_MySQliteFonction_Entreprise.Synchroniser_Serveur_Entreprise(LOGIN,PASSWORD,null,getResources().getString(R.string.EntrepriseActivity_import_Entreprise));
					if (sms_connexion)
					{
						Intent lIntent_To_ActivityEntreprise = new Intent(Activity_Authentification.this,Activity_Entreprise.class);
						this.startActivity(lIntent_To_ActivityEntreprise);
					}
				}
			}

			if (_RadioButton.getText().toString().equalsIgnoreCase("Encadreur Acadmique"))
			{
				Enseignant_ClearAll_BD();
				LOGIN=_EditText_Login_Authentification.getText().toString();
				PASSWORD=_EditText_Password_Authentification.getText().toString();

				sms_connexion=false; 
				sms_connexion=Verifier_Connexion();
				if (sms_connexion)
				{
					sms_connexion=_MySQliteFonction_Enseignant.Synchroniser_Serveur_EnseignantDepartementFiliereNiveauCycle(LOGIN,PASSWORD,getResources().getString(R.string.EnseignantActivity_import_EnseignatDepartementFiliereNiveauCycle));
					if (sms_connexion)
					{
						Intent lIntent_To_ActivityEnseignant = new Intent(Activity_Authentification.this,Activity_Enseignant.class);
						this.startActivity(lIntent_To_ActivityEnseignant);
					}
				}
			}

			if (_RadioButton.getText().toString().equalsIgnoreCase("Chef Dpartement"))
			{
				ChefDepartement_ClearAll_BD();
				LOGIN=_EditText_Login_Authentification.getText().toString();
				PASSWORD=_EditText_Password_Authentification.getText().toString();

				sms_connexion=false; 
				sms_connexion=Verifier_Connexion();
				if (sms_connexion)
				{
					sms_connexion=_MySQliteFonction_ChefDepartement.Synchroniser_Serveur_EnseignantDepartementFiliereNiveauCycle(LOGIN,PASSWORD,"Chef departement",getResources().getString(R.string.ChefDepartementActivity_import_EnseignatDepartementFiliereNiveauCycle));
					if (sms_connexion)
					{
						Intent lIntent_To_ActivityChefDepartement = new Intent(Activity_Authentification.this,Activity_ChefDepartement.class);
						this.startActivity(lIntent_To_ActivityChefDepartement);
					}
				}
			}
			break;
		case R.id.Activity_Authentification_Button_Annuler :
			_EditText_Login_Authentification.setText("");
			_EditText_Password_Authentification.setText("");
			break;
		default:
			break;
		}
	}
	public static void Etudiant_ClearAll_BD()
	{
		// clear etudiant
		_MySQliteFonction_Etudiant.ClearAll_Etudiant();
		_MySQliteFonction_Etudiant.ClearAll_Departement();
		_MySQliteFonction_Etudiant.ClearAll_Niveau();
		_MySQliteFonction_Etudiant.ClearAll_Filiere();
		_MySQliteFonction_Etudiant.ClearAll_Cycle();
		_MySQliteFonction_Etudiant.ClearAll_OffreStage();
		_MySQliteFonction_Etudiant.ClearAll_Entreprise();
		_MySQliteFonction_Etudiant.ClearAll_Condidater();
		_MySQliteFonction_Etudiant.ClearAll_Stage();
		_MySQliteFonction_Etudiant.ClearAll_TravailPfe();
		_MySQliteFonction_Etudiant.ClearAll_EncadreurPro();
		_MySQliteFonction_Etudiant.ClearAll_Enseignant();
		_MySQliteFonction_Etudiant.ClearAll_Demande();
	}
	public static void Entreprise_ClearAll_BD()
	{
		// clear entreprise
		_MySQliteFonction_Entreprise.clearAll_Stage();
		_MySQliteFonction_Entreprise.clearAll_TravailPfe();
		_MySQliteFonction_Entreprise.clearAll_EncadreurPro();
		_MySQliteFonction_Entreprise.ClearAll_Entreprise();
		_MySQliteFonction_Entreprise.ClearAll_Filiere();
		_MySQliteFonction_Entreprise.ClearAll_OffreStage();
		_MySQliteFonction_Entreprise.ClearAll_Condidater();
		_MySQliteFonction_Entreprise.ClearAll_Etudiant();
		_MySQliteFonction_Entreprise.ClearAll_Departement();//
		_MySQliteFonction_Entreprise.ClearAll_Niveau();
		_MySQliteFonction_Entreprise.ClearAll_Cycle();
	}
	public static void ChefDepartement_ClearAll_BD()
	{
		_MySQliteFonction_ChefDepartement.clearAll_Soutenance();
		_MySQliteFonction_ChefDepartement.clearAll_Deriger();
		_MySQliteFonction_ChefDepartement.clearAll_Salle();
		_MySQliteFonction_ChefDepartement.clearAll_EncadreurPro();
		_MySQliteFonction_ChefDepartement.clearAll_Demande();
		_MySQliteFonction_ChefDepartement.ClearAll_Condidater();
		_MySQliteFonction_ChefDepartement.ClearAll_Etudiant();
		_MySQliteFonction_ChefDepartement.ClearAll_Entreprise();
		_MySQliteFonction_ChefDepartement.ClearAll_OffreStage();
		_MySQliteFonction_ChefDepartement.ClearAll_Enseignant();
		_MySQliteFonction_ChefDepartement.ClearAll_Filiere();
		_MySQliteFonction_ChefDepartement.ClearAll_Niveau();
		_MySQliteFonction_ChefDepartement.ClearAll_Cycle();
		_MySQliteFonction_ChefDepartement.ClearAll_Departement();
		_MySQliteFonction_ChefDepartement.clearAll_Stage();
		_MySQliteFonction_ChefDepartement.clearAll_TravailPfe();
	}
	public static void Enseignant_ClearAll_BD()
	{
		_MySQliteFonction_Enseignant.ClearAll_Encadreurpro();
		_MySQliteFonction_Enseignant.ClearAll_Demande();
		_MySQliteFonction_Enseignant.clearAll_Stage();
		_MySQliteFonction_Enseignant.ClearAll_Entreprise();
		_MySQliteFonction_Enseignant.clearAll_TravailPfe();
		_MySQliteFonction_Enseignant.ClearAll_Enseignant();
		_MySQliteFonction_Enseignant.ClearAll_Filiere();
		_MySQliteFonction_Enseignant.ClearAll_Niveau();
		_MySQliteFonction_Enseignant.ClearAll_Cycle();
		_MySQliteFonction_Enseignant.ClearAll_Departement();
		_MySQliteFonction_Enseignant.ClearAll_Etudiant();
	}

	public void ChangerLogoInscriptionEntreprise()
	{
		if (_RadioButton.getText().toString().equalsIgnoreCase("Entreprise"))
		{
			_Button_InscriptionEntreprise.setBackgroundResource(R.drawable.button_inscription_entreprise);
		}else
		{
			_Button_InscriptionEntreprise.setBackgroundColor(Color.TRANSPARENT);
		}
	}
	public void RadioButtonBold()
	{
		_RadioButton.setTextColor(getResources().getColor(android.R.color.black));
	}
	public void RadioButtonAnimation(RadioButton _RadioButton1)
	{
		//_RadioButton1.setTypeface(Typeface.DEFAULT_BOLD);
		_RadioButton1.setTextColor(getResources().getColor(R.color.orange));
		Animation vanish =AnimationUtils.loadAnimation(this,R.anim.animation_radiobutton);
		_RadioButton1.startAnimation(vanish);
	}

	public boolean Verifier_Connexion()
	{
		boolean ok=false;
		try{
			URL myUrl = new URL(ClientHTTP.ADDRESSE_IP);
			URLConnection connection = myUrl.openConnection();
			connection.setConnectTimeout(1000);
			connection.connect();
			ok=true;
		} catch (Exception e)
		{
			ok=false;
			AlertDialog.Builder _Builder=new AlertDialog.Builder(Activity_Authentification.this);
			_Builder.setMessage("Erreur lors de la connexion avec le serveur");
			_Builder.show();
		}

		return ok;
	}


}