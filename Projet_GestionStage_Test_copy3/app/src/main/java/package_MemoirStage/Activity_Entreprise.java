package package_MemoirStage;

import java.util.ArrayList;
import java.util.HashMap;

import package_Foction_DataBase.MySQliteFonction_ChefDepartement;
import package_Foction_DataBase.MySQliteFonction_Entreprise;
import package_MyAdapter_Entreprise.MyDialog_Entreprise_DepotOffreStage_InsertUdate;
import package_MyAdapter_Entreprise.MyDialog_Entreprise_InscriptionInformation;
import package_MyAdapter_Entreprise.MyExpandableListAdapter_Entreprise_BoiteReception;
import package_MyAdapter_Entreprise.MyExpandableListAdapter_Entreprise_DepotOffreStage;
import package_MyAdapter_Etudiant.TabHost_Personaliser;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

import com.example.projet_gestionstage_test.R;

public class Activity_Entreprise extends Activity implements OnTabChangeListener, OnClickListener
{
	MySQliteFonction_Entreprise _mySQliteFonction_Entreprise;
	MyDialog_Entreprise_InscriptionInformation _MyDialog_EntrepriseInscription;
	MyDialog_Entreprise_DepotOffreStage_InsertUdate _MyDialog_Entreprise_DepotStageInsert;
	public static String ID_ENTREPRISE;

	private ArrayList<HashMap<String, String>> _List_OffreStage;
	private ArrayList<HashMap<String, String>> _List_Filiere;

	public static ExpandableListView _ExpandableListView_DepotOffreStage;
	public static ExpandableListView _ExpandableListView_BoiteReception;

	private Button _Button_DepotStageAjouter;
	private ImageView _ImageView_entreprise;
	private TextView _TextView_libelle;
	private TextView _TextView_email;
	private TextView _TextView_siteWeb;

	private TabHost_Personaliser _PersonaliserTabHost;
	private TabHost _TabHost_Entreprise;
	private TabSpec _TabSpec1;
	private TabSpec _TabSpec2;
	private TabSpec _TabSpec3;
	private TabSpec _TabSpec4;

	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_entreprise);
		//Appelle  Constructeur 
		_PersonaliserTabHost = new TabHost_Personaliser(Activity_Entreprise.this);
		_mySQliteFonction_Entreprise=new MySQliteFonction_Entreprise(Activity_Entreprise.this);

		_ExpandableListView_BoiteReception=(ExpandableListView) findViewById(R.id.Activity_Entreprise_ExpandableListView_BoiteReception);
		_ExpandableListView_DepotOffreStage=(ExpandableListView) findViewById(R.id.Activity_Entreprise_ExpandableListView_DepotStage);
		_Button_DepotStageAjouter=(Button) findViewById(R.id.Activity_Entreprise_Button_DepotStage_Ajouter);

		_ImageView_entreprise=(ImageView) findViewById(R.id.Activity_Entreprise_ImageView_entreprise);
		_TextView_libelle=(TextView) findViewById(R.id.Activity_Entreprise_TextView_Libelle);
		_TextView_email=(TextView) findViewById(R.id.Activity_Entreprise_TextView_Email);
		_TextView_siteWeb=(TextView) findViewById(R.id.Activity_Entreprise_TextView_SiteWeb);
		//
		_TabHost_Entreprise=(TabHost)findViewById(android.R.id.tabhost);
		// TabHost:
		_TabHost_Entreprise.setup();

		_TabSpec1=_TabHost_Entreprise.newTabSpec("tag1");
		_TabSpec1.setContent(R.id.Activity_Entreprise_Layout_DepotStage);
		_TabSpec1.setIndicator("Depot Offre Stage",getResources().getDrawable(R.drawable.depotoffrestage));
		_TabHost_Entreprise.addTab(_TabSpec1);

		_TabSpec2=_TabHost_Entreprise.newTabSpec("tag2");
		_TabSpec2.setContent(R.id.Activity_Entreprise_Layout_BoiteReception);
		_TabSpec2.setIndicator("Boite Reception",getResources().getDrawable(R.drawable.boitereception));
		_TabHost_Entreprise.addTab(_TabSpec2);

		_TabSpec3=_TabHost_Entreprise.newTabSpec("tag3");
		_TabSpec3.setContent(R.id.Activity_Entreprise_Layout_Calendrier);
		_TabSpec3.setIndicator("Calendrier",getResources().getDrawable(R.drawable.calendrier));
		_TabHost_Entreprise.addTab(_TabSpec3);

		_TabSpec4=_TabHost_Entreprise.newTabSpec("tag4");
		_TabSpec4.setContent(R.id.Activity_Entreprise_Layout_Historique);
		_TabSpec4.setIndicator("Historique",getResources().getDrawable(R.drawable.historique));
		_TabHost_Entreprise.addTab(_TabSpec4);
		/// Théme TabHost :
		_PersonaliserTabHost.TabHost_Perso(_TabHost_Entreprise);
		_TabHost_Entreprise.setOnTabChangedListener(this);
		/// Saisir Information Entreprise:
		RemplirEntreprise();
		/// Importer Filiere:
		Remplir_Filiere();
		/// Importer OffreStage:
		Remplir_DepotStage();
		/// Importer Condidater:
		Remplir_BoiteReception();
		/// image click entreprise :
		_ImageView_entreprise.setOnClickListener(this);
		_Button_DepotStageAjouter.setOnClickListener(this);

	}
	@Override
	public void onTabChanged(String arg0)
	{
		_PersonaliserTabHost.TabHost_Perso(_TabHost_Entreprise);
	}
	public void RemplirEntreprise()
	{
		ArrayList<HashMap<String, String>> _ArrayList = new ArrayList<HashMap<String,String>>();
		_ArrayList=_mySQliteFonction_Entreprise.Afficher_Entreprise();
		ID_ENTREPRISE=_ArrayList.get(0).get("ID_Entreprise");
		String libelle=_ArrayList.get(0).get("libelle_Entreprise");
		//String raison=_ArrayList.get(0).get("raison_Entreprise");
		//String numTel=_ArrayList.get(0).get("numTel_Entreprise");
		String email=_ArrayList.get(0).get("email_Entreprise");
		String siteWeb=_ArrayList.get(0).get("siteWeb_Entreprise");
		//String login=_ArrayList.get(0).get("login_Entreprise");
		//String password=_ArrayList.get(0).get("password_Entreprise");
		//String logo=_ArrayList.get(0).get("logo_Entreprise");

		_TextView_libelle.setText(libelle);
		_TextView_email.setText(email);
		_TextView_siteWeb.setText(siteWeb);
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.Activity_Entreprise_ImageView_entreprise:
			ArrayList<HashMap<String, String>> _List_Entreprise = new ArrayList<HashMap<String,String>>();
			_List_Entreprise=_mySQliteFonction_Entreprise.Afficher_Entreprise();
			_MyDialog_EntrepriseInscription=new MyDialog_Entreprise_InscriptionInformation(Activity_Entreprise.this,_List_Entreprise,"null");
			_MyDialog_EntrepriseInscription.DialogEntrepriseInscription();
			break;
		case R.id.Activity_Entreprise_Button_DepotStage_Ajouter:
			_MyDialog_Entreprise_DepotStageInsert=new MyDialog_Entreprise_DepotOffreStage_InsertUdate(Activity_Entreprise.this,null,Activity_Entreprise.ID_ENTREPRISE);
			_MyDialog_Entreprise_DepotStageInsert.DialogEntrepirse_DepostageInsert();
			break;
		default:
			break;
		}

	}

	public void Remplir_DepotStage()
	{
		_mySQliteFonction_Entreprise.ClearAll_OffreStage();
		_mySQliteFonction_Entreprise.Synchroniser_Serveur_OffreStage(ID_ENTREPRISE, getResources().getString(R.string.EntrepriseActivity_import_OffreStage));

		_List_OffreStage=new ArrayList<HashMap<String,String>>();
		_List_OffreStage=_mySQliteFonction_Entreprise.Afficher_OffreStage_DepotStage();
		_List_Filiere=new ArrayList<HashMap<String,String>>();
		_List_Filiere=_mySQliteFonction_Entreprise.Afficher_Filiere_DepotStage();
		if (_List_OffreStage.isEmpty()==false)
		{
			MyExpandableListAdapter_Entreprise_DepotOffreStage _MyExpandableListAdapter_Entreprise_DepotStage=new MyExpandableListAdapter_Entreprise_DepotOffreStage(Activity_Entreprise.this, _List_Filiere, _List_OffreStage);
			_ExpandableListView_DepotOffreStage.setAdapter(_MyExpandableListAdapter_Entreprise_DepotStage);
			_ExpandableListView_DepotOffreStage.setGroupIndicator(null);
		}
	}

	public void Remplir_Filiere()
	{
		_mySQliteFonction_Entreprise.ClearAll_Filiere();
		_mySQliteFonction_Entreprise.Synchroniser_Serveur_Filiere(getResources().getString(R.string.EntrepriseActivity_import_Filiere));
	}
	public void Remplir_BoiteReception()
	{
		_mySQliteFonction_Entreprise.ClearAll_Condidater();
		_mySQliteFonction_Entreprise.ClearAll_Etudiant();
		_mySQliteFonction_Entreprise.ClearAll_Departement();
		_mySQliteFonction_Entreprise.ClearAll_Niveau();
		_mySQliteFonction_Entreprise.ClearAll_Cycle();
		_mySQliteFonction_Entreprise.clearAll_EncadreurPro();
		_mySQliteFonction_Entreprise.clearAll_TravailPfe();
		_mySQliteFonction_Entreprise.clearAll_Stage();
		// pour l'affectation des encadreur pro
		String _URL0=getResources().getString(R.string.EntrepriseActivity_import_StageTravailPfe);
		_mySQliteFonction_Entreprise.Synchroniser_Serveur_StageTravailPfe(ID_ENTREPRISE, _URL0);
		// pour l'affectation des encadreur pro
		String _URL1=getResources().getString(R.string.ChefDepartementActivity_import_EncadreurPro);
		MySQliteFonction_ChefDepartement _MySQliteFonction_ChefDepartement=new MySQliteFonction_ChefDepartement(Activity_Entreprise.this);
		_MySQliteFonction_ChefDepartement.Synchroniser_Serveur_EncadreurPro(_URL1);

		String _URL=getResources().getString(R.string.EntrepriseActivity_import_CondidaterEtuDepNivCyc);
		boolean x=_mySQliteFonction_Entreprise.Synchroniser_Serveur_CondidaterEtuDepNivCyc(ID_ENTREPRISE, _URL);
		if (x)
		{
			ArrayList<HashMap<String, String>> _List_OffreStage=new ArrayList<HashMap<String,String>>();
			ArrayList<HashMap<String, String>> _List_Condidater=new ArrayList<HashMap<String,String>>();
			_List_OffreStage=_mySQliteFonction_Entreprise.Afficher_OffreStage_BoiteReception();
			_List_Condidater=_mySQliteFonction_Entreprise.Afficher_Condidater_BoiteReception();
			MyExpandableListAdapter_Entreprise_BoiteReception _MyExpandableListAdapter_Entreprise_BoiteReception = new MyExpandableListAdapter_Entreprise_BoiteReception(Activity_Entreprise.this, _List_OffreStage, _List_Condidater);
			_ExpandableListView_BoiteReception.setAdapter(_MyExpandableListAdapter_Entreprise_BoiteReception);
			_ExpandableListView_BoiteReception.setGroupIndicator(null);
		}
	}










	public void expandGroup(ExpandableListView _ExpandableListView,int groupPosition)
	{
		if(_ExpandableListView.isGroupExpanded(groupPosition))
			_ExpandableListView.collapseGroup(groupPosition);
		else
			_ExpandableListView.expandGroup(groupPosition);
	}
}