package package_Foction_DataBase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import package_ClassPersonaliser.Cycle;
import package_ClassPersonaliser.Demande;
import package_ClassPersonaliser.Departement;
import package_ClassPersonaliser.EncadreurPro;
import package_ClassPersonaliser.Enseignant;
import package_ClassPersonaliser.Entreprise;
import package_ClassPersonaliser.Etudiant;
import package_ClassPersonaliser.Filiere;
import package_ClassPersonaliser.Niveau;
import package_ClassPersonaliser.Stage;
import package_ClassPersonaliser.Travail_PFE;
import package_Creator_DataBase.MySQliteCreator_DataBase;
import package_MemoirStage.Activity_Enseignant;
import package_Utilitaire.ClientHTTP;
import package_Utilitaire.DataBase_Static;
import package_Utilitaire.MyAsyncTask_EcrireServeur;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

import com.example.projet_gestionstage_test.R;

public class MySQliteFonction_Enseignant 
{
	private ClientHTTP _ClientHTTP;
	private SQLiteDatabase _SqLiteDatabase;
	private MySQliteCreator_DataBase _SQliteCreator_ProjetStage;
	private Context pContext;

	public MySQliteFonction_Enseignant(Context pContext)
	{
		this.pContext=pContext;
		_SQliteCreator_ProjetStage=new MySQliteCreator_DataBase(pContext, DataBase_Static.DB_NAME, null, 1);
		openDB();
	}
	public void openDB()
	{
		_SqLiteDatabase=_SQliteCreator_ProjetStage.getWritableDatabase();
	}

	///////////////////// requette SQLITE :
	public ArrayList<HashMap<String, String>> Afficher_EncadreurPro_ENCADREMENT(int _ID_EncadreurPro)// ENCADREMENT
	{
		String sql=" select encadreur_pro.*"+
				" from encadreur_pro"+
				" where " +
				"encadreur_pro.Id_encadreurPro = "+_ID_EncadreurPro+" ; ";

		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();
				// Encadreur pro
				String nom_EncadreurPro=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENCADREURPRO_COL_NOM));
				String prenom_EncadreurPro=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENCADREURPRO_COL_PRENOM));
				String numTel_EncadreurPro=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENCADREURPRO_COL_NUmTEL));
				String email_EncadreurPro=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENCADREURPRO_COL_EMAIL));
				_HashMap.put("nom_EncadreurPro",nom_EncadreurPro);
				_HashMap.put("prenom_EncadreurPro",prenom_EncadreurPro);
				_HashMap.put("numTel_EncadreurPro",numTel_EncadreurPro);
				_HashMap.put("email_EncadreurPro",email_EncadreurPro);

				_ArrayList.add(_HashMap);
			}
		}
		return _ArrayList;
	}

	public ArrayList<HashMap<String, String>> Afficher_EtudiantEntreprise_ENCADREMENT()// ENCADREMENT
	{
		String sql=" select distinct etudiant.* , entreprise.* "
				+" from etudiant , entreprise , stage , travail_pfe"
				+" where "
				+" stage.id_entreprise_stage = entreprise.Id_entreprise and"
				+" stage.id_travailPfe_stage = travail_pfe.Id_travailPfe and"
				+" etudiant.id_travailPfe_etudiant = travail_pfe.Id_travailPfe and"
				+" travail_pfe.idensei_travailPfe= "+Activity_Enseignant.ID_ENSEIGNANT+" ; ";

		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();
				// Entreprise
				int ID_Entreprise=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_ID));
				String libelle_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_LIBELLE));
				String raison_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_RAISON));
				String numTel_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_NUmTEL));
				String email_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_EMAIL));
				String siteWeb_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_SITEWEB));
				String login_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_LOGIN));
				String password_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_PASSWORD));
				String logo_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_LOGO));
				_HashMap.put("ID_Entreprise",String.valueOf(ID_Entreprise));
				_HashMap.put("libelle_Entreprise",libelle_Entreprise);
				_HashMap.put("raison_Entreprise",raison_Entreprise);
				_HashMap.put("numTel_Entreprise",numTel_Entreprise);
				_HashMap.put("email_Entreprise",email_Entreprise);
				_HashMap.put("siteWeb_Entreprise",siteWeb_Entreprise);
				_HashMap.put("login_Entreprise",login_Entreprise);
				_HashMap.put("password_Entreprise",password_Entreprise);
				_HashMap.put("logo_Entreprise",logo_Entreprise);
				// Etudiant  :utiliser la fonction AfficherEtudiant pour récupérer les info
				int ID_Etudiant=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_ID));
				String Id_etudiant1_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_IdETUDIANT1));
				String Id_travailPfe_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_IdTRAVAILPFE));
				_HashMap.put("ID_Etudiant",String.valueOf(ID_Etudiant));
				_HashMap.put("Id_etudiant1_Etudiant",Id_etudiant1_Etudiant);
				_HashMap.put("Id_travailPfe_Etudiant",Id_travailPfe_Etudiant);

				_ArrayList.add(_HashMap);
			}
		}
		return _ArrayList;

	}
	public ArrayList<HashMap<String, String>> Afficher_Stage_ENCADREMENT()// ENCADREMENT
	{
		String sql=" select stage.* "
				+" from stage , travail_pfe"
				+" where "
				+" stage.id_travailPfe_stage=travail_pfe.Id_travailPfe and"
				+" travail_pfe.idensei_travailPfe="+Activity_Enseignant.ID_ENSEIGNANT+" ;";

		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();
				int _ID_Stage=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_ID));
				String _nom_Stage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_NOM));
				String _description_Stage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_DESCRIPTION));
				String _date_Stage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_DATE));
				String _Id_travailPfe_Stage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_IdTRAVAILPFE));
				String _Id_encadreurPro_Stage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_IdENCADREUrPRO));
				String _Id_offreStage_Stage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_IdOFFRESTAGE));
				String _Id_entreprise_Stage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_IdENTREPRISE));

				_HashMap.put("_ID_Stage", String.valueOf(_ID_Stage));
				_HashMap.put("_nom_Stage", _nom_Stage);
				_HashMap.put("_description_Stage", _description_Stage);
				_HashMap.put("_date_Stage", _date_Stage);
				_HashMap.put("_Id_travailPfe_Stage", _Id_travailPfe_Stage);
				_HashMap.put("_Id_encadreurPro_Stage", _Id_encadreurPro_Stage);
				_HashMap.put("_Id_offreStage_Stage", _Id_offreStage_Stage);
				_HashMap.put("_Id_entreprise_Stage", _Id_entreprise_Stage);

				_ArrayList.add(_HashMap);
			}
		}
		return _ArrayList;

	}

	public ArrayList<HashMap<String, String>> Afficher_Stage_DEMANDeENCADREMENT()// DEMANDE ENCADREMENT
	{
		String sql=" select distinct stage.* "
				+" from stage , demande , etudiant"
				+" where "
				+" demande.id_etudiant_demande=etudiant.Id_etudiant and "
				+" etudiant.id_travailPfe_etudiant=stage.id_travailPfe_stage  ";
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();
				int _ID_Stage=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_ID));
				String _nom_Stage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_NOM));
				String _description_Stage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_DESCRIPTION));
				String _date_Stage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_DATE));
				String _Id_travailPfe_Stage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_IdTRAVAILPFE));
				String _Id_encadreurPro_Stage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_IdENCADREUrPRO));
				String _Id_offreStage_Stage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_IdOFFRESTAGE));
				String _Id_entreprise_Stage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_IdENTREPRISE));

				_HashMap.put("_ID_Stage", String.valueOf(_ID_Stage));
				_HashMap.put("_nom_Stage", _nom_Stage);
				_HashMap.put("_description_Stage", _description_Stage);
				_HashMap.put("_date_Stage", _date_Stage);
				_HashMap.put("_Id_travailPfe_Stage", _Id_travailPfe_Stage);
				_HashMap.put("_Id_encadreurPro_Stage", _Id_encadreurPro_Stage);
				_HashMap.put("_Id_offreStage_Stage", _Id_offreStage_Stage);
				_HashMap.put("_Id_entreprise_Stage", _Id_entreprise_Stage);

				_ArrayList.add(_HashMap);
			}
		}
		return _ArrayList;

	}
	public ArrayList<HashMap<String, String>> Afficher_DemandeEtudiantEntreprise_DEMANDeENCADREMENT(int _ID_Enseignant)// DEMANDE ENCADREMENT
	{
		String sql=" select  etudiant.*, demande.* , entreprise.* "
				+" from etudiant , demande ,entreprise , stage "
				+" where "
				+ " demande.idensei_demande = "+_ID_Enseignant+" and "
				+" demande.id_etudiant_demande=etudiant.Id_etudiant and "
				+" etudiant.id_travailPfe_etudiant=stage.id_travailPfe_stage and"
				+" stage.id_entreprise_stage=entreprise.Id_entreprise ";
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();
				// Entreprise
				int ID_Entreprise=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_ID));
				String libelle_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_LIBELLE));
				String raison_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_RAISON));
				String numTel_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_NUmTEL));
				String email_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_EMAIL));
				String siteWeb_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_SITEWEB));
				String login_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_LOGIN));
				String password_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_PASSWORD));
				String logo_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_LOGO));
				_HashMap.put("ID_Entreprise",String.valueOf(ID_Entreprise));
				_HashMap.put("libelle_Entreprise",libelle_Entreprise);
				_HashMap.put("raison_Entreprise",raison_Entreprise);
				_HashMap.put("numTel_Entreprise",numTel_Entreprise);
				_HashMap.put("email_Entreprise",email_Entreprise);
				_HashMap.put("siteWeb_Entreprise",siteWeb_Entreprise);
				_HashMap.put("login_Entreprise",login_Entreprise);
				_HashMap.put("password_Entreprise",password_Entreprise);
				_HashMap.put("logo_Entreprise",logo_Entreprise);
				// Etudiant  :utiliser la fonction AfficherEtudiant pour récupérer les info
				int ID_Etudiant=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_ID));
				String Id_etudiant1_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_IdETUDIANT1));
				String Id_travailPfe_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_IdTRAVAILPFE));
				_HashMap.put("ID_Etudiant",String.valueOf(ID_Etudiant));
				_HashMap.put("Id_etudiant1_Etudiant",Id_etudiant1_Etudiant);
				_HashMap.put("Id_travailPfe_Etudiant",Id_travailPfe_Etudiant);
				// Demande 
				String confirmation_Demande=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.DEMANDE_COL_CONFIRMATION));
				String date_Demande=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.DEMANDE_COL_DATE));
				//int Id_enseignant_Demande=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.DEMANDE_COL_IdENSEIGNANT));
				int Id_enseignant_Demande=Integer.valueOf(Activity_Enseignant.ID_ENSEIGNANT);
				int Id_etudiant_Demande=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.DEMANDE_COL_IdETUDIANT));
				_HashMap.put("confirmation_Demande",confirmation_Demande);
				_HashMap.put("date_Demande",date_Demande);
				_HashMap.put("Id_enseignant_Demande", String.valueOf(Id_enseignant_Demande));
				_HashMap.put("Id_etudiant_Demande",String.valueOf(Id_etudiant_Demande));

				_ArrayList.add(_HashMap);
			}
		}
		return _ArrayList;

	}
	public ArrayList<HashMap<String, String>> Afficher_Etudiant_INFORMATION(int _ID_Etudiant)
	{ /// information  etudiant ou binome
		String sql="select etudiant.* ,cycle.*,niveau.* ,filiere.* ,departement.*"
				+" from etudiant ,departement ,filiere ,niveau ,cycle"
				+" where" +
				" etudiant.Id_etudiant = "+_ID_Etudiant+"  AND"
				+" departement.Id_departement=etudiant.id_departement_etudiant AND"
				+" filiere.Id_filiere=etudiant.id_filiere_etudiant AND"
				+" filiere.id_cycle_filiere = cycle.Id_cycle AND"
				+" niveau.id_filiere_niveau=filiere.Id_filiere ;";
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList_Etudiant = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();

				int ID_Etudiant=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_ID));
				String numInscrit_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_NUmINSCRIT));
				String cin_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_CIN));
				String nom_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_NOM));
				String prenom_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_PRENOM));
				String sexe_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_SEXE));
				String dateNaiss_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_DATENAISS));
				String lieuNaiss_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_LIEUNAISS));
				String numTel_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_NUmTEL));
				String email_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_EMAIL));
				String adresse_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_ADRESSE));
				String codePostal_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_CODePOSTAL));
				String image_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_IMAGE));
				int Id_departement_Etudiant=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_IdDEPARTEMENT));
				String Id_travailPfe_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_IdTRAVAILPFE));
				String Id_etudiant1_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_IdETUDIANT1));
				String Id_document_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_IdDOCUMENT));
				int Id_filiere_Etudiant=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_IdFILIERE));
				_HashMap.put("ID_Etudiant",String.valueOf( ID_Etudiant));
				_HashMap.put("numInscrit_Etudiant", numInscrit_Etudiant);
				_HashMap.put("cin_Etudiant", cin_Etudiant);
				_HashMap.put("nom_Etudiant", nom_Etudiant);
				_HashMap.put("prenom_Etudiant", prenom_Etudiant);
				_HashMap.put("sexe_Etudiant", sexe_Etudiant);
				_HashMap.put("dateNaiss_Etudiant", dateNaiss_Etudiant);
				_HashMap.put("lieuNaiss_Etudiant", lieuNaiss_Etudiant);
				_HashMap.put("numTel_Etudiant", numTel_Etudiant);
				_HashMap.put("email_Etudiant", email_Etudiant);
				_HashMap.put("adresse_Etudiant", adresse_Etudiant);
				_HashMap.put("codePostal_Etudiant", codePostal_Etudiant);
				_HashMap.put("image_Etudiant", image_Etudiant);
				_HashMap.put("Id_departement_Etudiant", String.valueOf(Id_departement_Etudiant));
				_HashMap.put("Id_travailPfe_Etudiant", Id_travailPfe_Etudiant);
				_HashMap.put("Id_etudiant1_Etudiant", Id_etudiant1_Etudiant);
				_HashMap.put("Id_document_Etudiant", Id_document_Etudiant);
				_HashMap.put("Id_filiere_Etudiant", String.valueOf(Id_filiere_Etudiant));
				String libelle_Departement=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.DEPARTEMENT_COL_LIBELLE));
				_HashMap.put("libelle_Departement", libelle_Departement);
				String num_Niveau=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.NIVEAU_COL_NUM));
				String designation_Niveau=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.NIVEAU_COL_DESIGNATION));
				_HashMap.put("num_Niveau", num_Niveau);
				_HashMap.put("designation_Niveau", designation_Niveau);
				String designation_Filiere=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.FILIERE_COL_DESIGNATION));
				_HashMap.put("designation_Filiere", designation_Filiere);
				int ID_cycle=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.CYCLE_COL_ID));
				String num_Cycle=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.CYCLE_COL_NUM));
				String designation_Cycle=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.CYCLE_COL_DESIGNATION));
				_HashMap.put("ID_cycle", String.valueOf(ID_cycle));
				_HashMap.put("num_Cycle", num_Cycle);
				_HashMap.put("designation_Cycle", designation_Cycle);	

				_ArrayList_Etudiant.add(_HashMap);
			}
		}
		return _ArrayList_Etudiant;
	}
	public ArrayList<HashMap<String, String>> Afficher_Filiere()
	{
		String sql=" SELECT  * FROM filiere ";
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();
				//FILIERE
				int ID_Filiere=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.FILIERE_COL_ID));
				String designation_Filiere=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.FILIERE_COL_DESIGNATION));

				_HashMap.put("ID_Filiere", String.valueOf(ID_Filiere));
				_HashMap.put("designation_Filiere", designation_Filiere);

				_ArrayList.add(_HashMap);
			}
		}
		return _ArrayList;
	}
	public ArrayList<HashMap<String, String>> Afficher_EnseignantDepartement()
	{
		String sql=" SELECT  * FROM departement, enseignant ";
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();
				// ENSEIGNANT
				int ID_Enseignant=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_ID));
				String cin_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_NCIN));
				String nom_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_NOM));
				String prenom_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_PRENOM));
				String sexe_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_SEXE));
				String dateNaiss_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_DATENAISS));
				String lieuNaiss_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_LIEUNAISS));
				String numTel_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_NUmTEL));
				String email_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_EMAIL));
				String adresse_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_ADRESSE));
				String ville_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_VILLE));
				String codePost_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_CODEPOSTALE));
				String grade_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_GRADE));
				String diplome_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_DIPLOME));
				String lieuDipl_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_LIEUDIPLOME));
				String situation_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_SITUATIONCIVILE));
				String titulaire_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_TITULAIRE));
				String factAdm_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_FACtADMIN));
				String encadrement_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_ENCADREMENT));
				String anneeDip_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_ANNEEDIPLOME));
				String enDetachement_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_EnDETACHEMENT));
				String matricule_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_MATRICULE));
				String specialite_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_SPECIALITE));
				_HashMap.put("ID_Enseignant",String.valueOf(ID_Enseignant ));
				_HashMap.put("cin_Enseignant",cin_Enseignant );  
				_HashMap.put("nom_Enseignant",nom_Enseignant );
				_HashMap.put("prenom_Enseignant", prenom_Enseignant);
				_HashMap.put("sexe_Enseignant", sexe_Enseignant);
				_HashMap.put("dateNaiss_Enseignant",dateNaiss_Enseignant );
				_HashMap.put("lieuNaiss_Enseignant", lieuNaiss_Enseignant);
				_HashMap.put("numTel_Enseignant",numTel_Enseignant );
				_HashMap.put("email_Enseignant",email_Enseignant );
				_HashMap.put("adresse_Enseignant",adresse_Enseignant );
				_HashMap.put("ville_Enseignant",ville_Enseignant );
				_HashMap.put("codePost_Enseignant", codePost_Enseignant);
				_HashMap.put("grade_Enseignant", grade_Enseignant);
				_HashMap.put("diplome_Enseignant", diplome_Enseignant);
				_HashMap.put("lieuDipl_Enseignant",lieuDipl_Enseignant );
				_HashMap.put("situation_Enseignant",situation_Enseignant );
				_HashMap.put("titulaire_Enseignant",titulaire_Enseignant );
				_HashMap.put("factAdm_Enseignant",factAdm_Enseignant );
				_HashMap.put("encadrement_Enseignant",encadrement_Enseignant );
				_HashMap.put("anneeDip_Enseignant",anneeDip_Enseignant );
				_HashMap.put("enDetachement_Enseignant",enDetachement_Enseignant );
				_HashMap.put("matricule_Enseignant",matricule_Enseignant );
				_HashMap.put("specialite_Enseignant",specialite_Enseignant );
				// DEPARTEMENT
				int ID_Departement=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.DEPARTEMENT_COL_ID));
				String libelle_Departement=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.DEPARTEMENT_COL_LIBELLE));
				_HashMap.put("ID_Departement",String.valueOf( ID_Departement));
				_HashMap.put("libelle_Departement", libelle_Departement);

				_ArrayList.add(_HashMap);
			}
		}
		return _ArrayList;
	}
	/////////////////////////// SYNCRONISATION SRVEUR :
	public boolean Synchroniser_Serveur_EncadreurPro(String _ID_Enseignant,String _URL)
	{
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();
		NameValuePair lParamNom=new BasicNameValuePair("PARAM_ID_ENSEIGNANT",_ID_Enseignant);
		lListofParams.add(lParamNom);
		_ClientHTTP=new ClientHTTP(pContext);
		String lRetour_Connecxion=_ClientHTTP.LireEcrireUrl(_URL, lListofParams);

		//		MyAsyncTask_LireServeur _MyAsyncTask =new MyAsyncTask_LireServeur(pContext, lListofParams, _URL);
		//		String lRetour_Connecxion=null;
		//		try {
		//			lRetour_Connecxion = _MyAsyncTask.execute().get();
		//		} catch (InterruptedException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		} catch (ExecutionException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		}

		try {
			JSONArray lArray_Etudiant=new JSONArray(lRetour_Connecxion);
			for (int i = 0; i < lArray_Etudiant.length(); i++)
			{
				JSONObject lObjc_P=lArray_Etudiant.getJSONObject(i);
				// EncadreurPro
				int ID_EncadreurPro=lObjc_P.getInt(DataBase_Static.ENCADREURPRO_COL_ID);
				String cin_EncadreurPro=lObjc_P.getString(DataBase_Static.ENCADREURPRO_COL_NCIN);
				String nom_EncadreurPro=lObjc_P.getString(DataBase_Static.ENCADREURPRO_COL_NOM);
				String prenom_EncadreurPro=lObjc_P.getString(DataBase_Static.ENCADREURPRO_COL_PRENOM);
				String numTel_EncadreurPro=lObjc_P.getString(DataBase_Static.ENCADREURPRO_COL_NUmTEL);
				String email_EncadreurPro=lObjc_P.getString(DataBase_Static.ENCADREURPRO_COL_EMAIL);
				EncadreurPro _EncadreurPro=new EncadreurPro(ID_EncadreurPro, cin_EncadreurPro, nom_EncadreurPro, prenom_EncadreurPro, numTel_EncadreurPro, email_EncadreurPro);

				int x=InsertInTo_BD_EncadreurPro(_EncadreurPro);
				if (  x==-1 ) 
				{
					Toast.makeText( pContext , "ERREUR Lors de l'Insertion de Donnees " ,Toast.LENGTH_LONG).show();
					return false;
				}
			}
		} 
		catch(JSONException e)
		{
			Toast.makeText( pContext , " Aucun Encadreur pro ..... " ,Toast.LENGTH_LONG).show();
			return false;
		}
		return true;
	}

	public boolean Synchroniser_Serveur_StagePfeEtudiantEntreprise(String _ID_Enseignant,String _URL)
	{
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();
		NameValuePair lParamNom=new BasicNameValuePair("PARAM_ID_ENSEIGNANT",_ID_Enseignant);
		lListofParams.add(lParamNom);
		_ClientHTTP=new ClientHTTP(pContext);
		String lRetour_Connecxion=_ClientHTTP.LireEcrireUrl(_URL, lListofParams);

		//		MyAsyncTask_LireServeur _MyAsyncTask =new MyAsyncTask_LireServeur(pContext, lListofParams, _URL);
		//		String lRetour_Connecxion=null;
		//		try {
		//			lRetour_Connecxion = _MyAsyncTask.execute().get();
		//		} catch (InterruptedException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		} catch (ExecutionException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		}

		try {
			JSONArray lArray_Etudiant=new JSONArray(lRetour_Connecxion);
			for (int i = 0; i < lArray_Etudiant.length(); i++)
			{
				JSONObject lObjc_P=lArray_Etudiant.getJSONObject(i);
				// Etudiant
				int ID_Etudiant=lObjc_P.getInt(DataBase_Static.ETUDIANT_COL_ID);
				String numInscrit_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_NUmINSCRIT);
				String cin_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_CIN);
				String nom_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_NOM);
				String prenom_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_PRENOM);
				String sexe_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_SEXE);
				String dateNaiss_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_DATENAISS);
				String lieuNaiss_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_LIEUNAISS);
				String numTel_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_NUmTEL);
				String email_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_EMAIL);
				String adresse_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_ADRESSE);
				String codePostale_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_CODePOSTAL);
				String image_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_IMAGE);
				int cle_departement_Etudiant=lObjc_P.getInt(DataBase_Static.ETUDIANT_COL_IdDEPARTEMENT);
				Integer cle_etudiant1_Etudiant=null;
				int cle_filiere_Etudiant=lObjc_P.getInt(DataBase_Static.ETUDIANT_COL_IdFILIERE);
				Integer cle_travailPfe_Etudiant=Verifier_Integer(lObjc_P.getString(DataBase_Static.ETUDIANT_COL_IdTRAVAILPFE));
				boolean cle_etudiant1_Test=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_IdETUDIANT1).equals("null");
				Integer cle_document_Etudiant=Verifier_Integer(lObjc_P.getString(DataBase_Static.ETUDIANT_COL_IdDOCUMENT));
				if (cle_etudiant1_Test==false)
				{
					cle_etudiant1_Etudiant=lObjc_P.getInt(DataBase_Static.ETUDIANT_COL_IdETUDIANT1);
					//Synchroniser_Serveur_Binome(String.valueOf(cle_etudiant1_Etudiant), pContext.getResources().getString(R.string.EntrepriseActivity_import_CondidaterBinome));
				}
				Etudiant _Classe_Etudiant=new Etudiant(ID_Etudiant, numInscrit_Etudiant, cin_Etudiant, nom_Etudiant, prenom_Etudiant, sexe_Etudiant, dateNaiss_Etudiant, lieuNaiss_Etudiant, numTel_Etudiant, email_Etudiant, adresse_Etudiant, codePostale_Etudiant, image_Etudiant, cle_departement_Etudiant, cle_travailPfe_Etudiant, cle_etudiant1_Etudiant, cle_filiere_Etudiant, cle_document_Etudiant);
				// Travail Pfe
				int _ID_TravailPfe=lObjc_P.getInt(DataBase_Static.TRAVAILPFE_COL_ID);
				String _libelle_TravailPfe=lObjc_P.getString(DataBase_Static.TRAVAILPFE_COL_LIBELLE);
				Integer _Id_enseignant_TravailPfe=Verifier_Integer(lObjc_P.getString(DataBase_Static.TRAVAILPFE_COL_IdENSEIGNANT));
				Integer _Id_soutenance_TravailPfe=Verifier_Integer(lObjc_P.getString(DataBase_Static.TRAVAILPFE_COL_IdSOUTENANCE));
				Travail_PFE _Travail_PFE=new Travail_PFE(_ID_TravailPfe, _libelle_TravailPfe, _Id_enseignant_TravailPfe, _Id_soutenance_TravailPfe);
				// Entreprise
				int lID_Entreprise=lObjc_P.getInt(DataBase_Static.ENTREPRISE_COL_ID);
				String libelle_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_LIBELLE);
				String raison_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_RAISON);
				String numTel_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_NUmTEL);
				String email_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_EMAIL);
				String siteWeb_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_SITEWEB);
				String login_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_LOGIN);
				String password_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_PASSWORD);
				String logo_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_LOGO);
				Integer Id_enseignant_Entreprise=Verifier_Integer(lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_IdENSEIGNANT));
				Entreprise _Entreprise =new Entreprise(lID_Entreprise, libelle_Entreprise, raison_Entreprise, numTel_Entreprise, email_Entreprise, siteWeb_Entreprise, login_Entreprise, password_Entreprise, logo_Entreprise,Id_enseignant_Entreprise);
				// Stage
				int _ID_Stage=lObjc_P.getInt(DataBase_Static.STAGE_COL_ID);
				String _nom_Stage=lObjc_P.getString(DataBase_Static.STAGE_COL_NOM);
				String _date_Stage=lObjc_P.getString(DataBase_Static.STAGE_COL_DATE);
				int _Id_travailPfe_Stage=lObjc_P.getInt(DataBase_Static.STAGE_COL_IdTRAVAILPFE);
				Integer _Id_encadreurPro_Stage=Verifier_Integer(lObjc_P.getString(DataBase_Static.STAGE_COL_IdENCADREUrPRO));
				Integer _Id_offreStage_Stage=Verifier_Integer(lObjc_P.getString(DataBase_Static.STAGE_COL_IdOFFRESTAGE));
				String _description_Stage=lObjc_P.getString(DataBase_Static.STAGE_COL_DESCRIPTION);
				int _Id_entreprise_Stage=lObjc_P.getInt(DataBase_Static.STAGE_COL_IdENTREPRISE);
				Stage _Stage = new Stage(_ID_Stage, _nom_Stage, _description_Stage, _date_Stage, _Id_travailPfe_Stage, _Id_encadreurPro_Stage, _Id_offreStage_Stage, _Id_entreprise_Stage);
				Log.d("_ID_Stage", String.valueOf(_ID_Stage));
				InsertInTo_BD_Entreprise(_Entreprise);
				InsertInTo_BD_Stage(_Stage);
				InsertInTo_BD_TravailPfe(_Travail_PFE);
				InsertInTo_BD_Etudiant(_Classe_Etudiant); 

			}
		} 
		catch(JSONException e)
		{
			//Toast.makeText( pContext , "  " ,Toast.LENGTH_LONG).show();
			return false;
		}
		return true;
	}

	public boolean Synchroniser_Serveur_Binome(String _ID_Etudiant,String URL_Connexion)
	{
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();
		NameValuePair lParamID_Enterprise=new BasicNameValuePair("PARAM_ID_ETUDIANT",_ID_Etudiant);
		lListofParams.add(lParamID_Enterprise);
		_ClientHTTP=new ClientHTTP(pContext);
		String lRetour_Connecxion=_ClientHTTP.LireEcrireUrl(URL_Connexion, lListofParams);

		//		MyAsyncTask_LireServeur _MyAsyncTask =new MyAsyncTask_LireServeur(pContext, lListofParams, URL_Connexion);
		//		String lRetour_Connecxion=null;
		//		try {
		//			lRetour_Connecxion = _MyAsyncTask.execute().get();
		//		} catch (InterruptedException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		} catch (ExecutionException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		}

		try{
			JSONArray lArray_Condidater=new JSONArray(lRetour_Connecxion);
			for (int i = 0; i < lArray_Condidater.length(); i++)
			{ 
				JSONObject lObjc_P=lArray_Condidater.getJSONObject(i);

				int ID_Etudiant=lObjc_P.getInt(DataBase_Static.ETUDIANT_COL_ID);
				String numInscrit_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_NUmINSCRIT);
				String cin_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_CIN);
				String nom_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_NOM);
				String prenom_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_PRENOM);
				String sexe_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_SEXE);
				String dateNaiss_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_DATENAISS);
				String lieuNaiss_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_LIEUNAISS);
				String numTel_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_NUmTEL);
				String email_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_EMAIL);
				String adresse_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_ADRESSE);
				String codePostale_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_CODePOSTAL);
				String image_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_IMAGE);
				int cle_departement_Etudiant=lObjc_P.getInt(DataBase_Static.ETUDIANT_COL_IdDEPARTEMENT);
				int cle_filiere_Etudiant=lObjc_P.getInt(DataBase_Static.ETUDIANT_COL_IdFILIERE);
				Integer cle_travailPfe_Etudiant=Verifier_Integer(lObjc_P.getString(DataBase_Static.ETUDIANT_COL_IdTRAVAILPFE));
				Integer cle_etudiant1_Etudiant=Verifier_Integer(lObjc_P.getString(DataBase_Static.ETUDIANT_COL_IdETUDIANT1));
				Integer cle_document_Etudiant=Verifier_Integer(lObjc_P.getString(DataBase_Static.ETUDIANT_COL_IdDOCUMENT));
				Etudiant _Etudiant=new Etudiant(ID_Etudiant, numInscrit_Etudiant, cin_Etudiant, nom_Etudiant, prenom_Etudiant, sexe_Etudiant, dateNaiss_Etudiant, lieuNaiss_Etudiant, numTel_Etudiant, email_Etudiant, adresse_Etudiant, codePostale_Etudiant, image_Etudiant, cle_departement_Etudiant, cle_travailPfe_Etudiant, cle_etudiant1_Etudiant, cle_filiere_Etudiant, cle_document_Etudiant);
				InsertInTo_BD_Etudiant(_Etudiant);
			}
		}catch(JSONException e)
		{
			Toast.makeText( pContext , "ERREUR " ,Toast.LENGTH_LONG).show();
			return false;
		}
		return true;
	}
	public boolean Synchroniser_Serveur_DemandeEtudiantTravailPfeStageEntreprise(String _ID_Enseignant,String _URL)
	{
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();
		NameValuePair lParamNom=new BasicNameValuePair("PARAM_ID_ENSEIGNANT",_ID_Enseignant);
		lListofParams.add(lParamNom);
		_ClientHTTP=new ClientHTTP(pContext);
		String lRetour_Connecxion=_ClientHTTP.LireEcrireUrl(_URL, lListofParams);

		//		MyAsyncTask_LireServeur _MyAsyncTask =new MyAsyncTask_LireServeur(pContext, lListofParams, _URL);
		//		String lRetour_Connecxion=null;
		//		try {
		//			lRetour_Connecxion = _MyAsyncTask.execute().get();
		//		} catch (InterruptedException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		} catch (ExecutionException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		}

		try {
			JSONArray lArray_Etudiant=new JSONArray(lRetour_Connecxion);
			for (int i = 0; i < lArray_Etudiant.length(); i++)
			{
				JSONObject lObjc_P=lArray_Etudiant.getJSONObject(i);
				// Demande
				String _confirmation_Demande=lObjc_P.getString(DataBase_Static.DEMANDE_COL_CONFIRMATION);
				String _date_Demande=lObjc_P.getString(DataBase_Static.DEMANDE_COL_DATE);
				int _Id_enseignant_Demande=Integer.parseInt(_ID_Enseignant);
				int _Id_etudiant_Demande=lObjc_P.getInt(DataBase_Static.DEMANDE_COL_IdETUDIANT);
				Demande _ClasseDemande=new Demande(_confirmation_Demande, _date_Demande, _Id_enseignant_Demande, _Id_etudiant_Demande);
				// Etudiant
				int ID_Etudiant=lObjc_P.getInt(DataBase_Static.ETUDIANT_COL_ID);
				String numInscrit_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_NUmINSCRIT);
				String cin_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_CIN);
				String nom_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_NOM);
				String prenom_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_PRENOM);
				String sexe_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_SEXE);
				String dateNaiss_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_DATENAISS);
				String lieuNaiss_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_LIEUNAISS);
				String numTel_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_NUmTEL);
				String email_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_EMAIL);
				String adresse_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_ADRESSE);
				String codePostale_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_CODePOSTAL);
				String image_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_IMAGE);
				int cle_departement_Etudiant=lObjc_P.getInt(DataBase_Static.ETUDIANT_COL_IdDEPARTEMENT);
				Integer cle_etudiant1_Etudiant=null;
				int cle_filiere_Etudiant=lObjc_P.getInt(DataBase_Static.ETUDIANT_COL_IdFILIERE);
				Integer cle_travailPfe_Etudiant=Verifier_Integer(lObjc_P.getString(DataBase_Static.ETUDIANT_COL_IdTRAVAILPFE));
				boolean cle_etudiant1_Test=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_IdETUDIANT1).equals("null");
				Integer cle_document_Etudiant=Verifier_Integer(lObjc_P.getString(DataBase_Static.ETUDIANT_COL_IdDOCUMENT));
				if (cle_etudiant1_Test==false)
				{
					cle_etudiant1_Etudiant=lObjc_P.getInt(DataBase_Static.ETUDIANT_COL_IdETUDIANT1);
					Synchroniser_Serveur_Binome(String.valueOf(cle_etudiant1_Etudiant), pContext.getResources().getString(R.string.EntrepriseActivity_import_CondidaterBinome));
				}
				Etudiant _Classe_Etudiant=new Etudiant(ID_Etudiant, numInscrit_Etudiant, cin_Etudiant, nom_Etudiant, prenom_Etudiant, sexe_Etudiant, dateNaiss_Etudiant, lieuNaiss_Etudiant, numTel_Etudiant, email_Etudiant, adresse_Etudiant, codePostale_Etudiant, image_Etudiant, cle_departement_Etudiant, cle_travailPfe_Etudiant, cle_etudiant1_Etudiant, cle_filiere_Etudiant, cle_document_Etudiant);
				// Travail Pfe
				int _ID_TravailPfe=lObjc_P.getInt(DataBase_Static.TRAVAILPFE_COL_ID);
				String _libelle_TravailPfe=lObjc_P.getString(DataBase_Static.TRAVAILPFE_COL_LIBELLE);
				Integer _Id_enseignant_TravailPfe=Verifier_Integer(lObjc_P.getString(DataBase_Static.TRAVAILPFE_COL_IdENSEIGNANT));
				Integer _Id_soutenance_TravailPfe=Verifier_Integer(lObjc_P.getString(DataBase_Static.TRAVAILPFE_COL_IdSOUTENANCE));
				Travail_PFE _Travail_PFE=new Travail_PFE(_ID_TravailPfe, _libelle_TravailPfe, _Id_enseignant_TravailPfe, _Id_soutenance_TravailPfe);
				// Entreprise
				int lID_Entreprise=lObjc_P.getInt(DataBase_Static.ENTREPRISE_COL_ID);
				String libelle_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_LIBELLE);
				String raison_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_RAISON);
				String numTel_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_NUmTEL);
				String email_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_EMAIL);
				String siteWeb_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_SITEWEB);
				String login_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_LOGIN);
				String password_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_PASSWORD);
				String logo_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_LOGO);
				Integer Id_enseignant_Entreprise=Verifier_Integer(lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_IdENSEIGNANT));
				Entreprise _Entreprise =new Entreprise(lID_Entreprise, libelle_Entreprise, raison_Entreprise, numTel_Entreprise, email_Entreprise, siteWeb_Entreprise, login_Entreprise, password_Entreprise, logo_Entreprise,Id_enseignant_Entreprise);
				// Stage
				int _ID_Stage=lObjc_P.getInt(DataBase_Static.STAGE_COL_ID);
				String _nom_Stage=lObjc_P.getString(DataBase_Static.STAGE_COL_NOM);
				String _date_Stage=lObjc_P.getString(DataBase_Static.STAGE_COL_DATE);
				int _Id_travailPfe_Stage=lObjc_P.getInt(DataBase_Static.STAGE_COL_IdTRAVAILPFE);
				Integer _Id_encadreurPro_Stage=Verifier_Integer(lObjc_P.getString(DataBase_Static.STAGE_COL_IdENCADREUrPRO));
				Integer _Id_offreStage_Stage=Verifier_Integer(lObjc_P.getString(DataBase_Static.STAGE_COL_IdOFFRESTAGE));
				String _description_Stage=lObjc_P.getString(DataBase_Static.STAGE_COL_DESCRIPTION);
				int _Id_entreprise_Stage=lObjc_P.getInt(DataBase_Static.STAGE_COL_IdENTREPRISE);
				Stage _Stage = new Stage(_ID_Stage, _nom_Stage, _description_Stage, _date_Stage, _Id_travailPfe_Stage, _Id_encadreurPro_Stage, _Id_offreStage_Stage, _Id_entreprise_Stage);

				Log.d("_ID_Stage", String.valueOf(_ID_Stage));
				InsertInTo_BD_Entreprise(_Entreprise);
				int h=InsertInTo_BD_Stage(_Stage);
				int z=InsertInTo_BD_TravailPfe(_Travail_PFE);
				int y=InsertInTo_BD_Etudiant(_Classe_Etudiant);
				int x=InsertInTo_BD_Demande(_ClasseDemande);

				if ( x==-1 || h==-1 || z==-1 || y==-1 )
				{
					Toast.makeText( pContext , "ERREUR Lors de l'Insertion de Donnees" ,Toast.LENGTH_LONG).show();
					return false;
				}
			}
		} 
		catch(JSONException e)
		{
			Toast.makeText( pContext , " Vous n'avez pas encore reçu de demande d'encadrement " ,Toast.LENGTH_LONG).show();
			return false;
		}
		return true;
	}

	public boolean Synchroniser_Serveur_EnseignantDepartementFiliereNiveauCycle(String login,String password,String _URL)
	{
		int x,y,z,h,j;
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();
		NameValuePair lParamCin=new BasicNameValuePair("PARAM_CIN",password);
		NameValuePair lParamNom=new BasicNameValuePair("PARAM_MATRICULE",login);
		lListofParams.add(lParamCin);
		lListofParams.add(lParamNom);
		_ClientHTTP=new ClientHTTP(pContext);
		String lRetour_Connecxion=_ClientHTTP.LireEcrireUrl(_URL, lListofParams);

		//		MyAsyncTask_LireServeur _MyAsyncTask =new MyAsyncTask_LireServeur(pContext, lListofParams, _URL);
		//		String lRetour_Connecxion=null;
		//		try {
		//			lRetour_Connecxion = _MyAsyncTask.execute().get();
		//		} catch (InterruptedException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		} catch (ExecutionException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		}

		try {
			JSONArray lArray_Etudiant=new JSONArray(lRetour_Connecxion);
			for (int i = 0; i < lArray_Etudiant.length(); i++)
			{
				JSONObject lObjc_P=lArray_Etudiant.getJSONObject(i);
				// Enseignant
				int _ID_Enseignant=lObjc_P.getInt(DataBase_Static.ENSEIGNANT_COL_ID);
				String Cin_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_NCIN);
				String Nom_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_NOM);
				String Prenom_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_PRENOM);
				String Sexe_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_SEXE);
				String DateNaiss_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_DATENAISS);
				String LieuNaiss_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_LIEUNAISS);
				String NumTel_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_NUmTEL);
				String Email_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_EMAIL);
				String adresse_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_ADRESSE);
				String Ville_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_VILLE);
				String CodePost_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_CODEPOSTALE);
				String Grade_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_GRADE);
				String Diplome_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_DIPLOME);
				String LieuDip_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_LIEUDIPLOME);
				String Situation_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_SITUATIONCIVILE);
				String Titulaire_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_TITULAIRE);
				String FactAdmin_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_FACtADMIN);
				String Encadreument_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_ENCADREMENT);
				String AnneeDip_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_ANNEEDIPLOME);
				String EnDetachement_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_EnDETACHEMENT);
				int Id_departement_Enseignant=lObjc_P.getInt(DataBase_Static.ENSEIGNANT_COL_IdDepartement);
				String Matricule_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_MATRICULE);
				String Specialite_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_SPECIALITE);
				Enseignant _Classe_Enseignant=new Enseignant(_ID_Enseignant, Cin_Enseignant, Nom_Enseignant, Prenom_Enseignant, Sexe_Enseignant, DateNaiss_Enseignant, LieuNaiss_Enseignant, NumTel_Enseignant, Email_Enseignant, adresse_Enseignant, Ville_Enseignant, CodePost_Enseignant, Grade_Enseignant, Diplome_Enseignant, LieuDip_Enseignant, Situation_Enseignant, Titulaire_Enseignant, FactAdmin_Enseignant, Encadreument_Enseignant, AnneeDip_Enseignant, EnDetachement_Enseignant, Matricule_Enseignant, Specialite_Enseignant, Id_departement_Enseignant);
				///// EPARTEMENT
				int ID_Departement=lObjc_P.getInt(DataBase_Static.DEPARTEMENT_COL_ID);
				String libelle_Departement=lObjc_P.getString(DataBase_Static.DEPARTEMENT_COL_LIBELLE);
				String abreviation_Departement=lObjc_P.getString(DataBase_Static.DEPARTEMENT_COL_ABREVIATION);
				String ordre_Departement=lObjc_P.getString(DataBase_Static.DEPARTEMENT_COL_ORDRE);
				Departement _Classe_Departement = new Departement(ID_Departement, libelle_Departement, abreviation_Departement, ordre_Departement);
				///// NIVEAU
				int ID_Niveau=lObjc_P.getInt(DataBase_Static.NIVEAU_COL_ID);
				String num_Niveau=lObjc_P.getString(DataBase_Static.NIVEAU_COL_NUM);
				String designation_Niveau=lObjc_P.getString(DataBase_Static.NIVEAU_COL_DESIGNATION);
				Integer cle_filiere_Niveau=lObjc_P.getInt(DataBase_Static.NIVEAU_COL_IdFiliere);
				Niveau _Classe_Niveau=new Niveau(ID_Niveau, num_Niveau, designation_Niveau, null, null, cle_filiere_Niveau);
				///// CYCLE
				int ID_Cycle=lObjc_P.getInt(DataBase_Static.CYCLE_COL_ID);
				String num_Cycle=lObjc_P.getString(DataBase_Static.CYCLE_COL_NUM);
				String designation_Cycle=lObjc_P.getString(DataBase_Static.CYCLE_COL_DESIGNATION);
				Cycle _Classe_Cycle= new Cycle(ID_Cycle, num_Cycle, designation_Cycle, null, null);
				///// FILIERE
				int ID_Filiere=lObjc_P.getInt(DataBase_Static.FILIERE_COL_ID);
				String designation_Filiere=lObjc_P.getString(DataBase_Static.FILIERE_COL_DESIGNATION);
				Integer cle_cycle_Filiere=lObjc_P.getInt(DataBase_Static.FILIERE_COL_IdCYCLE);
				Filiere _Classe_Filiere = new Filiere(ID_Filiere, designation_Filiere, cle_cycle_Filiere);
				// un seul enseignant ,departement mais plusieur filiere 
				y=InsertInTo_BD_Enseignant(_Classe_Enseignant);
				z=InsertInTo_BD_Departement(_Classe_Departement);
				h=InsertInTo_BD_Niveau(_Classe_Niveau);
				j=InsertInTo_BD_Cycle(_Classe_Cycle);		
				x=InsertInTo_BD_Filiere(_Classe_Filiere);
				if (x==-1 || j==-1 || h==-1 ||z==-1 || y==-1 )
				{
					Toast.makeText( pContext , "ERREUR Lors de l'Insertion de Donnees " ,Toast.LENGTH_LONG).show();
					return false;
				}
			}
		} 
		catch(JSONException e)
		{
			Toast.makeText( pContext , " ERREUR Login ou Password non valide " ,Toast.LENGTH_LONG).show();
			return false;
		}
		return true;
	}

	//////////////////// INSERT INTO BD :
	public int InsertInTo_BD_EncadreurPro(EncadreurPro _EncadreurPro)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.ENCADREURPRO_COL_ID,_EncadreurPro.getId_encadreurPro());
		lContentValues.put(DataBase_Static.ENCADREURPRO_COL_NCIN,_EncadreurPro.getCin_encadreurPro());
		lContentValues.put(DataBase_Static.ENCADREURPRO_COL_NOM,_EncadreurPro.getNom_encadreurPro());
		lContentValues.put(DataBase_Static.ENCADREURPRO_COL_PRENOM,_EncadreurPro.getPrenom_encadreurPro());
		lContentValues.put(DataBase_Static.ENCADREURPRO_COL_NUmTEL,_EncadreurPro.getNumTel_encadreurPro());
		lContentValues.put(DataBase_Static.ENCADREURPRO_COL_EMAIL,_EncadreurPro.getEmail_encadreurPro());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_ENCADREUR_PRO, null, lContentValues);
		return (int) x;
	}
	public int InsertInTo_BD_Stage(Stage _Stage)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.STAGE_COL_ID,_Stage.getId_stage());
		lContentValues.put(DataBase_Static.STAGE_COL_NOM,_Stage.getNom_stage());
		lContentValues.put(DataBase_Static.STAGE_COL_DATE,_Stage.getDate_stage());
		lContentValues.put(DataBase_Static.STAGE_COL_IdTRAVAILPFE,_Stage.getCle_travailPfe_stage());
		lContentValues.put(DataBase_Static.STAGE_COL_IdENCADREUrPRO,_Stage.getCle_encadreurPro_stage());
		lContentValues.put(DataBase_Static.STAGE_COL_IdOFFRESTAGE,_Stage.getCle_offreStage_stage());
		lContentValues.put(DataBase_Static.STAGE_COL_DESCRIPTION,_Stage.getDescription_stage());
		lContentValues.put(DataBase_Static.STAGE_COL_IdENTREPRISE,_Stage.getCle_entreprise_stage());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_STAGE, null, lContentValues);
		return (int) x;
	}
	public int InsertInTo_BD_Entreprise(Entreprise _Entreprise)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.ENTREPRISE_COL_ID,_Entreprise.getId_entreprise());
		lContentValues.put(DataBase_Static.ENTREPRISE_COL_LIBELLE,_Entreprise.getLibelle_entreprise());
		lContentValues.put(DataBase_Static.ENTREPRISE_COL_RAISON,_Entreprise.getRaison_entreprise());
		lContentValues.put(DataBase_Static.ENTREPRISE_COL_NUmTEL,_Entreprise.getNumTel_entreprise());
		lContentValues.put(DataBase_Static.ENTREPRISE_COL_EMAIL,_Entreprise.getEmail_entreprise());
		lContentValues.put(DataBase_Static.ENTREPRISE_COL_SITEWEB,_Entreprise.getSiteWeb_entreprise());
		lContentValues.put(DataBase_Static.ENTREPRISE_COL_LOGIN,_Entreprise.getLogin_entreprise());
		lContentValues.put(DataBase_Static.ENTREPRISE_COL_PASSWORD,_Entreprise.getPassword_entreprise());
		lContentValues.put(DataBase_Static.ENTREPRISE_COL_LOGO,_Entreprise.getLogo_entreprise());
		lContentValues.put(DataBase_Static.ENTREPRISE_COL_IdENSEIGNANT,_Entreprise.getCle_enseignant_entreprise());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_ENTREPRISE, null, lContentValues);
		return (int) x;
	}
	public int InsertInTo_BD_TravailPfe(Travail_PFE _Travail_PFE)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.TRAVAILPFE_COL_ID,_Travail_PFE.getID_TravailPFE());
		lContentValues.put(DataBase_Static.TRAVAILPFE_COL_LIBELLE,_Travail_PFE.getLibelle_TravailPFE());
		lContentValues.put(DataBase_Static.TRAVAILPFE_COL_IdENSEIGNANT,_Travail_PFE.getCle_enseignant_TravailPFE());
		lContentValues.put(DataBase_Static.TRAVAILPFE_COL_IdSOUTENANCE,_Travail_PFE.getCle_soutenance_TravailPFE());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_TRAVAILPFE, null, lContentValues);
		return (int) x;
	}
	public int InsertInTo_BD_Etudiant(Etudiant _Class_Etudiant)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.ETUDIANT_COL_ID,_Class_Etudiant.getId_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_NUmINSCRIT,_Class_Etudiant.getNum_inscrit());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_CIN,_Class_Etudiant.getCin_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_NOM,_Class_Etudiant.getNom_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_PRENOM,_Class_Etudiant.getPrenom_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_SEXE,_Class_Etudiant.getSexe_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_DATENAISS,_Class_Etudiant.getDateNaiss_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_LIEUNAISS,_Class_Etudiant.getLieuNaiss_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_NUmTEL,_Class_Etudiant.getNumTel_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_EMAIL,_Class_Etudiant.getEmail_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_ADRESSE,_Class_Etudiant.getAdresse_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_CODePOSTAL,_Class_Etudiant.getCodePostal_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_IMAGE,_Class_Etudiant.getImage_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_IdDEPARTEMENT,_Class_Etudiant.getCle_departement_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_IdTRAVAILPFE,_Class_Etudiant.getCle_travailPfe_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_IdETUDIANT1,_Class_Etudiant.getCle_etudiant1_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_IdFILIERE,_Class_Etudiant.getCle_filiere_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_IdDOCUMENT,_Class_Etudiant.getCle_document_etudiant());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_ETUDIANT, null, lContentValues);
		return (int) x;
	}
	public int InsertInTo_BD_Demande(Demande _Demande)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.DEMANDE_COL_CONFIRMATION,_Demande.getConfirmaiton_demande());
		lContentValues.put(DataBase_Static.DEMANDE_COL_DATE,_Demande.getDate_demande());
		lContentValues.put(DataBase_Static.DEMANDE_COL_IdENSEIGNANT,_Demande.getId_enseignant_demande());
		lContentValues.put(DataBase_Static.DEMANDE_COL_IdETUDIANT,_Demande.getId_etudiant_demande());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_DEMANDE, null, lContentValues);
		return (int) x;
	}
	public int InsertInTo_BD_Enseignant(Enseignant _Class_Enseignant)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_ID,_Class_Enseignant.getId_enseignant());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_NCIN,_Class_Enseignant.getCin_enseignant());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_NOM,_Class_Enseignant.getNom_enseignant());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_PRENOM,_Class_Enseignant.getPrenom_enseignant());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_SEXE,_Class_Enseignant.getSexe_enseignant());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_DATENAISS,_Class_Enseignant.getDateNaiss_enseignant());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_LIEUNAISS,_Class_Enseignant.getLieuNaiss_enseignant());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_NUmTEL,_Class_Enseignant.getNumTel_enseignant());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_EMAIL,_Class_Enseignant.getEmail_enseignant());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_ADRESSE,_Class_Enseignant.getAdresse_enseignant());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_VILLE,_Class_Enseignant.getVille());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_CODEPOSTALE,_Class_Enseignant.getCodePostale());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_GRADE,_Class_Enseignant.getGrade());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_DIPLOME,_Class_Enseignant.getDiplome());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_LIEUDIPLOME,_Class_Enseignant.getLieu_diplome());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_SITUATIONCIVILE,_Class_Enseignant.getSituation_civil());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_TITULAIRE,_Class_Enseignant.getTitulaire());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_FACtADMIN,_Class_Enseignant.getFactAdmin());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_ENCADREMENT,_Class_Enseignant.getEncadrement());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_ANNEEDIPLOME,_Class_Enseignant.getAnnee_diplome());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_EnDETACHEMENT,_Class_Enseignant.getEnDetachement());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_IdDepartement,_Class_Enseignant.getId_departement());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_MATRICULE,_Class_Enseignant.getMatricule());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_SPECIALITE,_Class_Enseignant.getSpecialite());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_ENSEIGNANT, null, lContentValues);
		return (int) x;
	}
	public int  InsertInTo_BD_Niveau(Niveau _Class_Niveau)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.NIVEAU_COL_ID,_Class_Niveau.getId_niveau());
		lContentValues.put(DataBase_Static.NIVEAU_COL_NUM,_Class_Niveau.getNum_niveau());
		lContentValues.put(DataBase_Static.NIVEAU_COL_DESIGNATION,_Class_Niveau.getDesignation_niveau());
		lContentValues.put(DataBase_Static.NIVEAU_COL_NbMODULE,_Class_Niveau.getNbModule_niveau());
		lContentValues.put(DataBase_Static.NIVEAU_COL_NbSECTION,_Class_Niveau.getNbSection_niveau());
		lContentValues.put(DataBase_Static.NIVEAU_COL_IdFiliere,_Class_Niveau.getCle_filiere_niveau());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_NIVEAU, null, lContentValues);
		return (int) x;
	}
	public int InsertInTo_BD_Cycle(Cycle _Class_Cycle)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.CYCLE_COL_ID, _Class_Cycle.getId_cycle());
		lContentValues.put(DataBase_Static.CYCLE_COL_NUM,_Class_Cycle.getNum_cycle());
		lContentValues.put(DataBase_Static.CYCLE_COL_DESIGNATION,_Class_Cycle.getDesignation_cycle());
		lContentValues.put(DataBase_Static.CYCLE_COL_NBrSEM,_Class_Cycle.getNbrSem_cycle());
		lContentValues.put(DataBase_Static.CYCLE_COL_NBrFILIERE,_Class_Cycle.getNbrFiliere_cycle());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_CYCLE, null, lContentValues);
		return (int) x;
	}
	public int InsertInTo_BD_Departement(Departement _Class_Departement)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.DEPARTEMENT_COL_ID,_Class_Departement.getId_departement());
		lContentValues.put(DataBase_Static.DEPARTEMENT_COL_LIBELLE,_Class_Departement.getLibelle_Departement());
		lContentValues.put(DataBase_Static.DEPARTEMENT_COL_ABREVIATION,_Class_Departement.getAbreviation_Departement());
		lContentValues.put(DataBase_Static.DEPARTEMENT_COL_ORDRE,_Class_Departement.getOrdre_Departement());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_DEPARTEMENT, null, lContentValues);
		return (int) x;
	}
	public int InsertInTo_BD_Filiere(Filiere _Class_Filiere)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.FILIERE_COL_ID, _Class_Filiere.getId_filiere());
		lContentValues.put(DataBase_Static.FILIERE_COL_DESIGNATION,_Class_Filiere.getDesignation_filiere());
		lContentValues.put(DataBase_Static.FILIERE_COL_IdCYCLE,_Class_Filiere.getCle_cycle_filiere());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_FILIERE, null, lContentValues);
		return (int) x;
	}
	//////////////////// SUPPRIMER CONTENUE TABLE :
	public void ClearAll_Encadreurpro()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_ENCADREUR_PRO);
	}
	public void ClearAll_Demande()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_DEMANDE);
	}
	public void ClearAll_Entreprise()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_ENTREPRISE);
	}
	public void clearAll_Stage()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_STAGE);
	}
	public void clearAll_TravailPfe()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_TRAVAILPFE);
	}
	public void  ClearAll_Etudiant()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_ETUDIANT);
	}
	public void ClearAll_Enseignant()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_ENSEIGNANT);
	}
	public void ClearAll_Departement()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_DEPARTEMENT);
	}
	public void ClearAll_Filiere()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_FILIERE);
	}
	public void ClearAll_Niveau()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_NIVEAU);
	}
	public void ClearAll_Cycle()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_CYCLE);
	}
	/////////////////////Insertion INTO SERVEUR :
	public boolean UpdateInTo_Serveur_Demande(String _Confimation_Demande,String _ID_Enseignant,String _ID_Etudiant,String _URL )
	{                     
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();

		NameValuePair lParamConfirmation=new BasicNameValuePair("PARAM_CONFIRMATION", _Confimation_Demande);
		NameValuePair lParamIdEnseignant=new BasicNameValuePair("PARAM_ID_ENSEIGNANT", _ID_Enseignant);
		NameValuePair lParamIdEtudiant=new BasicNameValuePair("PARAM_ID_ETUDIANT", _ID_Etudiant);

		lListofParams.add(lParamConfirmation);
		lListofParams.add(lParamIdEnseignant);
		lListofParams.add(lParamIdEtudiant);

		//_ClientHTTP=new ClientHTTP(pContext);
		//boolean lResult=_ClientHTTP.SendToUrl(_URL,lListofParams);
		MyAsyncTask_EcrireServeur _MyAsyncTask_EcrireServeur =new MyAsyncTask_EcrireServeur(pContext, lListofParams, _URL);
		boolean lResult=false;
		try {
			lResult = _MyAsyncTask_EcrireServeur.execute().get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		if(lResult)
		{
			Toast.makeText(pContext, " Modifier avec succès ", Toast.LENGTH_LONG).show();
			return true;
		}else
		{
			Toast.makeText(pContext, " échec lors des Modifications ", Toast.LENGTH_SHORT).show();
			return false;
		}
	}

	//////////////////// DELETTE InTo SERVEUR :

	//////////////////// Verifier information du SERVEUR :
	public boolean Watch_Serveur_Demande(String _ID_Etudiant,String _ID_Binome,String _URL) /// url...
	{
		boolean ok=false;
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();

		NameValuePair lParamIdEtudiant=new BasicNameValuePair("ID_ETUDIANT", _ID_Etudiant);
		NameValuePair lParamIdBinome=new BasicNameValuePair("ID_BINOME", _ID_Binome);

		lListofParams.add(lParamIdEtudiant);
		lListofParams.add(lParamIdBinome);

		_ClientHTTP=new ClientHTTP(pContext);
		String lRetour_Connecxion=_ClientHTTP.LireEcrireUrl(_URL, lListofParams);

		//		MyAsyncTask_LireServeur _MyAsyncTask_AfficherServeur =new MyAsyncTask_LireServeur(pContext, lListofParams, _URL);
		//		String lRetour_Connecxion=null;
		//		try {
		//			lRetour_Connecxion=_MyAsyncTask_AfficherServeur.execute().get();
		//		} catch (InterruptedException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		} catch (ExecutionException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		}

		try {
			JSONArray lArray=new JSONArray(lRetour_Connecxion);
			for (int i = 0; i < lArray.length(); i++)
			{
				ok =true;
			}
		} 
		catch(JSONException e)
		{
			ok= false;
		}
		return ok;
	}

	//////Verifier Integer
	public Integer Verifier_Integer(String valeur)
	{
		if (valeur.equals("null"))
		{
			return null;
		}else
		{
			return Integer.parseInt(valeur);	
		}
	}

}