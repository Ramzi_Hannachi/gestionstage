package package_Foction_DataBase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import package_ClassPersonaliser.Condidater;
import package_ClassPersonaliser.Cycle;
import package_ClassPersonaliser.Demande;
import package_ClassPersonaliser.Departement;
import package_ClassPersonaliser.Deriger;
import package_ClassPersonaliser.EncadreurPro;
import package_ClassPersonaliser.Enseignant;
import package_ClassPersonaliser.Entreprise;
import package_ClassPersonaliser.Etudiant;
import package_ClassPersonaliser.Filiere;
import package_ClassPersonaliser.Niveau;
import package_ClassPersonaliser.OffreStage;
import package_ClassPersonaliser.Salle;
import package_ClassPersonaliser.Soutenance;
import package_ClassPersonaliser.Stage;
import package_ClassPersonaliser.Travail_PFE;
import package_Creator_DataBase.MySQliteCreator_DataBase;
import package_Utilitaire.ClientHTTP;
import package_Utilitaire.DataBase_Static;
import package_Utilitaire.MyAsyncTask_EcrireServeur;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

import com.example.projet_gestionstage_test.R;

public class MySQliteFonction_ChefDepartement 
{
	private ClientHTTP _ClientHTTP;
	private SQLiteDatabase _SqLiteDatabase;
	private MySQliteCreator_DataBase _SQliteCreator_ProjetStage;
	private Context pContext;

	public MySQliteFonction_ChefDepartement(Context pContext)
	{
		this.pContext=pContext;
		_SQliteCreator_ProjetStage=new MySQliteCreator_DataBase(pContext, DataBase_Static.DB_NAME, null, 1);
		openDB();
	}
	public void openDB()
	{
		_SqLiteDatabase=_SQliteCreator_ProjetStage.getWritableDatabase();
	}

	///////////////////// requette SQLITE :

	public ArrayList<HashMap<String, String>> SoutenanceDerigerSalle_CALENDRIER(int _ID_TravailPfe)
	{
		String sql=" SELECT DISTINCT soutenance.* , deriger.* , salle.* , enseignant.*"
				+" FROM soutenance, deriger , salle , enseignant "
				+" WHERE soutenance.id_travailPfe_soutenance = "+_ID_TravailPfe+" "
				+" AND soutenance.Id_soutenance = deriger.id_soutenance_deriger " +
				"  AND soutenance.id_salle_soutenance = salle.Id_salle " +
				"  AND deriger.idensei_deriger = enseignant.Idensei " ;
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();
				// Soutenance
				int ID_Soutenance=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.SOUTENANCE_COL_ID));
				String Type_Soutenance=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.SOUTENANCE_COL_TYPE));
				String HeureDeb_Soutenance=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.SOUTENANCE_COL_HEUReDEB));
				String HeureFin_Soutenance=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.SOUTENANCE_COL_HEUReFIN));
				String Date_Soutenance=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.SOUTENANCE_COL_JOUR));
				String IdSalle_Soutenance=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.SOUTENANCE_COL_IdSALLE));
				String IdTravailPfe_Soutenance=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.SOUTENANCE_COL_IdTRAVAILPFE));
				_HashMap.put("ID_Soutenance",String.valueOf(ID_Soutenance));
				_HashMap.put("Type_Soutenance",Type_Soutenance);
				_HashMap.put("HeureDeb_Soutenance",HeureDeb_Soutenance);
				_HashMap.put("HeureFin_Soutenance",HeureFin_Soutenance);
				_HashMap.put("Date_Soutenance",Date_Soutenance);
				_HashMap.put("IdSalle_Soutenance",IdSalle_Soutenance);
				_HashMap.put("IdTravailPfe_Soutenance",IdTravailPfe_Soutenance);
				// Deriger
				String Type_Deriger=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.DERIGER_COL_TYPE));
				String IdEnseignant_Deriger=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.DERIGER_COL_IdENSEIGNANT));
				String IdSoutenance_Deriger=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.DERIGER_COL_IdSOUTENANCE));
				_HashMap.put("Type_Deriger",Type_Deriger);
				_HashMap.put("IdEnseignant_Deriger",IdEnseignant_Deriger);
				_HashMap.put("IdSoutenance_Deriger",IdSoutenance_Deriger);
				// Salle
				int ID_Salle=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.SALLE_COL_ID));
				String Bloc_Salle=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.SALLE_COL_BLOC));
				String Etage_Salle=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.SALLE_COL_ETAGE));
				String Abreviation_Salle=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.SALLE_COL_ABREVIATION));
				_HashMap.put("ID_Salle",String.valueOf(ID_Salle));
				_HashMap.put("Bloc_Salle",Bloc_Salle);
				_HashMap.put("Etage_Salle",Etage_Salle);
				_HashMap.put("Abreviation_Salle",Abreviation_Salle);
				// ENSEIGNANT
				int ID_Enseignant=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_ID));
				String nom_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_NOM));
				String prenom_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_PRENOM));
				String numTel_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_NUmTEL));
				String email_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_EMAIL));
				String grade_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_GRADE));
				String encadrement_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_ENCADREMENT));
				String matricule_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_MATRICULE));
				String specialite_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_SPECIALITE));
				_HashMap.put("ID_Enseignant",String.valueOf(ID_Enseignant ));
				_HashMap.put("nom_Enseignant",nom_Enseignant );
				_HashMap.put("prenom_Enseignant", prenom_Enseignant);
				_HashMap.put("numTel_Enseignant",numTel_Enseignant );
				_HashMap.put("email_Enseignant",email_Enseignant );
				_HashMap.put("grade_Enseignant", grade_Enseignant);
				_HashMap.put("encadrement_Enseignant",encadrement_Enseignant );
				_HashMap.put("matricule_Enseignant",matricule_Enseignant );
				_HashMap.put("specialite_Enseignant",specialite_Enseignant );
				
				_ArrayList.add(_HashMap);
			}
		}
		return _ArrayList;
	}


	public ArrayList<HashMap<String, String>> Salle_DialogCALENDRIER()
	{
		String sql=" select salle.* "
				+"from salle " ;
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList_Salle = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();

				// Salle
				int ID_Salle=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.SALLE_COL_ID));
				String bloc_Salle=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.SALLE_COL_BLOC));
				String etage_Salle=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.SALLE_COL_ETAGE));
				String abreviation_Salle=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.SALLE_COL_ABREVIATION));
				_HashMap.put("Image_Salle",String.valueOf(R.drawable.salle));
				_HashMap.put("ID_Salle",String.valueOf(ID_Salle));
				_HashMap.put("bloc_Salle","Bloc : "+bloc_Salle );
				_HashMap.put("etage_Salle","Etage : "+etage_Salle );
				_HashMap.put("abreviation_Salle","Numéro : "+abreviation_Salle );

				_ArrayList_Salle.add(_HashMap);
			}
		}
		return _ArrayList_Salle;
	}


	public ArrayList<HashMap<String, String>> Enseignant_DialogCALENDRIER()
	{
		// afficher la liste des enseignant(encadrement oui) pour etre choisit pour un rapporteur ou president
		String sql=" SELECT enseignant . * " 
				+"FROM enseignant " ;
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList_Enseignant = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();

				// Enseignant
				int ID_Enseignant=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_ID));
				String nom_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_NOM));
				String prenom_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_PRENOM));
				String numTel_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_NUmTEL));
				String email_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_EMAIL));
				String grade_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_GRADE));
				String encadrement_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_ENCADREMENT));
				String matricule_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_MATRICULE));
				String specialite_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_SPECIALITE));
				_HashMap.put("ID_Enseignant",String.valueOf(ID_Enseignant ));
				_HashMap.put("nom_Enseignant",nom_Enseignant );
				_HashMap.put("prenom_Enseignant", prenom_Enseignant);
				_HashMap.put("numTel_Enseignant",numTel_Enseignant );
				_HashMap.put("email_Enseignant",email_Enseignant );
				_HashMap.put("grade_Enseignant", grade_Enseignant);
				_HashMap.put("encadrement_Enseignant",encadrement_Enseignant );
				_HashMap.put("matricule_Enseignant",matricule_Enseignant );
				_HashMap.put("specialite_Enseignant",specialite_Enseignant );
				_ArrayList_Enseignant.add(_HashMap);
			}
		}
		return _ArrayList_Enseignant;
	}


	public ArrayList<HashMap<String, String>> EntrepriseEtudiantEncadreur_CALENDRIER(String _ID_TravailPfe)
	{
		String sql=" SELECT  entreprise . * , etudiant . * , enseignant . * , travail_pfe.*"
				+"FROM entreprise, etudiant, enseignant, stage, travail_pfe "
				+"WHERE entreprise.id_entreprise = stage.id_entreprise_stage "
				+"AND travail_pfe.id_travailPfe = "+_ID_TravailPfe+" "
				+"AND stage.id_travailPfe_stage = travail_pfe.id_travailPfe "
				+"AND travail_pfe.idensei_travailPfe = enseignant.Idensei "
				+"AND travail_pfe.id_travailPfe = etudiant.id_travailPfe_etudiant " ;
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();
				// Entreprise
				int ID_Entreprise=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_ID));
				String libelle_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_LIBELLE));
				String raison_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_RAISON));
				String numTel_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_NUmTEL));
				String email_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_EMAIL));
				String siteWeb_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_SITEWEB));
				String logo_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_LOGO));
				_HashMap.put("ID_Entreprise",String.valueOf(ID_Entreprise));
				_HashMap.put("libelle_Entreprise",libelle_Entreprise);
				_HashMap.put("raison_Entreprise",raison_Entreprise);
				_HashMap.put("numTel_Entreprise",numTel_Entreprise);
				_HashMap.put("email_Entreprise",email_Entreprise);
				_HashMap.put("siteWeb_Entreprise",siteWeb_Entreprise);
				_HashMap.put("logo_Entreprise",logo_Entreprise);
				// Etudiant
				int ID_Etudiant=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_ID));
				String Id_etudiant1_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_IdETUDIANT1));
				String Id_travailPfe_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_IdTRAVAILPFE));
				_HashMap.put("ID_Etudiant",String.valueOf(ID_Etudiant));
				_HashMap.put("Id_etudiant1_Etudiant",Id_etudiant1_Etudiant);
				_HashMap.put("Id_travailPfe_Etudiant",Id_travailPfe_Etudiant);
				// Enseignant
				int ID_Enseignant=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_ID));
				String nom_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_NOM));
				String prenom_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_PRENOM));
				String numTel_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_NUmTEL));
				String email_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_EMAIL));
				String matricule_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_MATRICULE));
				String specialite_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_SPECIALITE));
				_HashMap.put("ID_Enseignant",String.valueOf(ID_Enseignant ));
				_HashMap.put("nom_Enseignant",nom_Enseignant );
				_HashMap.put("prenom_Enseignant", prenom_Enseignant);
				_HashMap.put("numTel_Enseignant",numTel_Enseignant );
				_HashMap.put("email_Enseignant",email_Enseignant );
				_HashMap.put("matricule_Enseignant",matricule_Enseignant );
				_HashMap.put("specialite_Enseignant",specialite_Enseignant );
				// TravailPfe
				String Id_soutenance_TravailPfe=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.TRAVAILPFE_COL_IdSOUTENANCE));
				_HashMap.put("Id_soutenance_TravailPfe",Id_soutenance_TravailPfe );

				_ArrayList.add(_HashMap);
			}
		}
		return _ArrayList;
	}

	public ArrayList<HashMap<String, String>> Stage_CALENDRIER()
	{
		String sql=" SELECT stage.* "
				+" FROM stage, travail_pfe "
				+" WHERE stage.id_travailPfe_stage = travail_pfe.Id_travailPfe "
				+" AND travail_pfe.idensei_travailPfe IS NOT NULL  " ;
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList_Stage = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();

				// Stage
				int _ID_Stage=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_ID));
				String _nom_Stage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_NOM));
				String _date_Stage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_DATE));
				int _Id_travailPfe_Stage=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_IdTRAVAILPFE));
				String _Id_encadreurPro_Stage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_IdENCADREUrPRO));
				String _descriptiona_Stage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_DESCRIPTION));
				int _Id_entreprise_Stage=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_IdENTREPRISE));
				_HashMap.put("_ID_Stage",String.valueOf( _ID_Stage));
				_HashMap.put("_nom_Stage",_nom_Stage);
				_HashMap.put("_date_Stage",_date_Stage);
				_HashMap.put("_Id_travailPfe_Stage",String.valueOf( _Id_travailPfe_Stage));
				_HashMap.put("_Id_encadreurPro_Stage",_Id_encadreurPro_Stage);
				_HashMap.put("_descriptiona_Stage",_descriptiona_Stage);
				_HashMap.put("_Id_entreprise_Stage",String.valueOf(_Id_entreprise_Stage));

				_ArrayList_Stage.add(_HashMap);
			}
		}
		return _ArrayList_Stage;
	}

	public  boolean Verifier_StageExisteInDemandeEncadrement(String _Id_EtudiantDemande,String _Id_BinomeDemande,String _Id_EnseignantDemande)
	{
		boolean ok=false;
		String sql=" select demande.*"
				+" from demande"
				+" where "
				+" ( id_etudiant_demande = "+_Id_EtudiantDemande+" or id_etudiant_demande = "+_Id_BinomeDemande+" ) "
				+" and idensei_demande = "+_Id_EnseignantDemande+" "
				+" and confirmation_demande = '1' ; ";
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				ok=true;
			}
		}
		return ok;
	}

	public ArrayList<HashMap<String, String>> Afficher_Etudiant_AFFECTATIOnENCADREUR(String _ID_TravailPfe)
	{ 
		String sql=" select etudiant.* "
				+" from etudiant "
				+" where "
				+" etudiant.id_travailPfe_etudiant = "+_ID_TravailPfe+" ; " ;
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList_Etudiant = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();

				// Etudiant  :utiliser la fonction AfficherEtudiant pour récupérer les info
				int ID_Etudiant=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_ID));
				String Id_etudiant1_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_IdETUDIANT1));
				String Id_travailPfe_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_IdTRAVAILPFE));
				_HashMap.put("ID_Etudiant",String.valueOf(ID_Etudiant));
				_HashMap.put("Id_etudiant1_Etudiant",Id_etudiant1_Etudiant);
				_HashMap.put("Id_travailPfe_Etudiant",Id_travailPfe_Etudiant);

				_ArrayList_Etudiant.add(_HashMap);
			}
		}
		return _ArrayList_Etudiant;
	}	

	public ArrayList<HashMap<String, String>> Afficher_TravailStageEntreprise_AFFECTATIOnENCADREUR()
	{  
		String sql=" SELECT   travail_pfe . * , stage . * , entreprise . * "
				+" FROM   travail_pfe, stage, entreprise "
				+" WHERE "
				+" stage.id_travailPfe_stage = travail_pfe.Id_travailPfe"
				+" AND stage.id_entreprise_stage = entreprise.Id_entreprise " ;

		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList_Etudiant = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();
				// Entreprise
				int ID_Entreprise=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_ID));
				String libelle_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_LIBELLE));
				String raison_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_RAISON));
				String numTel_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_NUmTEL));
				String email_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_EMAIL));
				String siteWeb_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_SITEWEB));
				String logo_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_LOGO));
				_HashMap.put("ID_Entreprise",String.valueOf(ID_Entreprise));
				_HashMap.put("libelle_Entreprise",libelle_Entreprise);
				_HashMap.put("raison_Entreprise",raison_Entreprise);
				_HashMap.put("numTel_Entreprise",numTel_Entreprise);
				_HashMap.put("email_Entreprise",email_Entreprise);
				_HashMap.put("siteWeb_Entreprise",siteWeb_Entreprise);
				_HashMap.put("logo_Entreprise",logo_Entreprise);
				// Stage
				int _ID_Stage=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_ID));
				String _nom_Stage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_NOM));
				String _date_Stage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_DATE));
				int _Id_travailPfe_Stage=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_IdTRAVAILPFE));
				String _Id_encadreurPro_Stage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_IdENCADREUrPRO));
				String _descriptiona_Stage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_DESCRIPTION));
				int _Id_entreprise_Stage=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_IdENTREPRISE));
				_HashMap.put("_ID_Stage",String.valueOf( _ID_Stage));
				_HashMap.put("_nom_Stage",_nom_Stage);
				_HashMap.put("_date_Stage",_date_Stage);
				_HashMap.put("_Id_travailPfe_Stage",String.valueOf( _Id_travailPfe_Stage));
				_HashMap.put("_Id_encadreurPro_Stage",_Id_encadreurPro_Stage);
				_HashMap.put("_descriptiona_Stage",_descriptiona_Stage);
				_HashMap.put("_Id_entreprise_Stage",String.valueOf(_Id_entreprise_Stage));
				// Travail Pfe
				int _ID_TravailPfe=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.TRAVAILPFE_COL_ID));
				String _Id_enseignant_TravailPfe=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.TRAVAILPFE_COL_IdENSEIGNANT));
				_HashMap.put("_Id_enseignant_TravailPfe",_Id_enseignant_TravailPfe);
				_HashMap.put("_ID_TravailPfe",String.valueOf(_ID_TravailPfe));

				_ArrayList_Etudiant.add(_HashMap);
			}
		}
		return _ArrayList_Etudiant;
	}	

	public ArrayList<HashMap<String, String>> Afficher_EnseignantDepartement_AFFECTATIOnENCADREUR()
	{  
		String sql=" SELECT DISTINCT enseignant.* , departement.* "
				+" FROM enseignant, departement"
				+" WHERE "
				+" enseignant.id_departement_enseignant = departement.Id_departement";
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList_Etudiant = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();
				// ENSEIGNANT
				int ID_Enseignant=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_ID));
				String cin_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_NCIN));
				String nom_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_NOM));
				String prenom_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_PRENOM));
				String sexe_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_SEXE));
				String dateNaiss_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_DATENAISS));
				String lieuNaiss_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_LIEUNAISS));
				String numTel_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_NUmTEL));
				String email_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_EMAIL));
				String adresse_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_ADRESSE));
				String ville_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_VILLE));
				String codePost_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_CODEPOSTALE));
				String grade_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_GRADE));
				String diplome_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_DIPLOME));
				String lieuDipl_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_LIEUDIPLOME));
				String situation_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_SITUATIONCIVILE));
				String titulaire_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_TITULAIRE));
				String factAdm_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_FACtADMIN));
				String encadrement_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_ENCADREMENT));
				String anneeDip_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_ANNEEDIPLOME));
				String enDetachement_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_EnDETACHEMENT));
				String matricule_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_MATRICULE));
				String specialite_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_SPECIALITE));
				_HashMap.put("Image_Enseignant",String.valueOf(R.drawable.chefdepartement_fermer ));
				_HashMap.put("ID_Enseignant",String.valueOf(ID_Enseignant ));
				_HashMap.put("cin_Enseignant",cin_Enseignant );  
				_HashMap.put("nom_Enseignant",nom_Enseignant );
				_HashMap.put("prenom_Enseignant", prenom_Enseignant);
				_HashMap.put("sexe_Enseignant", sexe_Enseignant);
				_HashMap.put("dateNaiss_Enseignant",dateNaiss_Enseignant );
				_HashMap.put("lieuNaiss_Enseignant", lieuNaiss_Enseignant);
				_HashMap.put("numTel_Enseignant",numTel_Enseignant );
				_HashMap.put("email_Enseignant",email_Enseignant );
				_HashMap.put("adresse_Enseignant",adresse_Enseignant );
				_HashMap.put("ville_Enseignant",ville_Enseignant );
				_HashMap.put("codePost_Enseignant", codePost_Enseignant);
				_HashMap.put("grade_Enseignant", grade_Enseignant);
				_HashMap.put("diplome_Enseignant", diplome_Enseignant);
				_HashMap.put("lieuDipl_Enseignant",lieuDipl_Enseignant );
				_HashMap.put("situation_Enseignant",situation_Enseignant );
				_HashMap.put("titulaire_Enseignant",titulaire_Enseignant );
				_HashMap.put("factAdm_Enseignant",factAdm_Enseignant );
				_HashMap.put("encadrement_Enseignant",encadrement_Enseignant );
				_HashMap.put("anneeDip_Enseignant",anneeDip_Enseignant );
				_HashMap.put("enDetachement_Enseignant",enDetachement_Enseignant );
				_HashMap.put("matricule_Enseignant",matricule_Enseignant );
				_HashMap.put("specialite_Enseignant",specialite_Enseignant );
				// DEPARTEMENT
				int ID_Departement=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.DEPARTEMENT_COL_ID));
				String libelle_Departement=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.DEPARTEMENT_COL_LIBELLE));
				_HashMap.put("ID_Departement",String.valueOf( ID_Departement));
				_HashMap.put("libelle_Departement", libelle_Departement);


				_ArrayList_Etudiant.add(_HashMap);
			}
		}
		return _ArrayList_Etudiant;
	}	

	public ArrayList<HashMap<String, String>> Afficher_DemandeEtudiantTravailStageEntreprise_DEMANDeENCADREMENT(String _ID_EncadreurAcad)
	{  // afficher  enseignant et departement , de la table demande avec confirmation = 1 
		String sql=" SELECT demande . * , etudiant . * , travail_pfe . * , stage . * , entreprise . * "
				+" FROM demande, etudiant, travail_pfe, stage, entreprise"
				+" WHERE demande.idensei_demande = "+_ID_EncadreurAcad+""
				+" AND demande.id_etudiant_demande = etudiant.Id_etudiant"
				+" AND etudiant.id_travailPfe_etudiant = travail_pfe.Id_travailPfe"
				+" AND stage.id_travailPfe_stage = travail_pfe.Id_travailPfe"
				+" AND stage.id_entreprise_stage = entreprise.Id_entreprise ;";
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList_Etudiant = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();
				// Entreprise
				int ID_Entreprise=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_ID));
				String libelle_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_LIBELLE));
				String raison_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_RAISON));
				String numTel_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_NUmTEL));
				String email_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_EMAIL));
				String siteWeb_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_SITEWEB));
				//String login_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_LOGIN));
				//String password_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_PASSWORD));
				String logo_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_LOGO));
				_HashMap.put("ID_Entreprise",String.valueOf(ID_Entreprise));
				_HashMap.put("libelle_Entreprise",libelle_Entreprise);
				_HashMap.put("raison_Entreprise",raison_Entreprise);
				_HashMap.put("numTel_Entreprise",numTel_Entreprise);
				_HashMap.put("email_Entreprise",email_Entreprise);
				_HashMap.put("siteWeb_Entreprise",siteWeb_Entreprise);
				//_HashMap.put("login_Entreprise",login_Entreprise);
				//_HashMap.put("password_Entreprise",password_Entreprise);
				_HashMap.put("logo_Entreprise",logo_Entreprise);
				// Etudiant  :utiliser la fonction AfficherEtudiant pour récupérer les info
				int ID_Etudiant=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_ID));
				String Id_etudiant1_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_IdETUDIANT1));
				String Id_travailPfe_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_IdTRAVAILPFE));
				_HashMap.put("ID_Etudiant",String.valueOf(ID_Etudiant));
				_HashMap.put("Id_etudiant1_Etudiant",Id_etudiant1_Etudiant);
				_HashMap.put("Id_travailPfe_Etudiant",Id_travailPfe_Etudiant);
				// Demande 
				String confirmation_Demande=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.DEMANDE_COL_CONFIRMATION));
				String date_Demande=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.DEMANDE_COL_DATE));
				int Id_enseignant_Demande=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.DEMANDE_COL_IdENSEIGNANT));
				int Id_etudiant_Demande=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.DEMANDE_COL_IdETUDIANT));
				_HashMap.put("confirmation_Demande",confirmation_Demande);
				_HashMap.put("date_Demande",date_Demande);
				_HashMap.put("Id_enseignant_Demande", String.valueOf(Id_enseignant_Demande));
				_HashMap.put("Id_etudiant_Demande",String.valueOf(Id_etudiant_Demande));
				// Stage
				int _ID_Stage=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_ID));
				String _nom_Stage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_NOM));
				String _date_Stage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_DATE));
				int _Id_travailPfe_Stage=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_IdTRAVAILPFE));
				String _Id_encadreurPro_Stage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_IdENCADREUrPRO));
				//String _Id_offreStage_Stage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_IdOFFRESTAGE));
				String _descriptiona_Stage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_DESCRIPTION));
				//int _Id_entreprise_Stage=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_IdENTREPRISE));
				_HashMap.put("_ID_Stage",String.valueOf( _ID_Stage));
				_HashMap.put("_nom_Stage",_nom_Stage);
				_HashMap.put("_date_Stage",_date_Stage);
				_HashMap.put("_Id_travailPfe_Stage",String.valueOf( _Id_travailPfe_Stage));
				_HashMap.put("_Id_encadreurPro_Stage",_Id_encadreurPro_Stage);
				//_HashMap.put("_Id_offreStage_Stage",_Id_offreStage_Stage);
				_HashMap.put("_descriptiona_Stage",_descriptiona_Stage);
				//_HashMap.put("_Id_entreprise_Stage",String.valueOf(_Id_entreprise_Stage));
				// Travail Pfe
				//int _ID_TravailPfe=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.TRAVAILPFE_COL_ID));
				String _Id_enseignant_TravailPfe=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.TRAVAILPFE_COL_IdENSEIGNANT));
				_HashMap.put("_Id_enseignant_TravailPfe",_Id_enseignant_TravailPfe);
				//_HashMap.put("_ID_TravailPfe",String.valueOf(_ID_TravailPfe));

				_ArrayList_Etudiant.add(_HashMap);
			}
		}
		return _ArrayList_Etudiant;
	}	
	public ArrayList<HashMap<String, String>> Afficher_EnseignantDepartement_DEMANDeENCADREMENT()
	{  // afficher  enseignant et departement , de la table demande avec confirmation = 1 
		String sql=" SELECT DISTINCT enseignant.* , departement.* "
				+" FROM enseignant, demande, departement"
				+" WHERE demande.idensei_demande = enseignant.Idensei"
				+" AND enseignant.id_departement_enseignant = departement.Id_departement;";
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList_Etudiant = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();
				// ENSEIGNANT
				int ID_Enseignant=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_ID));
				String cin_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_NCIN));
				String nom_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_NOM));
				String prenom_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_PRENOM));
				String sexe_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_SEXE));
				String dateNaiss_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_DATENAISS));
				String lieuNaiss_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_LIEUNAISS));
				String numTel_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_NUmTEL));
				String email_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_EMAIL));
				String adresse_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_ADRESSE));
				String ville_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_VILLE));
				String codePost_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_CODEPOSTALE));
				String grade_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_GRADE));
				String diplome_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_DIPLOME));
				String lieuDipl_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_LIEUDIPLOME));
				String situation_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_SITUATIONCIVILE));
				String titulaire_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_TITULAIRE));
				String factAdm_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_FACtADMIN));
				String encadrement_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_ENCADREMENT));
				String anneeDip_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_ANNEEDIPLOME));
				String enDetachement_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_EnDETACHEMENT));
				String matricule_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_MATRICULE));
				String specialite_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_SPECIALITE));
				_HashMap.put("ID_Enseignant",String.valueOf(ID_Enseignant ));
				_HashMap.put("cin_Enseignant",cin_Enseignant );  
				_HashMap.put("nom_Enseignant",nom_Enseignant );
				_HashMap.put("prenom_Enseignant", prenom_Enseignant);
				_HashMap.put("sexe_Enseignant", sexe_Enseignant);
				_HashMap.put("dateNaiss_Enseignant",dateNaiss_Enseignant );
				_HashMap.put("lieuNaiss_Enseignant", lieuNaiss_Enseignant);
				_HashMap.put("numTel_Enseignant",numTel_Enseignant );
				_HashMap.put("email_Enseignant",email_Enseignant );
				_HashMap.put("adresse_Enseignant",adresse_Enseignant );
				_HashMap.put("ville_Enseignant",ville_Enseignant );
				_HashMap.put("codePost_Enseignant", codePost_Enseignant);
				_HashMap.put("grade_Enseignant", grade_Enseignant);
				_HashMap.put("diplome_Enseignant", diplome_Enseignant);
				_HashMap.put("lieuDipl_Enseignant",lieuDipl_Enseignant );
				_HashMap.put("situation_Enseignant",situation_Enseignant );
				_HashMap.put("titulaire_Enseignant",titulaire_Enseignant );
				_HashMap.put("factAdm_Enseignant",factAdm_Enseignant );
				_HashMap.put("encadrement_Enseignant",encadrement_Enseignant );
				_HashMap.put("anneeDip_Enseignant",anneeDip_Enseignant );
				_HashMap.put("enDetachement_Enseignant",enDetachement_Enseignant );
				_HashMap.put("matricule_Enseignant",matricule_Enseignant );
				_HashMap.put("specialite_Enseignant",specialite_Enseignant );
				// DEPARTEMENT
				int ID_Departement=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.DEPARTEMENT_COL_ID));
				String libelle_Departement=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.DEPARTEMENT_COL_LIBELLE));
				_HashMap.put("ID_Departement",String.valueOf( ID_Departement));
				_HashMap.put("libelle_Departement", libelle_Departement);


				_ArrayList_Etudiant.add(_HashMap);
			}
		}
		return _ArrayList_Etudiant;
	}	

	public ArrayList<HashMap<String, String>> Recherche_EncadreurPro_DEPOTSTAGE(String _Cin_EncadreurPro)
	{
		String sql=" SELECT encadreur_pro . * "
				+" FROM encadreur_pro "
				+" WHERE encadreur_pro.ncin_encadreurPro = ?" ;

		String[] args={_Cin_EncadreurPro};
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, args);
		ArrayList<HashMap<String, String>> _ArrayList_Etudiant = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();
				// EncadreurPro
				int ID_EncadreurPro=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ENCADREURPRO_COL_ID));
				String Cin_EncadreurPro=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENCADREURPRO_COL_NCIN));
				String Nom_EncadreurPro=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENCADREURPRO_COL_NOM));
				String Prenom_EncadreurPro=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENCADREURPRO_COL_PRENOM));
				String NumTel_EncadreurPro=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENCADREURPRO_COL_NUmTEL));
				String Email_EncadreurPro=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENCADREURPRO_COL_EMAIL));

				_HashMap.put("ID_EncadreurPro",String.valueOf(ID_EncadreurPro));
				_HashMap.put("Cin_EncadreurPro", Cin_EncadreurPro);
				_HashMap.put("Nom_EncadreurPro",Nom_EncadreurPro);
				_HashMap.put("Prenom_EncadreurPro",Prenom_EncadreurPro);
				_HashMap.put("NumTel_EncadreurPro",NumTel_EncadreurPro);
				_HashMap.put("Email_EncadreurPro",Email_EncadreurPro);

				_ArrayList_Etudiant.add(_HashMap);
			}
		}
		return _ArrayList_Etudiant;
	}	

	public ArrayList<HashMap<String, String>> Afficher_EncadreurPro_DEPOTSTAGE(String _ID_Stage)
	{ 
		String sql=" SELECT encadreur_pro . * "
				+" FROM encadreur_pro, stage"
				+" WHERE encadreur_pro.Id_encadreurPro = stage.id_encadreurPro_stage"
				+" AND stage.Id_stage = "+_ID_Stage+" " ;

		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList_Etudiant = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();
				// EncadreurPro
				int ID_EncadreurPro=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ENCADREURPRO_COL_ID));
				String Cin_EncadreurPro=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENCADREURPRO_COL_NCIN));
				String Nom_EncadreurPro=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENCADREURPRO_COL_NOM));
				String Prenom_EncadreurPro=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENCADREURPRO_COL_PRENOM));
				String NumTel_EncadreurPro=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENCADREURPRO_COL_NUmTEL));
				String Email_EncadreurPro=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENCADREURPRO_COL_EMAIL));

				_HashMap.put("ID_EncadreurPro",String.valueOf(ID_EncadreurPro));
				_HashMap.put("Cin_EncadreurPro", Cin_EncadreurPro);
				_HashMap.put("Nom_EncadreurPro",Nom_EncadreurPro);
				_HashMap.put("Prenom_EncadreurPro",Prenom_EncadreurPro);
				_HashMap.put("NumTel_EncadreurPro",NumTel_EncadreurPro);
				_HashMap.put("Email_EncadreurPro",Email_EncadreurPro);

				_ArrayList_Etudiant.add(_HashMap);
			}
		}
		return _ArrayList_Etudiant;
	}	

	public ArrayList<HashMap<String, String>> Afficher_Stage_DEPOTSTAGE()
	{ // afficher les stage et les etudiants qui ne prevenent pas d'une offre de stage
		String sql=" select distinct stage.* "+
				" from stage , travail_pfe , etudiant "+
				" where stage.id_travailPfe_stage=travail_pfe.Id_travailPfe and "+
				" stage.id_offreStage_stage is null and "+
				" etudiant.id_travailPfe_etudiant=travail_pfe.Id_travailPfe ;";
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList_Etudiant = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();

				int _ID_Stage=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_ID));
				String _nom_Stage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_NOM));
				String _date_Stage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_DATE));
				int _Id_travailPfe_Stage=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_IdTRAVAILPFE));
				String _Id_encadreurPro_Stage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_IdENCADREUrPRO));
				String _Id_offreStage_Stage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_IdOFFRESTAGE));
				String _descriptiona_Stage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_DESCRIPTION));
				int _Id_entreprise_Stage=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_IdENTREPRISE));

				_HashMap.put("_ID_Stage",String.valueOf( _ID_Stage));
				_HashMap.put("_nom_Stage",_nom_Stage);
				_HashMap.put("_date_Stage",_date_Stage);
				_HashMap.put("_Id_travailPfe_Stage",String.valueOf( _Id_travailPfe_Stage));
				_HashMap.put("_Id_encadreurPro_Stage",_Id_encadreurPro_Stage);
				_HashMap.put("_Id_offreStage_Stage",_Id_offreStage_Stage);
				_HashMap.put("_descriptiona_Stage",_descriptiona_Stage);
				_HashMap.put("_Id_entreprise_Stage",String.valueOf(_Id_entreprise_Stage));

				_ArrayList_Etudiant.add(_HashMap);
			}
		}
		return _ArrayList_Etudiant;
	}	

	public ArrayList<HashMap<String, String>> Afficher_IDEtudiant_DEPOTSTAGE(String _ID_Stage)
	{ // afficher les stage et les etudiants qui ne prevenent pas d'une offre de stage
		String sql=" select distinct etudiant.* "+
				" from stage , travail_pfe , etudiant "+
				" where" +
				" stage.Id_stage="+_ID_Stage+" and "+
				" stage.id_travailPfe_stage=travail_pfe.Id_travailPfe and "+
				" stage.id_offreStage_stage is null and "+
				" etudiant.id_travailPfe_etudiant=travail_pfe.Id_travailPfe ;";
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList_Etudiant = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();

				int ID_Etudiant=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_ID));
				String numInscrit_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_NUmINSCRIT));
				String nom_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_NOM));
				String prenom_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_PRENOM));

				_HashMap.put("ID_Etudiant",String.valueOf( ID_Etudiant));
				_HashMap.put("numInscrit_Etudiant", numInscrit_Etudiant);
				_HashMap.put("nom_Etudiant", nom_Etudiant);
				_HashMap.put("prenom_Etudiant", prenom_Etudiant);

				_ArrayList_Etudiant.add(_HashMap);
			}
		}
		return _ArrayList_Etudiant;
	}	

	public ArrayList<HashMap<String, String>> Afficher_Etudiant_INFORMATION(int _ID_Etudiant)
	{ /// information  etudiant 
		String sql="select etudiant.* ,cycle.*,niveau.* ,filiere.* ,departement.*"
				+" from etudiant ,departement ,filiere ,niveau ,cycle"
				+" where" +
				" etudiant.Id_etudiant = "+_ID_Etudiant+"  AND"
				+" departement.Id_departement=etudiant.id_departement_etudiant AND"
				+" filiere.Id_filiere=etudiant.id_filiere_etudiant AND"
				+" filiere.id_cycle_filiere = cycle.Id_cycle AND"
				+" niveau.id_filiere_niveau=filiere.Id_filiere ;";
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList_Etudiant = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();

				int ID_Etudiant=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_ID));
				String numInscrit_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_NUmINSCRIT));
				String cin_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_CIN));
				String nom_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_NOM));
				String prenom_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_PRENOM));
				String sexe_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_SEXE));
				String dateNaiss_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_DATENAISS));
				String lieuNaiss_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_LIEUNAISS));
				String numTel_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_NUmTEL));
				String email_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_EMAIL));
				String adresse_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_ADRESSE));
				String codePostal_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_CODePOSTAL));
				String image_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_IMAGE));
				int Id_departement_Etudiant=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_IdDEPARTEMENT));
				String Id_travailPfe_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_IdTRAVAILPFE));
				String Id_etudiant1_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_IdETUDIANT1));
				String Id_document_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_IdDOCUMENT));
				int Id_filiere_Etudiant=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_IdFILIERE));
				_HashMap.put("ID_Etudiant",String.valueOf( ID_Etudiant));
				_HashMap.put("numInscrit_Etudiant", numInscrit_Etudiant);
				_HashMap.put("cin_Etudiant", cin_Etudiant);
				_HashMap.put("nom_Etudiant", nom_Etudiant);
				_HashMap.put("prenom_Etudiant", prenom_Etudiant);
				_HashMap.put("sexe_Etudiant", sexe_Etudiant);
				_HashMap.put("dateNaiss_Etudiant", dateNaiss_Etudiant);
				_HashMap.put("lieuNaiss_Etudiant", lieuNaiss_Etudiant);
				_HashMap.put("numTel_Etudiant", numTel_Etudiant);
				_HashMap.put("email_Etudiant", email_Etudiant);
				_HashMap.put("adresse_Etudiant", adresse_Etudiant);
				_HashMap.put("codePostal_Etudiant", codePostal_Etudiant);
				_HashMap.put("image_Etudiant", image_Etudiant);
				_HashMap.put("Id_departement_Etudiant", String.valueOf(Id_departement_Etudiant));
				_HashMap.put("Id_travailPfe_Etudiant", Id_travailPfe_Etudiant);
				_HashMap.put("Id_etudiant1_Etudiant", Id_etudiant1_Etudiant);
				_HashMap.put("Id_document_Etudiant", Id_document_Etudiant);
				_HashMap.put("Id_filiere_Etudiant", String.valueOf(Id_filiere_Etudiant));
				String libelle_Departement=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.DEPARTEMENT_COL_LIBELLE));
				_HashMap.put("libelle_Departement", libelle_Departement);
				String num_Niveau=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.NIVEAU_COL_NUM));
				String designation_Niveau=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.NIVEAU_COL_DESIGNATION));
				_HashMap.put("num_Niveau", num_Niveau);
				_HashMap.put("designation_Niveau", designation_Niveau);
				String designation_Filiere=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.FILIERE_COL_DESIGNATION));
				_HashMap.put("designation_Filiere", designation_Filiere);
				int ID_cycle=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.CYCLE_COL_ID));
				String num_Cycle=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.CYCLE_COL_NUM));
				String designation_Cycle=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.CYCLE_COL_DESIGNATION));
				_HashMap.put("ID_cycle", String.valueOf(ID_cycle));
				_HashMap.put("num_Cycle", num_Cycle);
				_HashMap.put("designation_Cycle", designation_Cycle);	

				_ArrayList_Etudiant.add(_HashMap);
			}
		}
		return _ArrayList_Etudiant;
	}

	public ArrayList<HashMap<String, String>> Afficher_EtudiantEntreprise_DEMANDESTAGE(int _ID_offreStage)
	{ /// Stage
		String sql=" select distinct  etudiant.* , entreprise.*"
				+" from etudiant , entreprise , offre_stage , condidater"
				+" where "
				+" condidater.id_etudiant_condidater=etudiant.Id_etudiant and"
				+" condidater.id_offreStage_condidater=offre_stage.Id_offreStage and"
				+" condidater.id_offreStage_condidater ="+_ID_offreStage+" and"
				+" offre_stage.id_entreprise_offreStage=entreprise.Id_entreprise and "+

				" condidater.confirmation_condidater = 2; ";
		//" entreprise.idensei_entreprise is null and" +
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList_OffreStage = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();
				// Entreprise
				int ID_Entreprise=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_ID));
				String libelle_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_LIBELLE));
				String raison_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_RAISON));
				String numTel_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_NUmTEL));
				String email_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_EMAIL));
				String siteWeb_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_SITEWEB));
				String login_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_LOGIN));
				String password_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_PASSWORD));
				String logo_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_LOGO));
				_HashMap.put("ID_Entreprise",String.valueOf(ID_Entreprise));
				_HashMap.put("libelle_Entreprise",libelle_Entreprise);
				_HashMap.put("raison_Entreprise",raison_Entreprise);
				_HashMap.put("numTel_Entreprise",numTel_Entreprise);
				_HashMap.put("email_Entreprise",email_Entreprise);
				_HashMap.put("siteWeb_Entreprise",siteWeb_Entreprise);
				_HashMap.put("login_Entreprise",login_Entreprise);
				_HashMap.put("password_Entreprise",password_Entreprise);
				_HashMap.put("logo_Entreprise",logo_Entreprise);
				// Etudiant
				int ID_Etudiant=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_ID));
				String numInscrit_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_NUmINSCRIT));
				String cin_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_CIN));
				String nom_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_NOM));
				String prenom_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_PRENOM));
				String sexe_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_SEXE));
				String dateNaiss_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_DATENAISS));
				String lieuNaiss_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_LIEUNAISS));
				String numTel_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_NUmTEL));
				String email_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_EMAIL));
				String adresse_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_ADRESSE));
				String codePostal_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_CODePOSTAL));
				String image_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_IMAGE));
				int Id_filiere_Etudiant=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_IdFILIERE));
				_HashMap.put("ID_Etudiant",String.valueOf( ID_Etudiant));
				_HashMap.put("numInscrit_Etudiant", numInscrit_Etudiant);
				_HashMap.put("cin_Etudiant", cin_Etudiant);
				_HashMap.put("nom_Etudiant", nom_Etudiant);
				_HashMap.put("prenom_Etudiant", prenom_Etudiant);
				_HashMap.put("sexe_Etudiant", sexe_Etudiant);
				_HashMap.put("dateNaiss_Etudiant", dateNaiss_Etudiant);
				_HashMap.put("lieuNaiss_Etudiant", lieuNaiss_Etudiant);
				_HashMap.put("numTel_Etudiant", numTel_Etudiant);
				_HashMap.put("email_Etudiant", email_Etudiant);
				_HashMap.put("adresse_Etudiant", adresse_Etudiant);
				_HashMap.put("codePostal_Etudiant", codePostal_Etudiant);
				_HashMap.put("image_Etudiant", image_Etudiant);
				_HashMap.put("Id_filiere_Etudiant", String.valueOf(Id_filiere_Etudiant));

				_ArrayList_OffreStage.add(_HashMap);
			}
		}
		return _ArrayList_OffreStage;
	}

	public ArrayList<HashMap<String, String>> Afficher_OffreStage_DEMANDESTAGE()
	{ /// Stage
		String sql="SELECT distinct offre_stage.* " +
				"FROM offre_stage , entreprise , condidater " +
				"where" +
				" condidater.id_offreStage_condidater=offre_stage.Id_offreStage and "+
				" entreprise.idensei_entreprise is null and" +
				" condidater.confirmation_condidater = 2 ; ";
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList_OffreStage = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();

				int ID_OffreStage=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_ID));
				String nom_OffreStage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_NOM));
				String description_OffreStage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_DESCRIPTION));
				String confirmation_OffreStage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_CONFIRMATION));
				String date_OffreStage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_DATE));
				int cle_entreprise_OffreStage=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_IdENTREPRISE));
				String cle_stage_OffreStage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_IdSTAGE));
				String cle_travailPfe_OffreStage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_IdTRAVAILPFE));
				int cle_filiere_OffreStage=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_IdFILIERE));

				_HashMap.put("ID_OffreStage",String.valueOf(ID_OffreStage));
				_HashMap.put("nom_OffreStage",nom_OffreStage );
				_HashMap.put("description_OffreStage",description_OffreStage );
				_HashMap.put("confirmation_OffreStage",confirmation_OffreStage );
				_HashMap.put("date_OffreStage",date_OffreStage );
				_HashMap.put("cle_entreprise_OffreStage",String.valueOf(cle_entreprise_OffreStage ));
				_HashMap.put("cle_stage_OffreStage",cle_stage_OffreStage);
				_HashMap.put("cle_travailPfe_OffreStage",cle_travailPfe_OffreStage );
				_HashMap.put("cle_filiere_OffreStage",String.valueOf(cle_filiere_OffreStage));

				_ArrayList_OffreStage.add(_HashMap);
			}
		}
		return _ArrayList_OffreStage;
	}
	// DEPOT
	public ArrayList<HashMap<String, String>> Afficher_OffreStage_DEPOT()
	{ 
		String sql=" SELECT offre_stage.* "+
				" FROM offre_stage , entreprise "+
				" WHERE " +
				"offre_stage.id_entreprise_offreStage = entreprise.Id_entreprise "+
				" AND entreprise.idensei_entreprise is not null ; "; 
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList_OffreStage = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();

				int ID_OffreStage=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_ID));
				String nom_OffreStage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_NOM));
				String description_OffreStage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_DESCRIPTION));
				String confirmation_OffreStage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_CONFIRMATION));
				String date_OffreStage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_DATE));
				int cle_entreprise_OffreStage=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_IdENTREPRISE));
				String cle_stage_OffreStage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_IdSTAGE));
				String cle_travailPfe_OffreStage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_IdTRAVAILPFE));
				int cle_filiere_OffreStage=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_IdFILIERE));

				_HashMap.put("ID_OffreStage",String.valueOf(ID_OffreStage));
				_HashMap.put("nom_OffreStage",nom_OffreStage );
				_HashMap.put("description_OffreStage",description_OffreStage );
				_HashMap.put("confirmation_OffreStage",confirmation_OffreStage );
				_HashMap.put("date_OffreStage",date_OffreStage );
				_HashMap.put("cle_entreprise_OffreStage",String.valueOf(cle_entreprise_OffreStage ));
				_HashMap.put("cle_stage_OffreStage",cle_stage_OffreStage);
				_HashMap.put("cle_travailPfe_OffreStage",cle_travailPfe_OffreStage);
				_HashMap.put("cle_filiere_OffreStage",String.valueOf(cle_filiere_OffreStage));

				_ArrayList_OffreStage.add(_HashMap);
			}
		}
		return _ArrayList_OffreStage;
	}
	public ArrayList<HashMap<String, String>> Afficher_Entreprise_DEPOT()
	{
		String sql="SELECT * FROM entreprise " +
				"where" +
				" entreprise.idensei_entreprise is not null";
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();
				int ID_Entreprise=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_ID));
				String libelle_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_LIBELLE));
				String raison_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_RAISON));
				String numTel_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_NUmTEL));
				String email_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_EMAIL));
				String siteWeb_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_SITEWEB));
				String login_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_LOGIN));
				String password_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_PASSWORD));
				String logo_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_LOGO));
				String Id_enseignant_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_IdENSEIGNANT));

				_HashMap.put("ID_Entreprise",String.valueOf(ID_Entreprise));
				_HashMap.put("libelle_Entreprise",libelle_Entreprise);
				_HashMap.put("raison_Entreprise",raison_Entreprise);
				_HashMap.put("numTel_Entreprise",numTel_Entreprise);
				_HashMap.put("email_Entreprise",email_Entreprise);
				_HashMap.put("siteWeb_Entreprise",siteWeb_Entreprise);
				_HashMap.put("login_Entreprise",login_Entreprise);
				_HashMap.put("password_Entreprise",password_Entreprise);
				_HashMap.put("logo_Entreprise",logo_Entreprise); 
				_HashMap.put("Id_enseignant_Entreprise",Id_enseignant_Entreprise);

				_ArrayList.add(_HashMap);
			}
		}
		return _ArrayList;
	}
	// ENTREPRISE
	public ArrayList<HashMap<String, String>> Afficher_OffreStage_ENTREPRISE()
	{ 
		String sql=" SELECT offre_stage.* "+
				" FROM offre_stage , entreprise "+
				" WHERE " +
				" offre_stage.id_entreprise_offreStage = entreprise.Id_entreprise "+
				" AND entreprise.idensei_entreprise is null ; "; 
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList_OffreStage = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();

				int ID_OffreStage=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_ID));
				String nom_OffreStage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_NOM));
				String description_OffreStage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_DESCRIPTION));
				String confirmation_OffreStage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_CONFIRMATION));
				String date_OffreStage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_DATE));
				int cle_entreprise_OffreStage=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_IdENTREPRISE));
				String cle_stage_OffreStage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_IdSTAGE));
				String cle_travailPfe_OffreStage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_IdTRAVAILPFE));
				int cle_filiere_OffreStage=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_IdFILIERE));

				_HashMap.put("ID_OffreStage",String.valueOf(ID_OffreStage));
				_HashMap.put("nom_OffreStage",nom_OffreStage );
				_HashMap.put("description_OffreStage",description_OffreStage );
				_HashMap.put("confirmation_OffreStage",confirmation_OffreStage );
				_HashMap.put("date_OffreStage",date_OffreStage );
				_HashMap.put("cle_entreprise_OffreStage",String.valueOf(cle_entreprise_OffreStage ));
				_HashMap.put("cle_stage_OffreStage",cle_stage_OffreStage);
				_HashMap.put("cle_travailPfe_OffreStage",cle_travailPfe_OffreStage);
				_HashMap.put("cle_filiere_OffreStage",String.valueOf(cle_filiere_OffreStage));

				_ArrayList_OffreStage.add(_HashMap);
			}
		}
		return _ArrayList_OffreStage;
	}
	public ArrayList<HashMap<String, String>> Afficher_Entreprise_ENTREPRISE()
	{
		String sql="SELECT * " +
				"FROM entreprise " +
				"where" +
				" entreprise.idensei_entreprise is null";
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();
				int ID_Entreprise=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_ID));
				String libelle_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_LIBELLE));
				String raison_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_RAISON));
				String numTel_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_NUmTEL));
				String email_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_EMAIL));
				String siteWeb_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_SITEWEB));
				String login_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_LOGIN));
				String password_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_PASSWORD));
				String logo_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_LOGO));
				String Id_enseignant_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_IdENSEIGNANT));

				_HashMap.put("ID_Entreprise",String.valueOf(ID_Entreprise));
				_HashMap.put("libelle_Entreprise",libelle_Entreprise);
				_HashMap.put("raison_Entreprise",raison_Entreprise);
				_HashMap.put("numTel_Entreprise",numTel_Entreprise);
				_HashMap.put("email_Entreprise",email_Entreprise);
				_HashMap.put("siteWeb_Entreprise",siteWeb_Entreprise);
				_HashMap.put("login_Entreprise",login_Entreprise);
				_HashMap.put("password_Entreprise",password_Entreprise);
				_HashMap.put("logo_Entreprise",logo_Entreprise); 
				_HashMap.put("Id_enseignant_Entreprise",Id_enseignant_Entreprise);

				_ArrayList.add(_HashMap);
			}
		}
		return _ArrayList;
	}
	public ArrayList<HashMap<String, String>> Afficher_Filiere()
	{
		String sql=" SELECT  * FROM filiere ";
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();
				//FILIERE
				int ID_Filiere=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.FILIERE_COL_ID));
				String designation_Filiere=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.FILIERE_COL_DESIGNATION));

				_HashMap.put("ID_Filiere", String.valueOf(ID_Filiere));
				_HashMap.put("designation_Filiere", designation_Filiere);

				_ArrayList.add(_HashMap);
			}
		}
		return _ArrayList;
	}
	public ArrayList<HashMap<String, String>> Afficher_EnseignantDepartement()
	{
		String sql=" SELECT  * " +
				"FROM departement , enseignant ";
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();
				// ENSEIGNANT
				int ID_Enseignant=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_ID));
				String cin_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_NCIN));
				String nom_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_NOM));
				String prenom_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_PRENOM));
				String sexe_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_SEXE));
				String dateNaiss_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_DATENAISS));
				String lieuNaiss_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_LIEUNAISS));
				String numTel_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_NUmTEL));
				String email_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_EMAIL));
				String adresse_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_ADRESSE));
				String ville_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_VILLE));
				String codePost_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_CODEPOSTALE));
				String grade_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_GRADE));
				String diplome_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_DIPLOME));
				String lieuDipl_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_LIEUDIPLOME));
				String situation_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_SITUATIONCIVILE));
				String titulaire_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_TITULAIRE));
				String factAdm_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_FACtADMIN));
				String encadrement_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_ENCADREMENT));
				String anneeDip_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_ANNEEDIPLOME));
				String enDetachement_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_EnDETACHEMENT));
				String matricule_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_MATRICULE));
				String specialite_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_SPECIALITE));
				_HashMap.put("ID_Enseignant",String.valueOf(ID_Enseignant ));
				_HashMap.put("cin_Enseignant",cin_Enseignant );  
				_HashMap.put("nom_Enseignant",nom_Enseignant );
				_HashMap.put("prenom_Enseignant", prenom_Enseignant);
				_HashMap.put("sexe_Enseignant", sexe_Enseignant);
				_HashMap.put("dateNaiss_Enseignant",dateNaiss_Enseignant );
				_HashMap.put("lieuNaiss_Enseignant", lieuNaiss_Enseignant);
				_HashMap.put("numTel_Enseignant",numTel_Enseignant );
				_HashMap.put("email_Enseignant",email_Enseignant );
				_HashMap.put("adresse_Enseignant",adresse_Enseignant );
				_HashMap.put("ville_Enseignant",ville_Enseignant );
				_HashMap.put("codePost_Enseignant", codePost_Enseignant);
				_HashMap.put("grade_Enseignant", grade_Enseignant);
				_HashMap.put("diplome_Enseignant", diplome_Enseignant);
				_HashMap.put("lieuDipl_Enseignant",lieuDipl_Enseignant );
				_HashMap.put("situation_Enseignant",situation_Enseignant );
				_HashMap.put("titulaire_Enseignant",titulaire_Enseignant );
				_HashMap.put("factAdm_Enseignant",factAdm_Enseignant );
				_HashMap.put("encadrement_Enseignant",encadrement_Enseignant );
				_HashMap.put("anneeDip_Enseignant",anneeDip_Enseignant );
				_HashMap.put("enDetachement_Enseignant",enDetachement_Enseignant );
				_HashMap.put("matricule_Enseignant",matricule_Enseignant );
				_HashMap.put("specialite_Enseignant",specialite_Enseignant );
				// DEPARTEMENT
				int ID_Departement=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.DEPARTEMENT_COL_ID));
				String libelle_Departement=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.DEPARTEMENT_COL_LIBELLE));
				_HashMap.put("ID_Departement",String.valueOf( ID_Departement));
				_HashMap.put("libelle_Departement", libelle_Departement);

				_ArrayList.add(_HashMap);
			}
		}
		return _ArrayList;
	}
	///////////////////// SYNCHRONISATION SERVEUR :

	public boolean Synchroniser_Serveur_Deriger(String _URL)
	{ 
		int x=-1;
		_ClientHTTP=new ClientHTTP(pContext);
		String lRetour_Connecxion=_ClientHTTP.readFromUrl(_URL);

		try {
			JSONArray lArray_Salle=new JSONArray(lRetour_Connecxion);
			for (int i = 0; i < lArray_Salle.length(); i++)
			{
				JSONObject lObjc_P=lArray_Salle.getJSONObject(i);
				// Deriger
				String _Type_Deriger=lObjc_P.getString(DataBase_Static.DERIGER_COL_TYPE);
				String _IdEnseignant_Deriger=lObjc_P.getString(DataBase_Static.DERIGER_COL_IdENSEIGNANT);
				String _IdSoutenance_Deriger=lObjc_P.getString(DataBase_Static.DERIGER_COL_IdSOUTENANCE);
				Deriger _Deriger =new Deriger(_Type_Deriger, _IdEnseignant_Deriger, _IdSoutenance_Deriger);
				x=InsertInTo_BD_Deriger(_Deriger);
				if (x==-1)
				{
					Toast.makeText( pContext , "ERREUR Lors de l'Insertion de Donnees " ,Toast.LENGTH_LONG).show();
					return false;
				}
			}
		} 
		catch(JSONException e)
		{
			return false;
		}
		return true;
	}

	public boolean Synchroniser_Serveur_Soutenance(String _URL)
	{ 
		int x=-1;
		_ClientHTTP=new ClientHTTP(pContext);
		String lRetour_Connecxion=_ClientHTTP.readFromUrl(_URL);

		try {
			JSONArray lArray_Salle=new JSONArray(lRetour_Connecxion);
			for (int i = 0; i < lArray_Salle.length(); i++)
			{
				JSONObject lObjc_P=lArray_Salle.getJSONObject(i);
				// Soutenance
				int _ID_Soutenance=lObjc_P.getInt(DataBase_Static.SOUTENANCE_COL_ID);
				String _Type_Soutenance=lObjc_P.getString(DataBase_Static.SOUTENANCE_COL_TYPE);
				String _HeureDeb_Soutenance=lObjc_P.getString(DataBase_Static.SOUTENANCE_COL_HEUReDEB);
				String _HeureFin_Soutenance=lObjc_P.getString(DataBase_Static.SOUTENANCE_COL_HEUReFIN);
				String _Date_Soutenance=lObjc_P.getString(DataBase_Static.SOUTENANCE_COL_JOUR);
				int _IdSalle_Soutenance=lObjc_P.getInt(DataBase_Static.SOUTENANCE_COL_IdSALLE);
				int _IdTravailPfe_Soutenance=lObjc_P.getInt(DataBase_Static.SOUTENANCE_COL_IdTRAVAILPFE);
				Soutenance _Soutenance =new Soutenance(_ID_Soutenance, _Type_Soutenance, _HeureDeb_Soutenance, _HeureFin_Soutenance, _Date_Soutenance, _IdSalle_Soutenance, _IdTravailPfe_Soutenance);
				x=InsertInTo_BD_Soutenance(_Soutenance);
				if (x==-1)
				{
					Toast.makeText( pContext , "ERREUR Lors de l'Insertion de Donnees " ,Toast.LENGTH_LONG).show();
					return false;
				}
			}
		} 
		catch(JSONException e)
		{
			return false;
		}
		return true;
	}

	public boolean Synchroniser_Serveur_Salle(String _URL)
	{ 
		int x=-1;
		_ClientHTTP=new ClientHTTP(pContext);
		String lRetour_Connecxion=_ClientHTTP.readFromUrl(_URL);

		try {
			JSONArray lArray_Salle=new JSONArray(lRetour_Connecxion);
			for (int i = 0; i < lArray_Salle.length(); i++)
			{
				JSONObject lObjc_P=lArray_Salle.getJSONObject(i);
				// Salle
				int _ID_Salle=lObjc_P.getInt(DataBase_Static.SALLE_COL_ID);
				String _bloc_Salle=lObjc_P.getString(DataBase_Static.SALLE_COL_BLOC);
				String _etage_Salle=lObjc_P.getString(DataBase_Static.SALLE_COL_ETAGE);
				String _abreviation_Salle=lObjc_P.getString(DataBase_Static.SALLE_COL_ABREVIATION);
				String _type_Salle=lObjc_P.getString(DataBase_Static.SALLE_COL_TYPE);
				String _capacite_Salle=lObjc_P.getString(DataBase_Static.SALLE_COL_CAPACITE);
				String _etat_Salle=lObjc_P.getString(DataBase_Static.SALLE_COL_ETAT);
				String _videoProject_Salle=lObjc_P.getString(DataBase_Static.SALLE_COL_VIDEOPRO);
				String _informatique_Salle=lObjc_P.getString(DataBase_Static.SALLE_COL_INFORMATIQUE);

				Salle _Salle=new Salle(_ID_Salle, _bloc_Salle, _etage_Salle, _abreviation_Salle, _type_Salle, _capacite_Salle, _etat_Salle, _videoProject_Salle, _informatique_Salle);
				x=InsertInTo_BD_Salle(_Salle);
				if (x==-1)
				{
					Toast.makeText( pContext , "ERREUR Lors de l'Insertion de Donnees " ,Toast.LENGTH_LONG).show();
					return false;
				}
			}
		} 
		catch(JSONException e)
		{
			return false;
		}
		return true;
	}

	public boolean Synchroniser_Serveur_EncadreurPro(String _URL)
	{ 
		int x=-1;
		_ClientHTTP=new ClientHTTP(pContext);
		String lRetour_Connecxion=_ClientHTTP.readFromUrl(_URL);

		try {
			JSONArray lArray_Etudiant=new JSONArray(lRetour_Connecxion);
			for (int i = 0; i < lArray_Etudiant.length(); i++)
			{
				JSONObject lObjc_P=lArray_Etudiant.getJSONObject(i);
				// EncadreurPro

				int ID_EncadreurPro=lObjc_P.getInt(DataBase_Static.ENCADREURPRO_COL_ID);
				String Ncin_EncadreurPro=lObjc_P.getString(DataBase_Static.ENCADREURPRO_COL_NCIN);
				String Nom_EncadreurPro=lObjc_P.getString(DataBase_Static.ENCADREURPRO_COL_NOM);
				String Prenom_EncadreurPro=lObjc_P.getString(DataBase_Static.ENCADREURPRO_COL_PRENOM);
				String NumTel_EncadreurPro=lObjc_P.getString(DataBase_Static.ENCADREURPRO_COL_NUmTEL);
				String Email_EncadreurPro=lObjc_P.getString(DataBase_Static.ENCADREURPRO_COL_EMAIL);
				EncadreurPro _EncadreurPro=new EncadreurPro(ID_EncadreurPro, Ncin_EncadreurPro, Nom_EncadreurPro, Prenom_EncadreurPro, NumTel_EncadreurPro, Email_EncadreurPro);
				x=InsertInTo_BD_EncadreurPro(_EncadreurPro);

				if (x==-1)
				{
					Toast.makeText( pContext , "ERREUR Lors de l'Insertion de Donnees " ,Toast.LENGTH_LONG).show();
					return false;
				}
			}
		} 
		catch(JSONException e)
		{
			//Toast.makeText( pContext , "  " ,Toast.LENGTH_LONG).show();
			return false;
		}
		return true;
	}
	public boolean Synchroniser_Serveur_Demande(String _ID_Departement,String _URL)
	{ // les importer les demande dant la confirmation egal a 1 !!!!
		int x=-1;
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();
		NameValuePair lParamIdDepartement=new BasicNameValuePair("PARAM_ID_DEPARTEMENT",_ID_Departement);
		lListofParams.add(lParamIdDepartement);
		_ClientHTTP=new ClientHTTP(pContext);
		String lRetour_Connecxion=_ClientHTTP.LireEcrireUrl(_URL, lListofParams);

		//		MyAsyncTask_LireServeur _MyAsyncTask =new MyAsyncTask_LireServeur(pContext, lListofParams, _URL);
		//		String lRetour_Connecxion=null;
		//		try {
		//			lRetour_Connecxion = _MyAsyncTask.execute().get();
		//		} catch (InterruptedException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		} catch (ExecutionException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		}

		try {
			JSONArray lArray_Etudiant=new JSONArray(lRetour_Connecxion);
			for (int i = 0; i < lArray_Etudiant.length(); i++)
			{
				JSONObject lObjc_P=lArray_Etudiant.getJSONObject(i);
				// Demande
				String _confirmation_Demande=lObjc_P.getString(DataBase_Static.DEMANDE_COL_CONFIRMATION);
				String _date_Demande=lObjc_P.getString(DataBase_Static.DEMANDE_COL_DATE);
				int _IdEnseignant_Demande=lObjc_P.getInt(DataBase_Static.DEMANDE_COL_IdENSEIGNANT);
				int _IdEtudiant_Demande=lObjc_P.getInt(DataBase_Static.DEMANDE_COL_IdETUDIANT);

				Demande _Demande=new Demande(_confirmation_Demande, _date_Demande, _IdEnseignant_Demande, _IdEtudiant_Demande);
				x=InsertInTo_BD_Demande(_Demande);
				if (x==-1)
				{
					Toast.makeText( pContext , "ERREUR Lors de l'Insertion de Donnees " ,Toast.LENGTH_LONG).show();
					return false;
				}
			}
		} 
		catch(JSONException e)
		{
			//Toast.makeText( pContext , "  " ,Toast.LENGTH_LONG).show();
			return false;
		}
		return true;
	}

	public boolean Synchroniser_Serveur_EnseignantDepartementFiliereNiveauCycle_ENCADREMENT(String _ID_Departement,String _URL)
	{
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();
		NameValuePair lParamIdDepartement=new BasicNameValuePair("PARAM_ID_DEPARTEMENT",_ID_Departement);
		lListofParams.add(lParamIdDepartement);
		_ClientHTTP=new ClientHTTP(pContext);
		String lRetour_Connecxion=_ClientHTTP.LireEcrireUrl(_URL, lListofParams);

		//		MyAsyncTask_LireServeur _MyAsyncTask =new MyAsyncTask_LireServeur(pContext, lListofParams, _URL);
		//		String lRetour_Connecxion=null;
		//		try {
		//			lRetour_Connecxion = _MyAsyncTask.execute().get();
		//		} catch (InterruptedException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		} catch (ExecutionException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		}


		try {
			JSONArray lArray_Etudiant=new JSONArray(lRetour_Connecxion);
			for (int i = 0; i < lArray_Etudiant.length(); i++)
			{
				JSONObject lObjc_P=lArray_Etudiant.getJSONObject(i);
				// Enseignant
				int _ID_Enseignant=lObjc_P.getInt(DataBase_Static.ENSEIGNANT_COL_ID);
				String Cin_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_NCIN);
				String Nom_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_NOM);
				String Prenom_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_PRENOM);
				String Sexe_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_SEXE);
				String DateNaiss_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_DATENAISS);
				String LieuNaiss_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_LIEUNAISS);
				String NumTel_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_NUmTEL);
				String Email_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_EMAIL);
				String adresse_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_ADRESSE);
				String Ville_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_VILLE);
				String CodePost_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_CODEPOSTALE);
				String Grade_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_GRADE);
				String Diplome_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_DIPLOME);
				String LieuDip_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_LIEUDIPLOME);
				String Situation_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_SITUATIONCIVILE);
				String Titulaire_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_TITULAIRE);
				String FactAdmin_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_FACtADMIN);
				String Encadreument_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_ENCADREMENT);
				String AnneeDip_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_ANNEEDIPLOME);
				String EnDetachement_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_EnDETACHEMENT);
				int Id_departement_Enseignant=lObjc_P.getInt(DataBase_Static.ENSEIGNANT_COL_IdDepartement);
				String Matricule_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_MATRICULE);
				String Specialite_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_SPECIALITE);
				Enseignant _Classe_Enseignant=new Enseignant(_ID_Enseignant, Cin_Enseignant, Nom_Enseignant, Prenom_Enseignant, Sexe_Enseignant, DateNaiss_Enseignant, LieuNaiss_Enseignant, NumTel_Enseignant, Email_Enseignant, adresse_Enseignant, Ville_Enseignant, CodePost_Enseignant, Grade_Enseignant, Diplome_Enseignant, LieuDip_Enseignant, Situation_Enseignant, Titulaire_Enseignant, FactAdmin_Enseignant, Encadreument_Enseignant, AnneeDip_Enseignant, EnDetachement_Enseignant, Matricule_Enseignant, Specialite_Enseignant, Id_departement_Enseignant);
				///// EPARTEMENT
				int ID_Departement=lObjc_P.getInt(DataBase_Static.DEPARTEMENT_COL_ID);
				String libelle_Departement=lObjc_P.getString(DataBase_Static.DEPARTEMENT_COL_LIBELLE);
				String abreviation_Departement=lObjc_P.getString(DataBase_Static.DEPARTEMENT_COL_ABREVIATION);
				String ordre_Departement=lObjc_P.getString(DataBase_Static.DEPARTEMENT_COL_ORDRE);
				Departement _Classe_Departement = new Departement(ID_Departement, libelle_Departement, abreviation_Departement, ordre_Departement);
				///// NIVEAU
				int ID_Niveau=lObjc_P.getInt(DataBase_Static.NIVEAU_COL_ID);
				String num_Niveau=lObjc_P.getString(DataBase_Static.NIVEAU_COL_NUM);
				String designation_Niveau=lObjc_P.getString(DataBase_Static.NIVEAU_COL_DESIGNATION);
				Integer cle_filiere_Niveau=lObjc_P.getInt(DataBase_Static.NIVEAU_COL_IdFiliere);
				Niveau _Classe_Niveau=new Niveau(ID_Niveau, num_Niveau, designation_Niveau, null, null, cle_filiere_Niveau);
				///// CYCLE
				int ID_Cycle=lObjc_P.getInt(DataBase_Static.CYCLE_COL_ID);
				String num_Cycle=lObjc_P.getString(DataBase_Static.CYCLE_COL_NUM);
				String designation_Cycle=lObjc_P.getString(DataBase_Static.CYCLE_COL_DESIGNATION);
				Cycle _Classe_Cycle= new Cycle(ID_Cycle, num_Cycle, designation_Cycle, null, null);
				///// FILIERE
				int ID_Filiere=lObjc_P.getInt(DataBase_Static.FILIERE_COL_ID);
				String designation_Filiere=lObjc_P.getString(DataBase_Static.FILIERE_COL_DESIGNATION);
				Integer cle_cycle_Filiere=lObjc_P.getInt(DataBase_Static.FILIERE_COL_IdCYCLE);
				Filiere _Classe_Filiere = new Filiere(ID_Filiere, designation_Filiere, cle_cycle_Filiere);
				// un seul enseignant ,departement mais plusieur filiere 
				InsertInTo_BD_Enseignant(_Classe_Enseignant);
				InsertInTo_BD_Departement(_Classe_Departement);
				InsertInTo_BD_Niveau(_Classe_Niveau);
				InsertInTo_BD_Cycle(_Classe_Cycle);		
				InsertInTo_BD_Filiere(_Classe_Filiere);

			}
		} 
		catch(JSONException e)
		{
			Toast.makeText( pContext , " Aucun Encadreur Académique Importé " ,Toast.LENGTH_LONG).show();
			return false;
		}
		return true;
	}

	public boolean Synchroniser_Serveur_StageTravailPfeEtudiant(String _ID_Departement,String URL_Connexion)
	{
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();
		NameValuePair lParamID_Enterprise=new BasicNameValuePair("PARAM_ID_DEPARTEMENT",_ID_Departement);
		lListofParams.add(lParamID_Enterprise);
		_ClientHTTP=new ClientHTTP(pContext);
		String lRetour_Connecxion=_ClientHTTP.LireEcrireUrl(URL_Connexion, lListofParams);

		//		MyAsyncTask_LireServeur _MyAsyncTask =new MyAsyncTask_LireServeur(pContext, lListofParams, URL_Connexion);
		//		String lRetour_Connecxion=null;
		//		try {
		//			lRetour_Connecxion = _MyAsyncTask.execute().get();
		//		} catch (InterruptedException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		} catch (ExecutionException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		}

		try{
			JSONArray lArray_Condidater=new JSONArray(lRetour_Connecxion);
			for (int i = 0; i < lArray_Condidater.length(); i++)
			{ 
				JSONObject lObjc_P=lArray_Condidater.getJSONObject(i);
				// Stage
				int _ID_Stage=lObjc_P.getInt(DataBase_Static.STAGE_COL_ID);
				String _nom_Stage=lObjc_P.getString(DataBase_Static.STAGE_COL_NOM);
				String _date_Stage=lObjc_P.getString(DataBase_Static.STAGE_COL_DATE);
				int _Id_travailPfe_Stage=lObjc_P.getInt(DataBase_Static.STAGE_COL_IdTRAVAILPFE);
				Integer _Id_encadreurPro_Stage=Verifier_Integer(lObjc_P.getString(DataBase_Static.STAGE_COL_IdENCADREUrPRO));
				Integer _Id_offreStage_Stage=Verifier_Integer(lObjc_P.getString(DataBase_Static.STAGE_COL_IdOFFRESTAGE));
				String _description_Stage=lObjc_P.getString(DataBase_Static.STAGE_COL_DESCRIPTION);
				int _Id_entreprise_Stage=lObjc_P.getInt(DataBase_Static.STAGE_COL_IdENTREPRISE);
				Stage _Stage = new Stage(_ID_Stage, _nom_Stage, _description_Stage, _date_Stage, _Id_travailPfe_Stage, _Id_encadreurPro_Stage, _Id_offreStage_Stage, _Id_entreprise_Stage);
				// Travail Pfe
				int _ID_TravailPfe=lObjc_P.getInt(DataBase_Static.TRAVAILPFE_COL_ID);
				String _libelle_TravailPfe=lObjc_P.getString(DataBase_Static.TRAVAILPFE_COL_LIBELLE);
				Integer _Id_enseignant_TravailPfe=Verifier_Integer(lObjc_P.getString(DataBase_Static.TRAVAILPFE_COL_IdENSEIGNANT));
				Integer _Id_soutenance_TravailPfe=Verifier_Integer(lObjc_P.getString(DataBase_Static.TRAVAILPFE_COL_IdSOUTENANCE));
				Travail_PFE _Travail_PFE=new Travail_PFE(_ID_TravailPfe, _libelle_TravailPfe, _Id_enseignant_TravailPfe, _Id_soutenance_TravailPfe);
				// Etudiant
				int ID_Etudiant=lObjc_P.getInt(DataBase_Static.ETUDIANT_COL_ID);
				String numInscrit_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_NUmINSCRIT);
				String cin_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_CIN);
				String nom_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_NOM);
				String prenom_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_PRENOM);
				String sexe_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_SEXE);
				String dateNaiss_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_DATENAISS);
				String lieuNaiss_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_LIEUNAISS);
				String numTel_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_NUmTEL);
				String email_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_EMAIL);
				String adresse_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_ADRESSE);
				String codePostale_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_CODePOSTAL);
				String image_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_IMAGE);
				int cle_departement_Etudiant=lObjc_P.getInt(DataBase_Static.ETUDIANT_COL_IdDEPARTEMENT);
				int cle_filiere_Etudiant=lObjc_P.getInt(DataBase_Static.ETUDIANT_COL_IdFILIERE);
				Integer cle_travailPfe_Etudiant=Verifier_Integer(lObjc_P.getString(DataBase_Static.ETUDIANT_COL_IdTRAVAILPFE));
				Integer cle_etudiant1_Etudiant=null;
				boolean cle_etudiant1_Test=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_IdETUDIANT1).equals("null");
				if (cle_etudiant1_Test==false)
				{
					cle_etudiant1_Etudiant=lObjc_P.getInt(DataBase_Static.ETUDIANT_COL_IdETUDIANT1);
					Synchroniser_Serveur_Binome(String.valueOf(cle_etudiant1_Etudiant), pContext.getResources().getString(R.string.EntrepriseActivity_import_CondidaterBinome));
				}
				Integer cle_document_Etudiant=Verifier_Integer(lObjc_P.getString(DataBase_Static.ETUDIANT_COL_IdDOCUMENT));
				Etudiant _Etudiant=new Etudiant(ID_Etudiant, numInscrit_Etudiant, cin_Etudiant, nom_Etudiant, prenom_Etudiant, sexe_Etudiant, dateNaiss_Etudiant, lieuNaiss_Etudiant, numTel_Etudiant, email_Etudiant, adresse_Etudiant, codePostale_Etudiant, image_Etudiant, cle_departement_Etudiant, cle_travailPfe_Etudiant, cle_etudiant1_Etudiant, cle_filiere_Etudiant, cle_document_Etudiant);

				InsertInTo_BD_Etudiant(_Etudiant);
				InsertInTo_BD_TravailPfe(_Travail_PFE);
				InsertInTo_BD_Stage(_Stage);

			}
		}catch(JSONException e)
		{
			Toast.makeText( pContext , " Vous n'avez inséré aucun Stage pour le Moment " ,Toast.LENGTH_LONG).show();
			return false;
		}
		return true;
	}

	public boolean Synchroniser_Serveur_Binome(String _ID_Etudiant,String URL_Connexion)
	{
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();
		NameValuePair lParamID_Enterprise=new BasicNameValuePair("PARAM_ID_ETUDIANT",_ID_Etudiant);
		lListofParams.add(lParamID_Enterprise);
		_ClientHTTP=new ClientHTTP(pContext);
		String lRetour_Connecxion=_ClientHTTP.LireEcrireUrl(URL_Connexion, lListofParams);

		//		MyAsyncTask_LireServeur _MyAsyncTask =new MyAsyncTask_LireServeur(pContext, lListofParams, URL_Connexion);
		//		String lRetour_Connecxion=null;
		//		try {
		//			lRetour_Connecxion = _MyAsyncTask.execute().get();
		//		} catch (InterruptedException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		} catch (ExecutionException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		}

		try{
			JSONArray lArray_Condidater=new JSONArray(lRetour_Connecxion);
			for (int i = 0; i < lArray_Condidater.length(); i++)
			{ 
				JSONObject lObjc_P=lArray_Condidater.getJSONObject(i);

				int ID_Etudiant=lObjc_P.getInt(DataBase_Static.ETUDIANT_COL_ID);
				String numInscrit_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_NUmINSCRIT);
				String cin_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_CIN);
				String nom_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_NOM);
				String prenom_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_PRENOM);
				String sexe_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_SEXE);
				String dateNaiss_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_DATENAISS);
				String lieuNaiss_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_LIEUNAISS);
				String numTel_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_NUmTEL);
				String email_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_EMAIL);
				String adresse_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_ADRESSE);
				String codePostale_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_CODePOSTAL);
				String image_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_IMAGE);
				int cle_departement_Etudiant=lObjc_P.getInt(DataBase_Static.ETUDIANT_COL_IdDEPARTEMENT);
				int cle_filiere_Etudiant=lObjc_P.getInt(DataBase_Static.ETUDIANT_COL_IdFILIERE);
				Integer cle_travailPfe_Etudiant=Verifier_Integer(lObjc_P.getString(DataBase_Static.ETUDIANT_COL_IdTRAVAILPFE));
				Integer cle_etudiant1_Etudiant=Verifier_Integer(lObjc_P.getString(DataBase_Static.ETUDIANT_COL_IdETUDIANT1));
				Integer cle_document_Etudiant=Verifier_Integer(lObjc_P.getString(DataBase_Static.ETUDIANT_COL_IdDOCUMENT));

				Etudiant _Etudiant=new Etudiant(ID_Etudiant, numInscrit_Etudiant, cin_Etudiant, nom_Etudiant, prenom_Etudiant, sexe_Etudiant, dateNaiss_Etudiant, lieuNaiss_Etudiant, numTel_Etudiant, email_Etudiant, adresse_Etudiant, codePostale_Etudiant, image_Etudiant, cle_departement_Etudiant, cle_travailPfe_Etudiant, cle_etudiant1_Etudiant, cle_filiere_Etudiant, cle_document_Etudiant);
				InsertInTo_BD_Etudiant(_Etudiant);
			}
		}catch(JSONException e)
		{
			Toast.makeText( pContext , "ERREUR " ,Toast.LENGTH_LONG).show();
			return false;
		}
		return true;
	}
	public boolean Synchroniser_Serveur_CondidaterEtudiant(String _ID_Departement,String _URL)
	{
		int x=-1;
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();
		NameValuePair lParamFiliere=new BasicNameValuePair("PARAM_ID_DEPARTEMENT",_ID_Departement);
		lListofParams.add(lParamFiliere);
		_ClientHTTP=new ClientHTTP(pContext);
		String lRetour_Connecxion=_ClientHTTP.LireEcrireUrl(_URL, lListofParams);

		//		MyAsyncTask_LireServeur _MyAsyncTask =new MyAsyncTask_LireServeur(pContext, lListofParams, _URL);
		//		String lRetour_Connecxion=null;
		//		try {
		//			lRetour_Connecxion = _MyAsyncTask.execute().get();
		//		} catch (InterruptedException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		} catch (ExecutionException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		}

		try {
			JSONArray lArray_Etudiant=new JSONArray(lRetour_Connecxion);
			for (int i = 0; i < lArray_Etudiant.length(); i++)
			{
				JSONObject lObjc_P=lArray_Etudiant.getJSONObject(i);
				// Condidater
				String confirmation_Condidater=lObjc_P.getString(DataBase_Static.CONDIDATER_COL_CONFIRMATION);
				String date_Condidater=lObjc_P.getString(DataBase_Static.CONDIDATER_COL_DATE);
				int Id_etudiant_Condidater=lObjc_P.getInt(DataBase_Static.CONDIDATER_COL_IdETUDIANT);
				int Id_offreStage_Condidater=lObjc_P.getInt(DataBase_Static.CONDIDATER_COL_IdOFFRESTAGE);			
				Condidater _Condidater = new Condidater(confirmation_Condidater, date_Condidater, Id_etudiant_Condidater, Id_offreStage_Condidater);
				// Etudiant
				int ID_Etudiant=lObjc_P.getInt(DataBase_Static.ETUDIANT_COL_ID);
				String numInscrit_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_NUmINSCRIT);
				String cin_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_CIN);
				String nom_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_NOM);
				String prenom_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_PRENOM);
				String sexe_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_SEXE);
				String dateNaiss_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_DATENAISS);
				String lieuNaiss_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_LIEUNAISS);
				String numTel_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_NUmTEL);
				String email_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_EMAIL);
				String adresse_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_ADRESSE);
				String codePostale_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_CODePOSTAL);
				String image_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_IMAGE);
				int cle_departement_Etudiant=lObjc_P.getInt(DataBase_Static.ETUDIANT_COL_IdDEPARTEMENT);
				int cle_filiere_Etudiant=lObjc_P.getInt(DataBase_Static.ETUDIANT_COL_IdFILIERE);
				Integer cle_travailPfe_Etudiant=Verifier_Integer(lObjc_P.getString(DataBase_Static.ETUDIANT_COL_IdTRAVAILPFE));
				Integer cle_etudiant1_Etudiant=null;
				boolean cle_etudiant1_Test=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_IdETUDIANT1).equals("null");
				if (cle_etudiant1_Test==false)
				{
					cle_etudiant1_Etudiant=lObjc_P.getInt(DataBase_Static.ETUDIANT_COL_IdETUDIANT1);
					Synchroniser_Serveur_Binome(String.valueOf(cle_etudiant1_Etudiant), pContext.getResources().getString(R.string.EntrepriseActivity_import_CondidaterBinome));
				}
				Integer cle_document_Etudiant=Verifier_Integer(lObjc_P.getString(DataBase_Static.ETUDIANT_COL_IdDOCUMENT));

				Etudiant _Classe_Etudiant=new Etudiant(ID_Etudiant, numInscrit_Etudiant, cin_Etudiant, nom_Etudiant, prenom_Etudiant, sexe_Etudiant, dateNaiss_Etudiant, lieuNaiss_Etudiant, numTel_Etudiant, email_Etudiant, adresse_Etudiant, codePostale_Etudiant, image_Etudiant, cle_departement_Etudiant, cle_travailPfe_Etudiant, cle_etudiant1_Etudiant, cle_filiere_Etudiant, cle_document_Etudiant);
				InsertInTo_BD_Etudiant(_Classe_Etudiant);
				x=InsertInTo_BD_Condidater(_Condidater);
				if (x==-1)
				{
					Toast.makeText( pContext , "ERREUR Lors de l'Insertion de Donnees " ,Toast.LENGTH_LONG).show();
					return false;
				}
			}
		} 
		catch(JSONException e)
		{
			Toast.makeText( pContext , " Aucune Demande De Stage  " ,Toast.LENGTH_LONG).show();
			return false;
		}
		return true;
	}

	public boolean Synchroniser_Serveur_OffreStageEntreprise(String _ID_Departement,String _URL)
	{
		int x=-1;
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();
		NameValuePair lParamFiliere=new BasicNameValuePair("PARAM_ID_DEPARTEMENT",_ID_Departement);
		lListofParams.add(lParamFiliere);
		_ClientHTTP=new ClientHTTP(pContext);
		String lRetour_Connecxion=_ClientHTTP.LireEcrireUrl(_URL, lListofParams);

		//		MyAsyncTask_LireServeur _MyAsyncTask =new MyAsyncTask_LireServeur(pContext, lListofParams, _URL);
		//		String lRetour_Connecxion=null;
		//		try {
		//			lRetour_Connecxion = _MyAsyncTask.execute().get();
		//		} catch (InterruptedException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		} catch (ExecutionException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		}


		try {
			JSONArray lArray_Etudiant=new JSONArray(lRetour_Connecxion);
			for (int i = 0; i < lArray_Etudiant.length(); i++)
			{
				JSONObject lObjc_P=lArray_Etudiant.getJSONObject(i);
				// Entreprise
				int lID_Entreprise=lObjc_P.getInt(DataBase_Static.ENTREPRISE_COL_ID);
				String libelle_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_LIBELLE);
				String raison_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_RAISON);
				String numTel_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_NUmTEL);
				String email_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_EMAIL);
				String siteWeb_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_SITEWEB);
				String login_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_LOGIN);
				String password_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_PASSWORD);
				String logo_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_LOGO);
				Integer Id_enseignant_Entreprise=Verifier_Integer(lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_IdENSEIGNANT));
				Entreprise _Entreprise =new Entreprise(lID_Entreprise, libelle_Entreprise, raison_Entreprise, numTel_Entreprise, email_Entreprise, siteWeb_Entreprise, login_Entreprise, password_Entreprise, logo_Entreprise,Id_enseignant_Entreprise);
				InsertInTo_BD_Entreprise(_Entreprise);
				// OffreStage
				int lID_OffreStage=lObjc_P.getInt(DataBase_Static.OFFRESTAGE_COL_ID);
				String nom_OffreStage=lObjc_P.getString(DataBase_Static.OFFRESTAGE_COL_NOM);
				String description_OffreStage=lObjc_P.getString(DataBase_Static.OFFRESTAGE_COL_DESCRIPTION);
				String confirmation_OffreStage=lObjc_P.getString(DataBase_Static.OFFRESTAGE_COL_CONFIRMATION);
				String date_OffreStage=lObjc_P.getString(DataBase_Static.OFFRESTAGE_COL_DATE);
				int Id_entreprise_OffreStage=lObjc_P.getInt(DataBase_Static.OFFRESTAGE_COL_IdENTREPRISE);
				Integer Id_stage_OffreStage=Verifier_Integer(lObjc_P.getString(DataBase_Static.OFFRESTAGE_COL_IdSTAGE));
				Integer Id_travailPfe_OffreStage=Verifier_Integer(lObjc_P.getString(DataBase_Static.OFFRESTAGE_COL_IdTRAVAILPFE));
				int  Id_filiere_OffreStage=lObjc_P.getInt(DataBase_Static.OFFRESTAGE_COL_IdFILIERE);
				OffreStage _Classe_OffreStage =new OffreStage(lID_OffreStage, nom_OffreStage, description_OffreStage, confirmation_OffreStage, date_OffreStage, Id_entreprise_OffreStage, Id_stage_OffreStage, Id_travailPfe_OffreStage, Id_filiere_OffreStage);
				x=InsertInTo_BD_OffreStage(_Classe_OffreStage);
				if (x==-1)
				{
					Toast.makeText( pContext , "ERREUR Lors de l'Insertion de Donnees " ,Toast.LENGTH_LONG).show();
					return false;
				}
			}
		} 
		catch(JSONException e)
		{
			//Toast.makeText( pContext , " ERREUR Login ou Password non valide  " ,Toast.LENGTH_LONG).show();
			return false;
		}
		return true;
	}
	public boolean Synchroniser_Serveur_EnseignantDepartementFiliereNiveauCycle(String login,String password,String grade,String _URL)
	{
		int x=-1;
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();
		NameValuePair lParamGrade=new BasicNameValuePair("PARAM_FACTaDMIN",grade);
		NameValuePair lParamCin=new BasicNameValuePair("PARAM_CIN",password);
		NameValuePair lParamNom=new BasicNameValuePair("PARAM_MATRICULE",login);
		lListofParams.add(lParamGrade);
		lListofParams.add(lParamCin);
		lListofParams.add(lParamNom);
		_ClientHTTP=new ClientHTTP(pContext);
		String lRetour_Connecxion=_ClientHTTP.LireEcrireUrl(_URL, lListofParams);

		//		MyAsyncTask_LireServeur _MyAsyncTask =new MyAsyncTask_LireServeur(pContext, lListofParams, _URL);
		//		String lRetour_Connecxion=null;
		//		try {
		//			lRetour_Connecxion = _MyAsyncTask.execute().get();
		//		} catch (InterruptedException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		} catch (ExecutionException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		}


		try {
			JSONArray lArray_Etudiant=new JSONArray(lRetour_Connecxion);
			for (int i = 0; i < lArray_Etudiant.length(); i++)
			{
				JSONObject lObjc_P=lArray_Etudiant.getJSONObject(i);
				int _ID_Enseignant=lObjc_P.getInt(DataBase_Static.ENSEIGNANT_COL_ID);
				String Cin_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_NCIN);
				String Nom_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_NOM);
				String Prenom_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_PRENOM);
				String Sexe_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_SEXE);
				String DateNaiss_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_DATENAISS);
				String LieuNaiss_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_LIEUNAISS);
				String NumTel_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_NUmTEL);
				String Email_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_EMAIL);
				String adresse_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_ADRESSE);
				String Ville_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_VILLE);
				String CodePost_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_CODEPOSTALE);
				String Grade_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_GRADE);
				String Diplome_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_DIPLOME);
				String LieuDip_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_LIEUDIPLOME);
				String Situation_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_SITUATIONCIVILE);
				String Titulaire_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_TITULAIRE);
				String FactAdmin_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_FACtADMIN);
				String Encadreument_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_ENCADREMENT);
				String AnneeDip_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_ANNEEDIPLOME);
				String EnDetachement_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_EnDETACHEMENT);
				int Id_departement_Enseignant=lObjc_P.getInt(DataBase_Static.ENSEIGNANT_COL_IdDepartement);
				String Matricule_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_MATRICULE);
				String Specialite_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_SPECIALITE);
				Enseignant _Classe_Enseignant=new Enseignant(_ID_Enseignant, Cin_Enseignant, Nom_Enseignant, Prenom_Enseignant, Sexe_Enseignant, DateNaiss_Enseignant, LieuNaiss_Enseignant, NumTel_Enseignant, Email_Enseignant, adresse_Enseignant, Ville_Enseignant, CodePost_Enseignant, Grade_Enseignant, Diplome_Enseignant, LieuDip_Enseignant, Situation_Enseignant, Titulaire_Enseignant, FactAdmin_Enseignant, Encadreument_Enseignant, AnneeDip_Enseignant, EnDetachement_Enseignant, Matricule_Enseignant, Specialite_Enseignant, Id_departement_Enseignant);
				///// EPARTEMENT
				int ID_Departement=lObjc_P.getInt(DataBase_Static.DEPARTEMENT_COL_ID);
				String libelle_Departement=lObjc_P.getString(DataBase_Static.DEPARTEMENT_COL_LIBELLE);
				String abreviation_Departement=lObjc_P.getString(DataBase_Static.DEPARTEMENT_COL_ABREVIATION);
				String ordre_Departement=lObjc_P.getString(DataBase_Static.DEPARTEMENT_COL_ORDRE);
				Departement _Classe_Departement = new Departement(ID_Departement, libelle_Departement, abreviation_Departement, ordre_Departement);
				///// NIVEAU
				int ID_Niveau=lObjc_P.getInt(DataBase_Static.NIVEAU_COL_ID);
				String num_Niveau=lObjc_P.getString(DataBase_Static.NIVEAU_COL_NUM);
				String designation_Niveau=lObjc_P.getString(DataBase_Static.NIVEAU_COL_DESIGNATION);
				Integer cle_filiere_Niveau=lObjc_P.getInt(DataBase_Static.NIVEAU_COL_IdFiliere);
				Niveau _Classe_Niveau=new Niveau(ID_Niveau, num_Niveau, designation_Niveau, null, null, cle_filiere_Niveau);
				///// CYCLE
				int ID_Cycle=lObjc_P.getInt(DataBase_Static.CYCLE_COL_ID);
				String num_Cycle=lObjc_P.getString(DataBase_Static.CYCLE_COL_NUM);
				String designation_Cycle=lObjc_P.getString(DataBase_Static.CYCLE_COL_DESIGNATION);
				Cycle _Classe_Cycle= new Cycle(ID_Cycle, num_Cycle, designation_Cycle, null, null);
				///// FILIERE
				int ID_Filiere=lObjc_P.getInt(DataBase_Static.FILIERE_COL_ID);
				String designation_Filiere=lObjc_P.getString(DataBase_Static.FILIERE_COL_DESIGNATION);
				Integer cle_cycle_Filiere=lObjc_P.getInt(DataBase_Static.FILIERE_COL_IdCYCLE);
				Filiere _Classe_Filiere = new Filiere(ID_Filiere, designation_Filiere, cle_cycle_Filiere);
				// un seul enseignant ,departement mais plusieur filiere 
				InsertInTo_BD_Enseignant(_Classe_Enseignant);
				InsertInTo_BD_Departement(_Classe_Departement);
				InsertInTo_BD_Niveau(_Classe_Niveau);
				InsertInTo_BD_Cycle(_Classe_Cycle);		
				x=InsertInTo_BD_Filiere(_Classe_Filiere);
				if (x==-1)
				{
					Toast.makeText( pContext , "ERREUR Lors de l'Insertion de Donnees " ,Toast.LENGTH_LONG).show();
					return false;
				}
			}
		} 
		catch(JSONException e)
		{
			Toast.makeText( pContext , " ERREUR Login ou Password non valide " ,Toast.LENGTH_LONG).show();
			return false;
		}
		return true;
	}
	//////////////////// INSERT INTO BD :

	public int InsertInTo_BD_Soutenance(Soutenance _Soutenance )
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.SOUTENANCE_COL_ID,_Soutenance.get_ID_Soutenance());
		lContentValues.put(DataBase_Static.SOUTENANCE_COL_TYPE,_Soutenance.get_Type_Soutenance());
		lContentValues.put(DataBase_Static.SOUTENANCE_COL_HEUReDEB,_Soutenance.get_HeureDeb_Soutenance());
		lContentValues.put(DataBase_Static.SOUTENANCE_COL_HEUReFIN,_Soutenance.get_HeureFin_Soutenance());
		lContentValues.put(DataBase_Static.SOUTENANCE_COL_JOUR,_Soutenance.get_Date_Soutenance());
		lContentValues.put(DataBase_Static.SOUTENANCE_COL_IdSALLE,_Soutenance.get_IdSalle_Soutenance());
		lContentValues.put(DataBase_Static.SOUTENANCE_COL_IdTRAVAILPFE,_Soutenance.get_IdTravailPfe_Soutenance());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_SOUTENANCE, null, lContentValues);
		return (int) x;
	}

	public int InsertInTo_BD_Deriger(Deriger _Deriger )
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.DERIGER_COL_TYPE,_Deriger.getType_deriger());
		lContentValues.put(DataBase_Static.DERIGER_COL_IdENSEIGNANT,_Deriger.getIdEnseignant_deriger());
		lContentValues.put(DataBase_Static.DERIGER_COL_IdSOUTENANCE,_Deriger.getIdSoutenance_deriger());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_DERIGER, null, lContentValues);
		return (int) x;
	}

	public int InsertInTo_BD_Salle(Salle _Salle )
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.SALLE_COL_ID,_Salle.getId_Salle());
		lContentValues.put(DataBase_Static.SALLE_COL_BLOC,_Salle.getBloc_Salle());
		lContentValues.put(DataBase_Static.SALLE_COL_ETAGE,_Salle.getEtage_Salle());
		lContentValues.put(DataBase_Static.SALLE_COL_ABREVIATION,_Salle.getAbreviation_Salle());
		lContentValues.put(DataBase_Static.SALLE_COL_TYPE,_Salle.getType_Salle());
		lContentValues.put(DataBase_Static.SALLE_COL_CAPACITE,_Salle.getCapacite_Salle());
		lContentValues.put(DataBase_Static.SALLE_COL_ETAT,_Salle.getEtat_Salle());
		lContentValues.put(DataBase_Static.SALLE_COL_VIDEOPRO,_Salle.getVideoProject_Salle());
		lContentValues.put(DataBase_Static.SALLE_COL_INFORMATIQUE,_Salle.getInformatique_Salle());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_SALLE, null, lContentValues);
		return (int) x;
	}


	public int InsertInTo_BD_EncadreurPro(EncadreurPro _EncadreurPro )
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.ENCADREURPRO_COL_ID,_EncadreurPro.getId_encadreurPro());
		lContentValues.put(DataBase_Static.ENCADREURPRO_COL_NCIN,_EncadreurPro.getCin_encadreurPro());
		lContentValues.put(DataBase_Static.ENCADREURPRO_COL_NOM,_EncadreurPro.getNom_encadreurPro());
		lContentValues.put(DataBase_Static.ENCADREURPRO_COL_PRENOM,_EncadreurPro.getPrenom_encadreurPro());
		lContentValues.put(DataBase_Static.ENCADREURPRO_COL_NUmTEL,_EncadreurPro.getNumTel_encadreurPro());
		lContentValues.put(DataBase_Static.ENCADREURPRO_COL_EMAIL,_EncadreurPro.getEmail_encadreurPro());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_ENCADREUR_PRO, null, lContentValues);
		return (int) x;
	}

	public int InsertInTo_BD_Demande(Demande _Demande)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.DEMANDE_COL_CONFIRMATION,_Demande.getConfirmaiton_demande());
		lContentValues.put(DataBase_Static.DEMANDE_COL_DATE,_Demande.getDate_demande());
		lContentValues.put(DataBase_Static.DEMANDE_COL_IdENSEIGNANT,_Demande.getId_enseignant_demande());
		lContentValues.put(DataBase_Static.DEMANDE_COL_IdETUDIANT,_Demande.getId_etudiant_demande());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_DEMANDE, null, lContentValues);
		return (int) x;
	}
	public int InsertInTo_BD_Stage(Stage _Stage)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.STAGE_COL_ID,_Stage.getId_stage());
		lContentValues.put(DataBase_Static.STAGE_COL_NOM,_Stage.getNom_stage());
		lContentValues.put(DataBase_Static.STAGE_COL_DATE,_Stage.getDate_stage());
		lContentValues.put(DataBase_Static.STAGE_COL_IdTRAVAILPFE,_Stage.getCle_travailPfe_stage());
		lContentValues.put(DataBase_Static.STAGE_COL_IdENCADREUrPRO,_Stage.getCle_encadreurPro_stage());
		lContentValues.put(DataBase_Static.STAGE_COL_IdOFFRESTAGE,_Stage.getCle_offreStage_stage());
		lContentValues.put(DataBase_Static.STAGE_COL_DESCRIPTION,_Stage.getDescription_stage());
		lContentValues.put(DataBase_Static.STAGE_COL_IdENTREPRISE,_Stage.getCle_entreprise_stage());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_STAGE, null, lContentValues);
		return (int) x;
	}
	public int InsertInTo_BD_TravailPfe(Travail_PFE _Travail_PFE)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.TRAVAILPFE_COL_ID,_Travail_PFE.getID_TravailPFE());
		lContentValues.put(DataBase_Static.TRAVAILPFE_COL_LIBELLE,_Travail_PFE.getLibelle_TravailPFE());
		lContentValues.put(DataBase_Static.TRAVAILPFE_COL_IdENSEIGNANT,_Travail_PFE.getCle_enseignant_TravailPFE());
		lContentValues.put(DataBase_Static.TRAVAILPFE_COL_IdSOUTENANCE,_Travail_PFE.getCle_soutenance_TravailPFE());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_TRAVAILPFE, null, lContentValues);
		return (int) x;
	}
	public int InsertInTo_BD_Condidater(Condidater _Condidater)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.CONDIDATER_COL_CONFIRMATION,_Condidater.getConfirmaiton_condidater());
		lContentValues.put(DataBase_Static.CONDIDATER_COL_DATE,_Condidater.getDate_condidater());
		lContentValues.put(DataBase_Static.CONDIDATER_COL_IdETUDIANT,_Condidater.getId_etudiant_condidater());
		lContentValues.put(DataBase_Static.CONDIDATER_COL_IdOFFRESTAGE,_Condidater.getId_offreStage_condidater());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_CONDIDATER, null, lContentValues);
		return (int) x;
	}
	public int InsertInTo_BD_Etudiant(Etudiant _Class_Etudiant)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.ETUDIANT_COL_ID,_Class_Etudiant.getId_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_NUmINSCRIT,_Class_Etudiant.getNum_inscrit());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_CIN,_Class_Etudiant.getCin_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_NOM,_Class_Etudiant.getNom_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_PRENOM,_Class_Etudiant.getPrenom_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_SEXE,_Class_Etudiant.getSexe_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_DATENAISS,_Class_Etudiant.getDateNaiss_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_LIEUNAISS,_Class_Etudiant.getLieuNaiss_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_NUmTEL,_Class_Etudiant.getNumTel_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_EMAIL,_Class_Etudiant.getEmail_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_ADRESSE,_Class_Etudiant.getAdresse_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_CODePOSTAL,_Class_Etudiant.getCodePostal_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_IMAGE,_Class_Etudiant.getImage_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_IdDEPARTEMENT,_Class_Etudiant.getCle_departement_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_IdTRAVAILPFE,_Class_Etudiant.getCle_travailPfe_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_IdETUDIANT1,_Class_Etudiant.getCle_etudiant1_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_IdFILIERE,_Class_Etudiant.getCle_filiere_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_IdDOCUMENT,_Class_Etudiant.getCle_document_etudiant());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_ETUDIANT, null, lContentValues);
		return (int) x;
	}

	public int InsertInTo_BD_Entreprise(Entreprise _Entreprise)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.ENTREPRISE_COL_ID,_Entreprise.getId_entreprise());
		lContentValues.put(DataBase_Static.ENTREPRISE_COL_LIBELLE,_Entreprise.getLibelle_entreprise());
		lContentValues.put(DataBase_Static.ENTREPRISE_COL_RAISON,_Entreprise.getRaison_entreprise());
		lContentValues.put(DataBase_Static.ENTREPRISE_COL_NUmTEL,_Entreprise.getNumTel_entreprise());
		lContentValues.put(DataBase_Static.ENTREPRISE_COL_EMAIL,_Entreprise.getEmail_entreprise());
		lContentValues.put(DataBase_Static.ENTREPRISE_COL_SITEWEB,_Entreprise.getSiteWeb_entreprise());
		lContentValues.put(DataBase_Static.ENTREPRISE_COL_LOGIN,_Entreprise.getLogin_entreprise());
		lContentValues.put(DataBase_Static.ENTREPRISE_COL_PASSWORD,_Entreprise.getPassword_entreprise());
		lContentValues.put(DataBase_Static.ENTREPRISE_COL_LOGO,_Entreprise.getLogo_entreprise());
		lContentValues.put(DataBase_Static.ENTREPRISE_COL_IdENSEIGNANT,_Entreprise.getCle_enseignant_entreprise());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_ENTREPRISE, null, lContentValues);
		return (int) x;
	}
	public int InsertInTo_BD_OffreStage(OffreStage _OffreStage)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.OFFRESTAGE_COL_ID,_OffreStage.getId_offreStage());
		lContentValues.put(DataBase_Static.OFFRESTAGE_COL_NOM,_OffreStage.getNom_offreStage());
		lContentValues.put(DataBase_Static.OFFRESTAGE_COL_DESCRIPTION,_OffreStage.getDescription_offreStage());
		lContentValues.put(DataBase_Static.OFFRESTAGE_COL_CONFIRMATION,_OffreStage.getConfirmation_offreStage());
		lContentValues.put(DataBase_Static.OFFRESTAGE_COL_DATE,_OffreStage.getDate_offreStage());
		lContentValues.put(DataBase_Static.OFFRESTAGE_COL_IdENTREPRISE,_OffreStage.getCle_entreprise_offreStage());
		lContentValues.put(DataBase_Static.OFFRESTAGE_COL_IdSTAGE,_OffreStage.getCle_stage_offreStage());
		lContentValues.put(DataBase_Static.OFFRESTAGE_COL_IdTRAVAILPFE,_OffreStage.getCle_travailPfe_offreStage());
		lContentValues.put(DataBase_Static.OFFRESTAGE_COL_IdFILIERE,_OffreStage.getCle_filiere_offreStage());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_OFFRESTAGE, null, lContentValues);
		return (int) x;
	}
	public int InsertInTo_BD_Enseignant(Enseignant _Class_Enseignant)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_ID,_Class_Enseignant.getId_enseignant());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_NCIN,_Class_Enseignant.getCin_enseignant());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_NOM,_Class_Enseignant.getNom_enseignant());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_PRENOM,_Class_Enseignant.getPrenom_enseignant());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_SEXE,_Class_Enseignant.getSexe_enseignant());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_DATENAISS,_Class_Enseignant.getDateNaiss_enseignant());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_LIEUNAISS,_Class_Enseignant.getLieuNaiss_enseignant());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_NUmTEL,_Class_Enseignant.getNumTel_enseignant());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_EMAIL,_Class_Enseignant.getEmail_enseignant());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_ADRESSE,_Class_Enseignant.getAdresse_enseignant());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_VILLE,_Class_Enseignant.getVille());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_CODEPOSTALE,_Class_Enseignant.getCodePostale());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_GRADE,_Class_Enseignant.getGrade());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_DIPLOME,_Class_Enseignant.getDiplome());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_LIEUDIPLOME,_Class_Enseignant.getLieu_diplome());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_SITUATIONCIVILE,_Class_Enseignant.getSituation_civil());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_TITULAIRE,_Class_Enseignant.getTitulaire());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_FACtADMIN,_Class_Enseignant.getFactAdmin());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_ENCADREMENT,_Class_Enseignant.getEncadrement());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_ANNEEDIPLOME,_Class_Enseignant.getAnnee_diplome());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_EnDETACHEMENT,_Class_Enseignant.getEnDetachement());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_IdDepartement,_Class_Enseignant.getId_departement());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_MATRICULE,_Class_Enseignant.getMatricule());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_SPECIALITE,_Class_Enseignant.getSpecialite());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_ENSEIGNANT, null, lContentValues);
		return (int) x;
	}
	public int  InsertInTo_BD_Niveau(Niveau _Class_Niveau)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.NIVEAU_COL_ID,_Class_Niveau.getId_niveau());
		lContentValues.put(DataBase_Static.NIVEAU_COL_NUM,_Class_Niveau.getNum_niveau());
		lContentValues.put(DataBase_Static.NIVEAU_COL_DESIGNATION,_Class_Niveau.getDesignation_niveau());
		lContentValues.put(DataBase_Static.NIVEAU_COL_NbMODULE,_Class_Niveau.getNbModule_niveau());
		lContentValues.put(DataBase_Static.NIVEAU_COL_NbSECTION,_Class_Niveau.getNbSection_niveau());
		lContentValues.put(DataBase_Static.NIVEAU_COL_IdFiliere,_Class_Niveau.getCle_filiere_niveau());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_NIVEAU, null, lContentValues);
		return (int) x;
	}
	public int InsertInTo_BD_Cycle(Cycle _Class_Cycle)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.CYCLE_COL_ID, _Class_Cycle.getId_cycle());
		lContentValues.put(DataBase_Static.CYCLE_COL_NUM,_Class_Cycle.getNum_cycle());
		lContentValues.put(DataBase_Static.CYCLE_COL_DESIGNATION,_Class_Cycle.getDesignation_cycle());
		lContentValues.put(DataBase_Static.CYCLE_COL_NBrSEM,_Class_Cycle.getNbrSem_cycle());
		lContentValues.put(DataBase_Static.CYCLE_COL_NBrFILIERE,_Class_Cycle.getNbrFiliere_cycle());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_CYCLE, null, lContentValues);
		return (int) x;
	}
	public int InsertInTo_BD_Departement(Departement _Class_Departement)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.DEPARTEMENT_COL_ID,_Class_Departement.getId_departement());
		lContentValues.put(DataBase_Static.DEPARTEMENT_COL_LIBELLE,_Class_Departement.getLibelle_Departement());
		lContentValues.put(DataBase_Static.DEPARTEMENT_COL_ABREVIATION,_Class_Departement.getAbreviation_Departement());
		lContentValues.put(DataBase_Static.DEPARTEMENT_COL_ORDRE,_Class_Departement.getOrdre_Departement());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_DEPARTEMENT, null, lContentValues);
		return (int) x;
	}
	public int InsertInTo_BD_Filiere(Filiere _Class_Filiere)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.FILIERE_COL_ID, _Class_Filiere.getId_filiere());
		lContentValues.put(DataBase_Static.FILIERE_COL_DESIGNATION,_Class_Filiere.getDesignation_filiere());
		lContentValues.put(DataBase_Static.FILIERE_COL_IdCYCLE,_Class_Filiere.getCle_cycle_filiere());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_FILIERE, null, lContentValues);
		return (int) x;
	}
	//////////////////// SUPPRIMER CONTENUE TABLE :
	public void clearAll_Soutenance()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_SOUTENANCE);
	}public void clearAll_Deriger()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_DERIGER);
	}
	public void clearAll_Salle()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_SALLE);
	}
	public void clearAll_EncadreurPro()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_ENCADREUR_PRO);
	}
	public void clearAll_Demande()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_DEMANDE);
	}
	public void clearAll_Stage()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_STAGE);
	}
	public void clearAll_TravailPfe()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_TRAVAILPFE);
	}
	public void  ClearAll_Condidater()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_CONDIDATER);
	}
	public void  ClearAll_Etudiant()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_ETUDIANT);
	}
	public void ClearAll_OffreStage()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_OFFRESTAGE);
	}
	public void ClearAll_Entreprise()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_ENTREPRISE);
	}
	public void ClearAll_Enseignant()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_ENSEIGNANT);
	}
	public void ClearAll_Departement()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_DEPARTEMENT);
	}
	public void ClearAll_Filiere()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_FILIERE);
	}
	public void ClearAll_Niveau()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_NIVEAU);
	}
	public void ClearAll_Cycle()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_CYCLE);
	}
	///////////////////Insertion INTO SERVEUR :
	public boolean InsertInTo_Serveur_Deriger(String _type_jury,String _Id_Enseignant,String _Id_Soutenance,String _URL)
	{                     
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();

		NameValuePair lParamTupeJury=new BasicNameValuePair("PARAM_TYPE_JURY",_type_jury);
		NameValuePair lParamIdEnseignant=new BasicNameValuePair("PARAM_ID_ENSEIGNANT",_Id_Enseignant);
		NameValuePair lParamIdSoutenance=new BasicNameValuePair("PARAM_ID_SOUTENANCE",_Id_Soutenance);

		lListofParams.add(lParamTupeJury);
		lListofParams.add(lParamIdEnseignant);
		lListofParams.add(lParamIdSoutenance);

		_ClientHTTP=new ClientHTTP(pContext);
		boolean lResult=_ClientHTTP.SendToUrl(_URL,lListofParams);
		if(lResult)
		{
			Toast.makeText(pContext, " Insérer avec succès ", Toast.LENGTH_LONG).show();
			return true;
		}else
		{
			Toast.makeText(pContext, " échec lors de l'Insertion ", Toast.LENGTH_SHORT).show();
			return false;
		}
	}

	public String InsertInTo_Serveur_Soutenance(String _heureDeb,String _heureFin,String _date,String _Id_salle,String _Id_travailPfe,String _URL )
	{              
		String _ID_Soutenance="";
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();

		NameValuePair lParamHeureDeb=new BasicNameValuePair("PARAM_HEUReDEB",_heureDeb);
		NameValuePair lParamHeureFin=new BasicNameValuePair("PARAM_HEUReFIN",_heureFin);
		NameValuePair lParamDate=new BasicNameValuePair("PARAM_DATE",_date);
		NameValuePair lParamIdSalle=new BasicNameValuePair("PARAM_ID_SALLE",_Id_salle);
		NameValuePair lParamIdTravail=new BasicNameValuePair("PARAM_ID_TRAVAILPFE",_Id_travailPfe); 

		lListofParams.add(lParamHeureDeb);
		lListofParams.add(lParamHeureFin);
		lListofParams.add(lParamDate);
		lListofParams.add(lParamIdSalle);
		lListofParams.add(lParamIdTravail);

		_ClientHTTP=new ClientHTTP(pContext);
		String lRetour_Connecxion=_ClientHTTP.LireEcrireUrl(_URL, lListofParams);
		try {
			JSONArray lArray_Salle=new JSONArray(lRetour_Connecxion);
			for (int i = 0; i < lArray_Salle.length(); i++)
			{
				JSONObject lObjc_P=lArray_Salle.getJSONObject(i);
				_ID_Soutenance=lObjc_P.getString(DataBase_Static.SOUTENANCE_COL_ID);
				Log.d("_ID_Soutenance", _ID_Soutenance);
			}
		} 
		catch(JSONException e)
		{
			return _ID_Soutenance;
		}
		return _ID_Soutenance;
	}

	public boolean InsertInTo_Serveur_EncadreurPro(String _cin,String _nom,String _prenom,String _numTel,String _email,String _ID_Stage,String _URL )
	{                     
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();

		NameValuePair lParamCin=new BasicNameValuePair("PARAM_CIN",_cin);
		NameValuePair lParamNom=new BasicNameValuePair("PARAM_NOM",_nom);
		NameValuePair lParamPrenom=new BasicNameValuePair("PARAM_PRENOM",_prenom);
		NameValuePair lParamNumTel=new BasicNameValuePair("PARAM_NUMTEL",_numTel);
		NameValuePair lParamEmail=new BasicNameValuePair("PARAM_EMAIL",_email); 
		NameValuePair lParamIdStage=new BasicNameValuePair("PARAM_ID_STAGE",_ID_Stage);

		lListofParams.add(lParamCin);
		lListofParams.add(lParamNom);
		lListofParams.add(lParamPrenom);
		lListofParams.add(lParamNumTel);
		lListofParams.add(lParamEmail);
		lListofParams.add(lParamIdStage);

		_ClientHTTP=new ClientHTTP(pContext);
		boolean lResult=_ClientHTTP.SendToUrl(_URL,lListofParams);
		//		MyAsyncTask_EcrireServeur _MyAsyncTask_EcrireServeur =new MyAsyncTask_EcrireServeur(pContext, lListofParams, _URL);
		//		boolean lResult=false;
		//		try {
		//			lResult = _MyAsyncTask_EcrireServeur.execute().get();
		//		} catch (InterruptedException e) {
		//			// TODO Auto-generated catch block
		//			e.printStackTrace();
		//		} catch (ExecutionException e) {
		//			// TODO Auto-generated catch block
		//			e.printStackTrace();
		//		}

		if(lResult)
		{
			Toast.makeText(pContext, " Insérer avec succès ", Toast.LENGTH_LONG).show();
			return true;
		}else
		{
			Toast.makeText(pContext, " échec lors de l'Insertion ", Toast.LENGTH_SHORT).show();
			return false;
		}
	}
	public boolean InsertInTo_Serveur_TravailPfe(String _LibelleTravailPfe,String _URL )
	{                     
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();

		NameValuePair lParamLibelle=new BasicNameValuePair("PARAM_LIBELLeTRAVAILPFE",_LibelleTravailPfe);
		lListofParams.add(lParamLibelle);
		//_ClientHTTP=new ClientHTTP(pContext);
		//boolean lResult=_ClientHTTP.SendToUrl(_URL,lListofParams);
		MyAsyncTask_EcrireServeur _MyAsyncTask_EcrireServeur =new MyAsyncTask_EcrireServeur(pContext, lListofParams, _URL);
		boolean lResult=false;
		try {
			lResult = _MyAsyncTask_EcrireServeur.execute().get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		if(lResult)
		{
			Toast.makeText(pContext, " Insérer avec succès ", Toast.LENGTH_LONG).show();
			return true;
		}else
		{
			Toast.makeText(pContext, " échec lors de l'Insertion ", Toast.LENGTH_SHORT).show();
			return false;
		}
	}
	public boolean InsertInTo_Serveur_Stage(String _Nom_Stage,String _Date_Stage,String _Id_travailPfe_Stage,String _Id_offreStage_Stage,String _Description_Stage,String _Id_Entreprise,String _URL )
	{                     
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();

		NameValuePair lParamNom=new BasicNameValuePair("PARAM_NOM",_Nom_Stage);
		NameValuePair lParamDate=new BasicNameValuePair("PARAM_DATE",_Date_Stage);
		NameValuePair lParamIdTravail=new BasicNameValuePair("PARAM_ID_TRAVAILPFE",_Id_travailPfe_Stage);
		if (_Id_offreStage_Stage.equals("null")==false)
		{
			NameValuePair lParamIdOffreStage=new BasicNameValuePair("PARAM_ID_OFFRESTAGE",_Id_offreStage_Stage);
			lListofParams.add(lParamIdOffreStage);
		}
		NameValuePair lParamDescription=new BasicNameValuePair("PARAM_DESCRIPTION",_Description_Stage);
		NameValuePair lParamIntreprise=new BasicNameValuePair("PARAM_ID_ENTREPRISE",_Id_Entreprise);

		lListofParams.add(lParamNom);
		lListofParams.add(lParamDate); 
		lListofParams.add(lParamIdTravail);
		lListofParams.add(lParamDescription);
		lListofParams.add(lParamIntreprise);

		//_ClientHTTP=new ClientHTTP(pContext);
		//boolean lResult=_ClientHTTP.SendToUrl(_URL,lListofParams);
		MyAsyncTask_EcrireServeur _MyAsyncTask_EcrireServeur =new MyAsyncTask_EcrireServeur(pContext, lListofParams, _URL);
		boolean lResult=false;
		try {
			lResult = _MyAsyncTask_EcrireServeur.execute().get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		if(lResult)
		{
			Toast.makeText(pContext, " Insérer avec succès ", Toast.LENGTH_LONG).show();
			return true;
		}else
		{
			Toast.makeText(pContext, " échec lors de l'Insertion ", Toast.LENGTH_SHORT).show();
			return false;
		}
	}
	public boolean InsertInTo_Serveur_Entreprise(String _libelle,String _raison,String _numTel,String _email,String _siteWeb,String _login,String _password,String _Id_enseignant_Entreprise,String _URL ) ///url
	{                     
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();

		NameValuePair lParamLibelle=new BasicNameValuePair("PARAM_LIBELLE",_libelle);
		NameValuePair lParamRaison=new BasicNameValuePair("PARAM_RAISON",_raison);
		NameValuePair lParamTelephone=new BasicNameValuePair("PARAM_TELEPHONE",_numTel);
		NameValuePair lParamEmail=new BasicNameValuePair("PARAM_EMAIL",_email);
		NameValuePair lParamSiteWeb=new BasicNameValuePair("PARAM_WEB",_siteWeb);
		NameValuePair lParamLogin=new BasicNameValuePair("PARAM_LOGIN",_login);
		NameValuePair lParamPassword=new BasicNameValuePair("PARAM_PASSWORD",_password);
		NameValuePair lParamEnseignant=new BasicNameValuePair("PARAM_ID_ENSEIGNANT",_Id_enseignant_Entreprise);

		lListofParams.add(lParamLibelle);
		lListofParams.add(lParamRaison);
		lListofParams.add(lParamTelephone);
		lListofParams.add(lParamEmail);
		lListofParams.add(lParamSiteWeb);
		lListofParams.add(lParamLogin);
		lListofParams.add(lParamPassword);
		lListofParams.add(lParamEnseignant);

		//_ClientHTTP=new ClientHTTP(pContext);
		//boolean lResult=_ClientHTTP.SendToUrl(_URL,lListofParams);
		MyAsyncTask_EcrireServeur _MyAsyncTask_EcrireServeur =new MyAsyncTask_EcrireServeur(pContext, lListofParams, _URL);
		boolean lResult=false;
		try {
			lResult = _MyAsyncTask_EcrireServeur.execute().get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		if(lResult)
		{
			Toast.makeText(pContext, " Insérer avec succès ", Toast.LENGTH_LONG).show();
			return true;
		}else
		{
			Toast.makeText(pContext, " échec lors de l'initialisation ", Toast.LENGTH_SHORT).show();
			return false;
		}

	}
	///////////////////// UPDATE INTO SERVEUR :
	public boolean UpdateInTo_Serveur_EncadreurProStage_DELETEaFFECTATION(String _ID_Stage,String _URL )
	{                     
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();

		NameValuePair lParamIdStage=new BasicNameValuePair("PARAM_ID_SATAGE",_ID_Stage);

		lListofParams.add(lParamIdStage);

		_ClientHTTP=new ClientHTTP(pContext);
		boolean lResult=_ClientHTTP.SendToUrl(_URL,lListofParams);

		if(lResult)
		{
			Toast.makeText(pContext, " Modifier avec succès ", Toast.LENGTH_LONG).show();
			return true;
		}else
		{
			Toast.makeText(pContext, " échec lors de la Modification ", Toast.LENGTH_SHORT).show();
			return false;
		}
	}

	public boolean UpdateInTo_Serveur_EncadreurProStage_ADDaFFECTATION(String _cin,String _ID_Stage,String _URL )
	{                     
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();

		NameValuePair lParamCin=new BasicNameValuePair("PARAM_CIN",_cin);
		NameValuePair lParamIdStage=new BasicNameValuePair("PARAM_ID_STAGE",_ID_Stage);

		lListofParams.add(lParamCin);
		lListofParams.add(lParamIdStage);

		_ClientHTTP=new ClientHTTP(pContext);
		boolean lResult=_ClientHTTP.SendToUrl(_URL,lListofParams);

		if(lResult)
		{
			Toast.makeText(pContext, " Modifier avec succès ", Toast.LENGTH_LONG).show();
			return true;
		}else
		{
			Toast.makeText(pContext, " échec lors de la Modification ", Toast.LENGTH_SHORT).show();
			return false;
		}
	}
	public boolean UpdateInTo_Serveur_EncadreurPro(String _ID_EncadreurPro,String _cin,String _nom,String _prenom,String _numTel,String _email,String _URL) 
	{                     
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();
		NameValuePair lParamCin=new BasicNameValuePair("PARAM_CIN",_cin);
		NameValuePair lParamNom=new BasicNameValuePair("PARAM_NOM",_nom);
		NameValuePair lParamPrenom=new BasicNameValuePair("PARAM_PRENOM",_prenom);
		NameValuePair lParamNumTel=new BasicNameValuePair("PARAM_NUMTEL",_numTel);
		NameValuePair lParamEmail=new BasicNameValuePair("PARAM_EMAIL",_email); 
		NameValuePair lParamIdEncadreurPro=new BasicNameValuePair("PARAM_ID_ENCADREURPRO",_ID_EncadreurPro);

		lListofParams.add(lParamCin);
		lListofParams.add(lParamNom);
		lListofParams.add(lParamPrenom);
		lListofParams.add(lParamNumTel);
		lListofParams.add(lParamEmail);
		lListofParams.add(lParamIdEncadreurPro);

		_ClientHTTP=new ClientHTTP(pContext);
		boolean lResult=_ClientHTTP.SendToUrl(_URL,lListofParams);

		//		MyAsyncTask_EcrireServeur _MyAsyncTask_EcrireServeur =new MyAsyncTask_EcrireServeur(pContext, lListofParams, _URL);
		//		boolean lResult=false;
		//		try {
		//			lResult = _MyAsyncTask_EcrireServeur.execute().get();
		//		} catch (InterruptedException e) {
		//			// TODO Auto-generated catch block
		//			e.printStackTrace();
		//		} catch (ExecutionException e) {
		//			// TODO Auto-generated catch block
		//			e.printStackTrace();
		//		}

		if(lResult)
		{
			Toast.makeText(pContext, " Modifier avec succès ", Toast.LENGTH_LONG).show();
			return true;
		}else
		{
			Toast.makeText(pContext, " échec lors des Modifications ", Toast.LENGTH_SHORT).show();
			return false;
		}
	}

	public boolean UpdateInTo_Serveur_TravailPfe_ACADEMIQUE(String _IdEnseignant_TravailPfe,String _ID_TravailPfe,String _URL )
	{                     
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();
		if (_IdEnseignant_TravailPfe.equals("null")==false)
		{
			NameValuePair lParamIdEnseignant=new BasicNameValuePair("PARAM_ID_ENSEIGNANT",_IdEnseignant_TravailPfe);
			lListofParams.add(lParamIdEnseignant);
		}

		NameValuePair lParamIdTravailPfe=new BasicNameValuePair("PARAM_ID_TRAVAIlPFE",_ID_TravailPfe);
		lListofParams.add(lParamIdTravailPfe);

		_ClientHTTP=new ClientHTTP(pContext);
		boolean lResult=_ClientHTTP.SendToUrl(_URL,lListofParams);

		//		MyAsyncTask_EcrireServeur _MyAsyncTask_EcrireServeur =new MyAsyncTask_EcrireServeur(pContext, lListofParams, _URL);
		//		boolean lResult=false;
		//		try {
		//			lResult = _MyAsyncTask_EcrireServeur.execute().get();
		//		} catch (InterruptedException e) {
		//			// TODO Auto-generated catch block
		//			e.printStackTrace();
		//		} catch (ExecutionException e) {
		//			// TODO Auto-generated catch block
		//			e.printStackTrace();
		//		}

		if(lResult)
		{
			Toast.makeText(pContext, " Modifier avec succès ", Toast.LENGTH_LONG).show();
			return true;
		}else
		{
			Toast.makeText(pContext, " échec lors des Modifications ", Toast.LENGTH_SHORT).show();
			return false;
		}
	}

	public boolean UpdateInTo_Serveur_Demande(String _confirmation_Demande,String _IdEtudiant_Demande,String _IdEnseignant_Demande,String _URL )
	{                     
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();

		NameValuePair lParamConfirmation=new BasicNameValuePair("PARAM_CONFIRMATION",_confirmation_Demande);
		NameValuePair lParamIdEtudiant=new BasicNameValuePair("PARAM_ID_ETUDIANT",_IdEtudiant_Demande);
		NameValuePair lParamIdEnseignant=new BasicNameValuePair("PARAM_ID_ENSEIGNANT",_IdEnseignant_Demande);

		lListofParams.add(lParamConfirmation);
		lListofParams.add(lParamIdEtudiant);
		lListofParams.add(lParamIdEnseignant);

		//_ClientHTTP=new ClientHTTP(pContext);
		//boolean lResult=_ClientHTTP.SendToUrl(_URL,lListofParams);

		MyAsyncTask_EcrireServeur _MyAsyncTask_EcrireServeur =new MyAsyncTask_EcrireServeur(pContext, lListofParams, _URL);
		boolean lResult=false;
		try {
			lResult = _MyAsyncTask_EcrireServeur.execute().get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if(lResult)
		{
			Toast.makeText(pContext, " Modifier avec succès ", Toast.LENGTH_LONG).show();
			return true;
		}else
		{
			Toast.makeText(pContext, " échec lors des Modifications ", Toast.LENGTH_SHORT).show();
			return false;
		}
	}

	public boolean UpdateInTo_Serveur_TravailPfeStageEtudiant(String _ID_Stage,String _nom_Stage,String _date_Stage,String _description_Stage,String _ID_Etudiant_AV,String _ID_Etudiant_AP,String _ID_Binome_AV,String _ID_Binome_AP,String _ID_TravailPfe,String _URL )
	{                     
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();

		NameValuePair lParamIDStage=new BasicNameValuePair("PARAM_ID_STAGE",_ID_Stage);
		NameValuePair lParamNom=new BasicNameValuePair("PARAM_NOM_STAGE",_nom_Stage);
		NameValuePair lParamDate=new BasicNameValuePair("PARAM_DATE_STAGE",_date_Stage);
		NameValuePair lParamDescription=new BasicNameValuePair("PARAM_DESCRIPTION_STAGE",_description_Stage);

		NameValuePair lParamIDEtudiantAV=new BasicNameValuePair("PARAM_ID_ETUDIANT_AV",_ID_Etudiant_AV);
		NameValuePair lParamIDEtudiantAP=new BasicNameValuePair("PARAM_ID_ETUDIANT_AP",_ID_Etudiant_AP);
		NameValuePair lParamIDBinomeAV=new BasicNameValuePair("PARAM_ID_BINOME_AV",_ID_Binome_AV);
		NameValuePair lParamIDBinomeAP=new BasicNameValuePair("PARAM_ID_BINOME_AP",_ID_Binome_AP);
		NameValuePair lParamIDTravail=new BasicNameValuePair("PARAM_ID_TRAVAIPFE",_ID_TravailPfe);

		lListofParams.add(lParamIDStage);
		lListofParams.add(lParamNom);
		lListofParams.add(lParamDate);
		lListofParams.add(lParamDescription);

		lListofParams.add(lParamIDEtudiantAV);
		lListofParams.add(lParamIDEtudiantAP);
		lListofParams.add(lParamIDBinomeAV);
		lListofParams.add(lParamIDBinomeAP);
		lListofParams.add(lParamIDTravail);

		//_ClientHTTP=new ClientHTTP(pContext);
		//boolean lResult=_ClientHTTP.SendToUrl(_URL,lListofParams);
		MyAsyncTask_EcrireServeur _MyAsyncTask_EcrireServeur =new MyAsyncTask_EcrireServeur(pContext, lListofParams, _URL);
		boolean lResult=false;
		try {
			lResult = _MyAsyncTask_EcrireServeur.execute().get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		if(lResult)
		{
			Toast.makeText(pContext, " Modifier avec succès ", Toast.LENGTH_LONG).show();
			return true;
		}else
		{
			Toast.makeText(pContext, " échec lors des Modifications ", Toast.LENGTH_SHORT).show();
			return false;
		}
	}

	public boolean UpdateInTo_Serveur_OffreStage(String _confirmation,int _Id_offrestage,String _URL )
	{                     
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();

		NameValuePair lParamConfirmation=new BasicNameValuePair("PARAM_CONFIRMATION",_confirmation);
		NameValuePair lParamOffreStage=new BasicNameValuePair("PARAM_ID_OFFRESTAGE",String.valueOf(_Id_offrestage));
		lListofParams.add(lParamConfirmation);
		lListofParams.add(lParamOffreStage);

		//_ClientHTTP=new ClientHTTP(pContext);
		//boolean lResult=_ClientHTTP.SendToUrl(_URL,lListofParams);
		MyAsyncTask_EcrireServeur _MyAsyncTask_EcrireServeur =new MyAsyncTask_EcrireServeur(pContext, lListofParams, _URL);
		boolean lResult=false;
		try {
			lResult = _MyAsyncTask_EcrireServeur.execute().get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		if(lResult)
		{
			Toast.makeText(pContext, " Modifier avec succès ", Toast.LENGTH_LONG).show();
			return true;
		}else
		{
			Toast.makeText(pContext, " échec lors des Modifications ", Toast.LENGTH_SHORT).show();
			return false;
		}
	}

	public boolean UpdateInTo_Serveur_OffreStageEtudiant(String _ID_Stage,String _ID_TravailPfe,String _ID_OffreStage,String _ID_Etudiant,String _ID_Binome,String _URL )
	{                     
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();

		NameValuePair lParamIdStage=new BasicNameValuePair("PARAM_ID_STAGE", _ID_Stage);
		NameValuePair lParamIdTravail=new BasicNameValuePair("PARAM_ID_TRAVAILPFE", _ID_TravailPfe);
		NameValuePair lParamIdOffreStage=new BasicNameValuePair("PARAM_ID_OFFRESTAGE", _ID_OffreStage);
		NameValuePair lParamIdEtudiant=new BasicNameValuePair("PARAM_ID_ETUDIANT", _ID_Etudiant);
		NameValuePair lParamIdBinome=new BasicNameValuePair("PARAM_ID_BINOME", _ID_Binome);

		lListofParams.add(lParamIdStage);
		lListofParams.add(lParamIdTravail);
		lListofParams.add(lParamIdOffreStage);
		lListofParams.add(lParamIdEtudiant);
		lListofParams.add(lParamIdBinome);

		//_ClientHTTP=new ClientHTTP(pContext);
		//boolean lResult=_ClientHTTP.SendToUrl(_URL,lListofParams);
		MyAsyncTask_EcrireServeur _MyAsyncTask_EcrireServeur =new MyAsyncTask_EcrireServeur(pContext, lListofParams, _URL);
		boolean lResult=false;
		try {
			lResult = _MyAsyncTask_EcrireServeur.execute().get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		if(lResult)
		{
			Toast.makeText(pContext, " Modifier avec succès ", Toast.LENGTH_LONG).show();
			return true;
		}else
		{
			Toast.makeText(pContext, " échec lors des Modifications ", Toast.LENGTH_SHORT).show();
			return false;
		}
	}

	//////////////////// DELETTE InTo SERVEUR :
	public boolean DeleteInTo_Serveur_EncadreurPro(String _ID_Stage,String _ID_EncadreurPro,String _URL)
	{  // DEMANDE DE STAGE
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();

		NameValuePair lParamIdStage=new BasicNameValuePair("PARAM_ID_SATAGE", _ID_Stage );
		NameValuePair lParamIdEncadreurPro=new BasicNameValuePair("PARAM_ID_ENCADREURPRO", _ID_EncadreurPro );

		lListofParams.add(lParamIdStage);
		lListofParams.add(lParamIdEncadreurPro);

		_ClientHTTP=new ClientHTTP(pContext);
		boolean lResult=_ClientHTTP.SendToUrl(_URL,lListofParams);

		//		MyAsyncTask_EcrireServeur _MyAsyncTask_EcrireServeur =new MyAsyncTask_EcrireServeur(pContext, lListofParams, _URL);
		//		boolean lResult=false;
		//		try {
		//			lResult = _MyAsyncTask_EcrireServeur.execute().get();
		//		} catch (InterruptedException e) {
		//			// TODO Auto-generated catch block
		//			e.printStackTrace();
		//		} catch (ExecutionException e) {
		//			// TODO Auto-generated catch block
		//			e.printStackTrace();
		//		}


		if(lResult)
		{
			Toast.makeText(pContext, " Supprimer avec succès ", Toast.LENGTH_LONG).show();
			return true;
		}else
		{
			Toast.makeText(pContext, " échec lors de la Suppression ", Toast.LENGTH_SHORT).show();
			return false;
		}
	}
	public boolean DeleteUpdateInTo_Serveur_CondidaterOffreStageEtudiant(String _ID_OffreStage,String _ID_Etudiant,String _ID_Binome,String _URL )
	{  // DEMANDE DE STAGE
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();

		NameValuePair lParamIDOffreStage=new BasicNameValuePair("PARAM_ID_OFFRESTAGE", _ID_OffreStage );
		NameValuePair lParamIDEtudiant=new BasicNameValuePair("PARAM_ID_ETUDIANT", _ID_Etudiant );
		NameValuePair lParamIDBinome=new BasicNameValuePair("PARAM_ID_BINOME", _ID_Binome );

		lListofParams.add(lParamIDOffreStage);
		lListofParams.add(lParamIDEtudiant);
		lListofParams.add(lParamIDBinome);

		//_ClientHTTP=new ClientHTTP(pContext);
		//boolean lResult=_ClientHTTP.SendToUrl(_URL,lListofParams);
		MyAsyncTask_EcrireServeur _MyAsyncTask_EcrireServeur =new MyAsyncTask_EcrireServeur(pContext, lListofParams, _URL);
		boolean lResult=false;
		try {
			lResult = _MyAsyncTask_EcrireServeur.execute().get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		if(lResult)
		{
			Toast.makeText(pContext, " Supprimer avec succès ", Toast.LENGTH_LONG).show();
			return true;
		}else
		{
			Toast.makeText(pContext, " échec lors de la Suppression ", Toast.LENGTH_SHORT).show();
			return false;
		}
	}
	public boolean DeleteInTo_Serveur_TravailPfeStage(String _ID_TravailPfe,String _URL )
	{                     
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();

		NameValuePair lParamID=new BasicNameValuePair("PARAM_ID_TRAVAILPFE", _ID_TravailPfe );

		lListofParams.add(lParamID);
		//_ClientHTTP=new ClientHTTP(pContext);
		//boolean lResult=_ClientHTTP.SendToUrl(_URL,lListofParams);
		MyAsyncTask_EcrireServeur _MyAsyncTask_EcrireServeur =new MyAsyncTask_EcrireServeur(pContext, lListofParams, _URL);
		boolean lResult=false;
		try {
			lResult = _MyAsyncTask_EcrireServeur.execute().get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		if(lResult)
		{
			Toast.makeText(pContext, " Supprimer avec succès ", Toast.LENGTH_LONG).show();
			return true;
		}else
		{
			Toast.makeText(pContext, " échec lors de la Suppression ", Toast.LENGTH_SHORT).show();
			return false;
		}
	}
	public boolean DeleteInTo_Serveur_Entreprise(String _ID_Entreprise,String _URL )
	{                     
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();

		NameValuePair lParamID=new BasicNameValuePair("PARAM_ID_ENTREPRISE", _ID_Entreprise );

		lListofParams.add(lParamID);
		//_ClientHTTP=new ClientHTTP(pContext);
		//boolean lResult=_ClientHTTP.SendToUrl(_URL,lListofParams);
		MyAsyncTask_EcrireServeur _MyAsyncTask_EcrireServeur =new MyAsyncTask_EcrireServeur(pContext, lListofParams, _URL);
		boolean lResult=false;
		try {
			lResult = _MyAsyncTask_EcrireServeur.execute().get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		if(lResult)
		{
			Toast.makeText(pContext, " Supprimer avec succès ", Toast.LENGTH_LONG).show();
			return true;
		}else
		{
			Toast.makeText(pContext, " échec lors de la Suppression ", Toast.LENGTH_SHORT).show();
			return false;
		}
	}
	//////////////////// Afficher information du SERVEUR :
	public ArrayList<HashMap<String, String>> Watch_Serveur_Etudiant(String _numInscrit_Etudiant,String _Id_Etudiant,String _Id_Departement,String _URL) /// url...
	{
		ArrayList<HashMap<String, String>> _List = new ArrayList<HashMap<String,String>>();
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();
		NameValuePair lParamIdEtudiant=new BasicNameValuePair("PARAM_ID_ETUDIANT", _Id_Etudiant);
		NameValuePair lParamIdDepartement=new BasicNameValuePair("PARAM_ID_DEPARTEMENT", _Id_Departement);
		NameValuePair lParamNumInscrit=new BasicNameValuePair("PARAM_NUMINSCRIT_ETUDIANT", _numInscrit_Etudiant);

		lListofParams.add(lParamIdEtudiant);
		lListofParams.add(lParamIdDepartement);
		lListofParams.add(lParamNumInscrit);

		_ClientHTTP=new ClientHTTP(pContext);
		String lRetour_Connecxion=_ClientHTTP.LireEcrireUrl(_URL, lListofParams);

		//		MyAsyncTask_LireServeur _MyAsyncTask =new MyAsyncTask_LireServeur(pContext, lListofParams, _URL);
		//		String lRetour_Connecxion=null;
		//		try {
		//			lRetour_Connecxion = _MyAsyncTask.execute().get();
		//		} catch (InterruptedException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		} catch (ExecutionException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		}

		try {
			JSONArray lArray_Etudiant=new JSONArray(lRetour_Connecxion);

			for (int i = 0; i < lArray_Etudiant.length(); i++)
			{
				JSONObject lObjc_P=lArray_Etudiant.getJSONObject(i);
				HashMap<String, String> _HashMap = new HashMap<String, String>();
				// Etudiant
				int _ID_Etudiant=lObjc_P.getInt(DataBase_Static.ETUDIANT_COL_ID);
				String _NumInscrit_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_NUmINSCRIT);
				String _Nom_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_NOM);
				String _Prenom_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_PRENOM);
				String _Sexe_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_SEXE);
				String _DateNaiss_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_DATENAISS);
				String _LieauNaiss_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_LIEUNAISS);
				String _NumTel_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_NUmINSCRIT);
				String _Email_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_EMAIL);
				String _Adresse_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_ADRESSE);
				String _Image_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_IMAGE);
				Integer _Id_etudiant1_Etudiant=Verifier_Integer(lObjc_P.getString(DataBase_Static.ETUDIANT_COL_IdETUDIANT1));
				// Departement
				String  _Libelle_Departement=lObjc_P.getString(DataBase_Static.DEPARTEMENT_COL_LIBELLE);
				// Niveau
				String  _Num_Niveau=lObjc_P.getString(DataBase_Static.NIVEAU_COL_NUM);
				String  _Designation_Niveau=lObjc_P.getString(DataBase_Static.NIVEAU_COL_DESIGNATION);
				// Filiere
				String _Designation_Filiere=lObjc_P.getString(DataBase_Static.FILIERE_COL_DESIGNATION);
				// Cycle
				String _Num_Cycle=lObjc_P.getString(DataBase_Static.CYCLE_COL_NUM);
				String _Designation_Cycle=lObjc_P.getString(DataBase_Static.CYCLE_COL_DESIGNATION);

				_HashMap.put("_ID_Etudiant", String.valueOf(_ID_Etudiant));
				_HashMap.put("numInscrit_Etudiant",_NumInscrit_Etudiant );
				_HashMap.put("nom_Etudiant", _Nom_Etudiant);
				_HashMap.put("prenom_Etudiant", _Prenom_Etudiant);
				_HashMap.put("sexe_Etudiant",_Sexe_Etudiant );
				_HashMap.put("dateNaiss_Etudiant",_DateNaiss_Etudiant );
				_HashMap.put("lieuNaiss_Etudiant",_LieauNaiss_Etudiant );
				_HashMap.put("numTel_Etudiant",_NumTel_Etudiant );
				_HashMap.put("email_Etudiant",_Email_Etudiant );
				_HashMap.put("adresse_Etudiant",_Adresse_Etudiant );
				_HashMap.put("image_Etudiant",_Image_Etudiant );
				_HashMap.put("_Id_etudiant1_Etudiant", String.valueOf(_Id_etudiant1_Etudiant));
				_HashMap.put("libelle_Departement",_Libelle_Departement );
				_HashMap.put("num_Niveau",_Num_Niveau );
				_HashMap.put("designation_Niveau",_Designation_Niveau );
				_HashMap.put("designation_Filiere",_Designation_Filiere );
				_HashMap.put("num_Cycle", _Num_Cycle);
				_HashMap.put("designation_Cycle",_Designation_Cycle );

				_List.add(_HashMap);
			}
		} 
		catch(JSONException e)
		{
			Log.i("tagjsonexp",""+e.toString());
		}
		return _List;
	}

	public ArrayList<HashMap<String, String>> Watch_Serveur_TravailPfe(String _URL) /// url...
	{
		ArrayList<HashMap<String, String>> _List = new ArrayList<HashMap<String,String>>();

		_ClientHTTP=new ClientHTTP(pContext);
		String lRetour_Connecxion=_ClientHTTP.readFromUrl(_URL);

		//		MyAsyncTask_AfficherServeur _MyAsyncTask_AfficherServeur =new MyAsyncTask_AfficherServeur(pContext, _URL);
		//		String lRetour_Connecxion=null;
		//		try {
		//			lRetour_Connecxion=_MyAsyncTask_AfficherServeur.execute().get();
		//		} catch (InterruptedException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		} catch (ExecutionException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		}

		try {
			JSONArray lArray_Etudiant=new JSONArray(lRetour_Connecxion);

			for (int i = 0; i < lArray_Etudiant.length(); i++)
			{
				JSONObject lObjc_P=lArray_Etudiant.getJSONObject(i);
				HashMap<String, String> _HashMap = new HashMap<String, String>();

				int _ID_TravailPfe=lObjc_P.getInt(DataBase_Static.TRAVAILPFE_COL_ID);

				_HashMap.put("_ID_TravailPfe", String.valueOf(_ID_TravailPfe));

				_List.add(_HashMap);
			}
		} 
		catch(JSONException e)
		{
			Log.i("tagjsonexp",""+e.toString());
		}
		return _List;
	}
	public ArrayList<HashMap<String, String>> Watch_Serveur_Stage(String _ID_TravailPfe,String _URL) 
	{
		ArrayList<HashMap<String, String>> _List = new ArrayList<HashMap<String,String>>();
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();

		NameValuePair lParamIDTravail=new BasicNameValuePair("PARAM_ID_TRAVAILPFE",_ID_TravailPfe);

		lListofParams.add(lParamIDTravail);

		_ClientHTTP=new ClientHTTP(pContext);
		String lRetour_Connecxion=_ClientHTTP.LireEcrireUrl(_URL, lListofParams);

		//		MyAsyncTask_LireServeur _MyAsyncTask =new MyAsyncTask_LireServeur(pContext, lListofParams, _URL);
		//		String lRetour_Connecxion=null;
		//		try {
		//			lRetour_Connecxion = _MyAsyncTask.execute().get();
		//		} catch (InterruptedException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		} catch (ExecutionException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		}

		try {
			JSONArray lArray_Etudiant=new JSONArray(lRetour_Connecxion);

			for (int i = 0; i < lArray_Etudiant.length(); i++)
			{
				JSONObject lObjc_P=lArray_Etudiant.getJSONObject(i);
				HashMap<String, String> _HashMap = new HashMap<String, String>();

				int _ID_Stage=lObjc_P.getInt(DataBase_Static.STAGE_COL_ID);

				_HashMap.put("_ID_Stage", String.valueOf(_ID_Stage));

				_List.add(_HashMap);
			}
		} 
		catch(JSONException e)
		{
			Log.i("tagjsonexp",""+e.toString());
		}
		return _List;
	}

	//////Verifier Integer:
	public Integer Verifier_Integer(String valeur)
	{
		if (valeur.equals("null"))
		{
			return null;
		}else
		{
			return Integer.parseInt(valeur);	
		}
	}

}