package package_Foction_DataBase;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import package_ClassPersonaliser.Condidater;
import package_ClassPersonaliser.Cycle;
import package_ClassPersonaliser.Demande;
import package_ClassPersonaliser.Departement;
import package_ClassPersonaliser.EncadreurPro;
import package_ClassPersonaliser.Enseignant;
import package_ClassPersonaliser.Entreprise;
import package_ClassPersonaliser.Etudiant;
import package_ClassPersonaliser.Filiere;
import package_ClassPersonaliser.Niveau;
import package_ClassPersonaliser.OffreStage;
import package_ClassPersonaliser.Stage;
import package_ClassPersonaliser.Travail_PFE;
import package_Creator_DataBase.MySQliteCreator_DataBase;
import package_Utilitaire.ClientHTTP;
import package_Utilitaire.DataBase_Static;
import package_Utilitaire.MyAsyncTask_EcrireServeur;
import package_Utilitaire.MyAsyncTask_LireServeur;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

public class MySQliteFonction_Etudiant 
{
	private ClientHTTP _ClientHTTP;
	private SQLiteDatabase _SqLiteDatabase;
	private MySQliteCreator_DataBase _SQliteCreator_DataBase;
	private Context pContext;

	public MySQliteFonction_Etudiant(Context pContext)
	{
		this.pContext=pContext;
		_SQliteCreator_DataBase=new MySQliteCreator_DataBase(pContext, DataBase_Static.DB_NAME, null, 1);
		openDB();
	}
	public void openDB()
	{
		_SqLiteDatabase=_SQliteCreator_DataBase.getWritableDatabase();
	}

	///////////////////// requette SQLITE :
	public ArrayList<HashMap<String, String>> Afficher_Demande()
	{
		String sql=" SELECT  * FROM demande  ";
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList_Demande = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();

				String confirmation_Demande=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.DEMANDE_COL_CONFIRMATION));
				String date_Demande=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.DEMANDE_COL_DATE));
				int Id_enseignant_Demande=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.DEMANDE_COL_IdENSEIGNANT));
				int Id_etudiant_Demande=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.DEMANDE_COL_IdETUDIANT));

				_HashMap.put("confirmation_Demande",confirmation_Demande);
				_HashMap.put("date_Demande",date_Demande);
				_HashMap.put("Id_enseignant_Demande",String.valueOf(Id_enseignant_Demande));
				_HashMap.put("Id_etudiant_Demande",String.valueOf(Id_etudiant_Demande));
				Log.d("Id_enseignant_Demande", String.valueOf(Id_enseignant_Demande));
				_ArrayList_Demande.add(_HashMap);
			}
		}
		return _ArrayList_Demande;
	}

	public ArrayList<HashMap<String, String>> Afficher_Enseignant()
	{
		String sql=" SELECT  enseignant.* FROM enseignant ";
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList_Condidater = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();

				int ID_Enseignant=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_ID));
				String nom_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_NOM));
				String prenom_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_PRENOM));
				String email_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_EMAIL));
				String specialite_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_SPECIALITE));

				_HashMap.put("ID_Enseignant",String.valueOf(ID_Enseignant));
				_HashMap.put("nom_Enseignant",nom_Enseignant);
				_HashMap.put("prenom_Enseignant",prenom_Enseignant);
				_HashMap.put("email_Enseignant",email_Enseignant);
				_HashMap.put("specialite_Enseignant",specialite_Enseignant);

				Log.d("ID_Enseignant", String.valueOf(ID_Enseignant));
				_ArrayList_Condidater.add(_HashMap);
			}
		}
		return _ArrayList_Condidater;
	}

	public ArrayList<HashMap<String, String>> Afficher_EncadreurAca(String _ID_Etudiant)
	{
		String sql=" SELECT  enseignant.* FROM enseignant , etudiant , travail_pfe" +
				" where" +
				" etudiant.Id_etudiant="+_ID_Etudiant+" and" +
				" etudiant.id_travailPfe_etudiant=travail_pfe.Id_travailPfe and" +
				" travail_pfe.idensei_travailPfe=enseignant.idensei  ";
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList_Condidater = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();

				int ID_Enseignant=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_ID));
				String nom_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_NOM));
				String prenom_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_PRENOM));
				String email_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_EMAIL));
				String specialite_Enseignant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENSEIGNANT_COL_SPECIALITE));

				_HashMap.put("ID_Enseignant",String.valueOf(ID_Enseignant));
				_HashMap.put("nom_Enseignant",nom_Enseignant);
				_HashMap.put("prenom_Enseignant",prenom_Enseignant);
				_HashMap.put("email_Enseignant",email_Enseignant);
				_HashMap.put("specialite_Enseignant",specialite_Enseignant);

				Log.d("ID_Enseignant_EncadreurAcad", String.valueOf(ID_Enseignant));
				_ArrayList_Condidater.add(_HashMap);
			}
		}
		return _ArrayList_Condidater;
	}

	public ArrayList<HashMap<String, String>> Afficher_EncadreurPro()
	{
		String sql=" SELECT  * FROM encadreur_pro  ";
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList_Condidater = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();

				int ID_EncadreurPro=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ENCADREURPRO_COL_ID));
				String Nom_EncadreurPro=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENCADREURPRO_COL_NOM));
				String Prenom_EncadreurPro=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENCADREURPRO_COL_PRENOM));
				String Email_EncadreurPro=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENCADREURPRO_COL_EMAIL));

				_HashMap.put("ID_EncadreurPro",String.valueOf(ID_EncadreurPro));
				_HashMap.put("Nom_EncadreurPro",Nom_EncadreurPro);
				_HashMap.put("Prenom_EncadreurPro",Prenom_EncadreurPro);
				_HashMap.put("Email_EncadreurPro",Email_EncadreurPro);

				_ArrayList_Condidater.add(_HashMap);
			}
		}
		return _ArrayList_Condidater;
	}
	public ArrayList<HashMap<String, String>> Afficher_StageEntreprise()
	{
		String sql=" SELECT *  FROM  stage , entreprise " +
				" where " +
				"stage.id_entreprise_stage=entreprise.Id_entreprise  ";
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList_Condidater = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();

				int ID_Stage=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_ID));
				String Nom_Stage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_NOM));
				String Date_Stage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_DATE));
				String Id_travailPfe_Stage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_IdTRAVAILPFE));
				String Id_encadreurPro_Stage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_IdENCADREUrPRO));
				String Id_offreStage_Stage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_IdOFFRESTAGE));
				String description_Stage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_DESCRIPTION));
				String Id_entreprise_Stage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.STAGE_COL_IdENTREPRISE));

				int ID_Entreprise=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_ID));
				String libelle_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_LIBELLE));
				String raison_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_RAISON));
				String numTel_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_NUmTEL));
				String email_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_EMAIL));
				String siteWeb_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_SITEWEB));
				String logo_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_LOGO));

				_HashMap.put("ID_Stage",String.valueOf(ID_Stage));
				_HashMap.put("Nom_Stage",Nom_Stage);
				_HashMap.put("Date_Stage",Date_Stage);
				_HashMap.put("Id_travailPfe_Stage",Id_travailPfe_Stage);
				_HashMap.put("Id_encadreurPro_Stage",Id_encadreurPro_Stage);
				_HashMap.put("Id_offreStage_Stage",Id_offreStage_Stage);
				_HashMap.put("description_Stage",description_Stage);
				_HashMap.put("Id_entreprise_Stage",Id_entreprise_Stage);

				_HashMap.put("ID_Entreprise",String.valueOf(ID_Entreprise));
				_HashMap.put("libelle_Entreprise",libelle_Entreprise);
				_HashMap.put("raison_Entreprise",raison_Entreprise);
				_HashMap.put("numTel_Entreprise",numTel_Entreprise);
				_HashMap.put("email_Entreprise",email_Entreprise);
				_HashMap.put("siteWeb_Entreprise",siteWeb_Entreprise);
				_HashMap.put("logo_Entreprise",logo_Entreprise);

				_ArrayList_Condidater.add(_HashMap);
			}
		}
		return _ArrayList_Condidater;
	}
	public ArrayList<HashMap<String, String>> Afficher_Condidater()
	{
		String sql=" SELECT  * FROM condidater  ";
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList_Condidater = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();

				String confirmation_Condidater=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.CONDIDATER_COL_CONFIRMATION));
				String date_Condidater=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.CONDIDATER_COL_DATE));
				int Id_etudiant_Condidater=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.CONDIDATER_COL_IdETUDIANT));
				int Id_offreStage_Condidater=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.CONDIDATER_COL_IdOFFRESTAGE));

				_HashMap.put("confirmation_Condidater",confirmation_Condidater);
				_HashMap.put("date_Condidater",date_Condidater);
				_HashMap.put("Id_etudiant_Condidater",String.valueOf(Id_etudiant_Condidater));
				_HashMap.put("Id_offreStage_Condidater",String.valueOf(Id_offreStage_Condidater));

				_ArrayList_Condidater.add(_HashMap);
			}
		}
		return _ArrayList_Condidater;
	}
	public ArrayList<HashMap<String, String>> Afficher_OffreStageEntreprise()
	{
		String sql="SELECT DISTINCT * FROM offre_stage , entreprise" +
				" where" +
				" offre_stage.id_entreprise_offreStage = entreprise.Id_entreprise ";
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList_OffreStage = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();

				int ID_OffreStage=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_ID));
				String nom_OffreStage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_NOM));
				String description_OffreStage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_DESCRIPTION));
				String confirmation_OffreStage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_CONFIRMATION));
				String date_OffreStage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_DATE));
				int cle_entreprise_OffreStage=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_IdENTREPRISE));
				String cle_stage_OffreStage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_IdSTAGE));
				String cle_travailPfe_OffreStage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_IdTRAVAILPFE));
				int cle_filiere_OffreStage=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_IdFILIERE));
				int ID_Entreprise=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_ID));
				String libelle_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_LIBELLE));
				String raison_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_RAISON));
				String numTel_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_NUmTEL));
				String email_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_EMAIL));
				String siteWeb_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_SITEWEB));
				String login_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_LOGIN));
				String password_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_PASSWORD));
				String logo_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_LOGO));

				_HashMap.put("ID_OffreStage",String.valueOf(ID_OffreStage));
				_HashMap.put("nom_OffreStage",nom_OffreStage );
				_HashMap.put("description_OffreStage",description_OffreStage );
				_HashMap.put("confirmation_OffreStage",confirmation_OffreStage );
				_HashMap.put("date_OffreStage",date_OffreStage );
				_HashMap.put("cle_entreprise_OffreStage",String.valueOf(cle_entreprise_OffreStage ));
				_HashMap.put("cle_stage_OffreStage", cle_stage_OffreStage);
				_HashMap.put("cle_travailPfe_OffreStage", cle_travailPfe_OffreStage);
				_HashMap.put("cle_filiere_OffreStage",String.valueOf(cle_filiere_OffreStage));

				_HashMap.put("ID_Entreprise",String.valueOf(ID_Entreprise));
				_HashMap.put("libelle_Entreprise",libelle_Entreprise);
				_HashMap.put("raison_Entreprise",raison_Entreprise);
				_HashMap.put("numTel_Entreprise",numTel_Entreprise);
				_HashMap.put("email_Entreprise",email_Entreprise);
				_HashMap.put("siteWeb_Entreprise",siteWeb_Entreprise);
				_HashMap.put("login_Entreprise",login_Entreprise);
				_HashMap.put("password_Entreprise",password_Entreprise);
				_HashMap.put("logo_Entreprise",logo_Entreprise);

				_ArrayList_OffreStage.add(_HashMap);
			}
		}
		return _ArrayList_OffreStage;
	}

	public ArrayList<HashMap<String, String>> Afficher_Binome(String _ID_Etudiant)
	{
		String sql="SELECT * FROM etudiant , departement , niveau , filiere , cycle " +
				"where " +
				"etudiant.id_etudiant_1="+_ID_Etudiant+"";
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();
				int ID_Etudiant=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_ID));
				String numInscrit_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_NUmINSCRIT));
				String cin_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_CIN));
				String nom_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_NOM));
				String prenom_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_PRENOM));
				String sexe_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_SEXE));
				String dateNaiss_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_DATENAISS));
				String lieuNaiss_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_LIEUNAISS));
				String numTel_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_NUmTEL));
				String email_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_EMAIL));
				String adresse_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_ADRESSE));
				String codePostal_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_CODePOSTAL));
				String image_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_IMAGE));
				int Id_filiere_Etudiant=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_IdFILIERE));
				_HashMap.put("ID_Etudiant",String.valueOf( ID_Etudiant));
				_HashMap.put("numInscrit_Etudiant", numInscrit_Etudiant);
				_HashMap.put("cin_Etudiant", cin_Etudiant);
				_HashMap.put("nom_Etudiant", nom_Etudiant);
				_HashMap.put("prenom_Etudiant", prenom_Etudiant);
				_HashMap.put("sexe_Etudiant", sexe_Etudiant);
				_HashMap.put("dateNaiss_Etudiant", dateNaiss_Etudiant);
				_HashMap.put("lieuNaiss_Etudiant", lieuNaiss_Etudiant);
				_HashMap.put("numTel_Etudiant", numTel_Etudiant);
				_HashMap.put("email_Etudiant", email_Etudiant);
				_HashMap.put("adresse_Etudiant", adresse_Etudiant);
				_HashMap.put("codePostal_Etudiant", codePostal_Etudiant);
				_HashMap.put("image_Etudiant", image_Etudiant);
				_HashMap.put("Id_filiere_Etudiant", String.valueOf(Id_filiere_Etudiant));
				String libelle_Departement=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.DEPARTEMENT_COL_LIBELLE));
				_HashMap.put("libelle_Departement", libelle_Departement);
				String num_Niveau=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.NIVEAU_COL_NUM));
				String designation_Niveau=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.NIVEAU_COL_DESIGNATION));
				_HashMap.put("num_Niveau", num_Niveau);
				_HashMap.put("designation_Niveau", designation_Niveau);
				String designation_Filiere=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.FILIERE_COL_DESIGNATION));
				_HashMap.put("designation_Filiere", designation_Filiere);
				String num_Cycle=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.CYCLE_COL_NUM));
				String designation_Cycle=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.CYCLE_COL_DESIGNATION));
				_HashMap.put("num_Cycle", num_Cycle);
				_HashMap.put("designation_Cycle", designation_Cycle);
				_ArrayList.add(_HashMap);
			}
		}
		return _ArrayList;
	}

	public ArrayList<HashMap<String, String>> Afficher_EtuDepNivFilCyc(String LOGIN , String PASSWORD)
	{

		String sql="SELECT DISTINCT * FROM etudiant , departement , niveau , filiere , cycle" +
				" WHERE " +
				"cin_etudiant='"+PASSWORD+"' and numInscrit_etudiant='"+LOGIN+"' ;" ;
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();
				int ID_Etudiant=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_ID));
				String numInscrit_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_NUmINSCRIT));
				String cin_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_CIN));
				String nom_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_NOM));
				String prenom_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_PRENOM));
				String sexe_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_SEXE));
				String dateNaiss_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_DATENAISS));
				String lieuNaiss_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_LIEUNAISS));
				String numTel_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_NUmTEL));
				String email_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_EMAIL));
				String adresse_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_ADRESSE));
				String codePostal_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_CODePOSTAL));
				String image_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_IMAGE));
				int Id_filiere_Etudiant=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_IdFILIERE));
				_HashMap.put("ID_Etudiant",String.valueOf( ID_Etudiant));
				_HashMap.put("numInscrit_Etudiant", numInscrit_Etudiant);
				_HashMap.put("cin_Etudiant", cin_Etudiant);
				_HashMap.put("nom_Etudiant", nom_Etudiant);
				_HashMap.put("prenom_Etudiant", prenom_Etudiant);
				_HashMap.put("sexe_Etudiant", sexe_Etudiant);
				_HashMap.put("dateNaiss_Etudiant", dateNaiss_Etudiant);
				_HashMap.put("lieuNaiss_Etudiant", lieuNaiss_Etudiant);
				_HashMap.put("numTel_Etudiant", numTel_Etudiant);
				_HashMap.put("email_Etudiant", email_Etudiant);
				_HashMap.put("adresse_Etudiant", adresse_Etudiant);
				_HashMap.put("codePostal_Etudiant", codePostal_Etudiant);
				_HashMap.put("image_Etudiant", image_Etudiant);
				_HashMap.put("Id_filiere_Etudiant", String.valueOf(Id_filiere_Etudiant));
				String libelle_Departement=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.DEPARTEMENT_COL_LIBELLE));
				_HashMap.put("libelle_Departement", libelle_Departement);
				String num_Niveau=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.NIVEAU_COL_NUM));
				String designation_Niveau=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.NIVEAU_COL_DESIGNATION));
				_HashMap.put("num_Niveau", num_Niveau);
				_HashMap.put("designation_Niveau", designation_Niveau);
				String designation_Filiere=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.FILIERE_COL_DESIGNATION));
				_HashMap.put("designation_Filiere", designation_Filiere);
				int ID_cycle=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.CYCLE_COL_ID));
				String num_Cycle=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.CYCLE_COL_NUM));
				String designation_Cycle=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.CYCLE_COL_DESIGNATION));
				_HashMap.put("ID_cycle", String.valueOf(ID_cycle));
				_HashMap.put("num_Cycle", num_Cycle);
				_HashMap.put("designation_Cycle", designation_Cycle);

				_ArrayList.add(_HashMap);
			}
		}
		return _ArrayList;
	}


	///////////////////// SYNCRONISATION SRVEUR :
	public boolean Synchroniser_Serveur_Demande(String _ID_Etudiant,String _ID_Binome,String _URL)
	{
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();
		NameValuePair lParamID_Etudiant=new BasicNameValuePair("PARAM_ID_ETUDIANT",_ID_Etudiant);
		NameValuePair lParamID_Binome=new BasicNameValuePair("PARAM_ID_BINOME",_ID_Binome);
		lListofParams.add(lParamID_Etudiant);
		lListofParams.add(lParamID_Binome);
		_ClientHTTP=new ClientHTTP(pContext);
		String lRetour_Connecxion=_ClientHTTP.LireEcrireUrl(_URL, lListofParams);

		//		MyAsyncTask_LireServeur _MyAsyncTask =new MyAsyncTask_LireServeur(pContext, lListofParams, _URL);
		//		String lRetour_Connecxion=null;
		//		try {
		//			lRetour_Connecxion = _MyAsyncTask.execute().get();
		//		} catch (InterruptedException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		} catch (ExecutionException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		}

		
		try {
			JSONArray lArray_Etudiant=new JSONArray(lRetour_Connecxion);
			for (int i = 0; i < lArray_Etudiant.length(); i++)
			{
				JSONObject lObjc_P=lArray_Etudiant.getJSONObject(i);
				///// 
				String _confirmation_Demande=lObjc_P.getString(DataBase_Static.DEMANDE_COL_CONFIRMATION);
				String _date_Demande=lObjc_P.getString(DataBase_Static.DEMANDE_COL_DATE);
				int _Id_enseignant_Demande=lObjc_P.getInt(DataBase_Static.DEMANDE_COL_IdENSEIGNANT);
				int _Id_etudiant_Demande=lObjc_P.getInt(DataBase_Static.DEMANDE_COL_IdETUDIANT);
				Demande _Demande =new Demande(_confirmation_Demande, _date_Demande, _Id_enseignant_Demande, _Id_etudiant_Demande);
				int x=InsertInTo_BD_Demande(_Demande);
				if (x==-1) 
				{
					Toast.makeText( pContext , "ERREUR Lors de l'Insertion de Donnees " ,Toast.LENGTH_LONG).show();
					return false;
				}
			}
		} 
		catch(JSONException e)
		{
			Toast.makeText( pContext , " vous n'avez pas encore envoyé des permissions D'encadrement" ,Toast.LENGTH_LONG).show();
			return false;
		}
		return true;
	}
	public boolean Synchroniser_Serveur_Enseignant(String _ID_Etudiant,String _URL)
	{
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();
		NameValuePair lParamID_Etudiant=new BasicNameValuePair("PARAM_ID_ETUDIANT",_ID_Etudiant);
		lListofParams.add(lParamID_Etudiant);
		_ClientHTTP=new ClientHTTP(pContext);
		String lRetour_Connecxion=_ClientHTTP.LireEcrireUrl(_URL, lListofParams);

		//		MyAsyncTask_LireServeur _MyAsyncTask =new MyAsyncTask_LireServeur(pContext, lListofParams, _URL);
		//		String lRetour_Connecxion=null;
		//		try {
		//			lRetour_Connecxion = _MyAsyncTask.execute().get();
		//		} catch (InterruptedException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		} catch (ExecutionException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		}

		try {
			JSONArray lArray_Etudiant=new JSONArray(lRetour_Connecxion);
			for (int i = 0; i < lArray_Etudiant.length(); i++)
			{
				JSONObject lObjc_P=lArray_Etudiant.getJSONObject(i);

				int _ID_Enseignant=lObjc_P.getInt(DataBase_Static.ENSEIGNANT_COL_ID);
				String Cin_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_NCIN);
				String Nom_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_NOM);
				String Prenom_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_PRENOM);
				String Sexe_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_SEXE);
				String DateNaiss_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_DATENAISS);
				String LieuNaiss_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_LIEUNAISS);
				String NumTel_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_NUmTEL);
				String Email_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_EMAIL);
				String adresse_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_ADRESSE);
				String Ville_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_VILLE);
				String CodePost_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_CODEPOSTALE);
				String Grade_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_GRADE);
				String Diplome_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_DIPLOME);
				String LieuDip_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_LIEUDIPLOME);
				String Situation_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_SITUATIONCIVILE);
				String Titulaire_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_TITULAIRE);
				String FactAdmin_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_FACtADMIN);
				String Encadreument_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_ENCADREMENT);
				String AnneeDip_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_ANNEEDIPLOME);
				String EnDetachement_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_EnDETACHEMENT);
				int Id_departement_Enseignant=lObjc_P.getInt(DataBase_Static.ENSEIGNANT_COL_IdDepartement);
				String Matricule_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_MATRICULE);
				String Specialite_Enseignant=lObjc_P.getString(DataBase_Static.ENSEIGNANT_COL_SPECIALITE);

				Enseignant _Classe_Enseignant=new Enseignant(_ID_Enseignant, Cin_Enseignant, Nom_Enseignant, Prenom_Enseignant, Sexe_Enseignant, DateNaiss_Enseignant, LieuNaiss_Enseignant, NumTel_Enseignant, Email_Enseignant, adresse_Enseignant, Ville_Enseignant, CodePost_Enseignant, Grade_Enseignant, Diplome_Enseignant, LieuDip_Enseignant, Situation_Enseignant, Titulaire_Enseignant, FactAdmin_Enseignant, Encadreument_Enseignant, AnneeDip_Enseignant, EnDetachement_Enseignant, Matricule_Enseignant, Specialite_Enseignant, Id_departement_Enseignant);
				Log.d("_ID_Enseignant", String.valueOf(_ID_Enseignant));
				int x=InsertInTo_BD_Enseignant(_Classe_Enseignant);
				if (x==-1) 
				{
					Toast.makeText( pContext , " ERREUR Lors de l'Insertion de Donnees " ,Toast.LENGTH_LONG).show();
					return false;
				}
			}
		} 
		catch(JSONException e)
		{
			Toast.makeText( pContext , " Aucun Encadreur Académique n'est Disponible " ,Toast.LENGTH_LONG).show();
			return false;
		}
		return true;
	}
	public boolean Synchroniser_Serveur_EncadreurPro(String _ID_Etudiant,String _URL)
	{
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();
		NameValuePair lParamID_Etudiant=new BasicNameValuePair("PARAM_ID_ETUDIANT",_ID_Etudiant);
		lListofParams.add(lParamID_Etudiant);
		_ClientHTTP=new ClientHTTP(pContext);
		String lRetour_Connecxion=_ClientHTTP.LireEcrireUrl(_URL, lListofParams);

		//		MyAsyncTask_LireServeur _MyAsyncTask =new MyAsyncTask_LireServeur(pContext, lListofParams, _URL);
		//		String lRetour_Connecxion=null;
		//		try {
		//			lRetour_Connecxion = _MyAsyncTask.execute().get();
		//		} catch (InterruptedException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		} catch (ExecutionException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		}

		
		try {
			JSONArray lArray_Etudiant=new JSONArray(lRetour_Connecxion);
			for (int i = 0; i < lArray_Etudiant.length(); i++)
			{
				JSONObject lObjc_P=lArray_Etudiant.getJSONObject(i);

				int ID_EncadreurPro=lObjc_P.getInt(DataBase_Static.ENCADREURPRO_COL_ID);
				String Ncin_EncadreurPro=lObjc_P.getString(DataBase_Static.ENCADREURPRO_COL_NCIN);
				String Nom_EncadreurPro=lObjc_P.getString(DataBase_Static.ENCADREURPRO_COL_NOM);
				String Prenom_EncadreurPro=lObjc_P.getString(DataBase_Static.ENCADREURPRO_COL_PRENOM);
				String NumTel_EncadreurPro=lObjc_P.getString(DataBase_Static.ENCADREURPRO_COL_NUmTEL);
				String Email_EncadreurPro=lObjc_P.getString(DataBase_Static.ENCADREURPRO_COL_EMAIL);
				EncadreurPro _EncadreurPro=new EncadreurPro(ID_EncadreurPro, Ncin_EncadreurPro, Nom_EncadreurPro, Prenom_EncadreurPro, NumTel_EncadreurPro, Email_EncadreurPro);
				int x=InsertInTo_BD_EncadreurPro(_EncadreurPro);

				if (x==-1) 
				{
					Toast.makeText( pContext , " ERREUR Lors de l'Insertion de Donnees " ,Toast.LENGTH_LONG).show();
					return false;
				}
			}
		} 
		catch(JSONException e)
		{
			Toast.makeText( pContext , " vous n'avez pas Encore  D'encadreur Professionnel " ,Toast.LENGTH_LONG).show();
			return false;
		}
		return true;
	}
	public boolean Synchroniser_Serveur_StageTravailPfeEntreprise(String _ID_Etudiant,String _URL)
	{
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();
		NameValuePair lParamID_Etudiant=new BasicNameValuePair("PARAM_ID_ETUDIANT",_ID_Etudiant);
		lListofParams.add(lParamID_Etudiant);
		_ClientHTTP=new ClientHTTP(pContext);
		String lRetour_Connecxion=_ClientHTTP.LireEcrireUrl(_URL, lListofParams);

		//		MyAsyncTask_LireServeur _MyAsyncTask =new MyAsyncTask_LireServeur(pContext, lListofParams, _URL);
		//		String lRetour_Connecxion=null;
		//		try {
		//			lRetour_Connecxion = _MyAsyncTask.execute().get();
		//		} catch (InterruptedException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		} catch (ExecutionException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		}

		try {
			JSONArray lArray_Etudiant=new JSONArray(lRetour_Connecxion);
			for (int i = 0; i < lArray_Etudiant.length(); i++)
			{
				JSONObject lObjc_P=lArray_Etudiant.getJSONObject(i);

				int _ID_TravailPfe=lObjc_P.getInt(DataBase_Static.TRAVAILPFE_COL_ID);
				String _Libelle_TravailPfe=lObjc_P.getString(DataBase_Static.TRAVAILPFE_COL_LIBELLE);
				Integer _Id_enseignant_TravailPfe=Verifier_Integer(lObjc_P.getString(DataBase_Static.TRAVAILPFE_COL_IdENSEIGNANT));
				Integer _Id_soutenance_TravailPfe=Verifier_Integer(lObjc_P.getString(DataBase_Static.TRAVAILPFE_COL_IdSOUTENANCE));
				Travail_PFE _Travail_PFE = new Travail_PFE(_ID_TravailPfe, _Libelle_TravailPfe, _Id_enseignant_TravailPfe, _Id_soutenance_TravailPfe);
				// 

				int _ID_Stage=lObjc_P.getInt(DataBase_Static.STAGE_COL_ID);
				String _Nom_Stage=lObjc_P.getString(DataBase_Static.STAGE_COL_NOM);
				String _Date_Stage=lObjc_P.getString(DataBase_Static.STAGE_COL_DATE);
				Integer _Id_travailPfe_Stage=Verifier_Integer(lObjc_P.getString(DataBase_Static.STAGE_COL_IdTRAVAILPFE));
				Integer _Id_encadreurPro_Stage=Verifier_Integer(lObjc_P.getString(DataBase_Static.STAGE_COL_IdENCADREUrPRO));
				Integer _Id_offreStage_Stage=Verifier_Integer(lObjc_P.getString(DataBase_Static.STAGE_COL_IdOFFRESTAGE));
				String _Description_Stage=lObjc_P.getString(DataBase_Static.STAGE_COL_DESCRIPTION);
				Integer _Id_entreprise_Stage=Verifier_Integer(lObjc_P.getString(DataBase_Static.STAGE_COL_IdENTREPRISE));
				Stage _Stage = new Stage(_ID_Stage, _Nom_Stage, _Description_Stage, _Date_Stage, _Id_travailPfe_Stage, _Id_encadreurPro_Stage, _Id_offreStage_Stage, _Id_entreprise_Stage);
				//
				int lID_Entreprise=lObjc_P.getInt(DataBase_Static.ENTREPRISE_COL_ID);
				String libelle_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_LIBELLE);
				String raison_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_RAISON);
				String numTel_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_NUmTEL);
				String email_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_EMAIL);
				String siteWeb_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_SITEWEB);
				String login_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_LOGIN);
				String password_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_PASSWORD);
				String logo_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_LOGO);
				Integer Id_enseignant_Entreprise=Verifier_Integer(lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_IdENSEIGNANT));
				Entreprise _Entreprise =new Entreprise(lID_Entreprise, libelle_Entreprise, raison_Entreprise, numTel_Entreprise, email_Entreprise, siteWeb_Entreprise, login_Entreprise, password_Entreprise, logo_Entreprise,Id_enseignant_Entreprise);

				int x= InsertInTo_BD_TravilPfe(_Travail_PFE);
				int y= InsertInTo_BD_Stage(_Stage);
				int z=InsertInTo_BD_Entreprise(_Entreprise);

				if (x==-1 || y==-1 || z==-1) 
				{
					Toast.makeText( pContext , " ERREUR Lors de l'Insertion de Donnees " ,Toast.LENGTH_LONG).show();
					return false;
				}
			}
		} 
		catch(JSONException e)
		{
			Toast.makeText( pContext , " Vous n'êtes Associé à Aucun Stage " ,Toast.LENGTH_LONG).show();
			return false;
		}
		return true;
	}
	public boolean Synchroniser_Serveur_Condidater(String _ID_Etudiant,String _ID_Binome,String _URL)
	{
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();
		NameValuePair lParamID_Etudiant=new BasicNameValuePair("PARAM_ID_ETUDIANT",_ID_Etudiant);
		NameValuePair lParamID_Binome=new BasicNameValuePair("PARAM_ID_BINOME",_ID_Binome);
		lListofParams.add(lParamID_Etudiant);
		lListofParams.add(lParamID_Binome);
		_ClientHTTP=new ClientHTTP(pContext);
		String lRetour_Connecxion=_ClientHTTP.LireEcrireUrl(_URL, lListofParams);

		//		MyAsyncTask_LireServeur _MyAsyncTask =new MyAsyncTask_LireServeur(pContext, lListofParams, _URL);
		//		String lRetour_Connecxion=null;
		//		try {
		//			lRetour_Connecxion = _MyAsyncTask.execute().get();
		//		} catch (InterruptedException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		} catch (ExecutionException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		}

		
		try {
			JSONArray lArray_Etudiant=new JSONArray(lRetour_Connecxion);
			for (int i = 0; i < lArray_Etudiant.length(); i++)
			{
				JSONObject lObjc_P=lArray_Etudiant.getJSONObject(i);
				///// 
				String _confirmation_Condidater=lObjc_P.getString(DataBase_Static.CONDIDATER_COL_CONFIRMATION);
				String _date_Condidater=lObjc_P.getString(DataBase_Static.CONDIDATER_COL_DATE);
				int _Id_etudiant_Condidater=lObjc_P.getInt(DataBase_Static.CONDIDATER_COL_IdETUDIANT);
				int _Id_offreStage_Condidater=lObjc_P.getInt(DataBase_Static.CONDIDATER_COL_IdOFFRESTAGE);
				Condidater _Condidater=new Condidater(_confirmation_Condidater, _date_Condidater, _Id_etudiant_Condidater, _Id_offreStage_Condidater);
				int x=InsertInTo_BD_Condidater(_Condidater);
				if (x==-1) 
				{
					Toast.makeText( pContext , "ERREUR Lors de l'Insertion de Donnees " ,Toast.LENGTH_LONG).show();
					return false;
				}
			}
		} 
		catch(JSONException e)
		{
			Toast.makeText( pContext , " vous n'avez pas encore envoyé des permissions de Stage " ,Toast.LENGTH_LONG).show();
			return false;
		}
		return true;
	}
	public boolean Synchroniser_Serveur_OffreStageEntreprise(String _ID_Filiere,String _URL)
	{
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();
		NameValuePair lParamID=new BasicNameValuePair("PARAM_ID_FILIERE",_ID_Filiere);
		lListofParams.add(lParamID);
		_ClientHTTP=new ClientHTTP(pContext);
		String lRetour_Connecxion=_ClientHTTP.LireEcrireUrl(_URL, lListofParams);

		//		MyAsyncTask_LireServeur _MyAsyncTask =new MyAsyncTask_LireServeur(pContext, lListofParams, _URL);
		//		String lRetour_Connecxion=null;
		//		try {
		//			lRetour_Connecxion = _MyAsyncTask.execute().get();
		//		} catch (InterruptedException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		} catch (ExecutionException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		}

		
		try {
			JSONArray lArray_Etudiant=new JSONArray(lRetour_Connecxion);
			for (int i = 0; i < lArray_Etudiant.length(); i++)
			{
				JSONObject lObjc_P=lArray_Etudiant.getJSONObject(i);
				///// OffreSTage
				int ID_OffreStage=lObjc_P.getInt(DataBase_Static.OFFRESTAGE_COL_ID);
				String nom_OffreStage=lObjc_P.getString(DataBase_Static.OFFRESTAGE_COL_NOM);
				String description_OffreStage=lObjc_P.getString(DataBase_Static.OFFRESTAGE_COL_DESCRIPTION);
				String confirmation_OffreStage=lObjc_P.getString(DataBase_Static.OFFRESTAGE_COL_CONFIRMATION);
				String date_OffreStage=lObjc_P.getString(DataBase_Static.OFFRESTAGE_COL_DATE);
				int Id_entreprise_OffreStage=lObjc_P.getInt(DataBase_Static.OFFRESTAGE_COL_IdENTREPRISE);
				Integer Id_stage_OffreStage=Verifier_Integer(lObjc_P.getString(DataBase_Static.OFFRESTAGE_COL_IdSTAGE));
				Integer Id_travailPfe_OffreStage=Verifier_Integer(lObjc_P.getString(DataBase_Static.OFFRESTAGE_COL_IdTRAVAILPFE));
				int Id_filiere_OffreStage=lObjc_P.getInt(DataBase_Static.OFFRESTAGE_COL_IdFILIERE);
				int lID_Entreprise=lObjc_P.getInt(DataBase_Static.ENTREPRISE_COL_ID);
				String libelle_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_LIBELLE);
				String raison_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_RAISON);
				String numTel_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_NUmTEL);
				String email_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_EMAIL);
				String siteWeb_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_SITEWEB);
				String login_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_LOGIN);
				String password_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_PASSWORD);
				String logo_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_LOGO);
				Integer Id_enseignant_Entreprise=Verifier_Integer(lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_IdENSEIGNANT));

				OffreStage _OffreStage = new OffreStage(ID_OffreStage, nom_OffreStage, description_OffreStage, confirmation_OffreStage, date_OffreStage, Id_entreprise_OffreStage, Id_stage_OffreStage, Id_travailPfe_OffreStage, Id_filiere_OffreStage);
				Entreprise _Entreprise =new Entreprise(lID_Entreprise, libelle_Entreprise, raison_Entreprise, numTel_Entreprise, email_Entreprise, siteWeb_Entreprise, login_Entreprise, password_Entreprise, logo_Entreprise,Id_enseignant_Entreprise);
				InsertInTo_BD_Entreprise(_Entreprise);
				int x=InsertInTo_BD_OffreStage(_OffreStage);
				if (x==-1)
				{
					Toast.makeText( pContext , " ERREUR Lors de l'Insertion de Donnees " ,Toast.LENGTH_LONG).show();
					return false;
				}
			}
		} 
		catch(JSONException e)
		{
			Toast.makeText( pContext , " Aucune Offre de Stage " ,Toast.LENGTH_LONG).show();
			return false;
		}
		return true;
	}
	public boolean Synchroniser_Serveur_Binome(String login,String password,String URL_Connexion)
	{
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();
		NameValuePair lParamCin=new BasicNameValuePair("PARAM_CIN",password);
		NameValuePair lParamInscrit=new BasicNameValuePair("PARAM_NUM_INSCRIT",login);
		lListofParams.add(lParamCin);
		lListofParams.add(lParamInscrit);
		_ClientHTTP=new ClientHTTP(pContext);
		String lRetour_Connecxion=_ClientHTTP.LireEcrireUrl(URL_Connexion, lListofParams);

		//		MyAsyncTask_LireServeur _MyAsyncTask =new MyAsyncTask_LireServeur(pContext, lListofParams, URL_Connexion);
		//		String lRetour_Connecxion=null;
		//		try {
		//			lRetour_Connecxion = _MyAsyncTask.execute().get();
		//		} catch (InterruptedException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		} catch (ExecutionException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		}

		try {
			JSONArray lArray_Etudiant=new JSONArray(lRetour_Connecxion);
			for (int i = 0; i < lArray_Etudiant.length(); i++)
			{
				JSONObject lObjc_P=lArray_Etudiant.getJSONObject(i);
				///// ETUDIANT
				int ID_Etudiant=lObjc_P.getInt(DataBase_Static.ETUDIANT_COL_ID);
				String numInscrit_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_NUmINSCRIT);
				String cin_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_CIN);
				String nom_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_NOM);
				String prenom_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_PRENOM);
				String sexe_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_SEXE);
				String dateNaiss_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_DATENAISS);
				String lieuNaiss_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_LIEUNAISS);
				String numTel_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_NUmTEL);
				String email_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_EMAIL);
				String adresse_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_ADRESSE);
				String codePostale_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_CODePOSTAL);
				String image_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_IMAGE);
				int cle_departement_Etudiant=lObjc_P.getInt(DataBase_Static.ETUDIANT_COL_IdDEPARTEMENT);
				int cle_filiere_Etudiant=lObjc_P.getInt(DataBase_Static.ETUDIANT_COL_IdFILIERE);
				Integer cle_travailPfe_Etudiant=Verifier_Integer(lObjc_P.getString(DataBase_Static.ETUDIANT_COL_IdTRAVAILPFE));
				Integer cle_etudiant1_Etudiant=Verifier_Integer(lObjc_P.getString(DataBase_Static.ETUDIANT_COL_IdETUDIANT1));
				Integer cle_document_Etudiant=Verifier_Integer(lObjc_P.getString(DataBase_Static.ETUDIANT_COL_IdDOCUMENT));

				Etudiant _Classe_Etudiant=new Etudiant(ID_Etudiant, numInscrit_Etudiant, cin_Etudiant, nom_Etudiant, prenom_Etudiant, sexe_Etudiant, dateNaiss_Etudiant, lieuNaiss_Etudiant, numTel_Etudiant, email_Etudiant, adresse_Etudiant, codePostale_Etudiant, image_Etudiant, cle_departement_Etudiant, cle_travailPfe_Etudiant, cle_etudiant1_Etudiant, cle_filiere_Etudiant, cle_document_Etudiant);
				///// insertion into DATA_BASE
				int x=InsertInTo_BD_Etudiant(_Classe_Etudiant);
				if (x==-1 )
				{
					Toast.makeText( pContext , "ERREUR Lors de l'Insertion de Donnees " ,Toast.LENGTH_LONG).show();
					return false;
				}
			}
		} 
		catch(JSONException e)
		{
			Toast.makeText( pContext , " vous n'êtes associés à aucun collègue " ,Toast.LENGTH_LONG).show();
			return false;
		}
		return true;
	}
	public boolean Synchroniser_Serveur_EtuDepNivFilCyc(String login,String password,String URL_Connexion) 
	{
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();
		NameValuePair lParamCin=new BasicNameValuePair("PARAM_CIN",password);
		NameValuePair lParamInscrit=new BasicNameValuePair("PARAM_NUM_INSCRIT",login);
		lListofParams.add(lParamCin);
		lListofParams.add(lParamInscrit);
		_ClientHTTP=new ClientHTTP(pContext);
		String lRetour_Connecxion=_ClientHTTP.LireEcrireUrl(URL_Connexion, lListofParams);

		//		MyAsyncTask_LireServeur _MyAsyncTask =new MyAsyncTask_LireServeur(pContext, lListofParams, URL_Connexion);
		//		String lRetour_Connecxion=null;
		//		try {
		//			lRetour_Connecxion = _MyAsyncTask.execute().get();
		//		} catch (InterruptedException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		} catch (ExecutionException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		}

		
		try {
			JSONArray lArray_Etudiant=new JSONArray(lRetour_Connecxion);
			for (int i = 0; i < lArray_Etudiant.length(); i++)
			{
				JSONObject lObjc_P=lArray_Etudiant.getJSONObject(i);
				///// ETUDIANT
				int ID_Etudiant=lObjc_P.getInt(DataBase_Static.ETUDIANT_COL_ID);
				String numInscrit_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_NUmINSCRIT);
				String cin_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_CIN);
				String nom_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_NOM);
				String prenom_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_PRENOM);
				String sexe_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_SEXE);
				String dateNaiss_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_DATENAISS);
				String lieuNaiss_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_LIEUNAISS);
				String numTel_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_NUmTEL);
				String email_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_EMAIL);
				String adresse_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_ADRESSE);
				String codePostale_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_CODePOSTAL);
				String image_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_IMAGE);
				Integer cle_departement_Etudiant=lObjc_P.getInt(DataBase_Static.ETUDIANT_COL_IdDEPARTEMENT);
				Integer cle_filiere_Etudiant=lObjc_P.getInt(DataBase_Static.ETUDIANT_COL_IdFILIERE);
				Integer cle_travailPfe_Etudiant=Verifier_Integer(lObjc_P.getString(DataBase_Static.ETUDIANT_COL_IdTRAVAILPFE));
				Integer cle_etudiant1_Etudiant=Verifier_Integer(lObjc_P.getString(DataBase_Static.ETUDIANT_COL_IdETUDIANT1));
				Integer cle_document_Etudiant=Verifier_Integer(lObjc_P.getString(DataBase_Static.ETUDIANT_COL_IdDOCUMENT));

				Etudiant _Classe_Etudiant=new Etudiant(ID_Etudiant, numInscrit_Etudiant, cin_Etudiant, nom_Etudiant, prenom_Etudiant, sexe_Etudiant, dateNaiss_Etudiant, lieuNaiss_Etudiant, numTel_Etudiant, email_Etudiant, adresse_Etudiant, codePostale_Etudiant, image_Etudiant, cle_departement_Etudiant, cle_travailPfe_Etudiant, cle_etudiant1_Etudiant, cle_filiere_Etudiant, cle_document_Etudiant);
				////// DEPARTEMENT
				int ID_Departement=lObjc_P.getInt(DataBase_Static.DEPARTEMENT_COL_ID);
				String libelle_Departement=lObjc_P.getString(DataBase_Static.DEPARTEMENT_COL_LIBELLE);
				String abreviation_Departement=lObjc_P.getString(DataBase_Static.DEPARTEMENT_COL_ABREVIATION);
				String ordre_Departement=lObjc_P.getString(DataBase_Static.DEPARTEMENT_COL_ORDRE);
				Departement _Classe_Departement = new Departement(ID_Departement, libelle_Departement, abreviation_Departement, ordre_Departement);
				///// NIVEAU
				int ID_Niveau=lObjc_P.getInt(DataBase_Static.NIVEAU_COL_ID);
				String num_Niveau=lObjc_P.getString(DataBase_Static.NIVEAU_COL_NUM);
				String designation_Niveau=lObjc_P.getString(DataBase_Static.NIVEAU_COL_DESIGNATION);
				Integer cle_filiere_Niveau=lObjc_P.getInt(DataBase_Static.NIVEAU_COL_IdFiliere);
				Niveau _Classe_Niveau=new Niveau(ID_Niveau, num_Niveau, designation_Niveau, null, null, cle_filiere_Niveau);
				///// FILIERE
				int ID_Filiere=lObjc_P.getInt(DataBase_Static.FILIERE_COL_ID);
				String designation_Filiere=lObjc_P.getString(DataBase_Static.FILIERE_COL_DESIGNATION);
				Integer cle_cycle_Filiere=lObjc_P.getInt(DataBase_Static.FILIERE_COL_IdCYCLE);
				Filiere _Classe_Filiere = new Filiere(ID_Filiere, designation_Filiere, cle_cycle_Filiere);
				///// CYCLE
				int ID_Cycle=lObjc_P.getInt(DataBase_Static.CYCLE_COL_ID);
				String num_Cycle=lObjc_P.getString(DataBase_Static.CYCLE_COL_NUM);
				String designation_Cycle=lObjc_P.getString(DataBase_Static.CYCLE_COL_DESIGNATION);
				Cycle _Classe_Cycle= new Cycle(ID_Cycle, num_Cycle, designation_Cycle, null, null);
				///// insertion into DATA_BASE
				int x=InsertInTo_BD_Etudiant(_Classe_Etudiant);
				int y=InsertInTo_BD_Departement(_Classe_Departement);
				int z=InsertInTo_BD_Niveau(_Classe_Niveau);
				int h=InsertInTo_BD_Filiere(_Classe_Filiere);
				int g=InsertInTo_BD_Cycle(_Classe_Cycle);
				if (x==-1 | y==-1 | z==-1 | h==-1 | g==-1 )
				{
					Toast.makeText( pContext , "ERREUR Lors de l'Insertion de Donnees " ,Toast.LENGTH_LONG).show();
					return false;
				}
			}
		} 
		catch(JSONException e)
		{
			Toast.makeText( pContext , "ERREUR Login ou Password non valide " ,Toast.LENGTH_LONG).show();
			return false;
		}
		return true;
	}


	//////////////////// INSERT INTO BD :
	public int InsertInTo_BD_Demande(Demande _Demande)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.DEMANDE_COL_CONFIRMATION,_Demande.getConfirmaiton_demande());
		lContentValues.put(DataBase_Static.DEMANDE_COL_DATE,_Demande.getDate_demande());
		lContentValues.put(DataBase_Static.DEMANDE_COL_IdENSEIGNANT,_Demande.getId_enseignant_demande());
		lContentValues.put(DataBase_Static.DEMANDE_COL_IdETUDIANT,_Demande.getId_etudiant_demande());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_DEMANDE, null, lContentValues);
		return (int) x;
	}
	public int InsertInTo_BD_Enseignant(Enseignant _Class_Enseignant)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_ID,_Class_Enseignant.getId_enseignant());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_NCIN,_Class_Enseignant.getCin_enseignant());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_NOM,_Class_Enseignant.getNom_enseignant());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_PRENOM,_Class_Enseignant.getPrenom_enseignant());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_SEXE,_Class_Enseignant.getSexe_enseignant());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_DATENAISS,_Class_Enseignant.getDateNaiss_enseignant());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_LIEUNAISS,_Class_Enseignant.getLieuNaiss_enseignant());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_NUmTEL,_Class_Enseignant.getNumTel_enseignant());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_EMAIL,_Class_Enseignant.getEmail_enseignant());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_ADRESSE,_Class_Enseignant.getAdresse_enseignant());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_VILLE,_Class_Enseignant.getVille());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_CODEPOSTALE,_Class_Enseignant.getCodePostale());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_GRADE,_Class_Enseignant.getGrade());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_DIPLOME,_Class_Enseignant.getDiplome());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_LIEUDIPLOME,_Class_Enseignant.getLieu_diplome());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_SITUATIONCIVILE,_Class_Enseignant.getSituation_civil());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_TITULAIRE,_Class_Enseignant.getTitulaire());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_FACtADMIN,_Class_Enseignant.getFactAdmin());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_ENCADREMENT,_Class_Enseignant.getEncadrement());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_ANNEEDIPLOME,_Class_Enseignant.getAnnee_diplome());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_EnDETACHEMENT,_Class_Enseignant.getEnDetachement());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_IdDepartement,_Class_Enseignant.getId_departement());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_MATRICULE,_Class_Enseignant.getMatricule());
		lContentValues.put(DataBase_Static.ENSEIGNANT_COL_SPECIALITE,_Class_Enseignant.getSpecialite());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_ENSEIGNANT, null, lContentValues);
		return (int) x;
	}
	public int InsertInTo_BD_EncadreurPro(EncadreurPro _EncadreurPro )
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.ENCADREURPRO_COL_ID,_EncadreurPro.getId_encadreurPro());
		lContentValues.put(DataBase_Static.ENCADREURPRO_COL_NCIN,_EncadreurPro.getCin_encadreurPro());
		lContentValues.put(DataBase_Static.ENCADREURPRO_COL_NOM,_EncadreurPro.getNom_encadreurPro());
		lContentValues.put(DataBase_Static.ENCADREURPRO_COL_PRENOM,_EncadreurPro.getPrenom_encadreurPro());
		lContentValues.put(DataBase_Static.ENCADREURPRO_COL_NUmTEL,_EncadreurPro.getNumTel_encadreurPro());
		lContentValues.put(DataBase_Static.ENCADREURPRO_COL_EMAIL,_EncadreurPro.getEmail_encadreurPro());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_ENCADREUR_PRO, null, lContentValues);
		return (int) x;
	}

	public int InsertInTo_BD_TravilPfe(Travail_PFE _Travail_PFE )
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.TRAVAILPFE_COL_ID,_Travail_PFE.getID_TravailPFE());
		lContentValues.put(DataBase_Static.TRAVAILPFE_COL_LIBELLE,_Travail_PFE.getLibelle_TravailPFE());
		lContentValues.put(DataBase_Static.TRAVAILPFE_COL_IdENSEIGNANT,_Travail_PFE.getCle_enseignant_TravailPFE());
		lContentValues.put(DataBase_Static.TRAVAILPFE_COL_IdSOUTENANCE,_Travail_PFE.getCle_soutenance_TravailPFE());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_TRAVAILPFE, null, lContentValues);
		return (int) x;
	}
	public int InsertInTo_BD_Stage( Stage _Stage)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.STAGE_COL_ID,_Stage.getId_stage());
		lContentValues.put(DataBase_Static.STAGE_COL_NOM,_Stage.getNom_stage());
		lContentValues.put(DataBase_Static.STAGE_COL_DATE,_Stage.getDate_stage());
		lContentValues.put(DataBase_Static.STAGE_COL_IdTRAVAILPFE,_Stage.getCle_travailPfe_stage());
		lContentValues.put(DataBase_Static.STAGE_COL_IdENCADREUrPRO,_Stage.getCle_encadreurPro_stage());
		lContentValues.put(DataBase_Static.STAGE_COL_IdOFFRESTAGE,_Stage.getCle_offreStage_stage());
		lContentValues.put(DataBase_Static.STAGE_COL_DESCRIPTION,_Stage.getDescription_stage());
		lContentValues.put(DataBase_Static.STAGE_COL_IdENTREPRISE,_Stage.getCle_entreprise_stage());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_STAGE, null, lContentValues);
		return (int) x;
	}
	public int InsertInTo_BD_Condidater(Condidater _Condidater)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.CONDIDATER_COL_CONFIRMATION,_Condidater.getConfirmaiton_condidater());
		lContentValues.put(DataBase_Static.CONDIDATER_COL_DATE,_Condidater.getDate_condidater());
		lContentValues.put(DataBase_Static.CONDIDATER_COL_IdETUDIANT,_Condidater.getId_etudiant_condidater());
		lContentValues.put(DataBase_Static.CONDIDATER_COL_IdOFFRESTAGE,_Condidater.getId_offreStage_condidater());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_CONDIDATER, null, lContentValues);
		return (int) x;
	}
	public int InsertInTo_BD_Entreprise(Entreprise _Entreprise)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.ENTREPRISE_COL_ID,_Entreprise.getId_entreprise());
		lContentValues.put(DataBase_Static.ENTREPRISE_COL_LIBELLE,_Entreprise.getLibelle_entreprise());
		lContentValues.put(DataBase_Static.ENTREPRISE_COL_RAISON,_Entreprise.getRaison_entreprise());
		lContentValues.put(DataBase_Static.ENTREPRISE_COL_NUmTEL,_Entreprise.getNumTel_entreprise());
		lContentValues.put(DataBase_Static.ENTREPRISE_COL_EMAIL,_Entreprise.getEmail_entreprise());
		lContentValues.put(DataBase_Static.ENTREPRISE_COL_SITEWEB,_Entreprise.getSiteWeb_entreprise());
		lContentValues.put(DataBase_Static.ENTREPRISE_COL_LOGIN,_Entreprise.getLogin_entreprise());
		lContentValues.put(DataBase_Static.ENTREPRISE_COL_PASSWORD,_Entreprise.getPassword_entreprise());
		lContentValues.put(DataBase_Static.ENTREPRISE_COL_LOGO,_Entreprise.getLogo_entreprise());
		lContentValues.put(DataBase_Static.ENTREPRISE_COL_IdENSEIGNANT,_Entreprise.getCle_enseignant_entreprise());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_ENTREPRISE, null, lContentValues);
		return (int) x;
	}
	public int InsertInTo_BD_OffreStage(OffreStage _OffreStage)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.OFFRESTAGE_COL_ID,_OffreStage.getId_offreStage());
		lContentValues.put(DataBase_Static.OFFRESTAGE_COL_NOM,_OffreStage.getNom_offreStage());
		lContentValues.put(DataBase_Static.OFFRESTAGE_COL_DESCRIPTION,_OffreStage.getDescription_offreStage());
		lContentValues.put(DataBase_Static.OFFRESTAGE_COL_CONFIRMATION,_OffreStage.getConfirmation_offreStage());
		lContentValues.put(DataBase_Static.OFFRESTAGE_COL_DATE,_OffreStage.getDate_offreStage());
		lContentValues.put(DataBase_Static.OFFRESTAGE_COL_IdENTREPRISE,_OffreStage.getCle_entreprise_offreStage());
		lContentValues.put(DataBase_Static.OFFRESTAGE_COL_IdSTAGE,_OffreStage.getCle_stage_offreStage());
		lContentValues.put(DataBase_Static.OFFRESTAGE_COL_IdTRAVAILPFE,_OffreStage.getCle_travailPfe_offreStage());
		lContentValues.put(DataBase_Static.OFFRESTAGE_COL_IdFILIERE,_OffreStage.getCle_filiere_offreStage());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_OFFRESTAGE, null, lContentValues);
		return (int) x;
	}
	public int InsertInTo_BD_Etudiant(Etudiant _Class_Etudiant)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.ETUDIANT_COL_ID,_Class_Etudiant.getId_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_NUmINSCRIT,_Class_Etudiant.getNum_inscrit());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_CIN,_Class_Etudiant.getCin_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_NOM,_Class_Etudiant.getNom_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_PRENOM,_Class_Etudiant.getPrenom_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_SEXE,_Class_Etudiant.getSexe_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_DATENAISS,_Class_Etudiant.getDateNaiss_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_LIEUNAISS,_Class_Etudiant.getLieuNaiss_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_NUmTEL,_Class_Etudiant.getNumTel_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_EMAIL,_Class_Etudiant.getEmail_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_ADRESSE,_Class_Etudiant.getAdresse_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_CODePOSTAL,_Class_Etudiant.getCodePostal_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_IMAGE,_Class_Etudiant.getImage_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_IdDEPARTEMENT,_Class_Etudiant.getCle_departement_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_IdTRAVAILPFE,_Class_Etudiant.getCle_travailPfe_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_IdETUDIANT1,_Class_Etudiant.getCle_etudiant1_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_IdFILIERE,_Class_Etudiant.getCle_filiere_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_IdDOCUMENT,_Class_Etudiant.getCle_document_etudiant());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_ETUDIANT, null, lContentValues);
		return (int) x;
	}
	public int InsertInTo_BD_Departement(Departement _Class_Departement)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.DEPARTEMENT_COL_ID,_Class_Departement.getId_departement());
		lContentValues.put(DataBase_Static.DEPARTEMENT_COL_LIBELLE,_Class_Departement.getLibelle_Departement());
		lContentValues.put(DataBase_Static.DEPARTEMENT_COL_ABREVIATION,_Class_Departement.getAbreviation_Departement());
		lContentValues.put(DataBase_Static.DEPARTEMENT_COL_ORDRE,_Class_Departement.getOrdre_Departement());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_DEPARTEMENT, null, lContentValues);
		return (int) x;
	}
	public int  InsertInTo_BD_Niveau(Niveau _Class_Niveau)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.NIVEAU_COL_ID,_Class_Niveau.getId_niveau());
		lContentValues.put(DataBase_Static.NIVEAU_COL_NUM,_Class_Niveau.getNum_niveau());
		lContentValues.put(DataBase_Static.NIVEAU_COL_DESIGNATION,_Class_Niveau.getDesignation_niveau());
		lContentValues.put(DataBase_Static.NIVEAU_COL_NbMODULE,_Class_Niveau.getNbModule_niveau());
		lContentValues.put(DataBase_Static.NIVEAU_COL_NbSECTION,_Class_Niveau.getNbSection_niveau());
		lContentValues.put(DataBase_Static.NIVEAU_COL_IdFiliere,_Class_Niveau.getCle_filiere_niveau());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_NIVEAU, null, lContentValues);
		return (int) x;
	}
	public int InsertInTo_BD_Filiere(Filiere _Class_Filiere)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.FILIERE_COL_ID, _Class_Filiere.getId_filiere());
		lContentValues.put(DataBase_Static.FILIERE_COL_DESIGNATION,_Class_Filiere.getDesignation_filiere());
		lContentValues.put(DataBase_Static.FILIERE_COL_IdCYCLE,_Class_Filiere.getCle_cycle_filiere());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_FILIERE, null, lContentValues);
		return (int) x;
	}
	public int InsertInTo_BD_Cycle(Cycle _Class_Cycle)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.CYCLE_COL_ID, _Class_Cycle.getId_cycle());
		lContentValues.put(DataBase_Static.CYCLE_COL_NUM,_Class_Cycle.getNum_cycle());
		lContentValues.put(DataBase_Static.CYCLE_COL_DESIGNATION,_Class_Cycle.getDesignation_cycle());
		lContentValues.put(DataBase_Static.CYCLE_COL_NBrSEM,_Class_Cycle.getNbrSem_cycle());
		lContentValues.put(DataBase_Static.CYCLE_COL_NBrFILIERE,_Class_Cycle.getNbrFiliere_cycle());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_CYCLE, null, lContentValues);
		return (int) x;
	}

	//////////////////// SUPPRIMER CONTENUE TABLE :
	public void ClearAll_Demande()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_DEMANDE);
	}
	public void ClearAll_Enseignant()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_ENSEIGNANT);
	}
	public void ClearAll_EncadreurPro()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_ENCADREUR_PRO);
	}
	public void ClearAll_TravailPfe()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_TRAVAILPFE);
	}
	public void ClearAll_Stage()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_STAGE);
	}
	public void ClearAll_Etudiant()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_ETUDIANT);
	}
	public void ClearAll_Departement()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_DEPARTEMENT);
	}

	public void ClearAll_Filiere()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_FILIERE);
	}
	public void ClearAll_Niveau()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_NIVEAU);
	}
	public void ClearAll_Cycle()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_CYCLE);
	}
	public void ClearAll_OffreStage()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_OFFRESTAGE);
	}
	public void ClearAll_Entreprise()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_ENTREPRISE);
	}
	public void ClearAll_Condidater()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_CONDIDATER);
	}
	/////////////////////Insertion INTO SERVEUR :
	public boolean Insert_InToServeur_Demande(String _Date,String _Id_Enseignant,String _Id_Etudiant,String _URL) ///url
	{                      
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();

		NameValuePair lParamDate=new BasicNameValuePair("PARAM_DATE",_Date);
		NameValuePair lParamIdEnseignant=new BasicNameValuePair("PARAM_ID_ENSEIGNANT",_Id_Enseignant);
		NameValuePair lParamIdEtudiant=new BasicNameValuePair("PARAM_ID_ETUDIANT",_Id_Etudiant);

		lListofParams.add(lParamDate);
		lListofParams.add(lParamIdEnseignant);
		lListofParams.add(lParamIdEtudiant);

		//_ClientHTTP=new ClientHTTP(pContext);
		//boolean lResult=_ClientHTTP.SendToUrl(_URL,lListofParams);
		MyAsyncTask_EcrireServeur _MyAsyncTask_EcrireServeur =new MyAsyncTask_EcrireServeur(pContext, lListofParams, _URL);
		boolean lResult=false;
		try {
			lResult = _MyAsyncTask_EcrireServeur.execute().get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if(lResult)
		{
			Toast.makeText(pContext, " Demande de Permission d'encadrement Envoyé ", Toast.LENGTH_LONG).show();
			return true;
		}else
		{
			Toast.makeText(pContext, " Echec lors de l'envoi de la Permission d'encadrement ", Toast.LENGTH_LONG).show();
			return false;
		}
	}

	public boolean Insert_InToServeur_Condidater(String _Date,String _Id_Etudiant,String _Id_OffreStage,String _URL) ///url
	{                      
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();

		NameValuePair lParamDate=new BasicNameValuePair("PARAM_DATE",_Date);
		NameValuePair lParamIdEtudiant=new BasicNameValuePair("PARAM_ID_ETUDIANT",_Id_Etudiant);
		NameValuePair lParamIdOffreStage=new BasicNameValuePair("PARAM_ID_OFFRESTAGE",_Id_OffreStage);

		lListofParams.add(lParamDate);
		lListofParams.add(lParamIdEtudiant);
		lListofParams.add(lParamIdOffreStage);

		//_ClientHTTP=new ClientHTTP(pContext);
		//boolean lResult=_ClientHTTP.SendToUrl(_URL,lListofParams);

		MyAsyncTask_EcrireServeur _MyAsyncTask_EcrireServeur =new MyAsyncTask_EcrireServeur(pContext, lListofParams, _URL);
		boolean lResult=false;
		try {
			lResult = _MyAsyncTask_EcrireServeur.execute().get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if(lResult)
		{
			Toast.makeText(pContext, " Demande de Permission de Stage Envoyé ", Toast.LENGTH_LONG).show();
			return true;
		}else
		{
			Toast.makeText(pContext, " echec lors de l'envoi de la Permission de Stage ", Toast.LENGTH_LONG).show();
			return false;
		}
	}
	public void Insert_InToServeur_Etudiant_image(String _ID_ETUDIANT,String _UrlImage,String lURL )
	{
		try {
			URLConnection connection;
			connection = new URL(_UrlImage).openConnection();
			String contentType = connection.getHeaderField("Content-Type");
			Log.d("TAg ramzi !!!", contentType);
			if (contentType!=null)
			{
				boolean image = contentType.startsWith("image/");
				if (image==false)
				{
					Toast.makeText(pContext, "URL de l'Image non valide", Toast.LENGTH_LONG).show();
					return;
				}
			}else 
			{
				contentType="";
				Toast.makeText(pContext, "URL de l'Image non valide", Toast.LENGTH_LONG).show();
				return;
			}
		} catch (MalformedURLException e)
		{
			Toast.makeText(pContext, "URL de l'Image non valide", Toast.LENGTH_LONG).show();
			e.printStackTrace();
			return;
		} catch (IOException e) 
		{
			Toast.makeText(pContext, "URL de l'Image non valide", Toast.LENGTH_LONG).show();
			e.printStackTrace();
			return;
		}

		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();
		NameValuePair lParamName=new BasicNameValuePair("PARAM_IMAGE",_UrlImage);
		NameValuePair lParamPhone=new BasicNameValuePair("PARAM_ID",_ID_ETUDIANT);
		lListofParams.add(lParamName);
		lListofParams.add(lParamPhone);
		//ClientHTTP _ClientHTTP = new ClientHTTP(pContext);
		//boolean Resltat_image =_ClientHTTP.SendToUrl(lURL, lListofParams);

		MyAsyncTask_EcrireServeur _MyAsyncTask_EcrireServeur =new MyAsyncTask_EcrireServeur(pContext, lListofParams, lURL);
		boolean Result_image=false;
		try {
			Result_image = _MyAsyncTask_EcrireServeur.execute().get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (Result_image) 
		{			
			Toast.makeText(pContext, "Insertion avec succés", Toast.LENGTH_LONG).show();
		}else
		{
			Toast.makeText(pContext, "échec lors de l'insertion ", Toast.LENGTH_LONG).show();
		}
	}
	//////////////////// DELETTE InTo SERVEUR :
	////////////////////  UPDATE InTo SERVEUR :
	public boolean Update_InToServeur_Binome(String lID_EtudiantActu,String lID_EtudiantDispo,String _URL) ///url
	{                       //// 
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();

		NameValuePair lParamRef=new BasicNameValuePair("PARAM_ID_ETUDIANT_ACTU",lID_EtudiantActu);
		NameValuePair lParamPrix=new BasicNameValuePair("PARAM_ID_ETUDIANT_DISPO",lID_EtudiantDispo);

		lListofParams.add(lParamRef);
		lListofParams.add(lParamPrix);

		//_ClientHTTP=new ClientHTTP(pContext);
		//boolean lResult=_ClientHTTP.SendToUrl(_URL,lListofParams);

		MyAsyncTask_EcrireServeur _MyAsyncTask_EcrireServeur =new MyAsyncTask_EcrireServeur(pContext, lListofParams, _URL);
		boolean lResult=false;
		try {
			lResult = _MyAsyncTask_EcrireServeur.execute().get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if(lResult)
		{
			return true;
		}else
		{
			return false;
		}
	}

	//////////////////// Afficher information du SERVEUR :

	public ArrayList<HashMap<String, String>> Afficher_Futur_Binome(String _Id_cycle,String _Id_filiere,String _NumInscrit,String _URL) /// url...
	{
		ArrayList<HashMap<String, String>> _List = new ArrayList<HashMap<String,String>>();
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();

		NameValuePair lParamCycle=new BasicNameValuePair("PARAM_Id_CYCLE",_Id_cycle);
		NameValuePair lParamFiliere=new BasicNameValuePair("PARAM_Id_FILIERE",_Id_filiere);
		NameValuePair lParamNiveau=new BasicNameValuePair("PARAM_NUM_INSCRIT",_NumInscrit);

		lListofParams.add(lParamCycle);
		lListofParams.add(lParamFiliere);
		lListofParams.add(lParamNiveau);

		//_ClientHTTP=new ClientHTTP(pContext);
		//String lRetour_Connecxion=_ClientHTTP.LireEcrireUrl(_URL, lListofParams);

		MyAsyncTask_LireServeur _MyAsyncTask =new MyAsyncTask_LireServeur(pContext, lListofParams, _URL);
		String lRetour_Connecxion=null;
		try {
			lRetour_Connecxion = _MyAsyncTask.execute().get();
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ExecutionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			JSONArray lArray_Etudiant=new JSONArray(lRetour_Connecxion);

			for (int i = 0; i < lArray_Etudiant.length(); i++)
			{
				JSONObject lObjc_P=lArray_Etudiant.getJSONObject(i);
				HashMap<String, String> _HashMap = new HashMap<String, String>();
				// TABLE Etudiant
				int lID_Etudiant=lObjc_P.getInt(DataBase_Static.ETUDIANT_COL_ID);
				String lNumero_inscrit=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_NUmINSCRIT);
				String lNom_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_NOM);
				String lPrenom_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_PRENOM);

				_HashMap.put("lID_Etudiant", String.valueOf(lID_Etudiant));
				_HashMap.put("lNumero_inscrit", lNumero_inscrit);
				_HashMap.put("lNom_Etudiant", lNom_Etudiant);
				_HashMap.put("lPrenom_Etudiant", lPrenom_Etudiant);
				_List.add(_HashMap);
			}
		} 
		catch(JSONException e)
		{
			Log.i("tagjsonexp",""+e.toString());
		}
		return _List;
	}

	public Integer Verifier_Integer(String valeur)
	{
		if (valeur.equals("null"))
		{
			return null;
		}else
		{
			return Integer.parseInt(valeur);	
		}
	}

}