package package_Foction_DataBase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import package_ClassPersonaliser.Condidater;
import package_ClassPersonaliser.Cycle;
import package_ClassPersonaliser.Departement;
import package_ClassPersonaliser.EncadreurPro;
import package_ClassPersonaliser.Entreprise;
import package_ClassPersonaliser.Etudiant;
import package_ClassPersonaliser.Filiere;
import package_ClassPersonaliser.Niveau;
import package_ClassPersonaliser.OffreStage;
import package_ClassPersonaliser.Stage;
import package_ClassPersonaliser.Travail_PFE;
import package_Creator_DataBase.MySQliteCreator_DataBase;
import package_Utilitaire.ClientHTTP;
import package_Utilitaire.DataBase_Static;
import package_Utilitaire.MyAsyncTask_EcrireServeur;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

import com.example.projet_gestionstage_test.R;

public class MySQliteFonction_Entreprise 
{
	private ClientHTTP _ClientHTTP;
	private SQLiteDatabase _SqLiteDatabase;
	private MySQliteCreator_DataBase _SQliteCreator_ProjetStage;
	private Context pContext;

	public MySQliteFonction_Entreprise(Context pContext)
	{
		this.pContext=pContext;
		_SQliteCreator_ProjetStage=new MySQliteCreator_DataBase(pContext, DataBase_Static.DB_NAME, null, 1);
		openDB();
	}
	public void openDB()
	{
		_SqLiteDatabase=_SQliteCreator_ProjetStage.getWritableDatabase();
	}
	///////////////////// requette SQLITE :
	public ArrayList<HashMap<String, String>> Afficher_Etudiant(int _ID_Etudiant)
	{
		String sql="select etudiant.* ,cycle.*,niveau.* ,filiere.* ,departement.*"
				+" from etudiant ,departement ,filiere ,niveau ,cycle"
				+" where" +
				" etudiant.Id_etudiant = "+_ID_Etudiant+"  AND"
				+" departement.Id_departement=etudiant.id_departement_etudiant AND"
				+" filiere.Id_filiere=etudiant.id_filiere_etudiant AND"
				+" filiere.id_cycle_filiere = cycle.Id_cycle AND"
				+" niveau.id_filiere_niveau=filiere.Id_filiere ;";

		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList_Etudiant = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();

				int ID_Etudiant=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_ID));
				String numInscrit_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_NUmINSCRIT));
				String cin_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_CIN));
				String nom_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_NOM));
				String prenom_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_PRENOM));
				String sexe_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_SEXE));
				String dateNaiss_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_DATENAISS));
				String lieuNaiss_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_LIEUNAISS));
				String numTel_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_NUmTEL));
				String email_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_EMAIL));
				String adresse_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_ADRESSE));
				String codePostal_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_CODePOSTAL));
				String image_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_IMAGE));
				int Id_departement_Etudiant=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_IdDEPARTEMENT));
				String Id_travailPfe_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_IdTRAVAILPFE));
				String Id_etudiant1_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_IdETUDIANT1));
				String Id_document_Etudiant=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_IdDOCUMENT));
				int Id_filiere_Etudiant=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ETUDIANT_COL_IdFILIERE));
				_HashMap.put("ID_Etudiant",String.valueOf( ID_Etudiant));
				_HashMap.put("numInscrit_Etudiant", numInscrit_Etudiant);
				_HashMap.put("cin_Etudiant", cin_Etudiant);
				_HashMap.put("nom_Etudiant", nom_Etudiant);
				_HashMap.put("prenom_Etudiant", prenom_Etudiant);
				_HashMap.put("sexe_Etudiant", sexe_Etudiant);
				_HashMap.put("dateNaiss_Etudiant", dateNaiss_Etudiant);
				_HashMap.put("lieuNaiss_Etudiant", lieuNaiss_Etudiant);
				_HashMap.put("numTel_Etudiant", numTel_Etudiant);
				_HashMap.put("email_Etudiant", email_Etudiant);
				_HashMap.put("adresse_Etudiant", adresse_Etudiant);
				_HashMap.put("codePostal_Etudiant", codePostal_Etudiant);
				_HashMap.put("image_Etudiant", image_Etudiant);
				_HashMap.put("Id_departement_Etudiant", String.valueOf(Id_departement_Etudiant));
				_HashMap.put("Id_travailPfe_Etudiant", Id_travailPfe_Etudiant);
				_HashMap.put("Id_etudiant1_Etudiant", Id_etudiant1_Etudiant);
				_HashMap.put("Id_document_Etudiant", Id_document_Etudiant);
				_HashMap.put("Id_filiere_Etudiant", String.valueOf(Id_filiere_Etudiant));
				String libelle_Departement=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.DEPARTEMENT_COL_LIBELLE));
				_HashMap.put("libelle_Departement", libelle_Departement);
				String num_Niveau=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.NIVEAU_COL_NUM));
				String designation_Niveau=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.NIVEAU_COL_DESIGNATION));
				_HashMap.put("num_Niveau", num_Niveau);
				_HashMap.put("designation_Niveau", designation_Niveau);
				String designation_Filiere=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.FILIERE_COL_DESIGNATION));
				_HashMap.put("designation_Filiere", designation_Filiere);
				int ID_cycle=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.CYCLE_COL_ID));
				String num_Cycle=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.CYCLE_COL_NUM));
				String designation_Cycle=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.CYCLE_COL_DESIGNATION));
				_HashMap.put("ID_cycle", String.valueOf(ID_cycle));
				_HashMap.put("num_Cycle", num_Cycle);
				_HashMap.put("designation_Cycle", designation_Cycle);	

				_ArrayList_Etudiant.add(_HashMap);
			}
		}
		return _ArrayList_Etudiant;
	}


	public ArrayList<HashMap<String, String>> Recherche_EncadreurPro_BoiteReception(String _Cin_EncadreurPro)
	{
		String sql=" SELECT encadreur_pro . * "
				+" FROM encadreur_pro "
				+" WHERE encadreur_pro.ncin_encadreurPro = ?" ;

		String[] args={_Cin_EncadreurPro};
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, args);
		ArrayList<HashMap<String, String>> _ArrayList_Etudiant = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();
				// EncadreurPro
				int ID_EncadreurPro=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ENCADREURPRO_COL_ID));
				String Cin_EncadreurPro=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENCADREURPRO_COL_NCIN));
				String Nom_EncadreurPro=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENCADREURPRO_COL_NOM));
				String Prenom_EncadreurPro=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENCADREURPRO_COL_PRENOM));
				String NumTel_EncadreurPro=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENCADREURPRO_COL_NUmTEL));
				String Email_EncadreurPro=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENCADREURPRO_COL_EMAIL));

				_HashMap.put("ID_EncadreurPro",String.valueOf(ID_EncadreurPro));
				_HashMap.put("Cin_EncadreurPro", Cin_EncadreurPro);
				_HashMap.put("Nom_EncadreurPro",Nom_EncadreurPro);
				_HashMap.put("Prenom_EncadreurPro",Prenom_EncadreurPro);
				_HashMap.put("NumTel_EncadreurPro",NumTel_EncadreurPro);
				_HashMap.put("Email_EncadreurPro",Email_EncadreurPro);

				_ArrayList_Etudiant.add(_HashMap);
			}
		}
		return _ArrayList_Etudiant;
	}	

	public ArrayList<HashMap<String, String>> Afficher_EncadreurPro_BoiteReception(String _ID_Stage)
	{ 
		String sql=" SELECT encadreur_pro . * "
				+" FROM encadreur_pro, stage"
				+" WHERE encadreur_pro.Id_encadreurPro = stage.id_encadreurPro_stage"
				+" AND stage.Id_stage = "+_ID_Stage+" " ;

		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList_Etudiant = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();
				// EncadreurPro
				int ID_EncadreurPro=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ENCADREURPRO_COL_ID));
				String Cin_EncadreurPro=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENCADREURPRO_COL_NCIN));
				String Nom_EncadreurPro=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENCADREURPRO_COL_NOM));
				String Prenom_EncadreurPro=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENCADREURPRO_COL_PRENOM));
				String NumTel_EncadreurPro=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENCADREURPRO_COL_NUmTEL));
				String Email_EncadreurPro=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENCADREURPRO_COL_EMAIL));

				_HashMap.put("ID_EncadreurPro",String.valueOf(ID_EncadreurPro));
				_HashMap.put("Cin_EncadreurPro", Cin_EncadreurPro);
				_HashMap.put("Nom_EncadreurPro",Nom_EncadreurPro);
				_HashMap.put("Prenom_EncadreurPro",Prenom_EncadreurPro);
				_HashMap.put("NumTel_EncadreurPro",NumTel_EncadreurPro);
				_HashMap.put("Email_EncadreurPro",Email_EncadreurPro);

				_ArrayList_Etudiant.add(_HashMap);
			}
		}
		return _ArrayList_Etudiant;
	}	

	public ArrayList<HashMap<String, String>> Afficher_Condidater_BoiteReception()
	{
		String sql="SELECT * FROM condidater ";
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList_Condidater = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();

				String confirmation_Condidater=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.CONDIDATER_COL_CONFIRMATION));
				String date_Condidater=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.CONDIDATER_COL_DATE));
				int Id_etudiant_Condidater=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.CONDIDATER_COL_IdETUDIANT));
				int Id_offreStage_Condidater=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.CONDIDATER_COL_IdOFFRESTAGE));

				_HashMap.put("confirmation_Condidater",confirmation_Condidater);
				_HashMap.put("date_Condidater",date_Condidater);
				_HashMap.put("Id_etudiant_Condidater",String.valueOf(Id_etudiant_Condidater));
				_HashMap.put("Id_offreStage_Condidater",String.valueOf(Id_offreStage_Condidater));

				_ArrayList_Condidater.add(_HashMap);
			}
		}
		return _ArrayList_Condidater;
	}
	public ArrayList<HashMap<String, String>> Afficher_OffreStage_BoiteReception()
	{
		String sql="SELECT DISTINCT offre_stage.* " +
				" FROM offre_stage ,condidater " +
				" where " +
				" condidater.id_offreStage_condidater=offre_stage.Id_offreStage and" +
				" offre_stage.confirmation_offreStage != 0 ;" ;
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList_OffreStage = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();

				int ID_OffreStage=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_ID));
				String nom_OffreStage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_NOM));
				String description_OffreStage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_DESCRIPTION));
				String confirmation_OffreStage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_CONFIRMATION));
				String date_OffreStage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_DATE));
				int cle_entreprise_OffreStage=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_IdENTREPRISE));
				String cle_stage_OffreStage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_IdSTAGE));
				String cle_travailPfe_OffreStage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_IdTRAVAILPFE));
				int cle_filiere_OffreStage=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_IdFILIERE));

				_HashMap.put("ID_OffreStage",String.valueOf(ID_OffreStage));
				_HashMap.put("nom_OffreStage",nom_OffreStage );
				_HashMap.put("description_OffreStage",description_OffreStage );
				_HashMap.put("confirmation_OffreStage",confirmation_OffreStage );
				_HashMap.put("date_OffreStage",date_OffreStage );
				_HashMap.put("cle_entreprise_OffreStage",String.valueOf(cle_entreprise_OffreStage ));
				_HashMap.put("cle_stage_OffreStage",cle_stage_OffreStage);
				_HashMap.put("cle_travailPfe_OffreStage",cle_travailPfe_OffreStage );
				_HashMap.put("cle_filiere_OffreStage",String.valueOf(cle_filiere_OffreStage));

				_ArrayList_OffreStage.add(_HashMap);
			}
		}
		return _ArrayList_OffreStage;
	}
	public ArrayList<HashMap<String, String>> Afficher_OffreStage_DepotStage()
	{
		String sql="SELECT * FROM offre_stage ";
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList_OffreStage = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();

				int ID_OffreStage=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_ID));
				String nom_OffreStage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_NOM));
				String description_OffreStage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_DESCRIPTION));
				String confirmation_OffreStage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_CONFIRMATION));
				String date_OffreStage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_DATE));
				int cle_entreprise_OffreStage=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_IdENTREPRISE));
				String cle_stage_OffreStage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_IdSTAGE));
				String cle_travailPfe_OffreStage=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_IdTRAVAILPFE));
				int cle_filiere_OffreStage=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.OFFRESTAGE_COL_IdFILIERE));

				_HashMap.put("ID_OffreStage",String.valueOf(ID_OffreStage));
				_HashMap.put("nom_OffreStage",nom_OffreStage );
				_HashMap.put("description_OffreStage",description_OffreStage );
				_HashMap.put("confirmation_OffreStage",confirmation_OffreStage );
				_HashMap.put("date_OffreStage",date_OffreStage );
				_HashMap.put("cle_entreprise_OffreStage",String.valueOf(cle_entreprise_OffreStage ));
				_HashMap.put("cle_stage_OffreStage",cle_stage_OffreStage);
				_HashMap.put("cle_travailPfe_OffreStage",cle_travailPfe_OffreStage);
				_HashMap.put("cle_filiere_OffreStage",String.valueOf(cle_filiere_OffreStage));

				_ArrayList_OffreStage.add(_HashMap);
			}
		}
		return _ArrayList_OffreStage;
	}
	public ArrayList<HashMap<String, String>> Afficher_Filiere_DepotStage()
	{
		String sql="SELECT DISTINCT filiere.* FROM filiere , offre_stage " +
				"where " +
				"offre_stage.id_filiere_offreStage=filiere.Id_filiere";
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList_Filiere = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();
				int ID_Filiere=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.FILIERE_COL_ID));
				String designation_Filiere=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.FILIERE_COL_DESIGNATION));
				String cle_cycle_Filiere=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.FILIERE_COL_IdCYCLE));
				_HashMap.put("ID_Filiere",String.valueOf(ID_Filiere));
				_HashMap.put("designation_Filiere",designation_Filiere);
				_HashMap.put("cle_cycle_Filiere",cle_cycle_Filiere);

				_ArrayList_Filiere.add(_HashMap);
			}
		}
		return _ArrayList_Filiere;
	}
	public ArrayList<HashMap<String, String>> Afficher_Filiere()
	{
		String sql="SELECT filiere.* FROM filiere ";
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList_Filiere = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();
				int ID_Filiere=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.FILIERE_COL_ID));
				String designation_Filiere=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.FILIERE_COL_DESIGNATION));
				String cle_cycle_Filiere=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.FILIERE_COL_IdCYCLE));

				_HashMap.put("ID_Filiere",String.valueOf(ID_Filiere));
				_HashMap.put("designation_Filiere",designation_Filiere);
				_HashMap.put("cle_cycle_Filiere",cle_cycle_Filiere);

				_ArrayList_Filiere.add(_HashMap);
			}
		}
		return _ArrayList_Filiere;
	}
	public ArrayList<HashMap<String, String>> Afficher_Entreprise()
	{
		String sql="SELECT * FROM entreprise ";
		Cursor _Cursor=_SqLiteDatabase.rawQuery(sql, null);
		ArrayList<HashMap<String, String>> _ArrayList = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> _HashMap ;
		if (_Cursor.getCount()!=0)
		{
			while (_Cursor.moveToNext()==true)
			{
				_HashMap=new HashMap<String, String>();
				int ID_Entreprise=_Cursor.getInt(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_ID));
				String libelle_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_LIBELLE));
				String raison_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_RAISON));
				String numTel_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_NUmTEL));
				String email_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_EMAIL));
				String siteWeb_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_SITEWEB));
				String login_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_LOGIN));
				String password_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_PASSWORD));
				String logo_Entreprise=_Cursor.getString(_Cursor.getColumnIndex(DataBase_Static.ENTREPRISE_COL_LOGO));

				_HashMap.put("ID_Entreprise",String.valueOf(ID_Entreprise));
				_HashMap.put("libelle_Entreprise",libelle_Entreprise);
				_HashMap.put("raison_Entreprise",raison_Entreprise);
				_HashMap.put("numTel_Entreprise",numTel_Entreprise);
				_HashMap.put("email_Entreprise",email_Entreprise);
				_HashMap.put("siteWeb_Entreprise",siteWeb_Entreprise);
				_HashMap.put("login_Entreprise",login_Entreprise);
				_HashMap.put("password_Entreprise",password_Entreprise);
				_HashMap.put("logo_Entreprise",logo_Entreprise);

				_ArrayList.add(_HashMap);
			}
		}
		return _ArrayList;
	}
	///////////////////// SYNCRONISATION SRVEUR :
	public boolean Synchroniser_Serveur_StageTravailPfe(String _ID_Entreprise,String _URL)
	{
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();
		NameValuePair lParamID_Enterprise=new BasicNameValuePair("PARAM_ID_ENTREPRISE",_ID_Entreprise);
		lListofParams.add(lParamID_Enterprise);
		_ClientHTTP=new ClientHTTP(pContext);
		String lRetour_Connecxion=_ClientHTTP.LireEcrireUrl(_URL, lListofParams);
		try{
			JSONArray lArray_StageTravail=new JSONArray(lRetour_Connecxion);
			for (int i = 0; i < lArray_StageTravail.length(); i++)
			{ 
				JSONObject lObjc_P=lArray_StageTravail.getJSONObject(i);
				// Stage
				int _ID_Stage=lObjc_P.getInt(DataBase_Static.STAGE_COL_ID);
				String _nom_Stage=lObjc_P.getString(DataBase_Static.STAGE_COL_NOM);
				String _date_Stage=lObjc_P.getString(DataBase_Static.STAGE_COL_DATE);
				int _Id_travailPfe_Stage=lObjc_P.getInt(DataBase_Static.STAGE_COL_IdTRAVAILPFE);
				Integer _Id_encadreurPro_Stage=Verifier_Integer(lObjc_P.getString(DataBase_Static.STAGE_COL_IdENCADREUrPRO));
				Integer _Id_offreStage_Stage=Verifier_Integer(lObjc_P.getString(DataBase_Static.STAGE_COL_IdOFFRESTAGE));
				String _description_Stage=lObjc_P.getString(DataBase_Static.STAGE_COL_DESCRIPTION);
				int _Id_entreprise_Stage=lObjc_P.getInt(DataBase_Static.STAGE_COL_IdENTREPRISE);
				Stage _Stage = new Stage(_ID_Stage, _nom_Stage, _description_Stage, _date_Stage, _Id_travailPfe_Stage, _Id_encadreurPro_Stage, _Id_offreStage_Stage, _Id_entreprise_Stage);
				// Travail Pfe
				int _ID_TravailPfe=lObjc_P.getInt(DataBase_Static.TRAVAILPFE_COL_ID);
				String _libelle_TravailPfe=lObjc_P.getString(DataBase_Static.TRAVAILPFE_COL_LIBELLE);
				Integer _Id_enseignant_TravailPfe=Verifier_Integer(lObjc_P.getString(DataBase_Static.TRAVAILPFE_COL_IdENSEIGNANT));
				Integer _Id_soutenance_TravailPfe=Verifier_Integer(lObjc_P.getString(DataBase_Static.TRAVAILPFE_COL_IdSOUTENANCE));
				Travail_PFE _Travail_PFE=new Travail_PFE(_ID_TravailPfe, _libelle_TravailPfe, _Id_enseignant_TravailPfe, _Id_soutenance_TravailPfe);

				InsertInTo_BD_TravailPfe(_Travail_PFE);
				InsertInTo_BD_Stage(_Stage);
			}
		}catch(JSONException e)
		{
			Toast.makeText( pContext , "ERREUR " ,Toast.LENGTH_LONG).show();
			return false;
		}
		return true;
	}

	public boolean Synchroniser_Serveur_EncadreurPro(String _URL)
	{ 
		int x=-1;
		_ClientHTTP=new ClientHTTP(pContext);
		String lRetour_Connecxion=_ClientHTTP.readFromUrl(_URL);

		//		MyAsyncTask_LireServeur _MyAsyncTask =new MyAsyncTask_LireServeur(pContext, lListofParams, _URL);
		//		String lRetour_Connecxion=null;
		//		try {
		//			lRetour_Connecxion = _MyAsyncTask.execute().get();
		//		} catch (InterruptedException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		} catch (ExecutionException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		}

		try {
			JSONArray lArray_Etudiant=new JSONArray(lRetour_Connecxion);
			for (int i = 0; i < lArray_Etudiant.length(); i++)
			{
				JSONObject lObjc_P=lArray_Etudiant.getJSONObject(i);
				// EncadreurPro

				int ID_EncadreurPro=lObjc_P.getInt(DataBase_Static.ENCADREURPRO_COL_ID);
				String Ncin_EncadreurPro=lObjc_P.getString(DataBase_Static.ENCADREURPRO_COL_NCIN);
				String Nom_EncadreurPro=lObjc_P.getString(DataBase_Static.ENCADREURPRO_COL_NOM);
				String Prenom_EncadreurPro=lObjc_P.getString(DataBase_Static.ENCADREURPRO_COL_PRENOM);
				String NumTel_EncadreurPro=lObjc_P.getString(DataBase_Static.ENCADREURPRO_COL_NUmTEL);
				String Email_EncadreurPro=lObjc_P.getString(DataBase_Static.ENCADREURPRO_COL_EMAIL);
				EncadreurPro _EncadreurPro=new EncadreurPro(ID_EncadreurPro, Ncin_EncadreurPro, Nom_EncadreurPro, Prenom_EncadreurPro, NumTel_EncadreurPro, Email_EncadreurPro);
				x=InsertInTo_BD_EncadreurPro(_EncadreurPro);

				if (x==-1)
				{
					Toast.makeText( pContext , "ERREUR Lors de l'Insertion de Donnees " ,Toast.LENGTH_LONG).show();
					return false;
				}
			}
		} 
		catch(JSONException e)
		{
			//Toast.makeText( pContext , "  " ,Toast.LENGTH_LONG).show();
			return false;
		}
		return true;
	}

	public boolean Synchroniser_Serveur_Binome(String _ID_Etudiant,String URL_Connexion)
	{
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();
		NameValuePair lParamID_Enterprise=new BasicNameValuePair("PARAM_ID_ETUDIANT",_ID_Etudiant);
		lListofParams.add(lParamID_Enterprise);
		_ClientHTTP=new ClientHTTP(pContext);
		String lRetour_Connecxion=_ClientHTTP.LireEcrireUrl(URL_Connexion, lListofParams);

		//		MyAsyncTask_LireServeur _MyAsyncTask =new MyAsyncTask_LireServeur(pContext, lListofParams, URL_Connexion);
		//		String lRetour_Connecxion=null;
		//		try {
		//			lRetour_Connecxion = _MyAsyncTask.execute().get();
		//		} catch (InterruptedException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		} catch (ExecutionException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		}


		try{
			JSONArray lArray_Condidater=new JSONArray(lRetour_Connecxion);
			for (int i = 0; i < lArray_Condidater.length(); i++)
			{ 
				JSONObject lObjc_P=lArray_Condidater.getJSONObject(i);

				int ID_Etudiant=lObjc_P.getInt(DataBase_Static.ETUDIANT_COL_ID);
				String numInscrit_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_NUmINSCRIT);
				String cin_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_CIN);
				String nom_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_NOM);
				String prenom_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_PRENOM);
				String sexe_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_SEXE);
				String dateNaiss_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_DATENAISS);
				String lieuNaiss_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_LIEUNAISS);
				String numTel_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_NUmTEL);
				String email_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_EMAIL);
				String adresse_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_ADRESSE);
				String codePostale_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_CODePOSTAL);
				String image_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_IMAGE);
				int cle_departement_Etudiant=lObjc_P.getInt(DataBase_Static.ETUDIANT_COL_IdDEPARTEMENT);
				int cle_filiere_Etudiant=lObjc_P.getInt(DataBase_Static.ETUDIANT_COL_IdFILIERE);
				Integer cle_travailPfe_Etudiant=Verifier_Integer(lObjc_P.getString(DataBase_Static.ETUDIANT_COL_IdTRAVAILPFE));
				Integer cle_etudiant1_Etudiant=Verifier_Integer(lObjc_P.getString(DataBase_Static.ETUDIANT_COL_IdETUDIANT1));
				Integer cle_document_Etudiant=Verifier_Integer(lObjc_P.getString(DataBase_Static.ETUDIANT_COL_IdDOCUMENT));
				Etudiant _Etudiant=new Etudiant(ID_Etudiant, numInscrit_Etudiant, cin_Etudiant, nom_Etudiant, prenom_Etudiant, sexe_Etudiant, dateNaiss_Etudiant, lieuNaiss_Etudiant, numTel_Etudiant, email_Etudiant, adresse_Etudiant, codePostale_Etudiant, image_Etudiant, cle_departement_Etudiant, cle_travailPfe_Etudiant, cle_etudiant1_Etudiant, cle_filiere_Etudiant, cle_document_Etudiant);
				InsertInTo_BD_Etudiant(_Etudiant);
			}
		}catch(JSONException e)
		{
			Toast.makeText( pContext , "ERREUR " ,Toast.LENGTH_LONG).show();
			return false;
		}
		return true;
	}

	public boolean Synchroniser_Serveur_CondidaterEtuDepNivCyc(String _ID_Entreprise,String URL_Connexion)
	{
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();
		NameValuePair lParamID_Enterprise=new BasicNameValuePair("PARAM_ID_ENTREPRISE",_ID_Entreprise);
		lListofParams.add(lParamID_Enterprise);
		_ClientHTTP=new ClientHTTP(pContext);
		String lRetour_Connecxion=_ClientHTTP.LireEcrireUrl(URL_Connexion, lListofParams);

		//		MyAsyncTask_LireServeur _MyAsyncTask =new MyAsyncTask_LireServeur(pContext, lListofParams, URL_Connexion);
		//		String lRetour_Connecxion=null;
		//		try {
		//			lRetour_Connecxion = _MyAsyncTask.execute().get();
		//		} catch (InterruptedException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		} catch (ExecutionException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		}


		try{
			JSONArray lArray_Etudiant=new JSONArray(lRetour_Connecxion);
			for (int i = 0; i < lArray_Etudiant.length(); i++)
			{ 
				JSONObject lObjc_P=lArray_Etudiant.getJSONObject(i);
				///// ETUDIANT
				int ID_Etudiant=lObjc_P.getInt(DataBase_Static.ETUDIANT_COL_ID);
				String numInscrit_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_NUmINSCRIT);
				String cin_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_CIN);
				String nom_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_NOM);
				String prenom_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_PRENOM);
				String sexe_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_SEXE);
				String dateNaiss_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_DATENAISS);
				String lieuNaiss_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_LIEUNAISS);
				String numTel_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_NUmTEL);
				String email_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_EMAIL);
				String adresse_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_ADRESSE);
				String codePostale_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_CODePOSTAL);
				String image_Etudiant=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_IMAGE);
				int cle_departement_Etudiant=lObjc_P.getInt(DataBase_Static.ETUDIANT_COL_IdDEPARTEMENT);
				Integer cle_etudiant1_Etudiant=null;
				int cle_filiere_Etudiant=lObjc_P.getInt(DataBase_Static.ETUDIANT_COL_IdFILIERE);
				Integer cle_travailPfe_Etudiant=Verifier_Integer(lObjc_P.getString(DataBase_Static.ETUDIANT_COL_IdTRAVAILPFE));
				boolean cle_etudiant1_Test=lObjc_P.getString(DataBase_Static.ETUDIANT_COL_IdETUDIANT1).equals("null");
				Integer cle_document_Etudiant=Verifier_Integer(lObjc_P.getString(DataBase_Static.ETUDIANT_COL_IdDOCUMENT));
				if (cle_etudiant1_Test==false)
				{
					cle_etudiant1_Etudiant=lObjc_P.getInt(DataBase_Static.ETUDIANT_COL_IdETUDIANT1);
					Synchroniser_Serveur_Binome(String.valueOf(cle_etudiant1_Etudiant), pContext.getResources().getString(R.string.EntrepriseActivity_import_CondidaterBinome));
				}
				Etudiant _Classe_Etudiant=new Etudiant(ID_Etudiant, numInscrit_Etudiant, cin_Etudiant, nom_Etudiant, prenom_Etudiant, sexe_Etudiant, dateNaiss_Etudiant, lieuNaiss_Etudiant, numTel_Etudiant, email_Etudiant, adresse_Etudiant, codePostale_Etudiant, image_Etudiant, cle_departement_Etudiant, cle_travailPfe_Etudiant, cle_etudiant1_Etudiant, cle_filiere_Etudiant, cle_document_Etudiant);
				////// DEPARTEMENT
				int ID_Departement=lObjc_P.getInt(DataBase_Static.DEPARTEMENT_COL_ID);
				String libelle_Departement=lObjc_P.getString(DataBase_Static.DEPARTEMENT_COL_LIBELLE);
				String abreviation_Departement=lObjc_P.getString(DataBase_Static.DEPARTEMENT_COL_ABREVIATION);
				String ordre_Departement=lObjc_P.getString(DataBase_Static.DEPARTEMENT_COL_ORDRE);
				Departement _Classe_Departement = new Departement(ID_Departement, libelle_Departement, abreviation_Departement, ordre_Departement);
				///// NIVEAU
				int ID_Niveau=lObjc_P.getInt(DataBase_Static.NIVEAU_COL_ID);
				String num_Niveau=lObjc_P.getString(DataBase_Static.NIVEAU_COL_NUM);
				String designation_Niveau=lObjc_P.getString(DataBase_Static.NIVEAU_COL_DESIGNATION);
				Integer cle_filiere_Niveau=lObjc_P.getInt(DataBase_Static.NIVEAU_COL_IdFiliere);
				Niveau _Classe_Niveau=new Niveau(ID_Niveau, num_Niveau, designation_Niveau, null, null, cle_filiere_Niveau);
				///// CYCLE
				int ID_Cycle=lObjc_P.getInt(DataBase_Static.CYCLE_COL_ID);
				String num_Cycle=lObjc_P.getString(DataBase_Static.CYCLE_COL_NUM);
				String designation_Cycle=lObjc_P.getString(DataBase_Static.CYCLE_COL_DESIGNATION);
				Cycle _Classe_Cycle= new Cycle(ID_Cycle, num_Cycle, designation_Cycle, null, null);
				////// Condidater
				String confirmation_Condidater=lObjc_P.getString(DataBase_Static.CONDIDATER_COL_CONFIRMATION);
				String date_Condidater=lObjc_P.getString(DataBase_Static.CONDIDATER_COL_DATE);
				int Id_etudiant_Condidater=lObjc_P.getInt(DataBase_Static.CONDIDATER_COL_IdETUDIANT);
				int Id_offreStage_Condidater=lObjc_P.getInt(DataBase_Static.CONDIDATER_COL_IdOFFRESTAGE);			
				Condidater _Condidater = new Condidater(confirmation_Condidater, date_Condidater, Id_etudiant_Condidater, Id_offreStage_Condidater);
				///// insertion into DATA_BASE
				InsertInTo_BD_Etudiant(_Classe_Etudiant);
				InsertInTo_BD_Departement(_Classe_Departement);
				InsertInTo_BD_Niveau(_Classe_Niveau);
				InsertInTo_BD_Cycle(_Classe_Cycle);				

				if (InsertInTo_BD_Condidater(_Condidater)==-1)
				{
					Toast.makeText( pContext , "ERREUR Lors de l'Insertion de Données " ,Toast.LENGTH_LONG).show();
					return false;
				}
			}
		}catch(JSONException e)
		{
			Toast.makeText( pContext , " Vous n'avez Reçue aucun message dans la Boite de Réception ",Toast.LENGTH_LONG ).show();
			return false;
		}
		return true;
	}

	public boolean Synchroniser_Serveur_OffreStage(String _ID_Entreprise,String URL_Connexion)
	{

		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();
		NameValuePair lParamID_Enterprise=new BasicNameValuePair("PARAM_ID_ENTREPRISE",_ID_Entreprise);
		lListofParams.add(lParamID_Enterprise);
		_ClientHTTP=new ClientHTTP(pContext);
		String lRetour_Connecxion=_ClientHTTP.LireEcrireUrl(URL_Connexion, lListofParams);

		//		MyAsyncTask_LireServeur _MyAsyncTask =new MyAsyncTask_LireServeur(pContext, lListofParams, URL_Connexion);
		//		String lRetour_Connecxion=null;
		//		try {
		//			lRetour_Connecxion = _MyAsyncTask.execute().get();
		//		} catch (InterruptedException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		} catch (ExecutionException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		}


		try{
			JSONArray lArray_OffreStage=new JSONArray(lRetour_Connecxion);
			for (int i = 0; i < lArray_OffreStage.length(); i++)
			{ 
				JSONObject lObjc_P=lArray_OffreStage.getJSONObject(i);

				int lID_OffreStage=lObjc_P.getInt(DataBase_Static.OFFRESTAGE_COL_ID);
				String nom_OffreStage=lObjc_P.getString(DataBase_Static.OFFRESTAGE_COL_NOM);
				String description_OffreStage=lObjc_P.getString(DataBase_Static.OFFRESTAGE_COL_DESCRIPTION);
				String confirmation_OffreStage=lObjc_P.getString(DataBase_Static.OFFRESTAGE_COL_CONFIRMATION);
				String date_OffreStage=lObjc_P.getString(DataBase_Static.OFFRESTAGE_COL_DATE);
				int Id_entreprise_OffreStage=lObjc_P.getInt(DataBase_Static.OFFRESTAGE_COL_IdENTREPRISE);
				Integer Id_stage_OffreStage=Verifier_Integer(lObjc_P.getString(DataBase_Static.OFFRESTAGE_COL_IdSTAGE));
				Integer Id_travailPfe_OffreStage=Verifier_Integer(lObjc_P.getString(DataBase_Static.OFFRESTAGE_COL_IdTRAVAILPFE));
				int  Id_filiere_OffreStage=lObjc_P.getInt(DataBase_Static.OFFRESTAGE_COL_IdFILIERE);

				OffreStage _OffreStage = new OffreStage(lID_OffreStage, nom_OffreStage, description_OffreStage, confirmation_OffreStage, date_OffreStage, Id_entreprise_OffreStage, Id_stage_OffreStage, Id_travailPfe_OffreStage, Id_filiere_OffreStage);
				long x=InsertInTo_BD_OffreStage(_OffreStage);
				if (x==-1)
				{
					Toast.makeText( pContext , "ERREUR Lors de l'Insertion de Données " ,Toast.LENGTH_LONG).show();
					return false;
				}
			}
		}catch(JSONException e)
		{
			Toast.makeText( pContext , " Vous n'avez Deposer Aucune Offre De Stage " ,Toast.LENGTH_LONG).show();
			return false;
		}
		return true;
	}

	public boolean Synchroniser_Serveur_Filiere(String URL_Connexion)
	{
		int x=-1;
		_ClientHTTP=new ClientHTTP(pContext);
		String lRetour_Connecxion=_ClientHTTP.readFromUrl(URL_Connexion);

		//		MyAsyncTask_AfficherServeur _MyAsyncTask =new MyAsyncTask_AfficherServeur(pContext, URL_Connexion);
		//		String lRetour_Connecxion=null;
		//		try {
		//			lRetour_Connecxion = _MyAsyncTask.execute().get();
		//		} catch (InterruptedException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		} catch (ExecutionException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		}


		try{
			JSONArray lArray_Filiere=new JSONArray(lRetour_Connecxion);
			for (int i = 0; i < lArray_Filiere.length(); i++)
			{ 
				JSONObject lObjc_P=lArray_Filiere.getJSONObject(i);

				int ID_Filiere=lObjc_P.getInt(DataBase_Static.FILIERE_COL_ID);
				String designation_Filiere =lObjc_P.getString(DataBase_Static.FILIERE_COL_DESIGNATION);
				Integer cle_cycle_Filiere=Verifier_Integer(lObjc_P.getString(DataBase_Static.FILIERE_COL_IdCYCLE));

				Filiere _Filiere=new Filiere(ID_Filiere, designation_Filiere, cle_cycle_Filiere);
				x=InsertInTo_BD_Filiere(_Filiere);
				if (x==-1)
				{
					Toast.makeText( pContext , "ERREUR Lors de l'Insertion de Données " ,Toast.LENGTH_LONG).show();
					return false;
				}
			}
		}catch(JSONException e)
		{
			return false;
		}
		return true;
	}

	public boolean Synchroniser_Serveur_Entreprise(String login,String password, String id_enseignant ,String URL_Connexion)
	{
		int x=-1;

		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();
		NameValuePair lParamLogin=new BasicNameValuePair("PARAM_LOGIN",login);
		NameValuePair lParamPssword=new BasicNameValuePair("PARAM_PASSWORD",password); 
		NameValuePair lParamEnseignant=new BasicNameValuePair("PARAM_ID_ENSEIGNANT",id_enseignant);

		lListofParams.add(lParamLogin);
		lListofParams.add(lParamPssword);
		lListofParams.add(lParamEnseignant);

		_ClientHTTP=new ClientHTTP(pContext);
		String lRetour_Connecxion=_ClientHTTP.LireEcrireUrl(URL_Connexion, lListofParams);
		//		MyAsyncTask_LireServeur _MyAsyncTask =new MyAsyncTask_LireServeur(pContext, lListofParams, URL_Connexion);
		//		String lRetour_Connecxion=null;
		//		try {
		//			lRetour_Connecxion = _MyAsyncTask.execute().get();
		//		} catch (InterruptedException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		} catch (ExecutionException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		}

		try{
			JSONArray lArray_Departement=new JSONArray(lRetour_Connecxion);
			for (int i = 0; i < lArray_Departement.length(); i++)
			{ 
				JSONObject lObjc_P=lArray_Departement.getJSONObject(i);

				int lID_Entreprise=lObjc_P.getInt(DataBase_Static.ENTREPRISE_COL_ID);
				String libelle_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_LIBELLE);
				String raison_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_RAISON);
				String numTel_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_NUmTEL);
				String email_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_EMAIL);
				String siteWeb_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_SITEWEB);
				String login_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_LOGIN);
				String password_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_PASSWORD);
				String logo_Entreprise=lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_LOGO);
				Integer Id_enseignant_Entreprise=Verifier_Integer(lObjc_P.getString(DataBase_Static.ENTREPRISE_COL_IdENSEIGNANT));
				Entreprise _Entreprise =new Entreprise(lID_Entreprise, libelle_Entreprise, raison_Entreprise, numTel_Entreprise, email_Entreprise, siteWeb_Entreprise, login_Entreprise, password_Entreprise, logo_Entreprise,Id_enseignant_Entreprise);
				x=InsertInTo_BD_Entreprise(_Entreprise);
				if (x==-1 && id_enseignant.equals(null))
				{
					Toast.makeText( pContext , "ERREUR Lors de l'Insertion de Données " ,Toast.LENGTH_LONG).show();
					return false;
				}
			}
		}catch(JSONException e)
		{
			if (id_enseignant.equals(null))//(id_enseignant.equals(null))
			{
				Toast.makeText( pContext , "ERREUR Login ou Password non valide " ,Toast.LENGTH_LONG).show();
			}
			return false;
		}

		return true;
	}
	//////////////////// INSERT INTO BD :
	public int InsertInTo_BD_Stage(Stage _Stage)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.STAGE_COL_ID,_Stage.getId_stage());
		lContentValues.put(DataBase_Static.STAGE_COL_NOM,_Stage.getNom_stage());
		lContentValues.put(DataBase_Static.STAGE_COL_DATE,_Stage.getDate_stage());
		lContentValues.put(DataBase_Static.STAGE_COL_IdTRAVAILPFE,_Stage.getCle_travailPfe_stage());
		lContentValues.put(DataBase_Static.STAGE_COL_IdENCADREUrPRO,_Stage.getCle_encadreurPro_stage());
		lContentValues.put(DataBase_Static.STAGE_COL_IdOFFRESTAGE,_Stage.getCle_offreStage_stage());
		lContentValues.put(DataBase_Static.STAGE_COL_DESCRIPTION,_Stage.getDescription_stage());
		lContentValues.put(DataBase_Static.STAGE_COL_IdENTREPRISE,_Stage.getCle_entreprise_stage());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_STAGE, null, lContentValues);
		return (int) x;
	}
	public int InsertInTo_BD_TravailPfe(Travail_PFE _Travail_PFE)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.TRAVAILPFE_COL_ID,_Travail_PFE.getID_TravailPFE());
		lContentValues.put(DataBase_Static.TRAVAILPFE_COL_LIBELLE,_Travail_PFE.getLibelle_TravailPFE());
		lContentValues.put(DataBase_Static.TRAVAILPFE_COL_IdENSEIGNANT,_Travail_PFE.getCle_enseignant_TravailPFE());
		lContentValues.put(DataBase_Static.TRAVAILPFE_COL_IdSOUTENANCE,_Travail_PFE.getCle_soutenance_TravailPFE());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_TRAVAILPFE, null, lContentValues);
		return (int) x;
	}
	public int InsertInTo_BD_EncadreurPro(EncadreurPro _EncadreurPro )
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.ENCADREURPRO_COL_ID,_EncadreurPro.getId_encadreurPro());
		lContentValues.put(DataBase_Static.ENCADREURPRO_COL_NCIN,_EncadreurPro.getCin_encadreurPro());
		lContentValues.put(DataBase_Static.ENCADREURPRO_COL_NOM,_EncadreurPro.getNom_encadreurPro());
		lContentValues.put(DataBase_Static.ENCADREURPRO_COL_PRENOM,_EncadreurPro.getPrenom_encadreurPro());
		lContentValues.put(DataBase_Static.ENCADREURPRO_COL_NUmTEL,_EncadreurPro.getNumTel_encadreurPro());
		lContentValues.put(DataBase_Static.ENCADREURPRO_COL_EMAIL,_EncadreurPro.getEmail_encadreurPro());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_ENCADREUR_PRO, null, lContentValues);
		return (int) x;
	}
	public int InsertInTo_BD_Cycle(Cycle _Class_Cycle)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.CYCLE_COL_ID, _Class_Cycle.getId_cycle());
		lContentValues.put(DataBase_Static.CYCLE_COL_NUM,_Class_Cycle.getNum_cycle());
		lContentValues.put(DataBase_Static.CYCLE_COL_DESIGNATION,_Class_Cycle.getDesignation_cycle());
		lContentValues.put(DataBase_Static.CYCLE_COL_NBrSEM,_Class_Cycle.getNbrSem_cycle());
		lContentValues.put(DataBase_Static.CYCLE_COL_NBrFILIERE,_Class_Cycle.getNbrFiliere_cycle());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_CYCLE, null, lContentValues);
		return (int) x;
	}
	public int InsertInTo_BD_Departement(Departement _Class_Departement)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.DEPARTEMENT_COL_ID,_Class_Departement.getId_departement());
		lContentValues.put(DataBase_Static.DEPARTEMENT_COL_LIBELLE,_Class_Departement.getLibelle_Departement());
		lContentValues.put(DataBase_Static.DEPARTEMENT_COL_ABREVIATION,_Class_Departement.getAbreviation_Departement());
		lContentValues.put(DataBase_Static.DEPARTEMENT_COL_ORDRE,_Class_Departement.getOrdre_Departement());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_DEPARTEMENT, null, lContentValues);
		return (int) x;
	}
	public int  InsertInTo_BD_Niveau(Niveau _Class_Niveau)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.NIVEAU_COL_ID,_Class_Niveau.getId_niveau());
		lContentValues.put(DataBase_Static.NIVEAU_COL_NUM,_Class_Niveau.getNum_niveau());
		lContentValues.put(DataBase_Static.NIVEAU_COL_DESIGNATION,_Class_Niveau.getDesignation_niveau());
		lContentValues.put(DataBase_Static.NIVEAU_COL_NbMODULE,_Class_Niveau.getNbModule_niveau());
		lContentValues.put(DataBase_Static.NIVEAU_COL_NbSECTION,_Class_Niveau.getNbSection_niveau());
		lContentValues.put(DataBase_Static.NIVEAU_COL_IdFiliere,_Class_Niveau.getCle_filiere_niveau());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_NIVEAU, null, lContentValues);
		return (int) x;
	}
	public int InsertInTo_BD_Condidater(Condidater _Condidater)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.CONDIDATER_COL_CONFIRMATION,_Condidater.getConfirmaiton_condidater());
		lContentValues.put(DataBase_Static.CONDIDATER_COL_DATE,_Condidater.getDate_condidater());
		lContentValues.put(DataBase_Static.CONDIDATER_COL_IdETUDIANT,_Condidater.getId_etudiant_condidater());
		lContentValues.put(DataBase_Static.CONDIDATER_COL_IdOFFRESTAGE,_Condidater.getId_offreStage_condidater());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_CONDIDATER, null, lContentValues);
		return (int) x;
	}
	public int InsertInTo_BD_Etudiant(Etudiant _Class_Etudiant)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.ETUDIANT_COL_ID,_Class_Etudiant.getId_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_NUmINSCRIT,_Class_Etudiant.getNum_inscrit());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_CIN,_Class_Etudiant.getCin_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_NOM,_Class_Etudiant.getNom_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_PRENOM,_Class_Etudiant.getPrenom_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_SEXE,_Class_Etudiant.getSexe_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_DATENAISS,_Class_Etudiant.getDateNaiss_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_LIEUNAISS,_Class_Etudiant.getLieuNaiss_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_NUmTEL,_Class_Etudiant.getNumTel_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_EMAIL,_Class_Etudiant.getEmail_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_ADRESSE,_Class_Etudiant.getAdresse_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_CODePOSTAL,_Class_Etudiant.getCodePostal_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_IMAGE,_Class_Etudiant.getImage_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_IdDEPARTEMENT,_Class_Etudiant.getCle_departement_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_IdTRAVAILPFE,_Class_Etudiant.getCle_travailPfe_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_IdETUDIANT1,_Class_Etudiant.getCle_etudiant1_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_IdFILIERE,_Class_Etudiant.getCle_filiere_etudiant());
		lContentValues.put(DataBase_Static.ETUDIANT_COL_IdDOCUMENT,_Class_Etudiant.getCle_document_etudiant());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_ETUDIANT, null, lContentValues);
		return (int) x;
	}
	public long InsertInTo_BD_OffreStage(OffreStage _OffreStage)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.OFFRESTAGE_COL_ID,_OffreStage.getId_offreStage());
		lContentValues.put(DataBase_Static.OFFRESTAGE_COL_NOM,_OffreStage.getNom_offreStage());
		lContentValues.put(DataBase_Static.OFFRESTAGE_COL_DESCRIPTION,_OffreStage.getDescription_offreStage());
		lContentValues.put(DataBase_Static.OFFRESTAGE_COL_CONFIRMATION,_OffreStage.getConfirmation_offreStage());
		lContentValues.put(DataBase_Static.OFFRESTAGE_COL_DATE,_OffreStage.getDate_offreStage());
		lContentValues.put(DataBase_Static.OFFRESTAGE_COL_IdENTREPRISE,_OffreStage.getCle_entreprise_offreStage());
		lContentValues.put(DataBase_Static.OFFRESTAGE_COL_IdSTAGE,_OffreStage.getCle_stage_offreStage());
		lContentValues.put(DataBase_Static.OFFRESTAGE_COL_IdTRAVAILPFE,_OffreStage.getCle_travailPfe_offreStage());
		lContentValues.put(DataBase_Static.OFFRESTAGE_COL_IdFILIERE,_OffreStage.getCle_filiere_offreStage());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_OFFRESTAGE, null, lContentValues);
		return  x;
	}
	public int InsertInTo_BD_Filiere(Filiere _Filiere)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.FILIERE_COL_ID,_Filiere.getId_filiere());
		lContentValues.put(DataBase_Static.FILIERE_COL_DESIGNATION,_Filiere.getDesignation_filiere());
		lContentValues.put(DataBase_Static.FILIERE_COL_IdCYCLE,_Filiere.getCle_cycle_filiere());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_FILIERE, null, lContentValues);
		return (int) x;
	}
	public int InsertInTo_BD_Entreprise(Entreprise _Entreprise)
	{
		ContentValues lContentValues = new ContentValues();

		lContentValues.put(DataBase_Static.ENTREPRISE_COL_ID,_Entreprise.getId_entreprise());
		lContentValues.put(DataBase_Static.ENTREPRISE_COL_LIBELLE,_Entreprise.getLibelle_entreprise());
		lContentValues.put(DataBase_Static.ENTREPRISE_COL_RAISON,_Entreprise.getRaison_entreprise());
		lContentValues.put(DataBase_Static.ENTREPRISE_COL_NUmTEL,_Entreprise.getNumTel_entreprise());
		lContentValues.put(DataBase_Static.ENTREPRISE_COL_EMAIL,_Entreprise.getEmail_entreprise());
		lContentValues.put(DataBase_Static.ENTREPRISE_COL_SITEWEB,_Entreprise.getSiteWeb_entreprise());
		lContentValues.put(DataBase_Static.ENTREPRISE_COL_LOGIN,_Entreprise.getLogin_entreprise());
		lContentValues.put(DataBase_Static.ENTREPRISE_COL_PASSWORD,_Entreprise.getPassword_entreprise());
		lContentValues.put(DataBase_Static.ENTREPRISE_COL_LOGO,_Entreprise.getLogo_entreprise());
		lContentValues.put(DataBase_Static.ENTREPRISE_COL_IdENSEIGNANT,_Entreprise.getCle_enseignant_entreprise());

		long x=_SqLiteDatabase.insert(DataBase_Static.TABLE_ENTREPRISE, null, lContentValues);
		return (int) x;
	}

	//////////////////// SUPPRIMER CONTENUE TABLE :
	//mettre dans authentification
	public void clearAll_Stage()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_STAGE);
	}
	public void clearAll_TravailPfe()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_TRAVAILPFE);
	}
	public void clearAll_EncadreurPro()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_ENCADREUR_PRO);
	}
	public void ClearAll_OffreStage()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_OFFRESTAGE);
	}
	public void ClearAll_Entreprise()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_ENTREPRISE);
	}
	public void  ClearAll_Filiere()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_FILIERE);
	}
	public void  ClearAll_Condidater()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_CONDIDATER);
	}
	public void  ClearAll_Etudiant()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_ETUDIANT);
	}
	public void ClearAll_Departement()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_DEPARTEMENT);
	}
	public void ClearAll_Niveau()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_NIVEAU);
	}
	public void ClearAll_Cycle()
	{
		_SqLiteDatabase.execSQL("DELETE FROM "+DataBase_Static.TABLE_CYCLE);
	}
	/////////////////////Insertion INTO SERVEUR :
	public boolean InsertInTo_Serveur_OffreStage(String _nom,String _description,String _date,int _cle_Entreprise,int _cle_Filiere,String _URL)
	{
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();

		NameValuePair lParamNom=new BasicNameValuePair("PARAM_NOM",_nom);
		NameValuePair lParamDescription=new BasicNameValuePair("PARAM_DESCRIPTION",_description);
		NameValuePair lParamDate=new BasicNameValuePair("PARAM_DATE",_date);
		NameValuePair lParamIdEntreprise=new BasicNameValuePair("PARAM_Id_ENTREPRISE",String.valueOf(_cle_Entreprise));
		NameValuePair lParamIdFiliere=new BasicNameValuePair("PARAM_Id_FILIERE",String.valueOf(_cle_Filiere));

		lListofParams.add(lParamNom);
		lListofParams.add(lParamDescription);
		lListofParams.add(lParamDate);
		lListofParams.add(lParamIdEntreprise);
		lListofParams.add(lParamIdFiliere);

		//_ClientHTTP=new ClientHTTP(pContext);
		//boolean lResult=_ClientHTTP.SendToUrl(_URL,lListofParams);
		MyAsyncTask_EcrireServeur _MyAsyncTask_EcrireServeur =new MyAsyncTask_EcrireServeur(pContext, lListofParams, _URL);
		boolean lResult=false;
		try {
			lResult = _MyAsyncTask_EcrireServeur.execute().get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		if(lResult)
		{
			Toast.makeText(pContext, " Insérer avec succès ", Toast.LENGTH_LONG).show();
			return true;
		}else
		{
			Toast.makeText(pContext, " échec lors de l'initialisation ", Toast.LENGTH_SHORT).show();
			return false;
		}
	}

	public boolean InsertInTo_Serveur_Entreprise(String _libelle,String _raison,String _numTel,String _email,String _siteWeb,String _login,String _password,String _URL ) ///url
	{                     
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();

		NameValuePair lParamLibelle=new BasicNameValuePair("PARAM_LIBELLE",_libelle);
		NameValuePair lParamRaison=new BasicNameValuePair("PARAM_RAISON",_raison);
		NameValuePair lParamTelephone=new BasicNameValuePair("PARAM_TELEPHONE",_numTel);
		NameValuePair lParamEmail=new BasicNameValuePair("PARAM_EMAIL",_email);
		NameValuePair lParamSiteWeb=new BasicNameValuePair("PARAM_WEB",_siteWeb);
		NameValuePair lParamLogin=new BasicNameValuePair("PARAM_LOGIN",_login);
		NameValuePair lParamPassword=new BasicNameValuePair("PARAM_PASSWORD",_password);

		lListofParams.add(lParamLibelle);
		lListofParams.add(lParamRaison);
		lListofParams.add(lParamTelephone);
		lListofParams.add(lParamEmail);
		lListofParams.add(lParamSiteWeb);
		lListofParams.add(lParamLogin);
		lListofParams.add(lParamPassword);

		//_ClientHTTP=new ClientHTTP(pContext);
		//boolean lResult=_ClientHTTP.SendToUrl(_URL,lListofParams);
		MyAsyncTask_EcrireServeur _MyAsyncTask_EcrireServeur =new MyAsyncTask_EcrireServeur(pContext, lListofParams, _URL);
		boolean lResult=false;
		try {
			lResult = _MyAsyncTask_EcrireServeur.execute().get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		if(lResult)
		{
			Toast.makeText(pContext, " Insérer avec succès ", Toast.LENGTH_LONG).show();
			return true;
		}else
		{
			Toast.makeText(pContext, " échec lors de l'initialisation ", Toast.LENGTH_SHORT).show();
			return false;
		}

	}
	//////////////////// DELETTE InTo SERVEUR :
	public boolean DeleteInTo_Serveur_OffreStage(String _ID_OffreStage,String _URL )
	{                     
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();

		NameValuePair lParamID=new BasicNameValuePair("PARAM_ID", _ID_OffreStage );

		lListofParams.add(lParamID);
		//_ClientHTTP=new ClientHTTP(pContext);
		//boolean lResult=_ClientHTTP.SendToUrl(_URL,lListofParams);
		MyAsyncTask_EcrireServeur _MyAsyncTask_EcrireServeur =new MyAsyncTask_EcrireServeur(pContext, lListofParams, _URL);
		boolean lResult=false;
		try {
			lResult = _MyAsyncTask_EcrireServeur.execute().get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		if(lResult)
		{
			Toast.makeText(pContext, " Supprimer avec succès ", Toast.LENGTH_LONG).show();
			return true;
		}else
		{
			Toast.makeText(pContext, " échec lors de la Suppression ", Toast.LENGTH_SHORT).show();
			return false;
		}
	}

	//////////////////// UPDATE InTo SERVEUR:
	public boolean UpdateInTo_Serveur_CondidaterOffreStage(String _confirmation,int _Id_etudiant,int _Id_offrestage,String _URL )
	{                     
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();

		NameValuePair lParamConfirmation=new BasicNameValuePair("PARAM_CONFIRMATION",_confirmation);
		NameValuePair lParamEtudiant=new BasicNameValuePair("PARAM_ID_ETUDIANT",String.valueOf(_Id_etudiant));
		NameValuePair lParamOffreStage=new BasicNameValuePair("PARAM_ID_OFFRESTAGE",String.valueOf(_Id_offrestage));
		lListofParams.add(lParamConfirmation);
		lListofParams.add(lParamEtudiant);
		lListofParams.add(lParamOffreStage);

		//_ClientHTTP=new ClientHTTP(pContext);
		//boolean lResult=_ClientHTTP.SendToUrl(_URL,lListofParams);
		MyAsyncTask_EcrireServeur _MyAsyncTask_EcrireServeur =new MyAsyncTask_EcrireServeur(pContext, lListofParams, _URL);
		boolean lResult=false;
		try {
			lResult = _MyAsyncTask_EcrireServeur.execute().get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		if(lResult)
		{
			Toast.makeText(pContext, " Modifier avec succès ", Toast.LENGTH_LONG).show();
			return true;
		}else
		{
			Toast.makeText(pContext, " échec lors des Modifications ", Toast.LENGTH_SHORT).show();
			return false;
		}
	}
	public boolean UpdateInTo_Serveur_Condidater(String _confirmation,int _Id_etudiant,int _Id_offrestage,String _URL )
	{                     
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();

		NameValuePair lParamConfirmation=new BasicNameValuePair("PARAM_CONFIRMATION",_confirmation);
		NameValuePair lParamEtudiant=new BasicNameValuePair("PARAM_ID_ETUDIANT",String.valueOf(_Id_etudiant));
		NameValuePair lParamOffreStage=new BasicNameValuePair("PARAM_ID_OFFRESTAGE",String.valueOf(_Id_offrestage));
		lListofParams.add(lParamConfirmation);
		lListofParams.add(lParamEtudiant);
		lListofParams.add(lParamOffreStage);

		//_ClientHTTP=new ClientHTTP(pContext);
		//boolean lResult=_ClientHTTP.SendToUrl(_URL,lListofParams);
		MyAsyncTask_EcrireServeur _MyAsyncTask_EcrireServeur =new MyAsyncTask_EcrireServeur(pContext, lListofParams, _URL);
		boolean lResult=false;
		try {
			lResult = _MyAsyncTask_EcrireServeur.execute().get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		if(lResult)
		{
			Toast.makeText(pContext, " Modifier avec succès ", Toast.LENGTH_LONG).show();
			return true;
		}else
		{
			Toast.makeText(pContext, " échec lors des Modifications ", Toast.LENGTH_SHORT).show();
			return false;
		}
	}
	public boolean UpdateInTo_Serveur_OffreStage(int _ID_offrestage,String _nom,String _description,String _date,int _Id_filiere,String _URL )
	{                     
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();

		NameValuePair lParamNom=new BasicNameValuePair("PARAM_NOM",_nom);
		NameValuePair lParamDescription=new BasicNameValuePair("PARAM_DESCRIPTION",_description);
		NameValuePair lParamDate=new BasicNameValuePair("PARAM_DATE",_date);
		NameValuePair lParamID=new BasicNameValuePair("PARAM_ID",String.valueOf(_ID_offrestage));
		NameValuePair lParamFiliere=new BasicNameValuePair("PARAM_Id_FILIERE",String.valueOf(_Id_filiere));
		lListofParams.add(lParamNom);
		lListofParams.add(lParamDescription);
		lListofParams.add(lParamDate);
		lListofParams.add(lParamID);
		lListofParams.add(lParamFiliere);

		//_ClientHTTP=new ClientHTTP(pContext);
		//boolean lResult=_ClientHTTP.SendToUrl(_URL,lListofParams);
		MyAsyncTask_EcrireServeur _MyAsyncTask_EcrireServeur =new MyAsyncTask_EcrireServeur(pContext, lListofParams, _URL);
		boolean lResult=false;
		try {
			lResult = _MyAsyncTask_EcrireServeur.execute().get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		if(lResult)
		{
			Toast.makeText(pContext, " Modifier avec succès ", Toast.LENGTH_LONG).show();
			return true;
		}else
		{
			Toast.makeText(pContext, " échec lors des Modifications ", Toast.LENGTH_SHORT).show();
			return false;
		}
	}
	public boolean UpdateInTo_Serveur_Entreprise(String _ID_Entreprise ,String _libelle,String _raison,String _numTel,String _email,String _siteWeb,String _login,String _password,String _URL )
	{                     
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();

		NameValuePair lParamID=new BasicNameValuePair("PARAM_ID", _ID_Entreprise);
		NameValuePair lParamLibelle=new BasicNameValuePair("PARAM_LIBELLE",_libelle);
		NameValuePair lParamRaison=new BasicNameValuePair("PARAM_RAISON",_raison);
		NameValuePair lParamTelephone=new BasicNameValuePair("PARAM_TELEPHONE",_numTel);
		NameValuePair lParamEmail=new BasicNameValuePair("PARAM_EMAIL",_email);
		NameValuePair lParamSiteWeb=new BasicNameValuePair("PARAM_WEB",_siteWeb);
		NameValuePair lParamLogin=new BasicNameValuePair("PARAM_LOGIN",_login);
		NameValuePair lParamPassword=new BasicNameValuePair("PARAM_PASSWORD",_password);

		lListofParams.add(lParamID);
		lListofParams.add(lParamLibelle);
		lListofParams.add(lParamRaison);
		lListofParams.add(lParamTelephone);
		lListofParams.add(lParamEmail);
		lListofParams.add(lParamSiteWeb);
		lListofParams.add(lParamLogin);
		lListofParams.add(lParamPassword);

		//_ClientHTTP=new ClientHTTP(pContext);
		//boolean lResult=_ClientHTTP.SendToUrl(_URL,lListofParams);
		MyAsyncTask_EcrireServeur _MyAsyncTask_EcrireServeur =new MyAsyncTask_EcrireServeur(pContext, lListofParams, _URL);
		boolean lResult=false;
		try {
			lResult = _MyAsyncTask_EcrireServeur.execute().get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		if(lResult)
		{
			Toast.makeText(pContext, " Modifier avec succès ", Toast.LENGTH_LONG).show();
			return true;
		}else
		{
			Toast.makeText(pContext, " échec lors des Modifications ", Toast.LENGTH_SHORT).show();
			return false;
		}
	}

	//////////////////// Verif information du SERVEUR :
	public boolean Watch_Serveur_Condidater(String _Id_etudiantCondidater,String _Id_binomeCondidater,String _URL)
	{ 
		boolean ok=false;
		ArrayList<NameValuePair> lListofParams=new ArrayList<NameValuePair>();

		NameValuePair lParamIdEtudiant=new BasicNameValuePair("PARAM_ID_ETUDIANT",_Id_etudiantCondidater);
		NameValuePair lParamIdBinome=new BasicNameValuePair("PARAM_ID_BINOME",_Id_binomeCondidater);

		lListofParams.add(lParamIdEtudiant);
		lListofParams.add(lParamIdBinome);

		_ClientHTTP=new ClientHTTP(pContext);
		String lRetour_Connecxion=_ClientHTTP.LireEcrireUrl(_URL, lListofParams);

		try {
			JSONArray lArray_Etudiant=new JSONArray(lRetour_Connecxion);

			for (int i = 0; i < lArray_Etudiant.length(); i++)
			{
				ok=true;
			}
		} 
		catch(JSONException e)
		{
			Log.i("tagjsonexp",""+e.toString());
		}
		return ok;
	}

	////// Verifier Integer
	public Integer Verifier_Integer(String valeur)
	{
		if (valeur.equals("null"))
		{
			return null;
		}else
		{
			return Integer.parseInt(valeur);	
		}
	}

}