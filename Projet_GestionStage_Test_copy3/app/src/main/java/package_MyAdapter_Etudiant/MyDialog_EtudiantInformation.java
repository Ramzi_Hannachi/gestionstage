package package_MyAdapter_Etudiant;

import java.util.ArrayList;
import java.util.HashMap;

import package_Foction_DataBase.MySQliteFonction_Etudiant;
import package_MemoirStage.Activity_Etudiant;
import package_Utilitaire.ImageManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Bitmap;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.projet_gestionstage_test.R;

public class MyDialog_EtudiantInformation 
{
	private ArrayList<HashMap<String, String>> _ArrayList=new ArrayList<HashMap<String,String>>();
	private MySQliteFonction_Etudiant _MySQliteFonction_Etudiant;
	private ImageManager _ImageManager;
	private  Context _Context;

	public MyDialog_EtudiantInformation(Context _Context,ArrayList<HashMap<String, String>> _ArrayList)
	{
		this._Context=_Context;
		this._ArrayList=_ArrayList;
	}

	public void DialogEudiantInformation()
	{
		String numInscrit=_ArrayList.get(0).get("numInscrit_Etudiant");
		String nom=_ArrayList.get(0).get("nom_Etudiant");
		String prenom=_ArrayList.get(0).get("prenom_Etudiant");
		String sexe =_ArrayList.get(0).get("sexe_Etudiant");
		String dateNaiss=_ArrayList.get(0).get("dateNaiss_Etudiant");
		String lieuNaiss=_ArrayList.get(0).get("lieuNaiss_Etudiant");
		String numTel=_ArrayList.get(0).get("numTel_Etudiant");
		String email=_ArrayList.get(0).get("email_Etudiant");
		String adresse =_ArrayList.get(0).get("adresse_Etudiant");
		String image=_ArrayList.get(0).get("image_Etudiant");
		String libelle_Dep=_ArrayList.get(0).get("libelle_Departement");
		String num_Niv=_ArrayList.get(0).get("num_Niveau");
		String designation_Niv=_ArrayList.get(0).get("designation_Niveau");
		String designation_Fil=_ArrayList.get(0).get("designation_Filiere");
		String num_Cyc=_ArrayList.get(0).get("num_Cycle");
		String designation_cyc=_ArrayList.get(0).get("designation_Cycle");

		Dialog alert = new Dialog(_Context);//R.style.Theme_Transparent
		alert.requestWindowFeature(Window.FEATURE_NO_TITLE);
		alert.setContentView(R.layout.activity_etudiant_dialog_information);
		TextView _nomPrenom = (TextView)alert.findViewById(R.id.Activity_Etudiant_DialogInformation_NomPrenom);
		TextView _numInscrit = (TextView)alert.findViewById(R.id.Activity_Etudiant_DialogInformation_NumInscrit);
		TextView _sexe = (TextView)alert.findViewById(R.id.Activity_Etudiant_DialogInformation_Sexe);
		TextView _dateLieuNaiss = (TextView)alert.findViewById(R.id.Activity_Etudiant_DialogInformation_DateLieuNaiss);
		TextView _numTel = (TextView)alert.findViewById(R.id.Activity_Etudiant_DialogInformation_NumTel);
		TextView _email = (TextView)alert.findViewById(R.id.Activity_Etudiant_DialogInformation_Email);
		TextView _adresse = (TextView)alert.findViewById(R.id.Activity_Etudiant_DialogInformation_Adresse);
		TextView _libelleDep = (TextView)alert.findViewById(R.id.Activity_Etudiant_DialogInformation_libelleDep);
		TextView _numDesignationNiv = (TextView)alert.findViewById(R.id.Activity_Etudiant_DialogInformation_NumDesignationNiv);
		TextView _filiere = (TextView)alert.findViewById(R.id.Activity_Etudiant_DialogInformation_Filiere);
		TextView _cycle = (TextView)alert.findViewById(R.id.Activity_Etudiant_DialogInformation_Cycle);
		ImageView _image=(ImageView) alert.findViewById(R.id.Activity_Etudiant_DialogInformation_Image);

		_nomPrenom.setText(nom+" "+prenom);
		_numInscrit.setText(numInscrit);
		_sexe.setText(sexe);
		_dateLieuNaiss.setText(dateNaiss+" "+lieuNaiss);
		_numTel.setText(numTel);
		_email.setText(email);
		_adresse.setText(adresse);
		_libelleDep.setText(libelle_Dep);
		_numDesignationNiv.setText(num_Niv+" "+designation_Niv);
		_filiere.setText(designation_Fil);
		_cycle.setText(num_Cyc+" "+designation_cyc);
		_ImageManager = new ImageManager();
		Bitmap _Bitmap=_ImageManager.TelechargerImage(image);
		_image.setImageBitmap(_Bitmap);
		_image.setOnClickListener(new View.OnClickListener()
		{
			public void onClick(View arg0)
			{
				if (_Context instanceof Activity_Etudiant)
				{
					AlertDialog.Builder _Builder=new AlertDialog.Builder(_Context);
					_Builder.setTitle("Entrez l'URL de l'image que vous voulez insérée");
					LinearLayout layout = new LinearLayout(_Context);
					layout.setOrientation(LinearLayout.HORIZONTAL);
					layout.setGravity(Gravity.CENTER);
					final EditText _EditText_URL = new EditText(_Context);
					_EditText_URL.setHint(" URL IMAGE ");
					layout.addView(_EditText_URL);
					_Builder.setView(layout);
					_Builder.setNegativeButton("Annuler", null);
					_Builder.setPositiveButton("Confirmer",new OnClickListener()
					{
						public void onClick(DialogInterface dialog, int which) 
						{
							String _UrlImage=_EditText_URL.getText().toString();
							if (_UrlImage.equals("") || (_UrlImage.contains(".gif")==false && _UrlImage.contains(".jpg")==false && _UrlImage.contains(".jpeg")==false && _UrlImage.contains(".png")==false) )
							{
								Toast.makeText(_Context, "Veuillez Introduire un URL", Toast.LENGTH_SHORT).show();
								return;
							}
							String lURL=_Context.getResources().getString(R.string.EtudiantActivity_update_Etudiant_image);
							String _ID=_ArrayList.get(0).get("ID_Etudiant");
							_MySQliteFonction_Etudiant= new MySQliteFonction_Etudiant(_Context);
							_MySQliteFonction_Etudiant.Insert_InToServeur_Etudiant_image(_ID,_UrlImage, lURL);
						}
					});
					_Builder.show();
				}
			}
		});
		alert.show();
	}
}
