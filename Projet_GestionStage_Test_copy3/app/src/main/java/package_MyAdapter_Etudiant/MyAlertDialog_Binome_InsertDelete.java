package package_MyAdapter_Etudiant;

import java.util.ArrayList;
import java.util.HashMap;

import package_Foction_DataBase.MySQliteFonction_Etudiant;
import package_MemoirStage.Activity_Authentification;
import package_MemoirStage.Activity_Etudiant;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.text.Html;
import android.text.InputType;
import android.view.Gravity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.projet_gestionstage_test.R;

public class MyAlertDialog_Binome_InsertDelete
{
	private ArrayList<HashMap<String, String>> _List_Binome;
	private MySQliteFonction_Etudiant _MySQliteFonction_Etudiant;
	private String lID_EtudiantDisponible;

	private Button 	_Button_InsertDelte_Binome;

	private Context _Context;

	public MyAlertDialog_Binome_InsertDelete(Context _Context,Button _Button_InsertDelte_Binome)
	{
		this._Context=_Context;
		this._Button_InsertDelte_Binome=_Button_InsertDelte_Binome;
	}

	public void Insert_Binome()
	{
		AlertDialog.Builder _Builder1=new AlertDialog.Builder(_Context);
		_Builder1.setTitle("Ajouter Un Collège");
		_Builder1.setMessage("Veuillez entrer le numéro d'inscription de l'étudiant a Ajouter");
		LinearLayout layout = new LinearLayout(_Context);
		layout.setOrientation(LinearLayout.HORIZONTAL);
		layout.setGravity(Gravity.CENTER);
		final EditText _EditText_Binome = new EditText(_Context);
		_EditText_Binome.setHint(" Num Inscription ");
		_EditText_Binome.setInputType(InputType.TYPE_CLASS_NUMBER);
		layout.addView(_EditText_Binome);
		_Builder1.setView(layout);
		_Builder1.setNegativeButton("Annuler", null);
		_Builder1.setPositiveButton("Confirmer", new OnClickListener()
		{
			public void onClick(DialogInterface arg0, int arg1)
			{
				String lNum_inscrit=_EditText_Binome.getText().toString();
				String lNum_inscrit_etudiant1=package_MemoirStage.Activity_Etudiant.NUM_INSCRIPTION;
				if (lNum_inscrit.equals("") || lNum_inscrit.equals(lNum_inscrit_etudiant1))
				{
					Toast.makeText(_Context, "veuillez introduire un numéro d'inscription Valide", Toast.LENGTH_SHORT).show();
					return;
				}
				///
				String _URL=_Context.getResources().getString(R.string.EtudiantActivity_display_Binome);
				_List_Binome=new ArrayList<HashMap<String,String>>();
				_MySQliteFonction_Etudiant=new MySQliteFonction_Etudiant(_Context);
				_List_Binome=_MySQliteFonction_Etudiant.Afficher_Futur_Binome(Activity_Etudiant.Id_CYCLE,Activity_Etudiant.Id_FILIERE, lNum_inscrit, _URL);
				if (_List_Binome.isEmpty())
				{
					Toast.makeText(_Context, "Cet étudiant n'existe pas ou n'appartient pas à Votre filière ", Toast.LENGTH_LONG).show();
					return;
				}
				lID_EtudiantDisponible =_List_Binome.get(0).get("lID_Etudiant");
				String lNum_inscrit_EtudiantDispo=_List_Binome.get(0).get("lNumero_inscrit");
				String lnom_EtudiantDispo =_List_Binome.get(0).get("lNom_Etudiant");
				String lprenom_EtudiantDispo =_List_Binome.get(0).get("lPrenom_Etudiant");

				AlertDialog.Builder _Builder2=new AlertDialog.Builder(_Context);
				_Builder2.setTitle("Information Etudiant");
				String SMS="<br />"+"Numéro d'inscription :"+lNum_inscrit_EtudiantDispo+"<br />"+"Nom :"+lnom_EtudiantDispo+"<br />"+"Prenom :"+lprenom_EtudiantDispo;
				_Builder2.setMessage(Html.fromHtml("<font color=\"#000000 \">" + SMS + "</font>",null, null));
				_Builder2.setNegativeButton("Annuler", null);
				_Builder2.setPositiveButton("Confirmer",new OnClickListener()
				{
					public void onClick(DialogInterface arg0, int arg1) 
					{
						String _URL=_Context.getResources().getString(R.string.EtudiantActivity_updatePlus_Binome);
						boolean x=_MySQliteFonction_Etudiant.Update_InToServeur_Binome(String.valueOf(Activity_Etudiant.ID_ETUDIANT),lID_EtudiantDisponible, _URL);
						if (x)
						{
							_Button_InsertDelte_Binome.setBackgroundResource(R.drawable.etudiant_button_ajouter_binome_vert);	
							Activity_Authentification.Etudiant_ClearAll_BD();
							_MySQliteFonction_Etudiant.Synchroniser_Serveur_EtuDepNivFilCyc(Activity_Authentification.LOGIN, Activity_Authentification.PASSWORD, _Context.getResources().getString(R.string.EtudiantActivity_import_EtuDepNivFilCyc));
							((Activity)_Context).finish();
							Intent lIntent_To_ActivityEtudiant = new Intent(_Context,Activity_Etudiant.class);
							_Context.startActivity(lIntent_To_ActivityEtudiant);
						}else
						{
							Toast.makeText(_Context, "Echec Lors de l'insertion ", Toast.LENGTH_SHORT).show();
						}
					}
				});
				AlertDialog dialog2 = _Builder2.show();
				TextView messageText = (TextView)dialog2.findViewById(android.R.id.message);
				messageText.setGravity(Gravity.CENTER);
				messageText.setTextSize(14);
				dialog2.show();
				_Builder2.show();
			}
		});
		_Builder1.show();
		_EditText_Binome.getLayoutParams().width=170;
	}
	public void Delete_Binome()
	{
		AlertDialog.Builder _Builder = new AlertDialog.Builder(_Context);
		_Builder.setTitle("Supprimer Un Collège");
		_Builder.setMessage("êtes-vous sûr de vouloir supprimer ce collègue?");
		_Builder.setNegativeButton("Annuler", null);
		_Builder.setPositiveButton("Confirmer", new OnClickListener()
		{
			public void onClick(DialogInterface arg0, int arg1)
			{
				String _URL=_Context.getResources().getString(R.string.EtudiantActivity_updateMoin_Binome);
				_MySQliteFonction_Etudiant=new MySQliteFonction_Etudiant(_Context);
				boolean x=_MySQliteFonction_Etudiant.Update_InToServeur_Binome(Activity_Etudiant.ID_ETUDIANT,Activity_Etudiant.ID_BINOME, _URL);
				if (x)
				{
					_Button_InsertDelte_Binome.setBackgroundResource(R.drawable.etudiant_button_ajouter_binome_vert);	
					Activity_Authentification.Etudiant_ClearAll_BD();
					_MySQliteFonction_Etudiant.Synchroniser_Serveur_EtuDepNivFilCyc(Activity_Authentification.LOGIN, Activity_Authentification.PASSWORD, _Context.getResources().getString(R.string.EtudiantActivity_import_EtuDepNivFilCyc));
					((Activity)_Context).finish();
					Intent lIntent_To_ActivityEtudiant = new Intent(_Context,Activity_Etudiant.class);
					_Context.startActivity(lIntent_To_ActivityEtudiant);
				}else
				{
					Toast.makeText(_Context, "Echec Lors de la suppression ", Toast.LENGTH_SHORT).show();
				}
			}
		});
		_Builder.show();
	}
}
