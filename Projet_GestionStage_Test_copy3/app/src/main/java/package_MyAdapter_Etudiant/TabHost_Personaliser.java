package package_MyAdapter_Etudiant;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable.ConstantState;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;

import com.example.projet_gestionstage_test.R;

public class TabHost_Personaliser
{
	Context _Context;

	public TabHost_Personaliser(Context _Context)
	{
		this._Context=_Context;
	}

	public void TabHost_Perso(TabHost _TabHost)
	{
		_TabHost.getTabWidget().setStripEnabled(true); 

		for (int i = 0; i < _TabHost.getTabWidget().getChildCount(); i++)
		{
			TabWidget tabwidget = _TabHost.getTabWidget();

			//  Th�me du TabHost personalis�
			tabwidget.getChildTabViewAt(i).setBackgroundResource(R.drawable.tab_unselected_holo);//tab_unselected_holo

			// TextView du widget non s�l�ctionn�
			TextView tv_nonSelect = (TextView)tabwidget.getChildTabViewAt(i).findViewById(android.R.id.title);
			tv_nonSelect.setTextColor(_Context.getResources().getColor(R.color.white));
			tv_nonSelect.setTypeface(Typeface.DEFAULT_BOLD);
			// TextView du widget s�l�ctionn� 
			TextView tv_select = (TextView) _TabHost.getCurrentTabView().findViewById(android.R.id.title);
			tv_select.setTextColor(Color.parseColor("#000000"));

			// modifier la couleur du widget (non click�)
			tabwidget.getChildTabViewAt(i).setBackgroundColor(Color.parseColor("#FFFFFF"));
			// modifier la couleur du widget lors du clickage
			_TabHost.getCurrentTabView().setBackgroundColor(Color.parseColor("#FFFFFF"));


			// imgae du widget non s�l�ction :
			ImageView image_nonSelect=(ImageView) tabwidget.getChildAt(i).findViewById(android.R.id.icon);

			ConstantState Image_nonSelect=image_nonSelect.getDrawable().getConstantState();

			ConstantState Image_offre_stage_orange=_Context.getResources().getDrawable(R.drawable.offre_stage_orange).getConstantState();
			ConstantState Image_depotoffrestage_orange=_Context.getResources().getDrawable(R.drawable.depotoffrestage_orange).getConstantState();
			ConstantState Image_demandestage_orange=_Context.getResources().getDrawable(R.drawable.demandestage_orange).getConstantState();
			ConstantState Image_depotstage_orange=_Context.getResources().getDrawable(R.drawable.depotstage_orange).getConstantState();
			ConstantState Image_boitereception_orange=_Context.getResources().getDrawable(R.drawable.boitereception_orange).getConstantState();
			ConstantState Image_affectation_encadreur_orange=_Context.getResources().getDrawable(R.drawable.affectation_encadreur_orange).getConstantState();
			ConstantState Image_calendrier_orange=_Context.getResources().getDrawable(R.drawable.calendrier_orange).getConstantState();
			ConstantState Image_encadreument_orange=_Context.getResources().getDrawable(R.drawable.encadreument_orange).getConstantState();
			ConstantState Image_mon_stage_orange=_Context.getResources().getDrawable(R.drawable.mon_stage_orange).getConstantState();
			ConstantState Image_historique_orange=_Context.getResources().getDrawable(R.drawable.historique_orange).getConstantState();

			if (Image_nonSelect.equals(Image_offre_stage_orange))
			{
				image_nonSelect.setImageDrawable(_Context.getResources().getDrawable(R.drawable.offre_stage));
			}
			if (Image_nonSelect.equals(Image_depotoffrestage_orange))
			{
				image_nonSelect.setImageDrawable(_Context.getResources().getDrawable(R.drawable.depotoffrestage));
			}
			if (Image_nonSelect.equals(Image_demandestage_orange))
			{
				image_nonSelect.setImageDrawable(_Context.getResources().getDrawable(R.drawable.demandestage));
			}
			if (Image_nonSelect.equals(Image_depotstage_orange))
			{
				image_nonSelect.setImageDrawable(_Context.getResources().getDrawable(R.drawable.depotstage));
			}
			if (Image_nonSelect.equals(Image_boitereception_orange))
			{
				image_nonSelect.setImageDrawable(_Context.getResources().getDrawable(R.drawable.boitereception));
			}
			if (Image_nonSelect.equals(Image_affectation_encadreur_orange))
			{
				image_nonSelect.setImageDrawable(_Context.getResources().getDrawable(R.drawable.affectation_encadreur));
			}
			if (Image_nonSelect.equals(Image_calendrier_orange))
			{
				image_nonSelect.setImageDrawable(_Context.getResources().getDrawable(R.drawable.calendrier));
			}
			if (Image_nonSelect.equals(Image_encadreument_orange))
			{
				image_nonSelect.setImageDrawable(_Context.getResources().getDrawable(R.drawable.encadreument));
			}
			if (Image_nonSelect.equals(Image_mon_stage_orange))
			{
				image_nonSelect.setImageDrawable(_Context.getResources().getDrawable(R.drawable.mon_stage));
			}
			if (Image_nonSelect.equals(Image_historique_orange))
			{
				image_nonSelect.setImageDrawable(_Context.getResources().getDrawable(R.drawable.historique));
			}

			// image du widget s�l�ctionn� :
			ImageView image_select =(ImageView) _TabHost.getCurrentTabView().findViewById(android.R.id.icon);

			ConstantState Image_select=image_select.getDrawable().getConstantState();

			ConstantState Image_offre_stage=_Context.getResources().getDrawable(R.drawable.offre_stage).getConstantState();
			ConstantState Image_depotoffrestage=_Context.getResources().getDrawable(R.drawable.depotoffrestage).getConstantState();
			ConstantState Image_demandestage=_Context.getResources().getDrawable(R.drawable.demandestage).getConstantState();
			ConstantState Image_depotstage=_Context.getResources().getDrawable(R.drawable.depotstage).getConstantState();
			ConstantState Image_boitereception=_Context.getResources().getDrawable(R.drawable.boitereception).getConstantState();
			ConstantState Image_affectation_encadreur=_Context.getResources().getDrawable(R.drawable.affectation_encadreur).getConstantState();
			ConstantState Image_calendrier=_Context.getResources().getDrawable(R.drawable.calendrier).getConstantState();
			ConstantState Image_encadreument=_Context.getResources().getDrawable(R.drawable.encadreument).getConstantState();
			ConstantState Image_mon_stage=_Context.getResources().getDrawable(R.drawable.mon_stage).getConstantState();
			ConstantState Image_historique=_Context.getResources().getDrawable(R.drawable.historique).getConstantState();

			if (Image_select.equals(Image_offre_stage))
			{
				image_select.setImageDrawable(_Context.getResources().getDrawable(R.drawable.offre_stage_orange));
			}
			if (Image_select.equals(Image_depotoffrestage))
			{
				image_select.setImageDrawable(_Context.getResources().getDrawable(R.drawable.depotoffrestage_orange));
			}
			if (Image_select.equals(Image_demandestage))
			{
				image_select.setImageDrawable(_Context.getResources().getDrawable(R.drawable.demandestage_orange));
			}
			if (Image_select.equals(Image_depotstage))
			{
				image_select.setImageDrawable(_Context.getResources().getDrawable(R.drawable.depotstage_orange));
			}
			if (Image_select.equals(Image_boitereception))
			{
				image_select.setImageDrawable(_Context.getResources().getDrawable(R.drawable.boitereception_orange));
			}
			if (Image_select.equals(Image_affectation_encadreur))
			{
				image_select.setImageDrawable(_Context.getResources().getDrawable(R.drawable.affectation_encadreur_orange));
			}
			if (Image_select.equals(Image_calendrier))
			{
				image_select.setImageDrawable(_Context.getResources().getDrawable(R.drawable.calendrier_orange));
			}
			if (Image_select.equals(Image_encadreument))
			{
				image_select.setImageDrawable(_Context.getResources().getDrawable(R.drawable.encadreument_orange));
			}
			if (Image_select.equals(Image_mon_stage))
			{
				image_select.setImageDrawable(_Context.getResources().getDrawable(R.drawable.mon_stage_orange));
			}
			if (Image_select.equals(Image_historique))
			{
				image_select.setImageDrawable(_Context.getResources().getDrawable(R.drawable.historique_orange));
			}


		}
	}
}
