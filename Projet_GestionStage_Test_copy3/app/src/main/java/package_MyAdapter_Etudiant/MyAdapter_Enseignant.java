package package_MyAdapter_Etudiant;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import package_Foction_DataBase.MySQliteFonction_Etudiant;
import package_MemoirStage.Activity_Etudiant;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.projet_gestionstage_test.R;


public class MyAdapter_Enseignant extends BaseAdapter implements android.view.View.OnClickListener 
{

	private ArrayList<HashMap<String, String>> _ArrayList_Enseignant=new ArrayList<HashMap<String,String>>();
	private ArrayList<HashMap<String, String>> _ArrayList_Demande=new ArrayList<HashMap<String,String>>();

	private Context _Context;
	private LayoutInflater _LayoutInflater;
	private MySQliteFonction_Etudiant _MySQliteFonction_Etudiant;

	public MyAdapter_Enseignant(Context _Context,ArrayList<HashMap<String, String>> _ArrayList_Enseignant,ArrayList<HashMap<String, String>> _ArrayList_Demande)
	{
		this._LayoutInflater=LayoutInflater.from(_Context);
		this._Context=_Context;
		this._ArrayList_Enseignant=_ArrayList_Enseignant;
		this._ArrayList_Demande=_ArrayList_Demande;
		this._MySQliteFonction_Etudiant=new MySQliteFonction_Etudiant(_Context);
	}

	@Override
	public int getCount() 
	{
		return _ArrayList_Enseignant.size();
	}

	@Override
	public Object getItem(int position)
	{
		return _ArrayList_Enseignant.get(position);
	}

	@Override
	public long getItemId(int position) 
	{
		return position;
	}
	public class ViewHolder
	{
		TextView _TextView_Nom;
		TextView _TextView_Prenom;
		TextView _TextView_Email;
		TextView _TextView_Specialite;

		Button _Button_AjouterEnseignant;
	}


	@Override
	public View getView(final int position, View convertView, ViewGroup parent) 
	{
		final ViewHolder _ViewHolder;
		if (convertView==null)
		{
			_ViewHolder=new ViewHolder();

			convertView=_LayoutInflater.inflate(R.layout.activity_etudiant_strocture_enseignant, null);

			_ViewHolder._TextView_Nom=(TextView) convertView.findViewById(R.id.strocture_enseignant_TextView_nom);
			_ViewHolder._TextView_Prenom=(TextView) convertView.findViewById(R.id.strocture_enseignant_TextView_prenom);
			_ViewHolder._TextView_Email=(TextView) convertView.findViewById(R.id.strocture_enseignant_TextView_email);
			_ViewHolder._TextView_Specialite=(TextView) convertView.findViewById(R.id.strocture_enseignant_TextView_specialite);

			_ViewHolder._Button_AjouterEnseignant=(Button) convertView.findViewById(R.id.strocture_enseignant_Button_AjouterEncadreurAcad);

			convertView.setTag(_ViewHolder);
		}else 
		{
			_ViewHolder=(ViewHolder) convertView.getTag();
		}
		_ViewHolder._TextView_Nom.setText(_ArrayList_Enseignant.get(position).get("nom_Enseignant"));
		_ViewHolder._TextView_Prenom.setText(_ArrayList_Enseignant.get(position).get("prenom_Enseignant"));
		_ViewHolder._TextView_Email.setText(_ArrayList_Enseignant.get(position).get("email_Enseignant"));
		_ViewHolder._TextView_Specialite.setText(_ArrayList_Enseignant.get(position).get("specialite_Enseignant"));

		_ViewHolder._Button_AjouterEnseignant.setTag(R.id.position,position);
		_ViewHolder._Button_AjouterEnseignant.setTag(R.id.ViewHolder,_ViewHolder);
		_ViewHolder._Button_AjouterEnseignant.setOnClickListener(this);

		Verifier_DemandeEnseignant(_ViewHolder, position);
		return convertView;
	}
	public String Verifier_DemandeEnseignant(ViewHolder _ViewHolder ,int position)
	{
		String ok="";
		if (_ArrayList_Demande.isEmpty()==false)
		{
			for (int i = 0; i < _ArrayList_Demande.size(); i++) 
			{
				String _Id_enseignant_Demande=_ArrayList_Demande.get(i).get("Id_enseignant_Demande");
				String _ID_Enseignant=_ArrayList_Enseignant.get(position).get("ID_Enseignant");

				if (_Id_enseignant_Demande.equals(_ID_Enseignant)==true) 
				{   
					String _confirmation_Demande = _ArrayList_Demande.get(i).get("confirmation_Demande");
					if (_confirmation_Demande.equals("null"))
					{
						_ViewHolder._Button_AjouterEnseignant.setBackgroundResource(R.drawable.button_ajouter_offre_envoyer);
						ok= "null";
					}
					if (_confirmation_Demande.equals("1"))
					{
						_ViewHolder._Button_AjouterEnseignant.setBackgroundResource(R.drawable.add_encadreur_accepter);
						
						ok= "1";
					}
					if (_confirmation_Demande.equals("0"))
					{
						_ViewHolder._Button_AjouterEnseignant.setBackgroundResource(R.drawable.add_encadreur_refuser);
						ok= "0";
					}
				}
			}
			if (ok.equals("")) 
			{
				_ViewHolder._Button_AjouterEnseignant.setBackgroundResource(R.drawable.add_encadreur);
			}
		}
		return ok;
	}
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.strocture_enseignant_Button_AjouterEncadreurAcad:
			final int position=(Integer)v.getTag(R.id.position);
			final ViewHolder _ViewHolder=(ViewHolder) v.getTag(R.id.ViewHolder);
			String verif=Verifier_DemandeEnseignant(_ViewHolder, position);
			if (verif.equals("null")) 
			{
				AlertDialog.Builder _Builder0=new Builder(_Context);
				_Builder0.setMessage(" Votre Demande a été Envoyée ");
				_Builder0.show();
			}else if (verif.equals("0"))
			{
				AlertDialog.Builder _Builder1=new Builder(_Context);
				_Builder1.setMessage(" Votre Demande a été Refusée ");
				_Builder1.show();
			}else if (verif.equals("1"))
			{
				AlertDialog.Builder _Builder2=new Builder(_Context);
				_Builder2.setMessage(" Votre Demande a été Acceptée  ");
				_Builder2.show();
			}else if (verif.equals(""))
			{
				Envoyer_Demande(position, _ViewHolder);
			}

			break;

		default:
			break;
		}
	}
	public void Envoyer_Demande(final int position,final ViewHolder _ViewHolder)
	{
		AlertDialog.Builder _Builder=new Builder(_Context);
		_Builder.setTitle("Envoi Demande D'encadrement");
		_Builder.setMessage("voulez-vous envoyer une demande d'encadrement a "+_ArrayList_Enseignant.get(position).get("nom_Enseignant")+" "+_ArrayList_Enseignant.get(position).get("prenom_Enseignant"));
		_Builder.setPositiveButton("Confirmer", new OnClickListener()
		{
			public void onClick(DialogInterface arg0, int arg1) 
			{
				Calendar c=Calendar.getInstance();
				int annee = c.get(Calendar.YEAR);
				int mois = c.get(Calendar.MONTH);
				int jour = c.get(Calendar.DAY_OF_MONTH);
				String _Date=String.valueOf(jour)+"/"+String.valueOf(mois)+"/"+String.valueOf(annee);
				String _ID_Etudiant=Activity_Etudiant.ID_ETUDIANT;
				//String _ID_Binome=Activity_Etudiant.ID_BINOME;
				String _ID_Enseignant=_ArrayList_Enseignant.get(position).get("ID_Enseignant");
				String _URL0=_Context.getResources().getString(R.string.EtudiantActivity_insert_Demande);
				boolean x=_MySQliteFonction_Etudiant.Insert_InToServeur_Demande(_Date, _ID_Enseignant, _ID_Etudiant, _URL0);
				if (x)
				{    // a supprimer
					//_MySQliteFonction_Etudiant.ClearAll_Demande();
					//String _URL1=_Context.getResources().getString(R.string.EtudiantActivity_import_Demande);
					//_MySQliteFonction_Etudiant.Synchroniser_Serveur_Demande(_ID_Etudiant, _ID_Binome, _URL1);
					//_ViewHolder._Button_AjouterEnseignant.setBackgroundResource(R.drawable.button_ajouter_offre_envoyer);
					Refresh_Activity();
				}
			}
		});
		_Builder.setNegativeButton("Annuler", null);
		_Builder.show();
	}

	public void Refresh_Activity()
	{
		((Activity) _Context).finish();
		Intent _Intent_To_ActivityEtudiant=new Intent(_Context,Activity_Etudiant.class);
		_Context.startActivity(_Intent_To_ActivityEtudiant);
	}

}
