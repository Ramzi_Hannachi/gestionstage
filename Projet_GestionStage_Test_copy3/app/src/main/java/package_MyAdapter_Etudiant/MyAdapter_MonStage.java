package package_MyAdapter_Etudiant;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.AlertDialog;
import android.content.Context;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.projet_gestionstage_test.R;

public class MyAdapter_MonStage extends BaseAdapter implements OnClickListener
{
	private ArrayList<HashMap<String, String>> _ArrayList_StageEntreprise = new ArrayList<HashMap<String,String>>();
	private ArrayList<HashMap<String, String>> _ArrayList_EncadreurAcad =new ArrayList<HashMap<String,String>>();
	private ArrayList<HashMap<String, String>> _ArrayList_EncadreurPro =new ArrayList<HashMap<String,String>>();
	private ArrayList<HashMap<String, String>> _ArrayList_Enseignant =new ArrayList<HashMap<String,String>>();
	private ArrayList<HashMap<String, String>> _ArrayList_Demande =new ArrayList<HashMap<String,String>>();

	private LayoutInflater _LayoutInflater;
	private Context _Context;

	public MyAdapter_MonStage(Context _Context,ArrayList<HashMap<String, String>> _ArrayList_StageEntreprise,ArrayList<HashMap<String, String>> _ArrayList_EncadreurAcad,ArrayList<HashMap<String, String>> _ArrayList_EncadreurPro,ArrayList<HashMap<String, String>> _ArrayList_Enseignant,ArrayList<HashMap<String, String>> _ArrayList_Demande)
	{
		this._LayoutInflater=LayoutInflater.from(_Context);
		this._ArrayList_StageEntreprise=_ArrayList_StageEntreprise;
		this._ArrayList_EncadreurAcad=_ArrayList_EncadreurAcad;
		this._ArrayList_EncadreurPro=_ArrayList_EncadreurPro;
		this._ArrayList_Enseignant= _ArrayList_Enseignant;
		this._ArrayList_Demande=_ArrayList_Demande;
		this._Context=_Context;
	}
	@Override
	public int getCount()
	{
		return _ArrayList_StageEntreprise.size();
	}

	@Override
	public Object getItem(int arg0)
	{
		return _ArrayList_StageEntreprise.get(arg0);
	}

	@Override
	public long getItemId(int arg0)
	{
		return arg0;
	}
	private class ViewHolder
	{
		private Button _Button_EncadreurPro_Information;
		private TextView _TextView_EncadreurPro_nom;
		private TextView _TextView_EncadreurPro_prenom;
		private TextView _TextView_EncadreurPro_Email;

		private TextView _TextView_Entreprise_nom;
		private TextView _TextView_Entreprise_numTel;
		private TextView _TextView_Entreprise_Email;
		private TextView _TextView_Entreprise_siteWeb;

		private Button _Button_Stage_NomDescription;

		private Button _Button_Ajouter_EncadreurAcad;
		private TextView _TextView_EncadreurAcad_nom; 
		private TextView _TextView_EncadreurAcad_prenom;
		private TextView _TextView_EncadreurAcad_Email;
		private TextView _TextView_EncadreurAcad_Specialite;

		private LinearLayout _LinearLayout_AjouterEncadreurAcad;
		private TextView _TextView_AjouterEncadreurAcad;

		private LinearLayout _LinearLayout_EncadreurAcad;
		private TextView _TextView_EncadreurAcad;
		private ImageView _ImageView_EncadreurAcad;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup arg2)
	{

		final ViewHolder _ViewHolder;

		if (convertView==null)
		{
			_ViewHolder=new ViewHolder();
			convertView=_LayoutInflater.inflate(R.layout.activity_etudiant_strocture_monstage, null);
			// Encadreur Pro
			_ViewHolder._Button_EncadreurPro_Information=(Button) convertView.findViewById(R.id.strocture_monstage_encadreurPro_Button_Information);
			_ViewHolder._TextView_EncadreurPro_nom=(TextView) convertView.findViewById(R.id.strocture_monstage_encadreurPro_TextView_Nom);
			_ViewHolder._TextView_EncadreurPro_prenom=(TextView) convertView.findViewById(R.id.strocture_monstage_encadreurPro_TextView_Prenom);
			_ViewHolder._TextView_EncadreurPro_Email=(TextView) convertView.findViewById(R.id.strocture_monstage_encadreurPro_TextView_email);
			// Entreprise
			_ViewHolder._TextView_Entreprise_nom=(TextView) convertView.findViewById(R.id.strocture_monstage_Entreprise_TextView_Nom);
			_ViewHolder._TextView_Entreprise_numTel=(TextView) convertView.findViewById(R.id.strocture_monstage_Entreprise_TextView_NumTel);
			_ViewHolder._TextView_Entreprise_Email=(TextView) convertView.findViewById(R.id.strocture_monstage_Entreprise_TextView_Email);
			_ViewHolder._TextView_Entreprise_siteWeb=(TextView) convertView.findViewById(R.id.strocture_monstage_Entreprise_TextView_siteWeb);
			// Stage
			_ViewHolder._Button_Stage_NomDescription=(Button) convertView.findViewById(R.id.strocture_monstage_Stage_Button_NomDescription);
			// Encadreur Acad
			_ViewHolder._Button_Ajouter_EncadreurAcad=(Button) convertView.findViewById(R.id.strocture_monstage_encadreurAcad_Button_ajouter);
			_ViewHolder._TextView_EncadreurAcad_nom=(TextView) convertView.findViewById(R.id.strocture_monstage_encadreurAcad_TextView_Nom);
			_ViewHolder._TextView_EncadreurAcad_prenom=(TextView) convertView.findViewById(R.id.strocture_monstage_encadreurAcad_TextView_Prenom);
			_ViewHolder._TextView_EncadreurAcad_Email=(TextView) convertView.findViewById(R.id.strocture_monstage_encadreurAcad_TextView_email);
			_ViewHolder._TextView_EncadreurAcad_Specialite=(TextView) convertView.findViewById(R.id.strocture_monstage_encadreurAcad_TextView_specialite);
			//
			_ViewHolder._LinearLayout_AjouterEncadreurAcad=(LinearLayout) convertView.findViewById(R.id.strocture_monstage_Layout_AjouterEncadreurAcad);
			_ViewHolder._TextView_AjouterEncadreurAcad=(TextView) convertView.findViewById(R.id.strocture_monstage_TextView_AjouterEncadreurAcad);
			//
			_ViewHolder._LinearLayout_EncadreurAcad=(LinearLayout) convertView.findViewById(R.id.strocture_monstage_Layout_EncadreurAcad);
			_ViewHolder._TextView_EncadreurAcad=(TextView) convertView.findViewById(R.id.strocture_monstage_TextView_EncadreurAcad);
			_ViewHolder._ImageView_EncadreurAcad=(ImageView) convertView.findViewById(R.id.strocture_monstage_ImageView_EncadreurAcad);

			convertView.setTag(_ViewHolder);
		}else 
		{
			_ViewHolder=(ViewHolder) convertView.getTag();
		}
		_ViewHolder._TextView_Entreprise_nom.setText(_ArrayList_StageEntreprise.get(0).get("libelle_Entreprise"));
		_ViewHolder._TextView_Entreprise_numTel.setText(_ArrayList_StageEntreprise.get(0).get("numTel_Entreprise"));
		_ViewHolder._TextView_Entreprise_Email.setText(_ArrayList_StageEntreprise.get(0).get("email_Entreprise")); 
		_ViewHolder._TextView_Entreprise_siteWeb.setText(_ArrayList_StageEntreprise.get(0).get("siteWeb_Entreprise"));

		_ViewHolder._Button_Stage_NomDescription.setOnClickListener(this);
		_ViewHolder._Button_EncadreurPro_Information.setOnClickListener(this);

		_ViewHolder._Button_Ajouter_EncadreurAcad.setOnClickListener(this);

		VerifierEncadreurAcad(_ViewHolder._LinearLayout_AjouterEncadreurAcad,_ViewHolder. _TextView_AjouterEncadreurAcad, _ViewHolder._LinearLayout_EncadreurAcad, _ViewHolder._TextView_EncadreurAcad, _ViewHolder._ImageView_EncadreurAcad);
		GererEncadreurAcad(_ViewHolder._Button_Ajouter_EncadreurAcad, _ViewHolder._TextView_EncadreurAcad_nom, _ViewHolder._TextView_EncadreurAcad_prenom, _ViewHolder._TextView_EncadreurAcad_Email, _ViewHolder._TextView_EncadreurAcad_Specialite);
		GererEncadreurPro(_ViewHolder._Button_EncadreurPro_Information,_ViewHolder. _TextView_EncadreurPro_nom, _ViewHolder._TextView_EncadreurPro_prenom, _ViewHolder._TextView_EncadreurPro_Email);

		return convertView;
	}
	public void onClick(View v)
	{
		switch (v.getId()) 
		{
		case R.id.strocture_monstage_Stage_Button_NomDescription:
			String _stage_description=_ArrayList_StageEntreprise.get(0).get("description_Stage");
			String _stage_nom=_ArrayList_StageEntreprise.get(0).get("Nom_Stage");
			AlertDialog.Builder box = new AlertDialog.Builder(_Context);
			box.setTitle("Stage");
			String SMS="<br />"+_stage_nom+"<br />"+"<br />"+_stage_description+"<br />";
			box.setMessage(Html.fromHtml("<font color=\"#000000 \">" + SMS + "</font>",null, null));
			AlertDialog dialog = box.show();
			TextView messageText = (TextView)dialog.findViewById(android.R.id.message);
			messageText.setGravity(Gravity.CENTER);
			messageText.setTextSize(14);
			dialog.show();
			break;

		case R.id.strocture_monstage_encadreurPro_Button_Information :
			AlertDialog.Builder box1 = new AlertDialog.Builder(_Context);
			String SMS1="Un Encadreur Professionnel n'a pas encore été affecté par le Chef de Département ou L'entreprise ";
			box1.setMessage(Html.fromHtml("<font color=\"#000000 \">" + SMS1 + "</font>",null, null));
			AlertDialog dialog1 = box1.show();
			TextView messageText1 = (TextView)dialog1.findViewById(android.R.id.message);
			messageText1.setGravity(Gravity.CENTER);
			messageText1.setTextSize(14);
			dialog1.show();
			break;

		case R.id.strocture_monstage_encadreurAcad_Button_ajouter:
			MyAdapter_Enseignant _MyAdapter_Enseignant = new MyAdapter_Enseignant(_Context, _ArrayList_Enseignant,_ArrayList_Demande);
			AlertDialog.Builder _Builder=new AlertDialog.Builder(_Context);
			_Builder.setTitle("Choisir Un Encadreur Académique");
			_Builder.setAdapter(_MyAdapter_Enseignant, null);
			ListView _ListView=new ListView(_Context);
			_Builder.setView(_ListView);
			AlertDialog dialog2 = _Builder.create();
			dialog2.show();
			break;

		default:
			break;
		}
	}
	public void VerifierEncadreurAcad(LinearLayout _LinearLayout_AjouterEncadreurAcad,TextView _TextView_AjouterEncadreurAcad,LinearLayout _LinearLayout_EncadreurAcad ,TextView _TextView_EncadreurAcad,ImageView _ImageView_EncadreurAcad )
	{
		if (_ArrayList_EncadreurAcad.isEmpty()==false)
		{
			_LinearLayout_AjouterEncadreurAcad.setVisibility(View.GONE);
			_TextView_AjouterEncadreurAcad.setVisibility(View.GONE);

			_LinearLayout_EncadreurAcad.setVisibility(View.VISIBLE);
			_TextView_EncadreurAcad.setVisibility(View.VISIBLE);
			_ImageView_EncadreurAcad.setVisibility(View.VISIBLE);
		}else
		{
			_LinearLayout_AjouterEncadreurAcad.setVisibility(View.VISIBLE);
			_TextView_AjouterEncadreurAcad.setVisibility(View.VISIBLE);

			_LinearLayout_EncadreurAcad.setVisibility(View.GONE);
			_TextView_EncadreurAcad.setVisibility(View.GONE);
			_ImageView_EncadreurAcad.setVisibility(View.GONE);
		}
	}
	public void GererEncadreurAcad(Button _Button_Ajouter_EncadreurAcad,TextView _TextView_EncadreurAcad_nom,TextView _TextView_EncadreurAcad_prenom,TextView _TextView_EncadreurAcad_Email,TextView _TextView_EncadreurAcad_Specialite)
	{
		if (_ArrayList_EncadreurAcad.isEmpty()==false) 
		{
			_Button_Ajouter_EncadreurAcad.setEnabled(false);
			_TextView_EncadreurAcad_nom.setText(_ArrayList_EncadreurAcad.get(0).get("nom_Enseignant"));
			_TextView_EncadreurAcad_prenom.setText(_ArrayList_EncadreurAcad.get(0).get("prenom_Enseignant"));
			_TextView_EncadreurAcad_Email.setText(_ArrayList_EncadreurAcad.get(0).get("email_Enseignant"));
			_TextView_EncadreurAcad_Specialite.setText(_ArrayList_EncadreurAcad.get(0).get("specialite_Enseignant"));
		}else
		{
			_Button_Ajouter_EncadreurAcad.setEnabled(true);
		}
	}
	public void GererEncadreurPro(Button _Button_EncadreurPro_Information,TextView _TextView_EncadreurPro_nom,TextView _TextView_EncadreurPro_prenom,TextView _TextView_EncadreurPro_Email)
	{
		if (_ArrayList_EncadreurPro.isEmpty()==false)
		{
			_Button_EncadreurPro_Information.setEnabled(false);
			_TextView_EncadreurPro_nom.setText(_ArrayList_EncadreurPro.get(0).get("Nom_EncadreurPro"));
			_TextView_EncadreurPro_prenom.setText(_ArrayList_EncadreurPro.get(0).get("Prenom_EncadreurPro"));
			_TextView_EncadreurPro_Email.setText(_ArrayList_EncadreurPro.get(0).get("Email_EncadreurPro"));
		}else
		{
			_Button_EncadreurPro_Information.setEnabled(true);
		}
	}

}
