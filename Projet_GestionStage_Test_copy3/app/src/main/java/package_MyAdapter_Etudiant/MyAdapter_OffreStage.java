package package_MyAdapter_Etudiant;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import package_Foction_DataBase.MySQliteFonction_Etudiant;
import package_MemoirStage.Activity_Etudiant;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.projet_gestionstage_test.R;



public class MyAdapter_OffreStage extends BaseAdapter
{

	private MySQliteFonction_Etudiant _MySQliteFonction_Etudiant;

	private ArrayList<HashMap<String, String>> _ArrayList_OffreStage = new ArrayList<HashMap<String,String>>();
	private ArrayList<HashMap<String, String>> _ArrayList_Condidaer=new ArrayList<HashMap<String,String>>();

	private LayoutInflater _LayoutInflater;
	private Context _Context;


	public MyAdapter_OffreStage(Context _Context,ArrayList<HashMap<String, String>> _ArrayList_OffreStage)
	{
		this._LayoutInflater=LayoutInflater.from(_Context);
		this._ArrayList_OffreStage=_ArrayList_OffreStage;
		this._Context=_Context;
	}

	@Override
	public int getCount() 
	{
		return _ArrayList_OffreStage.size();
	}

	@Override
	public Object getItem(int arg0) 
	{
		return _ArrayList_OffreStage.get(arg0);
	}

	@Override
	public long getItemId(int arg0)
	{
		return arg0;
	}

	private class ViewHolder
	{
		TextView _TextView_offreStage_Nom;
		TextView _TextView_entreprise_siteWeb;
		TextView _TextView_entreprise_Numero;

		Button _Button_Description_offreStage;
		Button _Button_Description_Entreprise;

		Button _Button_Ajouter_offreStage;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		final ViewHolder _ViewHolder;

		if (convertView==null)
		{
			_ViewHolder=new ViewHolder();
			convertView=_LayoutInflater.inflate(R.layout.activity_etudiant_strocture_offrestage, null);

			_ViewHolder._TextView_offreStage_Nom=(TextView) convertView.findViewById(R.id.strocture_OffreStage_TextView_Nom);
			_ViewHolder._TextView_entreprise_siteWeb=(TextView) convertView.findViewById(R.id.strocture_OffreStage_TextView_SiteWeb);
			_ViewHolder._TextView_entreprise_Numero=(TextView) convertView.findViewById(R.id.strocture_OffreStage_TextView_NumEntreprise);

			_ViewHolder._Button_Description_offreStage=(Button) convertView.findViewById(R.id.strocture_OffreStage_Button_descriptionOffreStage);
			_ViewHolder._Button_Description_Entreprise=(Button) convertView.findViewById(R.id.strocture_offreStage_Button_DescriptionEntreprise);

			_ViewHolder._Button_Ajouter_offreStage=(Button) convertView.findViewById(R.id.strocture_OffreStage_Button_Ajouter_offrestage);

			convertView.setTag(_ViewHolder);
		}else
		{
			_ViewHolder=(ViewHolder) convertView.getTag();
		}
		_ViewHolder._TextView_offreStage_Nom.setText(_ArrayList_OffreStage.get(position).get("nom_OffreStage"));
		_ViewHolder._TextView_entreprise_siteWeb.setText(_ArrayList_OffreStage.get(position).get("siteWeb_Entreprise"));
		_ViewHolder._TextView_entreprise_Numero.setText(_ArrayList_OffreStage.get(position).get("numTel_Entreprise"));

		//// Button Description OffreStage !!!!
		_ViewHolder._Button_Description_offreStage.setTag(position);
		_ViewHolder._Button_Description_offreStage.setOnClickListener(new View.OnClickListener()
		{
			public void onClick(View v)
			{
				int position=(Integer) _ViewHolder._Button_Description_offreStage.getTag();

				String _offreStage_description=_ArrayList_OffreStage.get(position).get("description_OffreStage");

				AlertDialog.Builder box = new AlertDialog.Builder(_Context);
				box.setTitle("Description Stage");
				String SMS="<br />"+_offreStage_description+"<br />";
				box.setMessage(Html.fromHtml("<font color=\"#000000 \">" + SMS + "</font>",null, null));
				AlertDialog dialog = box.show();
				TextView messageText = (TextView)dialog.findViewById(android.R.id.message);
				messageText.setGravity(Gravity.CENTER);
				messageText.setTextSize(14);
				dialog.show();
			}
		});

		//// Button Description Entreprise!!!
		_ViewHolder._Button_Description_Entreprise.setTag(position);
		_ViewHolder._Button_Description_Entreprise.setOnClickListener(new View.OnClickListener()
		{
			public void onClick(View v) 
			{
				int position=(Integer) _ViewHolder._Button_Description_Entreprise.getTag();

				String _entreprise_Libelle=_ArrayList_OffreStage.get(position).get("libelle_Entreprise");
				String _entreprise_Raison=_ArrayList_OffreStage.get(position).get("raison_Entreprise");
				String _entreprise_Email=_ArrayList_OffreStage.get(position).get("email_Entreprise");
				String _entreprise_SiteWeb=_ArrayList_OffreStage.get(position).get("siteWeb_Entreprise");

				AlertDialog.Builder box = new AlertDialog.Builder(_Context);
				box.setTitle("Entreprise");
				String SMS="<br />"+_entreprise_Libelle+"<br />"+_entreprise_Raison+"<br />"+_entreprise_Email+"<br />"+_entreprise_SiteWeb;
				box.setMessage(Html.fromHtml("<font color=\"#000000 \">" + SMS + "</font>",null, null));
				AlertDialog dialog = box.show();
				TextView messageText = (TextView)dialog.findViewById(android.R.id.message);
				messageText.setGravity(Gravity.CENTER);
				messageText.setTextSize(14);
				dialog.show();
			}
		});
		Verifier_CondidaterOffreStage(_ViewHolder, position);
		// Ajouter OffreStage
		_ViewHolder._Button_Ajouter_offreStage.setTag(position);
		_ViewHolder._Button_Ajouter_offreStage.setOnClickListener(new View.OnClickListener()
		{
			public void onClick(View v)
			{
				int position=(Integer) _ViewHolder._Button_Ajouter_offreStage.getTag();
				_MySQliteFonction_Etudiant=new MySQliteFonction_Etudiant(_Context);
				_ArrayList_Condidaer=_MySQliteFonction_Etudiant.Afficher_Condidater();
				boolean trouv=false;
				if (_ArrayList_Condidaer.isEmpty()==false)
				{
					for (int i = 0; i < _ArrayList_Condidaer.size(); i++) 
					{
						String _Id_offreStage_Condidater=_ArrayList_Condidaer.get(i).get("Id_offreStage_Condidater");
						String _ID_OffreStage=_ArrayList_OffreStage.get(position).get("ID_OffreStage");
						if (_Id_offreStage_Condidater.equals(_ID_OffreStage))
						{
							trouv=true;
							String _confirmation_Condidater=_ArrayList_Condidaer.get(i).get("confirmation_Condidater");
							if (_confirmation_Condidater.equals("null"))
							{
								//Toast.makeText(_Context, " demande envoyé ! ", Toast.LENGTH_LONG).show();
								AlertDialog.Builder _Builder=new AlertDialog.Builder(_Context);
								_Builder.setTitle("Information");
								_Builder.setMessage("Demande d'entretien envoyé");
								_Builder.show();
								return;
							}
							if (_confirmation_Condidater.equals("1"))
							{
								//Toast.makeText(_Context, " demande accepté ! ", Toast.LENGTH_LONG).show();
								AlertDialog.Builder _Builder=new AlertDialog.Builder(_Context);
								_Builder.setTitle("Information");
								_Builder.setMessage("Demande d'entretien accepté");
								_Builder.show();
								return;
							}
							if (_confirmation_Condidater.equals("0"))
							{
								//Toast.makeText(_Context, " demande refusé ! ", Toast.LENGTH_LONG).show();
								AlertDialog.Builder _Builder=new AlertDialog.Builder(_Context);
								_Builder.setTitle("Information");
								_Builder.setMessage("Demande d'entretien refusé");
								_Builder.show();
								return;
							}if (_confirmation_Condidater.equals("2"))
							{
								AlertDialog.Builder _Builder3 = new AlertDialog.Builder(_Context);
								_Builder3.setTitle("Information");
								_Builder3.setMessage(" Vous Avez Signé une Demande de Stage pour cette offre de stage " +
										", elle est en cours d'évaluation par le chef de d'épartement ");
								_Builder3.show();
								return;
							}
						}
					}
				}
				if (trouv==false)
				{
					Envoyer_OffreStage(_ViewHolder);
				}
			}
		});
		return convertView;
	}
	///// Verifier offreStage / ChoisirStage !!
	public void Verifier_CondidaterOffreStage(ViewHolder _ViewHolder,int position)
	{
		_MySQliteFonction_Etudiant=new MySQliteFonction_Etudiant(_Context);
		_ArrayList_Condidaer=_MySQliteFonction_Etudiant.Afficher_Condidater();
		if (_ArrayList_Condidaer.isEmpty()==false)
		{
			for (int i = 0; i < _ArrayList_Condidaer.size(); i++) 
			{
				String Id_offreStage_Condidater=_ArrayList_Condidaer.get(i).get("Id_offreStage_Condidater");
				String ID_OffreStage=_ArrayList_OffreStage.get(position).get("ID_OffreStage");
				_ViewHolder._Button_Ajouter_offreStage.setBackgroundResource(R.drawable.button_ajouter_offre_noir);
				if (Id_offreStage_Condidater.equals(ID_OffreStage))
				{
					String _confirmation_Condidater=_ArrayList_Condidaer.get(i).get("confirmation_Condidater");
					if (_confirmation_Condidater.equals("null"))
					{
						_ViewHolder._Button_Ajouter_offreStage.setBackgroundResource(R.drawable.button_ajouter_offre_envoyer);
						return;
					}
					if (_confirmation_Condidater.equals("1"))
					{
						_ViewHolder._Button_Ajouter_offreStage.setBackgroundResource(R.drawable.button_ajouter_offre_accepter);
						return;
					}
					if (_confirmation_Condidater.equals("0"))
					{
						_ViewHolder._Button_Ajouter_offreStage.setBackgroundResource(R.drawable.button_ajouter_offre_refuser);
						return;
					}if (_confirmation_Condidater.equals("2"))
					{
						_ViewHolder._Button_Ajouter_offreStage.setBackgroundResource(R.drawable.stage_evaluation);
						return;
					}
				}
			}
		}
	}
	public void Envoyer_OffreStage( final ViewHolder _ViewHolder)
	{
		AlertDialog.Builder _Builder=new AlertDialog.Builder(_Context);
		_Builder.setTitle("Envoi de Permission pour un Entretien");
		_Builder.setMessage("etes vous sure de vouloir envoyer une Demande d'entretien pour ce stage a cette Entreprise");
		_Builder.setNegativeButton("Annuler", null);
		_Builder.setPositiveButton("Confirmer", new OnClickListener() 
		{
			public void onClick(DialogInterface arg0, int arg1) 
			{
				int position=(Integer) _ViewHolder._Button_Ajouter_offreStage.getTag();
				_MySQliteFonction_Etudiant=new MySQliteFonction_Etudiant(_Context);
				Calendar c=Calendar.getInstance();
				int annee = c.get(Calendar.YEAR);
				int mois = c.get(Calendar.MONTH);
				int jour = c.get(Calendar.DAY_OF_MONTH);
				String _Date=String.valueOf(jour)+"/"+String.valueOf(mois)+"/"+String.valueOf(annee);
				String _Id_Etudiant=Activity_Etudiant.ID_ETUDIANT;
				String _Id_OffreStage=_ArrayList_OffreStage.get(position).get("ID_OffreStage");
				String _URL=_Context.getResources().getString(R.string.EtudiantActivity_insert_Condidater);
				boolean x=_MySQliteFonction_Etudiant.Insert_InToServeur_Condidater(_Date, _Id_Etudiant, _Id_OffreStage, _URL);
				// sychronier  condidater
				if (x)
				{
					_MySQliteFonction_Etudiant.ClearAll_Condidater();
					String _URL1=_Context.getResources().getString(R.string.EtudiantActivity_import_Condidater);
					_MySQliteFonction_Etudiant.Synchroniser_Serveur_Condidater(Activity_Etudiant.ID_ETUDIANT,Activity_Etudiant.ID_BINOME,_URL1);
					_ViewHolder._Button_Ajouter_offreStage.setBackgroundResource(R.drawable.button_ajouter_offre_envoyer);
				}
			}
		});
		AlertDialog dialog1 = _Builder.show();
		TextView message = (TextView)dialog1.findViewById(android.R.id.message);
		message.setGravity(Gravity.CENTER);
		message.setTextSize(14);
		dialog1.show();
	}

}

