package package_MyAdapter_ChefDepartement;

import java.util.ArrayList;
import java.util.HashMap;

import package_Foction_DataBase.MySQliteFonction_ChefDepartement;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.projet_gestionstage_test.R;

public class MyDialog_ChefDepartement_Information
{
	private MySQliteFonction_ChefDepartement _MySQliteFonction_ChefDepartement;

	private ArrayList<HashMap<String, String>> _ArrayList_DeparetementChef = new ArrayList<HashMap<String,String>>();
	private ArrayList<HashMap<String, String>> _ArrayList_Filiere = new ArrayList<HashMap<String,String>>();
	private Context _Context;
	private String _proprietaire="";

	public MyDialog_ChefDepartement_Information(Context _Context , ArrayList<HashMap<String, String>> _ArrayList_DeparetementChef,ArrayList<HashMap<String, String>> _ArrayList_Filiere,String _proprietaire)
	{
		this._Context=_Context;
		this._ArrayList_DeparetementChef=_ArrayList_DeparetementChef;
		this._ArrayList_Filiere=_ArrayList_Filiere;
		this._proprietaire=_proprietaire;
	}
	public void MyDialogChefDepartement_Information()
	{
		Dialog alert = new Dialog(_Context);//R.style.Theme_Transparent
		alert.requestWindowFeature(Window.FEATURE_NO_TITLE);
		alert.setContentView(R.layout.activity_chefdepartement_dialog_information);
		ImageView _ImageView_Image=(ImageView) alert.findViewById(R.id.Activity_ChefDepartement_DialogInformation_Image);
		TextView _TextView_NomPrenom=(TextView) alert.findViewById(R.id.Activity_ChefDepartement_DialogInformation_NomPrenom);
		TextView _TextView_Sexe=(TextView) alert.findViewById(R.id.Activity_ChefDepartement_DialogInformation_Sexe);
		TextView _TextView_DateLieuNaiss=(TextView) alert.findViewById(R.id.Activity_ChefDepartement_DialogInformation_DateLieuNaiss);
		TextView _TextView_NumTel=(TextView) alert.findViewById(R.id.Activity_ChefDepartement_DialogInformation_NumTel);
		TextView _TextView_Email=(TextView) alert.findViewById(R.id.Activity_ChefDepartement_DialogInformation_Email);
		TextView _TextView_Adresse=(TextView) alert.findViewById(R.id.Activity_ChefDepartement_DialogInformation_Adresse);
		TextView _TextView_Ville=(TextView) alert.findViewById(R.id.Activity_ChefDepartement_DialogInformation_Ville);
		TextView _TextView_Grade=(TextView) alert.findViewById(R.id.Activity_ChefDepartement_DialogInformation_Grade);
		TextView _TextView_DiplomeLieuAnn=(TextView) alert.findViewById(R.id.Activity_ChefDepartement_DialogInformation_DiplomeLieuAnnee);
		TextView _TextView_Situation=(TextView) alert.findViewById(R.id.Activity_ChefDepartement_DialogInformation_Situation);
		TextView _TextView_Titulaire=(TextView) alert.findViewById(R.id.Activity_ChefDepartement_DialogInformation_Titulaire);
		TextView _TextView_FactAdmin=(TextView) alert.findViewById(R.id.Activity_ChefDepartement_DialogInformation_FactAdmin);
		TextView _TextView_Encadrement=(TextView) alert.findViewById(R.id.Activity_ChefDepartement_DialogInformation_Encadrement);
		TextView _TextView_EnDetachement0=(TextView) alert.findViewById(R.id.Activity_ChefDepartement_DialogInformation_EnDetachement0);
		TextView _TextView_EnDetachement=(TextView) alert.findViewById(R.id.Activity_ChefDepartement_DialogInformation_EnDetachement);
		TextView _TextView_Specialite=(TextView) alert.findViewById(R.id.Activity_ChefDepartement_DialogInformation_Specialite);
		TextView _TextView_Departement=(TextView) alert.findViewById(R.id.Activity_ChefDepartement_DialogInformation_Departement);
		Spinner _Spinner_Filiere=(Spinner) alert.findViewById(R.id.Activity_ChefDepartement_DialogInformation_Filiere);

		String _nom=_ArrayList_DeparetementChef.get(0).get("nom_Enseignant");
		String _prenom=_ArrayList_DeparetementChef.get(0).get("prenom_Enseignant");
		String _sexe=_ArrayList_DeparetementChef.get(0).get("sexe_Enseignant");
		String _dateNaiss=_ArrayList_DeparetementChef.get(0).get("dateNaiss_Enseignant");
		String _lieuNaiss=_ArrayList_DeparetementChef.get(0).get("lieuNaiss_Enseignant");
		String _numTel=_ArrayList_DeparetementChef.get(0).get("numTel_Enseignant");
		String _email=_ArrayList_DeparetementChef.get(0).get("email_Enseignant");
		String _adresse=_ArrayList_DeparetementChef.get(0).get("adresse_Enseignant");
		String _ville=_ArrayList_DeparetementChef.get(0).get("ville_Enseignant");
		String _grade=_ArrayList_DeparetementChef.get(0).get("grade_Enseignant");
		String _diplomeLieu=_ArrayList_DeparetementChef.get(0).get("lieuDipl_Enseignant");
		String _diplomeAnnee=_ArrayList_DeparetementChef.get(0).get("anneeDip_Enseignant");
		String _situation=_ArrayList_DeparetementChef.get(0).get("situation_Enseignant");
		String _titulaire=_ArrayList_DeparetementChef.get(0).get("titulaire_Enseignant");
		String _factAdmin=_ArrayList_DeparetementChef.get(0).get("factAdm_Enseignant");
		String _encadrement=_ArrayList_DeparetementChef.get(0).get("encadrement_Enseignant");
		if (_encadrement.equals("1"))
		{
			_encadrement="Oui";
		}else
		{
			_encadrement="Non";
		}
		if (_factAdmin.equals(null)==false)
		{
			_TextView_EnDetachement0.setVisibility(View.GONE);
			_TextView_EnDetachement.setVisibility(View.GONE);
		}else
		{
			_TextView_EnDetachement0.setVisibility(View.VISIBLE);
			_TextView_EnDetachement.setVisibility(View.VISIBLE);
		}
		String _enDetachement=_ArrayList_DeparetementChef.get(0).get("enDetachement_Enseignant");
		String _specialite=_ArrayList_DeparetementChef.get(0).get("specialite_Enseignant");
		String _departement=_ArrayList_DeparetementChef.get(0).get("libelle_Departement");

		_TextView_NomPrenom.setText(_nom+" "+_prenom);
		_TextView_Sexe.setText(_sexe);
		_TextView_DateLieuNaiss.setText(_dateNaiss+" "+_lieuNaiss);
		_TextView_NumTel.setText(_numTel);
		_TextView_Email.setText(_email);
		_TextView_Adresse.setText(_adresse);
		_TextView_Ville.setText(_ville);
		_TextView_Grade.setText(_grade);
		_TextView_DiplomeLieuAnn.setText(_diplomeLieu+" "+_diplomeAnnee);
		_TextView_Situation.setText(_situation);
		_TextView_Titulaire.setText(_titulaire);
		_TextView_FactAdmin.setText(_factAdmin);
		_TextView_Encadrement.setText(_encadrement);
		_TextView_EnDetachement.setText(_enDetachement);
		_TextView_Specialite.setText(_specialite);
		_TextView_Departement.setText(_departement);
		Spinner_Filiere(_Spinner_Filiere);
		Verifier_ChefDepartementEseignant(_ImageView_Image);
		alert.show();
	}
	public void Verifier_ChefDepartementEseignant(ImageView _ImageView_Image)
	{
		if (_proprietaire.equals("Enseignant")) 
		{
			_ImageView_Image.setImageResource(R.drawable.encadreur_acad);
		}
		if (_proprietaire.equals("ChefDepartement")) 
		{
			_ImageView_Image.setImageResource(R.drawable.chefdepartement);
		}
	}
	public void Spinner_Filiere(Spinner _Spinner)
	{
		_MySQliteFonction_ChefDepartement=new MySQliteFonction_ChefDepartement(_Context);
		_ArrayList_Filiere=new ArrayList<HashMap<String,String>>();
		_ArrayList_Filiere=_MySQliteFonction_ChefDepartement.Afficher_Filiere();
		ArrayList<String> _ListFiliere=new ArrayList<String>();
		for (int i = 0; i < _ArrayList_Filiere.size(); i++)
		{
			_ListFiliere.add(_ArrayList_Filiere.get(i).get("designation_Filiere"));
		}
		ArrayAdapter<String> _Adapter = new ArrayAdapter<String>(_Context, R.layout.activity_entreprise_strocture_textview_filiere, _ListFiliere);
		_Spinner.setAdapter(_Adapter);
	}

}

