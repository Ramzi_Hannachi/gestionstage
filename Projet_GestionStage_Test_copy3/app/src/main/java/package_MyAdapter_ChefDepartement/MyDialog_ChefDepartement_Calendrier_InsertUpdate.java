package package_MyAdapter_ChefDepartement;

import java.util.ArrayList;
import java.util.HashMap;

import package_Foction_DataBase.MySQliteFonction_ChefDepartement;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.TimePicker;

import com.example.projet_gestionstage_test.R;

public class MyDialog_ChefDepartement_Calendrier_InsertUpdate implements OnClickListener
{
	private MySQliteFonction_ChefDepartement _MySQliteFonction_ChefDepartement;

	private Context _Context;

	DatePicker _DatePicker_Date;
	TimePicker _TimePicker_HeureDebut;
	TimePicker _TimePicker_HeureFin;
	TextView _TextView_President;
	TextView _TextView_Rapporteur;
	TextView _TextView_Salle;
	Button _Button_President;
	Button _Button_Rapporteur;
	Button _Button_Salle;
	Button _Button_Confirmer;
	Button _Button_Annuler;

	String _Id_Rapporteur=null;
	String _Id_President=null;
	String _Id_Salle=null;

	HashMap<String, String> _HashMap_=new HashMap<String, String>();
	String _ID_TravailPfe;
	String _Id_EncadreurAcad;

	public MyDialog_ChefDepartement_Calendrier_InsertUpdate(Context _Context ,HashMap<String, String> _HashMap_ , String _ID_TravailPfe, String _Id_EncadreurAcad)
	{
		this._Context=_Context;
		this._HashMap_=_HashMap_;
		this._ID_TravailPfe=_ID_TravailPfe;
		this._Id_EncadreurAcad=_Id_EncadreurAcad;
		this._MySQliteFonction_ChefDepartement=new MySQliteFonction_ChefDepartement(_Context);
	}

	public void MyDialogCalendrier_InsertUpdate()
	{
		Dialog alert = new Dialog(_Context);
		alert.requestWindowFeature(Window.FEATURE_NO_TITLE);
		alert.setContentView(R.layout.activity_chefdepartement_dialog_calendrier);

		_DatePicker_Date=(DatePicker) alert.findViewById(R.id.Activity_ChefDepartement_Dialog_Calendrier_DatePicker_Date);
		_TimePicker_HeureDebut=(TimePicker) alert.findViewById(R.id.Activity_ChefDepartement_Dialog_Calendrier_TimePicker_HeureDebut);
		_TimePicker_HeureDebut.setIs24HourView(true);
		_TimePicker_HeureFin=(TimePicker) alert.findViewById(R.id.Activity_ChefDepartement_Dialog_Calendrier_TimePicker_HeureFin);
		_TimePicker_HeureFin.setIs24HourView(true);

		_Button_President=(Button) alert.findViewById(R.id.Activity_ChefDepartement_Dialog_Calendrier_Button_President);
		_Button_Rapporteur=(Button) alert.findViewById(R.id.Activity_ChefDepartement_Dialog_Calendrier_Button_Rapporteur);
		_Button_Salle=(Button) alert.findViewById(R.id.Activity_ChefDepartement_Dialog_Calendrier_Button_Salle);

		_TextView_President=(TextView) alert.findViewById(R.id.Activity_ChefDepartement_Dialog_Calendrier_TextView_President);
		_TextView_Rapporteur=(TextView) alert.findViewById(R.id.Activity_ChefDepartement_Dialog_Calendrier_TextView_Rapporteur);
		_TextView_Salle=(TextView) alert.findViewById(R.id.Activity_ChefDepartement_Dialog_Calendrier_TextView_Salle);

		_Button_Confirmer=(Button) alert.findViewById(R.id.Activity_ChefDepartement_Dialog_Calendrier_Button_confirmer);
		_Button_Annuler=(Button) alert.findViewById(R.id.Activity_ChefDepartement_Dialog_Calendrier_Button_annuler);
		//
		_Button_President.setOnClickListener(this);
		_Button_Rapporteur.setOnClickListener(this);
		_Button_Salle.setOnClickListener(this);
		_Button_Confirmer.setOnClickListener(this);
		_Button_Annuler.setOnClickListener(this);

		if (_HashMap_.isEmpty()==false) 
		{

		}

		alert.show();
	}

	public String getDate()
	{
		String jour_stage=String.valueOf(_DatePicker_Date.getDayOfMonth());
		String mois_stage=String.valueOf(_DatePicker_Date.getMonth());
		String annee_stage=String.valueOf(_DatePicker_Date.getYear());
		String _date_stage=jour_stage+"/"+mois_stage+"/"+annee_stage;
		return _date_stage;
	}
	public String getHeurDebut()
	{
		String heur=String.valueOf(_TimePicker_HeureDebut.getCurrentHour());
		String minute=String.valueOf(_TimePicker_HeureDebut.getCurrentMinute());
		String heur_minute=heur+":"+minute;
		return heur_minute;
	}
	public String getHeurFin()
	{
		String heur=String.valueOf(_TimePicker_HeureFin.getCurrentHour());
		String minute=String.valueOf(_TimePicker_HeureFin.getCurrentMinute());
		String heur_minute=heur+":"+minute;
		return heur_minute;
	}

	public void onClick(View v) 
	{
		switch (v.getId()) 
		{
		case R.id.Activity_ChefDepartement_Dialog_Calendrier_Button_President:
			final ArrayList<HashMap<String, String>> _ArrayList_Enseignant =_MySQliteFonction_ChefDepartement.Afficher_EnseignantDepartement_AFFECTATIOnENCADREUR();
			String [] from0={"Image_Enseignant","nom_Enseignant","prenom_Enseignant","matricule_Enseignant"};
			int [] to0={R.id.strocture_RapporteurPresident_ImageView,R.id.strocture_RapporteurPresident_TextView_Nom,R.id.strocture_RapporteurPresident_TextView_Prenom,R.id.strocture_RapporteurPresident_TextView_Matricule};

			AlertDialog.Builder _Builder=new AlertDialog.Builder(_Context);
			_Builder.setTitle("Liste des enseignants");
			SimpleAdapter _SimpleAdapter_President = new SimpleAdapter(_Context, _ArrayList_Enseignant, R.layout.activity_chefdepartement_strocture_rapporteurpresident, from0, to0);
			_Builder.setSingleChoiceItems(_SimpleAdapter_President, 0, new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int position)
				{
					_Id_President=_ArrayList_Enseignant.get(position).get("ID_Enseignant");
					String _nom_enseignant=_ArrayList_Enseignant.get(position).get("nom_Enseignant");
					String _prenom_enseignant=_ArrayList_Enseignant.get(position).get("prenom_Enseignant");
					_TextView_President.setText(" "+_nom_enseignant+" "+_prenom_enseignant+" ");
					_TextView_President.setTextColor(_Context.getResources().getColor(android.R.color.black));
					dialog.dismiss();
				}
			});
			_Builder.show();
			break;
		case R.id.Activity_ChefDepartement_Dialog_Calendrier_Button_Rapporteur:
			final ArrayList<HashMap<String, String>> _ArrayList_Enseignantt =_MySQliteFonction_ChefDepartement.Afficher_EnseignantDepartement_AFFECTATIOnENCADREUR();
			String [] from1={"Image_Enseignant","nom_Enseignant","prenom_Enseignant","matricule_Enseignant"};
			int [] to1={R.id.strocture_RapporteurPresident_ImageView,R.id.strocture_RapporteurPresident_TextView_Nom,R.id.strocture_RapporteurPresident_TextView_Prenom,R.id.strocture_RapporteurPresident_TextView_Matricule};

			AlertDialog.Builder _Builder1=new AlertDialog.Builder(_Context);
			_Builder1.setTitle("Liste des enseignants");
			SimpleAdapter _SimpleAdapter_Rapporteur = new SimpleAdapter(_Context, _ArrayList_Enseignantt, R.layout.activity_chefdepartement_strocture_rapporteurpresident, from1, to1);
			_Builder1.setSingleChoiceItems(_SimpleAdapter_Rapporteur, 0, new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int position)
				{
					_Id_Rapporteur=_ArrayList_Enseignantt.get(position).get("ID_Enseignant");
					String _nom_enseignant=_ArrayList_Enseignantt.get(position).get("nom_Enseignant");
					String _prenom_enseignant=_ArrayList_Enseignantt.get(position).get("prenom_Enseignant");
					_TextView_Rapporteur.setText(" "+_nom_enseignant+" "+_prenom_enseignant+" ");
					_TextView_Rapporteur.setTextColor(_Context.getResources().getColor(android.R.color.black));
					dialog.dismiss();
				}
			});
			_Builder1.show();
			break;

		case R.id.Activity_ChefDepartement_Dialog_Calendrier_Button_Salle:
			final ArrayList<HashMap<String, String>> _ArrayList_Salle=_MySQliteFonction_ChefDepartement.Salle_DialogCALENDRIER();
			String [] from={"Image_Salle","bloc_Salle","etage_Salle","abreviation_Salle"};
			int [] to={R.id.strocture_Salle_ImageView,R.id.strocture_Salle_TextView_Bloc,R.id.strocture_Salle_TextView_Etage,R.id.strocture_Salle_TextView_Abreviation};
			// AlertBuilder
			AlertDialog.Builder _Builder2=new AlertDialog.Builder(_Context);
			_Builder2.setTitle("Liste des Salles");
			SimpleAdapter _SimpleAdapter = new SimpleAdapter(_Context, _ArrayList_Salle, R.layout.activity_chefdepartement_strocture_salle, from, to);
			_Builder2.setSingleChoiceItems(_SimpleAdapter, 0, new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int position)
				{
					_Id_Salle=_ArrayList_Salle.get(position).get("ID_Salle");
					String _abreviation_Salle=_ArrayList_Salle.get(position).get("abreviation_Salle");
					_TextView_Salle.setText(_abreviation_Salle);
					_TextView_Salle.setTextColor(_Context.getResources().getColor(android.R.color.black));
					dialog.dismiss();
				}
			});
			_Builder2.show();
			break;

		case R.id.Activity_ChefDepartement_Dialog_Calendrier_Button_confirmer:
			if (Verifier_Date())
			{
				if (Verifier_Time())
				{
					if (Verifier_Jury())
					{
						if (Verifier_Salle())
						{
							if (_HashMap_.isEmpty())
							{ // insertion
								Insertion_Soutenance();
							}else
							{// modification

							}
						}
					}
				}
			}
			break;
		case R.id.Activity_ChefDepartement_Dialog_Calendrier_Button_annuler:
			_TextView_President.setText(" Président ");
			_TextView_President.setTextColor(_Context.getResources().getColor(R.color.gray));
			_TextView_Rapporteur.setText(" Rapporteur ");
			_TextView_Rapporteur.setTextColor(_Context.getResources().getColor(R.color.gray));
			_TextView_Salle.setText(" Salle ");
			_TextView_Salle.setTextColor(_Context.getResources().getColor(R.color.gray));
			break;
		default:
			break;
		}
	}

	public void Insertion_Soutenance()
	{
		AlertDialog.Builder _Builder0=new AlertDialog.Builder(_Context);
		_Builder0.setTitle("Insertion Soutenance");
		_Builder0.setMessage("Êtes-vous sûr de vouloir ajouter cette soutenance");
		_Builder0.setPositiveButton("Confirmer", new DialogInterface.OnClickListener() 
		{
			public void onClick(DialogInterface arg0, int arg1) 
			{
				String _URL0=_Context.getResources().getString(R.string.ChefDepartementActivity_insert_Soutenance);
				String _ID_Soutenance=_MySQliteFonction_ChefDepartement.InsertInTo_Serveur_Soutenance(getHeurDebut(), getHeurFin(), getDate(), _Id_Salle, _ID_TravailPfe, _URL0);
				if (_ID_Soutenance.equals("")==false)
				{
					String _URL1=_Context.getResources().getString(R.string.ChefDepartementActivity_insert_Deriger);	
					boolean y=_MySQliteFonction_ChefDepartement.InsertInTo_Serveur_Deriger("President", _Id_President, _ID_Soutenance, _URL1);
					boolean x=_MySQliteFonction_ChefDepartement.InsertInTo_Serveur_Deriger("Rapporteur", _Id_Rapporteur, _ID_Soutenance, _URL1);
					if (x && y)
					{
						Refresh();
					}
				}
			}
		});
		_Builder0.setNegativeButton("Annuler", null);
		_Builder0.show();
	}

	public boolean Verifier_Date()
	{
		String _mois_stage=String.valueOf(_DatePicker_Date.getMonth());
		if (_mois_stage.equals("4"))
		{
			return true;
		}else
		{
			AlertDialog.Builder _Builder0=new AlertDialog.Builder(_Context);
			_Builder0.setTitle("Erreur");
			_Builder0.setMessage("La date de la soutenance est invalide");
			_Builder0.show();
			return false;
		}
	}

	public boolean Verifier_Salle()
	{
		if (_Id_Salle==null)
		{
			AlertDialog.Builder _Builder0=new AlertDialog.Builder(_Context);
			_Builder0.setTitle("Erreur");
			_Builder0.setMessage("Vous devez choisir une salle avant de confirmer");
			_Builder0.show();
			return false;
		}else 
		{
			return true;
		}
	}

	public boolean Verifier_Jury()
	{
		boolean ok=false;
		if (_Id_President!=null && _Id_Rapporteur!=null)
		{
			if (_Id_EncadreurAcad==_Id_President)
			{
				AlertDialog.Builder _Builder0=new AlertDialog.Builder(_Context);
				_Builder0.setTitle("Erreur");
				_Builder0.setMessage("L'encadreur Académique et le président de jury sont identiques ");
				_Builder0.show();
			}else if (_Id_EncadreurAcad==_Id_Rapporteur) 
			{
				AlertDialog.Builder _Builder0=new AlertDialog.Builder(_Context);
				_Builder0.setTitle("Erreur");
				_Builder0.setMessage("L'encadreur Académique et le rapporteur sont identiques ");
				_Builder0.show();
			}else if (_Id_President==_Id_Rapporteur)
			{
				AlertDialog.Builder _Builder0=new AlertDialog.Builder(_Context);
				_Builder0.setTitle("Erreur");
				_Builder0.setMessage("Le président de jury et le rapporteur sont identiques ");
				_Builder0.show();
			}else
			{
				ok= true;	
			}
		}else
		{
			AlertDialog.Builder _Builder0=new AlertDialog.Builder(_Context);
			_Builder0.setTitle("Erreur");
			_Builder0.setMessage("Vous devez choisir un président et un rapporteur avant de confirmer");
			_Builder0.show();
		}
		return ok;
	}

	public boolean Verifier_Time()
	{
		int heurDebut=_TimePicker_HeureDebut.getCurrentHour();
		int heurFin=_TimePicker_HeureFin.getCurrentHour();
		int minuteDebut=_TimePicker_HeureDebut.getCurrentMinute();
		int minuteFin=_TimePicker_HeureFin.getCurrentMinute();

		int result_heure=0;
		int result_minute=0;
		result_heure=heurFin-heurDebut;
		if (result_heure>=0)
		{
			if (minuteDebut>=minuteFin)
			{
				result_minute=((heurFin-heurDebut)*60)-(minuteDebut-minuteFin);
				if (result_minute>=60)
				{
					result_heure=result_minute/60;
					result_minute=result_minute%60;
				}else
				{
					result_heure=0;
				}
			}else
			{
				result_minute=minuteFin-minuteDebut;
			}
			if (result_minute<0)
			{
				AlertDialog.Builder _Builder0=new AlertDialog.Builder(_Context);
				_Builder0.setTitle("Erreur");
				_Builder0.setMessage("Heures invalide");
				_Builder0.show();
				return false;
			}else
			{
				AlertDialog.Builder _Builder=new AlertDialog.Builder(_Context);
				_Builder.setTitle("Infromation");
				_Builder.setMessage("La durée de la soutenance : "+ result_heure+"Heures et "+result_minute+"Minutes");
				_Builder.show();	
				return true;
			}
		}else
		{
			AlertDialog.Builder _Builder0=new AlertDialog.Builder(_Context);
			_Builder0.setTitle("Erreur");
			_Builder0.setMessage("heures invalide");
			_Builder0.show();
			return false;
		}


	}

	public void Refresh()
	{
		((Activity) _Context).finish();
		Intent lIntent_To_Activity = new Intent(_Context, ((Activity)_Context).getClass());
		_Context.startActivity(lIntent_To_Activity);

	}

}