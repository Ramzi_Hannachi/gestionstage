package package_MyAdapter_ChefDepartement;

import java.util.ArrayList;
import java.util.HashMap;

import package_Foction_DataBase.MySQliteFonction_ChefDepartement;
import package_Foction_DataBase.MySQliteFonction_Entreprise;
import package_MemoirStage.Activity_ChefDepartement;
import package_MyAdapter_Entreprise.MyDialog_Entreprise_DepotOffreStage_InsertUdate;
import package_MyAdapter_Entreprise.MyDialog_Entreprise_InscriptionInformation;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.projet_gestionstage_test.R;

public class MyExpandableListAdapter_ChefDepartement_OffreStage extends BaseExpandableListAdapter implements OnClickListener 
{
	private String TYPE_OFFRESTAGE;

	private MySQliteFonction_ChefDepartement _MySQliteFonction_ChefDepartement;

	private Context _Context;
	private ArrayList<HashMap<String, String>> _List_OffreStage=new ArrayList<HashMap<String,String>>();
	private ArrayList<HashMap<String, String>> _List_Entreprise=new ArrayList<HashMap<String,String>>();

	public MyExpandableListAdapter_ChefDepartement_OffreStage(Context _Context,ArrayList<HashMap<String, String>> _List_OffreStage,ArrayList<HashMap<String, String>> _List_Entreprise,String TYPE_OFFRESTAGE)
	{
		this._Context=_Context;
		this._List_Entreprise=_List_Entreprise;
		this._List_OffreStage=_List_OffreStage;

		this.TYPE_OFFRESTAGE=TYPE_OFFRESTAGE;
	}

	public HashMap<String, String> getChild(int groupPosition, int childPosition)
	{
		String _ID_Entreprise=_List_Entreprise.get(groupPosition).get("ID_Entreprise");
		ArrayList<HashMap<String, String>> _list_offreStage=new ArrayList<HashMap<String,String>>();
		for (int i = 0; i < _List_OffreStage.size(); i++)
		{
			if (_List_OffreStage.get(i).get("cle_entreprise_OffreStage").equals(_ID_Entreprise))
			{
				_list_offreStage.add(_List_OffreStage.get(i));
			}
		}
		return _list_offreStage.get(childPosition);
	}

	public long getChildId(int groupPosition, int childPosition)
	{
		return childPosition;
	}

	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View contentView,ViewGroup parent)
	{
		final HashMap<String, String> _list_offreStage=getChild(groupPosition, childPosition);
		if (contentView==null)
		{
			LayoutInflater layoutInflater=(LayoutInflater) _Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			contentView=layoutInflater.inflate(R.layout.activity_chefdepartement_strocture_offrestage_child, null);
		}
		TextView _TextView_SiteWeb=(TextView) contentView.findViewById(R.id.ActivityChefDepartement_strocture_OffreStage_Child_TextView_SiteWebEntreprise);
		TextView _TextView_NumTel=(TextView) contentView.findViewById(R.id.ActivityChefDepartement_strocture_OffreStage_Child_TextView_NumTelEntreprise);
		TextView _TextView_NomOffre=(TextView) contentView.findViewById(R.id.ActivityChefDepartement_strocture_OffreStage_Child_TextView_NomOffreStage);
		Button _Button_DescriptionOffreStage=(Button) contentView.findViewById(R.id.ActivityChefDepartement_strocture_OffreStage_Child_TextView_DescriptionOffreStage);
		Button _Button_Confirmation=(Button) contentView.findViewById(R.id.ActivityChefDepartement_strocture_OffreStage_Child_Button_Confirmation);
		TextView _TextView_Date=(TextView) contentView.findViewById(R.id.ActivityChefDepartement_strocture_OffreStage_Child_TextView_DateOffreStage);

		Button _Button_Confirmaiton_Add=(Button) contentView.findViewById(R.id.ActivityChefDepartement_strocture_OffreStage_Child_Button_ConfirmerOffreStage);
		Button _Button_Confirmation_Remove=(Button) contentView.findViewById(R.id.ActivityChefDepartement_strocture_OffreStage_Child_Button_RefuerOffreStage);
		Button _Button_Option=(Button) contentView.findViewById(R.id.ActivityChefDepartement_strocture_OffreStage_Child_Button_Option);

		_TextView_SiteWeb.setText(_List_Entreprise.get(groupPosition).get("siteWeb_Entreprise"));
		_TextView_NumTel.setText(_List_Entreprise.get(groupPosition).get("numTel_Entreprise"));
		_TextView_NomOffre.setText(_list_offreStage.get("nom_OffreStage"));
		_TextView_Date.setText(_list_offreStage.get("date_OffreStage"));

		_Button_DescriptionOffreStage.setTag(R.id.description_offreStage,_list_offreStage.get("description_OffreStage"));
		_Button_DescriptionOffreStage.setOnClickListener(this);

		_Button_Confirmaiton_Add.setTag(R.id.id_offreStage, _list_offreStage.get("ID_OffreStage"));
		_Button_Confirmaiton_Add.setOnClickListener(this);

		_Button_Confirmation_Remove.setTag(R.id.id_offreStage, _list_offreStage.get("ID_OffreStage"));
		_Button_Confirmation_Remove.setOnClickListener(this);

		_Button_Option.setTag(R.id.position,groupPosition);
		_Button_Option.setTag(R.id._HashMap,_list_offreStage);
		_Button_Option.setOnClickListener(this);

		Log.d("_list_offreStage", String.valueOf(_list_offreStage));

		Verifier_Confirmation(groupPosition,_list_offreStage, _Button_Confirmaiton_Add, _Button_Confirmation_Remove, _Button_Confirmation);
		Verifier_Option_Child(_list_offreStage,_Button_Option);
		Verifier_Visibilite_Etudiant(_Button_Confirmaiton_Add,_Button_Confirmation_Remove);
		return contentView;
	}

	@SuppressWarnings("unchecked")
	public void onClick(View v)
	{
		switch (v.getId()) 
		{
		case R.id.ActivityChefDepartement_strocture_OffreStage_Child_TextView_DescriptionOffreStage:
			String _offreStage_description=(String) v.getTag(R.id.description_offreStage);
			AlertDialog.Builder box = new AlertDialog.Builder(_Context);
			box.setTitle("Description Offre De Stage");
			String SMS="<br />"+_offreStage_description+"<br />";
			box.setMessage(Html.fromHtml("<font color=\"#000000 \">" + SMS + "</font>",null, null));
			AlertDialog dialog = box.show();
			TextView messageText = (TextView)dialog.findViewById(android.R.id.message);
			messageText.setGravity(Gravity.CENTER);
			messageText.setTextSize(14);
			dialog.show();
			break;

		case R.id.ActivityChefDepartement_strocture_OffreStage_Child_Button_ConfirmerOffreStage:
			String _ID_offreStage= (String) v.getTag(R.id.id_offreStage);
			if (TYPE_OFFRESTAGE.equals("ENTREPRISE"))
			{
				Accepter_OffreStage(_ID_offreStage);	
			}
			if (TYPE_OFFRESTAGE.equals("DEPOT")) 
			{
				ConfirmerVisibliter_OffreStage(_ID_offreStage);
			}
			break;

		case R.id.ActivityChefDepartement_strocture_OffreStage_Child_Button_RefuerOffreStage:
			String _ID_offreStage1= (String) v.getTag(R.id.id_offreStage);
			if (TYPE_OFFRESTAGE.equals("ENTREPRISE"))
			{
				Refuser_OffreStage(_ID_offreStage1);	
			}
			break;

		case R.id.ActivityChefDepartement_strocture_OffreStage_Child_Button_Option:
			final HashMap<String, String> _HashMap_offreStage=(HashMap<String, String>) v.getTag(R.id._HashMap);
			final int groupPosition_child=(Integer) v.getTag(R.id.position);
			final String _ID_Entreprise_child=_List_Entreprise.get(groupPosition_child).get("ID_Entreprise");
			AlertDialog.Builder _Builder=new AlertDialog.Builder(_Context);
			_Builder.setTitle(" Options ");
			//_Builder.setMessage(" Que Voulez-Vous Faire ? ");
			_Builder.setItems(new CharSequence[]
					{ " Modifier l'Offre de Stage ", " Supprimer l'Offre de Stage"},
					new DialogInterface.OnClickListener()
			{
				public void onClick(DialogInterface dialog, int which)
				{
					switch (which)
					{
					case 0:
						// modifier l'offre de stage
						MyDialog_Entreprise_DepotOffreStage_InsertUdate _MyDialog_Entreprise_DepotStage_InsertUdate1 = new MyDialog_Entreprise_DepotOffreStage_InsertUdate(_Context,_HashMap_offreStage ,_ID_Entreprise_child);
						_MyDialog_Entreprise_DepotStage_InsertUdate1.DialogEntrepirse_DepostageInsert();
						break;
					case 1:
						// supprimer l'offre de stage
						Supprimer_OffreStage(_HashMap_offreStage);
						break;
					}
				}
			});
			_Builder.create().show();
			break;

		case R.id.ActivityChefDepartement_strocture_OffreStage_Parent_Option : 
			final int groupPosition_parent=(Integer) v.getTag(R.id.position);
			final String _ID_Entreprise_parent=_List_Entreprise.get(groupPosition_parent).get("ID_Entreprise");
			AlertDialog.Builder _Builder_parent=new AlertDialog.Builder(_Context);
			_Builder_parent.setTitle(" Options ");
			//_Builder.setMessage(" Que Voulez-Vous Faire ? ");
			_Builder_parent.setItems(new CharSequence[]
					{" Modifier l'Entreprise ", " Supprimer l'Entreprise "," Ajouter une Offre de Stage "},
					new DialogInterface.OnClickListener()
			{
				public void onClick(DialogInterface dialog, int which)
				{
					switch (which)
					{
					case 0:
						// modification entreprise
						ArrayList<HashMap<String, String>> _ArrayList_EntrepriseInformation= new  ArrayList<HashMap<String,String>>();
						Log.d("groupPosition_parent",String.valueOf(groupPosition_parent));
						_ArrayList_EntrepriseInformation.add(getGroup(groupPosition_parent));
						MyDialog_Entreprise_InscriptionInformation _MyDialog_Entreprise_InscriptionInformation = new MyDialog_Entreprise_InscriptionInformation(_Context, _ArrayList_EntrepriseInformation,null);
						_MyDialog_Entreprise_InscriptionInformation.DialogEntrepriseInscription();
						break;
					case 1:
						// supprimer entreprise
						Log.d("_ID_Entreprise_parent",_ID_Entreprise_parent );
						Supprimer_Entreprise(_ID_Entreprise_parent);
						break;
					case 2:
						// ajouter offre stage
						MyDialog_Entreprise_DepotOffreStage_InsertUdate _MyDialog_Entreprise_DepotStage_InsertUdate0 = new MyDialog_Entreprise_DepotOffreStage_InsertUdate(_Context,null ,_ID_Entreprise_parent);
						_MyDialog_Entreprise_DepotStage_InsertUdate0.DialogEntrepirse_DepostageInsert();
						break;
					}
				}
			});
			_Builder_parent.create().show();

			break;
		default:
			break;
		}
	}
	public void Supprimer_Entreprise(final String _ID_Entreprise)
	{
		AlertDialog.Builder _Builder =new AlertDialog.Builder(_Context);
		_Builder.setTitle("Suppression");
		_Builder.setMessage("êtes-vous sûr de vouloir supprimer cette Entreprise ? , " +
				"veuillez vérifier que toutes les offres de stage et les stages en relation avec cette entreprise en été supprimer");
		_Builder.setNegativeButton("Annuler", null);
		_Builder.setPositiveButton("Confirmer", new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface arg0, int arg1)
			{
				String _URL=_Context.getResources().getString(R.string.ChefDepartementActivity_delete_Entreprise);
				_MySQliteFonction_ChefDepartement= new MySQliteFonction_ChefDepartement(_Context);
				boolean x=_MySQliteFonction_ChefDepartement.DeleteInTo_Serveur_Entreprise(_ID_Entreprise, _URL);
				if (x)
				{
					Refresh_Activity();
				}
			}
		});
		_Builder.show();
	}
	public void Supprimer_OffreStage(final HashMap<String, String> _list_offreStage)
	{
		AlertDialog.Builder _Builder =new AlertDialog.Builder(_Context);
		_Builder.setTitle("Suppression");
		_Builder.setMessage("êtes-vous sûr de vouloir supprimer cette offre de stage ?");
		_Builder.setNegativeButton("Annuler", null);
		_Builder.setPositiveButton("Confirmer", new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface arg0, int arg1)
			{
				MySQliteFonction_Entreprise _MySQliteFonction_Entreprise=new MySQliteFonction_Entreprise(_Context);
				boolean x=_MySQliteFonction_Entreprise.DeleteInTo_Serveur_OffreStage(_list_offreStage.get("ID_OffreStage"), _Context.getResources().getString(R.string.EntrepriseActivity_delete_OffreStage));
				if (x)
				{   
					Refresh_Activity();	
				}
			}
		});
		_Builder.show();
	}
	public void Verifier_Visibilite_Etudiant(Button _Button_Confirmation_Add,Button _Button_Confirmation_Remove)
	{
		if (TYPE_OFFRESTAGE.equals("DEPOT"))
		{
			_Button_Confirmation_Add.setBackgroundResource(R.drawable.cadenas_rouge);
			_Button_Confirmation_Remove.setVisibility(View.GONE);
		}
		if (TYPE_OFFRESTAGE.equals("ENTREPRISE"))
		{
			_Button_Confirmation_Add.setBackgroundResource(R.drawable.ajouter_confirmation);
			_Button_Confirmation_Remove.setBackgroundResource(R.drawable.supprimer_confirmation);
		}
	}
	public void Verifier_Option_Child(HashMap<String, String> _list_offreStage,Button _Button_Option) 
	{
		if (TYPE_OFFRESTAGE.equals("DEPOT") && _list_offreStage.get("confirmation_OffreStage").equals("null"))
		{
			_Button_Option.setVisibility(View.VISIBLE);
		}
		if (TYPE_OFFRESTAGE.equals("DEPOT") && _list_offreStage.get("confirmation_OffreStage").equals("null")==false)
		{
			_Button_Option.setVisibility(View.GONE);
		}
		if (TYPE_OFFRESTAGE.equals("ENTREPRISE"))
		{
			_Button_Option.setVisibility(View.GONE);
		}
	}
	//
	public void Accepter_OffreStage(final String _ID_offreStage)
	{
		AlertDialog.Builder _Builder0 =new AlertDialog.Builder(_Context);
		_Builder0.setTitle("Confirmation");
		_Builder0.setMessage("Êtes-vous sûr de vouloir Accepter l'offre de Stage ? , Opération irréversible !");
		_Builder0.setNegativeButton("Annuler", null);
		_Builder0.setPositiveButton("Confirmer", new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int which)
			{
				String _URL=_Context.getResources().getString(R.string.ChefDepartementActivity_update_OffreStage);
				_MySQliteFonction_ChefDepartement=new MySQliteFonction_ChefDepartement(_Context);
				boolean x=_MySQliteFonction_ChefDepartement.UpdateInTo_Serveur_OffreStage("1", Integer.parseInt(_ID_offreStage), _URL);
				if (x) 
				{
					Refresh_Activity();
				}
			}
		});
		_Builder0.show();
	}
	public void Refuser_OffreStage(final String _ID_offreStage1)
	{
		AlertDialog.Builder _Builder1 =new AlertDialog.Builder(_Context);
		_Builder1.setTitle("Réfutation");
		_Builder1.setMessage("Êtes-vous sûr de vouloir Réfuter l'offre de Stage ?");
		_Builder1.setNegativeButton("Annuler", null);
		_Builder1.setPositiveButton("Confirmer", new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int which)
			{
				String _URL=_Context.getResources().getString(R.string.ChefDepartementActivity_update_OffreStage);
				_MySQliteFonction_ChefDepartement=new MySQliteFonction_ChefDepartement(_Context);
				boolean x=_MySQliteFonction_ChefDepartement.UpdateInTo_Serveur_OffreStage("0", Integer.parseInt(_ID_offreStage1), _URL);
				if (x) 
				{
					Refresh_Activity();
				}
			}
		});
		_Builder1.show();
	}
	public void ConfirmerVisibliter_OffreStage(final String _ID_offreStage)
	{
		AlertDialog.Builder _Builder0 =new AlertDialog.Builder(_Context);
		_Builder0.setTitle("Visibilité");
		_Builder0.setMessage("Êtes-vous sûr de vouloir rendre cette offre de Stage visible par les Etudiant , Opération irréversible !");
		_Builder0.setNegativeButton("Annuler", null);
		_Builder0.setPositiveButton("Confirmer", new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int which)
			{
				String _URL=_Context.getResources().getString(R.string.ChefDepartementActivity_update_OffreStage);
				_MySQliteFonction_ChefDepartement=new MySQliteFonction_ChefDepartement(_Context);
				boolean x=_MySQliteFonction_ChefDepartement.UpdateInTo_Serveur_OffreStage("1", Integer.parseInt(_ID_offreStage), _URL);
				if (x) 
				{
					Refresh_Activity();
				}
			}
		});
		_Builder0.show();
	}
	//
	public void Verifier_Confirmation(int groupPosition,HashMap<String, String> _list_offreStage,Button _Button_Confirmation_Add,Button _Button_Confirmation_Remove,Button _Button_Confirmation)
	{
		String confirmation_OffreStage=_list_offreStage.get("confirmation_OffreStage");
		String _Id_stage_OffreStage=_list_offreStage.get("cle_stage_OffreStage");//_List_OffreStage.get(groupPosition).get("cle_stage_OffreStage");
		String _Id_travailPfe_OffreStage=_list_offreStage.get("cle_travailPfe_OffreStage");//_List_OffreStage.get(groupPosition).get("cle_travailPfe_OffreStage");

		if (confirmation_OffreStage.equals("null"))
		{
			_Button_Confirmation_Add.setVisibility(View.VISIBLE);
			_Button_Confirmation_Remove.setVisibility(View.VISIBLE);
			_Button_Confirmation.setBackgroundResource(R.drawable.button_ajouter_offre_noir);
		}
		if (confirmation_OffreStage.equals("0"))
		{
			_Button_Confirmation_Add.setVisibility(View.VISIBLE);
			_Button_Confirmation_Remove.setVisibility(View.GONE);	
			_Button_Confirmation.setBackgroundResource(R.drawable.button_ajouter_offre_refuser);
		}
		if (confirmation_OffreStage.equals("1")  )
		{
			_Button_Confirmation_Add.setVisibility(View.GONE);
			_Button_Confirmation_Remove.setVisibility(View.GONE);	
			_Button_Confirmation.setBackgroundResource(R.drawable.button_ajouter_offre_accepter);
		}
		if (confirmation_OffreStage.equals("2") && _Id_stage_OffreStage==null && _Id_travailPfe_OffreStage==null)
		{
			_Button_Confirmation_Add.setVisibility(View.GONE);
			_Button_Confirmation_Remove.setVisibility(View.GONE);	
			_Button_Confirmation.setBackgroundResource(R.drawable.button_ajouter_offre_accepter);
		}
		if ( confirmation_OffreStage.equals("2") && _Id_stage_OffreStage!=null && _Id_travailPfe_OffreStage!=null )
		{
			_Button_Confirmation_Add.setVisibility(View.GONE);
			_Button_Confirmation_Remove.setVisibility(View.GONE);	
			_Button_Confirmation.setBackgroundResource(R.drawable.button_signer_stage_vert);

		}
	}
	public int getChildrenCount(int groupPosition)
	{
		String _ID_Entreprise=_List_Entreprise.get(groupPosition).get("ID_Entreprise");
		ArrayList<HashMap<String, String>> _list_offreStage=new ArrayList<HashMap<String,String>>();
		for (int i = 0; i < _List_OffreStage.size(); i++)
		{
			if (_List_OffreStage.get(i).get("cle_entreprise_OffreStage").equals(_ID_Entreprise))
			{
				_list_offreStage.add(_List_OffreStage.get(i));
			}
		}
		return _list_offreStage.size();
	}

	public HashMap<String, String> getGroup(int groupPosition)
	{
		return _List_Entreprise.get(groupPosition);
	}
	public int getGroupCount()
	{
		return _List_Entreprise.size();
	}
	public long getGroupId(int groupPosition)
	{
		return groupPosition;
	}
	public View getGroupView( int groupPosition, boolean childPosition, View contentView, ViewGroup parent)
	{
		HashMap<String, String> _list_Entreprise=  getGroup(groupPosition);
		if (contentView==null)
		{
			LayoutInflater layoutInflater=(LayoutInflater) _Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			contentView=layoutInflater.inflate(R.layout.activity_chefdepartement_strocture_offrestage_parent, null);
		}

		final TextView _TextView_libelleEntreprise=(TextView) contentView.findViewById(R.id.ActivityChefDepartement_strocture_OffreStage_Parent_TextView_Libelle);
		ImageView _ImageView=(ImageView) contentView.findViewById(R.id.ActivityChefDepartement_strocture_OffreStage_Parent_ImageView);
		Button _Button_Option_Parent=(Button) contentView.findViewById(R.id.ActivityChefDepartement_strocture_OffreStage_Parent_Option);

		if (childPosition) 
		{ //séléctionnné
			_TextView_libelleEntreprise.setTextColor(_Context.getResources().getColor(R.color.orange));
			_ImageView.setImageDrawable(_Context.getResources().getDrawable(R.drawable.entreprise_ouvert));
		}else 
		{ //non séléctionné
			_TextView_libelleEntreprise.setTextColor(_Context.getResources().getColor(android.R.color.secondary_text_light));
			_ImageView.setImageDrawable(_Context.getResources().getDrawable(R.drawable.entreprise_fermer));
		}

		Verifier_Option_Parent(_Button_Option_Parent);

		_TextView_libelleEntreprise.setTag(R.id.position,groupPosition);
		_TextView_libelleEntreprise.setText(_list_Entreprise.get("libelle_Entreprise"));
		_TextView_libelleEntreprise.setOnClickListener(new OnClickListener()
		{
			public void onClick(View arg0) 
			{
				int groupPosition=(Integer) _TextView_libelleEntreprise.getTag(R.id.position);
				if (TYPE_OFFRESTAGE.equals("DEPOT"))
				{
					(((Activity_ChefDepartement) _Context)).expandGroup(Activity_ChefDepartement._ExpandableListView_OffreStage_Depot,groupPosition);
				}
				if (TYPE_OFFRESTAGE.equals("ENTREPRISE"))
				{
					(((Activity_ChefDepartement) _Context)).expandGroup(Activity_ChefDepartement._ExpandableListView_OffreStage_Entreprise,groupPosition);
				}
			}
		});

		_Button_Option_Parent.setTag(R.id.position,groupPosition);
		_Button_Option_Parent.setOnClickListener(this);

		return contentView;
	}
	public void Verifier_Option_Parent(Button _Button_Option_Parent)
	{
		if (TYPE_OFFRESTAGE.equals("DEPOT"))
		{
			_Button_Option_Parent.setVisibility(View.VISIBLE);
		}
		if (TYPE_OFFRESTAGE.equals("ENTREPRISE"))
		{
			_Button_Option_Parent.setVisibility(View.GONE);
		}
	}
	public boolean hasStableIds()
	{
		return false;
	}
	public boolean isChildSelectable(int arg0, int arg1)
	{
		return false;
	}

	public void Refresh_Activity()
	{
		((Activity) _Context).finish();
		Intent _Intent_To_ActivityChefDepartement=new Intent(_Context,Activity_ChefDepartement.class);
		_Context.startActivity(_Intent_To_ActivityChefDepartement);
	}
}
