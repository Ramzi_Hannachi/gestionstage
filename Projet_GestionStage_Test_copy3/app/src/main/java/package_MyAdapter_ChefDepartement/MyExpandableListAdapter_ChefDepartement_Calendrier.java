package package_MyAdapter_ChefDepartement;

import java.util.ArrayList;
import java.util.HashMap;

import package_Foction_DataBase.MySQliteFonction_ChefDepartement;
import package_MemoirStage.Activity_ChefDepartement;
import package_MyAdapter_Etudiant.MyDialog_EtudiantInformation;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.projet_gestionstage_test.R;

public class MyExpandableListAdapter_ChefDepartement_Calendrier extends BaseExpandableListAdapter implements OnClickListener 
{
	MySQliteFonction_ChefDepartement _MySQliteFonction_ChefDepartement;

	private Context _Context;
	private ArrayList<HashMap<String, String>> _List_Stage=new ArrayList<HashMap<String,String>>();

	private String _ID_Binome=null;

	public MyExpandableListAdapter_ChefDepartement_Calendrier(Context _Context,ArrayList<HashMap<String, String>> _List_Stage )
	{
		_MySQliteFonction_ChefDepartement=new MySQliteFonction_ChefDepartement(_Context);

		this._Context=_Context;
		this._List_Stage=_List_Stage;
	}

	public HashMap<String, String> getChild(int groupPosition, int childPosition) 
	{
		String _ID_TravailPfe=_List_Stage.get(groupPosition).get("_Id_travailPfe_Stage");
		ArrayList<HashMap<String, String>> _List_EntrepriseEtudiantEncadreur=new ArrayList<HashMap<String,String>>();
		_List_EntrepriseEtudiantEncadreur=_MySQliteFonction_ChefDepartement.EntrepriseEtudiantEncadreur_CALENDRIER(_ID_TravailPfe);
		if (_List_EntrepriseEtudiantEncadreur.size()==2)
		{
			return _List_EntrepriseEtudiantEncadreur.get(1);
		}else
		{
			return _List_EntrepriseEtudiantEncadreur.get(childPosition);
		}
	}
	public int getChildrenCount(int groupPosition)
	{
		String _ID_TravailPfe=_List_Stage.get(groupPosition).get("_Id_travailPfe_Stage");
		ArrayList<HashMap<String, String>> _List_EntrepriseEtudiantEncadreur=new ArrayList<HashMap<String,String>>();
		_List_EntrepriseEtudiantEncadreur=_MySQliteFonction_ChefDepartement.EntrepriseEtudiantEncadreur_CALENDRIER(_ID_TravailPfe);

		if (_List_EntrepriseEtudiantEncadreur.size()==2) 
		{
			return 1;
		}else
		{
			return _List_EntrepriseEtudiantEncadreur.size();
		}
	}
	public long getChildId(int groupPosition, int childPosition)
	{
		return childPosition;
	}
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View contentView,ViewGroup parent)
	{
		HashMap<String, String> _list_EntrepriseEtudiantEncadreur=getChild(groupPosition, childPosition);	
		if (contentView==null)
		{
			LayoutInflater layoutInflater=(LayoutInflater) _Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			contentView=layoutInflater.inflate(R.layout.activity_chefdepartement_strocture_calendrier_child, null);
		}
		Button _Button_Entreprise=(Button) contentView.findViewById(R.id.ActivityChefDepartement_strocture_Calendrier_Child_Button_Entreprise);
		Button _Button_Description=(Button) contentView.findViewById(R.id.ActivityChefDepartement_strocture_Calendrier_Child_Button_Description);
		Button _Button_Etudiant1=(Button) contentView.findViewById(R.id.ActivityChefDepartement_strocture_Calendrier_Child_Button_Etudiant1);
		Button _Button_Etudiant2=(Button) contentView.findViewById(R.id.ActivityChefDepartement_strocture_Calendrier_Child_Button_Etudiant2);
		Button _Button_EncadreurAcad=(Button) contentView.findViewById(R.id.ActivityChefDepartement_strocture_Calendrier_Child_Button_EncadreurAcad);
		Button _Button_Rapporteur=(Button) contentView.findViewById(R.id.ActivityChefDepartement_strocture_Calendrier_Child_Button_Rapporteur);
		Button _Button_President=(Button) contentView.findViewById(R.id.ActivityChefDepartement_strocture_Calendrier_Child_Button_President);
		TextView _TextView_Jour=(TextView) contentView.findViewById(R.id.ActivityChefDepartement_strocture_Calendrier_Child_TextView_Jour);
		TextView _TextView_HeurDebut=(TextView) contentView.findViewById(R.id.ActivityChefDepartement_strocture_Calendrier_Child_TextView_HeurDebut);
		TextView _TextView_HeurFin=(TextView) contentView.findViewById(R.id.ActivityChefDepartement_strocture_Calendrier_Child_TextView_HeurFin);
		TextView _TextView_Salle=(TextView) contentView.findViewById(R.id.ActivityChefDepartement_strocture_Calendrier_Child_TextView_Salle);
		Button _Button_Option=(Button) contentView.findViewById(R.id.ActivityChefDepartement_strocture_Calendrier_Child_Button_Option);

		View _Layout_RapporteurPresident=contentView.findViewById(R.id.ActivityChefDepartement_strocture_Calendrier_Child_layout_RapporteurPresident);
		View _Layout_Soutenance=contentView.findViewById(R.id.ActivityChefDepartement_strocture_Calendrier_Child_layout_Soutenance);

		String _ID_Soutenance=_list_EntrepriseEtudiantEncadreur.get("Id_soutenance_TravailPfe");

		_Button_Entreprise.setTag(R.id._HashMap,_list_EntrepriseEtudiantEncadreur);
		_Button_Entreprise.setOnClickListener(this);

		_Button_Description.setTag(R.id.position,groupPosition);
		_Button_Description.setOnClickListener(this);

		_Button_Etudiant1.setTag(R.id.ID_etudiant,_list_EntrepriseEtudiantEncadreur.get("ID_Etudiant"));
		_Button_Etudiant1.setOnClickListener(this);

		_Button_Etudiant2.setOnClickListener(this);

		_Button_EncadreurAcad.setTag(R.id._HashMap,_list_EntrepriseEtudiantEncadreur);
		_Button_EncadreurAcad.setOnClickListener(this);

		_Button_President.setTag(R.id._HashMap,_list_EntrepriseEtudiantEncadreur);
		_Button_President.setOnClickListener(this);

		_Button_Rapporteur.setTag(R.id._HashMap,_list_EntrepriseEtudiantEncadreur);
		_Button_Rapporteur.setOnClickListener(this);

		_Button_Option.setTag(R.id._HashMap,_list_EntrepriseEtudiantEncadreur);
		_Button_Option.setOnClickListener(this);

		Verification_Etudiant(_list_EntrepriseEtudiantEncadreur.get("ID_Etudiant"), _Button_Etudiant2);
		Afficher_InformationSoutenance(_Layout_Soutenance,_Layout_RapporteurPresident,_ID_Soutenance,_list_EntrepriseEtudiantEncadreur, _TextView_Jour, _TextView_HeurDebut, _TextView_HeurFin, _TextView_Salle);
		return contentView;
	}

	public void Afficher_InformationSoutenance(View _Layout_Soutenance,View _layout,String _ID_Soutenance,HashMap<String, String> _list_EntrepriseEtudiantEncadreur,TextView _TextView_Jour,TextView _TextView_HeurDebut,TextView _TextView_HeurFin,TextView _TextView_Salle)
	{
		if (_ID_Soutenance!=null)
		{
			_layout.setVisibility(View.VISIBLE);
			_Layout_Soutenance.setVisibility(View.VISIBLE);

			int _ID_TravailPfe=Integer.valueOf(_list_EntrepriseEtudiantEncadreur.get("Id_travailPfe_Etudiant"));
			ArrayList<HashMap<String, String>> _list_SoutenanceDerigerSalle=new ArrayList<HashMap<String,String>>();
			_list_SoutenanceDerigerSalle=_MySQliteFonction_ChefDepartement.SoutenanceDerigerSalle_CALENDRIER(_ID_TravailPfe);

			_TextView_Jour.setText(_list_SoutenanceDerigerSalle.get(0).get("Date_Soutenance"));
			_TextView_HeurDebut.setText(_list_SoutenanceDerigerSalle.get(0).get("HeureDeb_Soutenance"));
			_TextView_HeurFin.setText(_list_SoutenanceDerigerSalle.get(0).get("HeureFin_Soutenance"));
			_TextView_Salle.setText(_list_SoutenanceDerigerSalle.get(0).get("Abreviation_Salle"));
		}else
		{
			_layout.setVisibility(View.GONE);
			_Layout_Soutenance.setVisibility(View.GONE);
		}
	}

	public void Verification_Etudiant(String _ID_Etudiant,Button _Button_Binome)
	{
		ArrayList<HashMap<String, String>> _list_etudiant=_MySQliteFonction_ChefDepartement.Afficher_Etudiant_INFORMATION(Integer.parseInt(_ID_Etudiant));
		_ID_Binome=_list_etudiant.get(0).get("Id_etudiant1_Etudiant");
		if (_ID_Binome==null)
		{
			_Button_Binome.setVisibility(View.GONE);
		}else
		{
			_Button_Binome.setVisibility(View.VISIBLE);
		}
	}

	@SuppressWarnings("unchecked")
	public void onClick(View v) 
	{
		switch (v.getId())
		{
		case R.id.ActivityChefDepartement_strocture_Calendrier_Child_Button_Option :
			final HashMap<String, String> _list_EntrepriseEtudiantEncadreur= (HashMap<String, String>) v.getTag(R.id._HashMap);
			AlertDialog.Builder _Builder4=new AlertDialog.Builder(_Context);
			_Builder4.setTitle(" Options ");
			_Builder4.setItems(new CharSequence[]
					{ " Programmer/Modifier Soutenance "},
					new DialogInterface.OnClickListener()
			{
				public void onClick(DialogInterface dialog, int which)
				{
					switch (which)
					{
					case 0:
						HashMap<String, String> _HashMap=new HashMap<String, String>();
						String _Id_EncadreurAcad=_list_EntrepriseEtudiantEncadreur.get("ID_Enseignant");
						String _ID_TravailPfe=_list_EntrepriseEtudiantEncadreur.get("Id_travailPfe_Etudiant");

						MyDialog_ChefDepartement_Calendrier_InsertUpdate _MyDialog_ChefDepartement_Calendrier_InsertUpdate = new MyDialog_ChefDepartement_Calendrier_InsertUpdate(_Context, _HashMap, _ID_TravailPfe,_Id_EncadreurAcad);
						_MyDialog_ChefDepartement_Calendrier_InsertUpdate.MyDialogCalendrier_InsertUpdate();
						break;
					}
				}
			});
			_Builder4.create().show();

			break;
		case R.id.ActivityChefDepartement_strocture_Calendrier_Child_Button_Entreprise:
			HashMap<String, String> _list_EtudiantEntrepirse=(HashMap<String, String>) v.getTag(R.id._HashMap);
			String _entreprise_Libelle=_list_EtudiantEntrepirse.get("libelle_Entreprise");
			String _entreprise_Raison=_list_EtudiantEntrepirse.get("raison_Entreprise");
			String _entreprise_Email=_list_EtudiantEntrepirse.get("email_Entreprise");
			String _entreprise_SiteWeb=_list_EtudiantEntrepirse.get("siteWeb_Entreprise");
			AlertDialog.Builder box0 = new AlertDialog.Builder(_Context);
			box0.setTitle("Entreprise");
			String SMS0="<br />"+_entreprise_Libelle+"<br />"+_entreprise_Raison+"<br />"+_entreprise_Email+"<br />"+_entreprise_SiteWeb;
			box0.setMessage(Html.fromHtml("<font color=\"#000000 \">" + SMS0 + "</font>",null, null));
			AlertDialog dialog0 = box0.show();
			TextView messageText0 = (TextView)dialog0.findViewById(android.R.id.message);
			messageText0.setGravity(Gravity.CENTER);
			messageText0.setTextSize(14);
			dialog0.show();
			break;

		case R.id.ActivityChefDepartement_strocture_Calendrier_Child_Button_Description:
			int position0=(Integer) v.getTag(R.id.position);
			AlertDialog.Builder box1 = new AlertDialog.Builder(_Context);
			box1.setTitle("Description Demande Stage");
			String SMS1="<br />"+_List_Stage.get(position0).get("_descriptiona_Stage") +"<br />";
			box1.setMessage(Html.fromHtml("<font color=\"#000000 \">" + SMS1 + "</font>",null, null));
			AlertDialog dialog1 = box1.show();
			TextView messageText1 = (TextView)dialog1.findViewById(android.R.id.message);
			messageText1.setGravity(Gravity.CENTER);
			messageText1.setTextSize(14);
			dialog1.show();
			break;

		case R.id.ActivityChefDepartement_strocture_Calendrier_Child_Button_Etudiant1:
			String _ID_Etudiant= (String) v.getTag(R.id.ID_etudiant);
			ArrayList<HashMap<String, String>> _list_etudiant=new ArrayList<HashMap<String,String>>();
			_list_etudiant=_MySQliteFonction_ChefDepartement.Afficher_Etudiant_INFORMATION(Integer.parseInt(_ID_Etudiant));
			MyDialog_EtudiantInformation _MyDialog_EtudiantInformation=new MyDialog_EtudiantInformation(_Context, _list_etudiant);
			_MyDialog_EtudiantInformation.DialogEudiantInformation();
			break;

		case R.id.ActivityChefDepartement_strocture_Calendrier_Child_Button_Etudiant2:
			ArrayList<HashMap<String, String>> _list_Binome =new ArrayList<HashMap<String,String>>();
			if (_ID_Binome.equals("null")==false)
			{
				_list_Binome=_MySQliteFonction_ChefDepartement.Afficher_Etudiant_INFORMATION(Integer.parseInt(_ID_Binome));
				MyDialog_EtudiantInformation _MyDialog_EtudiantInformation1=new MyDialog_EtudiantInformation(_Context, _list_Binome);
				_MyDialog_EtudiantInformation1.DialogEudiantInformation();
			}
			break;
		case R.id.ActivityChefDepartement_strocture_Calendrier_Child_Button_EncadreurAcad: 
			HashMap<String, String> _list_enseignant=(HashMap<String, String>) v.getTag(R.id._HashMap);
			String _nom_encadreurAcad= "<br />"+_list_enseignant.get("nom_Enseignant");
			String _prenom_encadreurAcad="<br />"+ _list_enseignant.get("prenom_Enseignant");
			String _numTel_encadreurAcad="<br />"+ _list_enseignant.get("numTel_Enseignant");
			String _email_encadreurAcad="<br />"+ _list_enseignant.get("email_Enseignant");
			String _matricule_encadreurAcad= _list_enseignant.get("matricule_Enseignant");
			String _specialite_encadreurAcad="<br />"+ _list_enseignant.get("specialite_Enseignant");
			AlertDialog.Builder box2 = new AlertDialog.Builder(_Context);
			box2.setTitle("Information Encadreur Académique");
			String SMS2=_matricule_encadreurAcad+_nom_encadreurAcad+_prenom_encadreurAcad+
					_specialite_encadreurAcad+_numTel_encadreurAcad+_email_encadreurAcad;
			box2.setMessage(Html.fromHtml("<font color=\"#000000 \">" + SMS2 + "</font>",null, null));
			AlertDialog dialog2 = box2.show();
			TextView messageText2 = (TextView)dialog2.findViewById(android.R.id.message);
			messageText2.setGravity(Gravity.CENTER);
			messageText2.setTextSize(14);
			dialog2.show();
			break;

		case R.id.ActivityChefDepartement_strocture_Calendrier_Child_Button_Rapporteur:
			HashMap<String, String> _list_EntrepriseEtudiantEncadreur0=(HashMap<String, String>) v.getTag(R.id._HashMap);
			int _ID_TravailPfe0=Integer.valueOf(_list_EntrepriseEtudiantEncadreur0.get("Id_travailPfe_Etudiant"));
			String _Type_Jury0="Rapporteur";
			Afficher_RapporteurPresident(_Type_Jury0, _ID_TravailPfe0);
			break;

		case R.id.ActivityChefDepartement_strocture_Calendrier_Child_Button_President:
			HashMap<String, String> _list_EntrepriseEtudiantEncadreur1=(HashMap<String, String>) v.getTag(R.id._HashMap);
			int _ID_TravailPfe1=Integer.valueOf(_list_EntrepriseEtudiantEncadreur1.get("Id_travailPfe_Etudiant"));
			String _Type_Jury1="President";
			Afficher_RapporteurPresident(_Type_Jury1, _ID_TravailPfe1);
			break;
		default:
			break;
		}
	}

	public void Afficher_RapporteurPresident(String _Type_Jury , int _ID_TravailPfe0)
	{
		ArrayList<HashMap<String, String>> _list_SoutenanceDerigerSalle0=new ArrayList<HashMap<String,String>>();
		_list_SoutenanceDerigerSalle0=_MySQliteFonction_ChefDepartement.SoutenanceDerigerSalle_CALENDRIER(_ID_TravailPfe0);
		for (int i = 0; i < _list_SoutenanceDerigerSalle0.size(); i++)
		{
			String _Type_Deriger=_list_SoutenanceDerigerSalle0.get(i).get("Type_Deriger");
			if (_Type_Deriger.equals("Rapporteur"))
			{
				AlertDialog.Builder boxr = new AlertDialog.Builder(_Context); 
				boxr.setTitle("Information "+_Type_Jury); 
				String _matricule_enseignant= _list_SoutenanceDerigerSalle0.get(i).get("matricule_Enseignant"); 
				String _nom_enseignant="<br />"+_list_SoutenanceDerigerSalle0.get(i).get("nom_Enseignant") ; 
				String _prenom_enseignant="<br />"+ _list_SoutenanceDerigerSalle0.get(i).get("prenom_Enseignant"); 
				String _grade_enseignant="<br />"+ _list_SoutenanceDerigerSalle0.get(i).get("grade_Enseignant"); 
				String _specialite_enseignant="<br />"+ _list_SoutenanceDerigerSalle0.get(i).get("specialite_Enseignant"); 
				String _encadrement_enseignant= _list_SoutenanceDerigerSalle0.get(i).get("encadrement_Enseignant"); 
				if (_encadrement_enseignant.equals("1")) 
				{_encadrement_enseignant="<br />"+"Encadrer";} 
				else 
				{_encadrement_enseignant="<br />"+"Ne pas encadrer";} 
				String _numTel_enseignant="<br />"+ _list_SoutenanceDerigerSalle0.get(i).get("numTel_Enseignant"); 
				String _email_enseignant="<br />"+_list_SoutenanceDerigerSalle0.get(i).get("email_Enseignant") ; 
				boxr.setMessage(Html.fromHtml("<font color=\"#000000 \">" + _matricule_enseignant+_nom_enseignant+_prenom_enseignant 
						+_grade_enseignant+_specialite_enseignant+_encadrement_enseignant+_numTel_enseignant+_email_enseignant+ "</font>",null, null)); 
				AlertDialog dialogr = boxr.show(); 
				TextView messageTextr = (TextView)dialogr.findViewById(android.R.id.message); 
				messageTextr.setGravity(Gravity.CENTER); 
				messageTextr.setTextSize(15); 
				dialogr.show();
			}
		}
	}

	////////////////////////


	public HashMap<String, String> getGroup(int groupPosition)
	{
		return _List_Stage.get(groupPosition);
	}

	public int getGroupCount() 
	{
		return _List_Stage.size();
	}
	public long getGroupId(int groupPosition) 
	{
		return groupPosition;
	}

	public View getGroupView(int groupPosition, boolean isLastChild, View contentView, ViewGroup parent)
	{
		HashMap<String, String> _list_Stage=getGroup(groupPosition);
		if (contentView==null)
		{
			LayoutInflater layoutInflater=(LayoutInflater) _Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			contentView=layoutInflater.inflate(R.layout.activity_chefdepartement_strocture_calendrier_parent, null);
		}
		final TextView _TextView_Stage=(TextView) contentView.findViewById(R.id.ActivityChefDepartement_strocture_Calendrier_Parent_TextView_nom);
		ImageView _ImageView=(ImageView) contentView.findViewById(R.id.ActivityChefDepartement_strocture_Calendrier_Parent_ImageView);

		_TextView_Stage.setText(_list_Stage.get("_nom_Stage"));

		if (isLastChild) 
		{ //séléctionnné
			_TextView_Stage.setTextColor(_Context.getResources().getColor(R.color.orange));
			_ImageView.setImageDrawable(_Context.getResources().getDrawable(R.drawable.calendrier_ouvert));
		}else 
		{ //non séléctionné
			_TextView_Stage.setTextColor(_Context.getResources().getColor(android.R.color.secondary_text_light));
			_ImageView.setImageDrawable(_Context.getResources().getDrawable(R.drawable.calendrier_fermer));
		}

		_TextView_Stage.setTag(R.id.position,groupPosition);
		_TextView_Stage.setOnClickListener(new OnClickListener()
		{
			public void onClick(View arg0) 
			{
				int groupPosition=(Integer) _TextView_Stage.getTag(R.id.position);
				(((Activity_ChefDepartement) _Context)).expandGroup(Activity_ChefDepartement._ExpandableListView_Calendrier,groupPosition);
			}
		});

		return contentView;
	}
	public boolean hasStableIds()
	{
		return false;
	}
	public boolean isChildSelectable(int arg0, int arg1) 
	{
		return false;
	}



	public void Refresh_Activity()
	{
		((Activity) _Context).finish();
		Intent _Intent_To_ActivityChefDepartement=new Intent(_Context,Activity_ChefDepartement.class);
		_Context.startActivity(_Intent_To_ActivityChefDepartement);
	}

}
