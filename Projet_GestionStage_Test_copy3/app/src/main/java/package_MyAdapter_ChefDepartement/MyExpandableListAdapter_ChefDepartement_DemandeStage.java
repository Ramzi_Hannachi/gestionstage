package package_MyAdapter_ChefDepartement;

import java.util.ArrayList;
import java.util.HashMap;

import package_Foction_DataBase.MySQliteFonction_ChefDepartement;
import package_MemoirStage.Activity_ChefDepartement;
import package_MyAdapter_Entreprise.MyDialog_Entreprise_DepotOffreStage_InsertUdate;
import package_MyAdapter_Etudiant.MyDialog_EtudiantInformation;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.projet_gestionstage_test.R;

public class MyExpandableListAdapter_ChefDepartement_DemandeStage extends BaseExpandableListAdapter implements OnClickListener 
{
	MySQliteFonction_ChefDepartement _MySQliteFonction_ChefDepartement;

	private Context _Context;
	private ArrayList<HashMap<String, String>> _List_OffreStage=new ArrayList<HashMap<String,String>>();
	private ArrayList<HashMap<String, String>> _List_EtudiantEntreprise=new ArrayList<HashMap<String,String>>();

	private String _ID_Binome=null;

	public MyExpandableListAdapter_ChefDepartement_DemandeStage(Context _Context,ArrayList<HashMap<String, String>> _List_OffreStage )
	{
		_MySQliteFonction_ChefDepartement=new MySQliteFonction_ChefDepartement(_Context);

		this._Context=_Context;
		this._List_OffreStage=_List_OffreStage;
	}

	public HashMap<String, String> getChild(int groupPosition, int childPosition) 
	{
		String _ID_OffreStage=_List_OffreStage.get(groupPosition).get("ID_OffreStage");
		_List_EtudiantEntreprise=_MySQliteFonction_ChefDepartement.Afficher_EtudiantEntreprise_DEMANDESTAGE(Integer.parseInt(_ID_OffreStage));
		return _List_EtudiantEntreprise.get(childPosition);
	}
	public long getChildId(int groupPosition, int childPosition)
	{
		return childPosition;
	}
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View contentView,ViewGroup parent)
	{
		HashMap<String, String> _list_EtudiantEntrepirse=getChild(groupPosition, childPosition);	
		if (contentView==null)
		{
			LayoutInflater layoutInflater=(LayoutInflater) _Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			contentView=layoutInflater.inflate(R.layout.activity_chefdepartement_strocture_demandestage_child, null);
		}
		Button _Button_Entreprise=(Button) contentView.findViewById(R.id.ActivityChefDepartement_strocture_DemandeStage_Child_Button_Entreprise);
		Button _Button_Description=(Button) contentView.findViewById(R.id.ActivityChefDepartement_strocture_DemandeStage_Child_Button_Description);
		Button _Button_Etudiant1=(Button) contentView.findViewById(R.id.ActivityChefDepartement_strocture_DemandeStage_Child_Button_Etudiant1);
		Button _Button_Etudiant2=(Button) contentView.findViewById(R.id.ActivityChefDepartement_strocture_DemandeStage_Child_Button_Etudiant2);
		Button _Button_StageAdd=(Button) contentView.findViewById(R.id.ActivityChefDepartement_strocture_DemandeStage_Child_Button_StageAdd);
		Button _Button_Option=(Button) contentView.findViewById(R.id.ActivityChefDepartement_strocture_DemandeStage_Child_Button_Option);

		_Button_Entreprise.setTag(R.id._HashMap,_list_EtudiantEntrepirse);
		_Button_Entreprise.setOnClickListener(this);

		_Button_Description.setTag(R.id.position,groupPosition);
		_Button_Description.setOnClickListener(this);

		_Button_Etudiant1.setTag(R.id.ID_etudiant,_list_EtudiantEntrepirse.get("ID_Etudiant"));
		_Button_Etudiant1.setOnClickListener(this);

		_Button_Etudiant2.setOnClickListener(this);

		_Button_StageAdd.setTag(R.id.ID_etudiant,_list_EtudiantEntrepirse.get("ID_Etudiant"));
		_Button_StageAdd.setTag(R.id.position,groupPosition);
		_Button_StageAdd.setTag(R.id.Button,_Button_StageAdd);
		_Button_StageAdd.setOnClickListener(this);

		_Button_Option.setTag(R.id._HashMap,getGroup(groupPosition));
		_Button_Option.setTag(R.id.ID_etudiant,_list_EtudiantEntrepirse.get("ID_Etudiant"));
		_Button_Option.setOnClickListener(this);

		Verification_Etudiant(_list_EtudiantEntrepirse.get("ID_Etudiant"), _Button_Etudiant2);
		Verifier_OffreStage_IS_Stage(groupPosition, _Button_StageAdd);
		Verification_ButtonOption(getGroup(groupPosition), _Button_Option);
		return contentView;
	}

	public void Verification_ButtonOption(HashMap<String, String> _list_offreStage, Button _Button_Option)
	{
		String _ID_Stage=_list_offreStage.get("cle_stage_OffreStage");
		String _ID_travailPfe_Stage=_list_offreStage.get("cle_travailPfe_OffreStage");

		if ( _ID_Stage==null && _ID_travailPfe_Stage==null )
		{
			_Button_Option.setVisibility(View.GONE);
		}
		if ( _ID_Stage!=null && _ID_travailPfe_Stage!=null )
		{
			_Button_Option.setVisibility(View.VISIBLE);
		}
	}

	public boolean Verifier_OffreStage_IS_Stage(int groupPosition,Button _Button_StageAdd)
	{
		String _Id_stage_OffreStage=_List_OffreStage.get(groupPosition).get("cle_stage_OffreStage");
		String _Id_travailPfe_OffreStage=_List_OffreStage.get(groupPosition).get("cle_travailPfe_OffreStage");
		if (_Id_stage_OffreStage!=null && _Id_travailPfe_OffreStage!=null) 
		{
			_Button_StageAdd.setBackgroundResource(R.drawable.ajouter_confirmation_vert);
			return true;   
		}else
		{
			_Button_StageAdd.setBackgroundResource(R.drawable.ajouter_confirmation);
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	public void onClick(View v) 
	{
		switch (v.getId())
		{
		case R.id.ActivityChefDepartement_strocture_DemandeStage_Child_Button_Option :
			final HashMap<String, String> _list_offreStage=(HashMap<String, String>) v.getTag(R.id._HashMap);
			final String _ID_Etudianttt=(String) v.getTag(R.id.ID_etudiant);
			AlertDialog.Builder _Builder4=new AlertDialog.Builder(_Context);
			_Builder4.setTitle(" Options ");
			_Builder4.setItems(new CharSequence[]
					{ " Modifier le Stage ", " Supprimer le Stage"},
					new DialogInterface.OnClickListener()
			{
				public void onClick(DialogInterface dialog, int which)
				{
					switch (which)
					{
					case 0:
						// modifier offrestage , travailpfe , stage
						String _ID_entreprise_OffreStage=_list_offreStage.get("cle_entreprise_OffreStage");
						MyDialog_Entreprise_DepotOffreStage_InsertUdate _MyDialog_Entreprise_DepotOffreStage_InsertUdate =new MyDialog_Entreprise_DepotOffreStage_InsertUdate(_Context, _list_offreStage, _ID_entreprise_OffreStage);
						_MyDialog_Entreprise_DepotOffreStage_InsertUdate.DialogEntrepirse_DepostageInsert();

						break;
					case 1:
						// supprimer travailPfe , stage , modifier  offrestage etudiant , supprimer condidater
						Supprimer_Stage(_list_offreStage, _ID_Etudianttt);
						break;
					}
				}
			});
			_Builder4.create().show();

			break;
		case R.id.ActivityChefDepartement_strocture_DemandeStage_Child_Button_Entreprise:
			HashMap<String, String> _list_EtudiantEntrepirse=(HashMap<String, String>) v.getTag(R.id._HashMap);
			String _entreprise_Libelle=_list_EtudiantEntrepirse.get("libelle_Entreprise");
			String _entreprise_Raison=_list_EtudiantEntrepirse.get("raison_Entreprise");
			String _entreprise_Email=_list_EtudiantEntrepirse.get("email_Entreprise");
			String _entreprise_SiteWeb=_list_EtudiantEntrepirse.get("siteWeb_Entreprise");
			AlertDialog.Builder box0 = new AlertDialog.Builder(_Context);
			box0.setTitle("Entreprise");
			String SMS0="<br />"+_entreprise_Libelle+"<br />"+_entreprise_Raison+"<br />"+_entreprise_Email+"<br />"+_entreprise_SiteWeb;
			box0.setMessage(Html.fromHtml("<font color=\"#000000 \">" + SMS0 + "</font>",null, null));
			AlertDialog dialog0 = box0.show();
			TextView messageText0 = (TextView)dialog0.findViewById(android.R.id.message);
			messageText0.setGravity(Gravity.CENTER);
			messageText0.setTextSize(14);
			dialog0.show();
			break;

		case R.id.ActivityChefDepartement_strocture_DemandeStage_Child_Button_Description:
			int position0=(Integer) v.getTag(R.id.position);
			AlertDialog.Builder box1 = new AlertDialog.Builder(_Context);
			box1.setTitle("Description Demande Stage");
			String SMS1="<br />"+_List_OffreStage.get(position0).get("description_OffreStage") +"<br />";
			box1.setMessage(Html.fromHtml("<font color=\"#000000 \">" + SMS1 + "</font>",null, null));
			AlertDialog dialog1 = box1.show();
			TextView messageText1 = (TextView)dialog1.findViewById(android.R.id.message);
			messageText1.setGravity(Gravity.CENTER);
			messageText1.setTextSize(14);
			dialog1.show();
			break;

		case R.id.ActivityChefDepartement_strocture_DemandeStage_Child_Button_Etudiant1:
			String _ID_Etudiant= (String) v.getTag(R.id.ID_etudiant);
			ArrayList<HashMap<String, String>> _list_etudiant=new ArrayList<HashMap<String,String>>();
			_list_etudiant=_MySQliteFonction_ChefDepartement.Afficher_Etudiant_INFORMATION(Integer.parseInt(_ID_Etudiant));
			MyDialog_EtudiantInformation _MyDialog_EtudiantInformation=new MyDialog_EtudiantInformation(_Context, _list_etudiant);
			_MyDialog_EtudiantInformation.DialogEudiantInformation();
			break;

		case R.id.ActivityChefDepartement_strocture_DemandeStage_Child_Button_Etudiant2:
			ArrayList<HashMap<String, String>> _list_Binome =new ArrayList<HashMap<String,String>>();
			if (_ID_Binome.equals("null")==false)
			{
				_list_Binome=_MySQliteFonction_ChefDepartement.Afficher_Etudiant_INFORMATION(Integer.parseInt(_ID_Binome));
				MyDialog_EtudiantInformation _MyDialog_EtudiantInformation1=new MyDialog_EtudiantInformation(_Context, _list_Binome);
				_MyDialog_EtudiantInformation1.DialogEudiantInformation();
			}
			break;

		case R.id.ActivityChefDepartement_strocture_DemandeStage_Child_Button_StageAdd:
			final int position1= (Integer) v.getTag(R.id.position);
			Button _Button_StageAdd=(Button) v.getTag(R.id.Button);
			boolean x=Verifier_OffreStage_IS_Stage(position1, _Button_StageAdd);
			if (x==false)// l'offre de stage n'est pas un stage !!! ajouter un stage
			{
				final String _ID_Etudiantt=(String) v.getTag(R.id.ID_etudiant);
				AlertDialog.Builder _Builder = new AlertDialog.Builder(_Context);
				_Builder.setTitle("Ajouter Stage");
				_Builder.setMessage("Êtes-vous sûr de vouloir transformer cette demande de Stage en Stage ?");
				_Builder.setNegativeButton("Annuler", null);
				_Builder.setPositiveButton("Confirmer", new DialogInterface.OnClickListener()
				{
					public void onClick(DialogInterface arg0, int arg1) 
					{ // insert and watch Travail pfe
						String _Nom_OffreStage=_List_OffreStage.get(position1).get("nom_OffreStage");
						boolean x0=_MySQliteFonction_ChefDepartement.InsertInTo_Serveur_TravailPfe(_Nom_OffreStage, _Context.getResources().getString(R.string.ChefDepartementActivity_insert_TravailPfe));
						if (x0)
						{
							ArrayList<HashMap<String, String>> _list_TravailPfe=new ArrayList<HashMap<String,String>>();
							_list_TravailPfe=_MySQliteFonction_ChefDepartement.Watch_Serveur_TravailPfe(_Context.getResources().getString(R.string.ChefDepartementActivity_display_TravailPfe));
							String _Nom_Stage=_Nom_OffreStage;
							String _Date_Stage=_List_OffreStage.get(position1).get("date_OffreStage");
							String _Id_travailPfe_Stage=_list_TravailPfe.get(0).get("_ID_TravailPfe");
							String _Id_offreStage_Stage=_List_OffreStage.get(position1).get("ID_OffreStage");;
							String _Description_Stage=_List_OffreStage.get(position1).get("description_OffreStage");;
							String _Id_Enterprise=_List_OffreStage.get(position1).get("cle_entreprise_OffreStage");
							// insert and watch Stage
							boolean x1=_MySQliteFonction_ChefDepartement.InsertInTo_Serveur_Stage(_Nom_Stage, _Date_Stage, _Id_travailPfe_Stage, _Id_offreStage_Stage, _Description_Stage,_Id_Enterprise, _Context.getResources().getString(R.string.ChefDepartementActivity_insert_Stage_ENTREPRISE));
							if (x1)
							{
								ArrayList<HashMap<String, String>> _list_Stage=new ArrayList<HashMap<String,String>>();
								_list_Stage=_MySQliteFonction_ChefDepartement.Watch_Serveur_Stage(_Id_travailPfe_Stage, _Context.getResources().getString(R.string.ChefDepartementActivity_display_Stage));
								String _ID_Stage=_list_Stage.get(0).get("_ID_Stage");
								// update etudiant binome offreStage
								boolean x2=_MySQliteFonction_ChefDepartement.UpdateInTo_Serveur_OffreStageEtudiant(_ID_Stage, _Id_travailPfe_Stage, _Id_offreStage_Stage, _ID_Etudiantt,_ID_Binome, _Context.getResources().getString(R.string.ChefDepartementActivity_update_OffreStageEtudiant));
								if (x2)
								{
									((Activity) _Context).finish();
									Intent _Intent_To_ActivityChefDepartement=new Intent(_Context,Activity_ChefDepartement.class);
									_Context.startActivity(_Intent_To_ActivityChefDepartement);

								}
							}
						}
					}
				});
				_Builder.show();
			}else
			{ /// l'offre de stage est un stage !!! message pour informer l'utilisateur que c'est un stage !!!
				AlertDialog.Builder _Builder3=new AlertDialog.Builder(_Context);
				_Builder3.setMessage(" vous avez choisi cette demande de stage comme un stage  ");
				_Builder3.show();
			}
			break;

		default:
			break;
		}
	}
	public void Supprimer_Stage(final HashMap<String, String> _list_offreStage , final String _ID_Etudianttt)
	{
		AlertDialog.Builder _Builder = new AlertDialog.Builder(_Context);
		_Builder.setTitle("Suppression");
		_Builder.setMessage("Êtes-vous sûr de vouloir Supprimer ce Stage Définitivement ?");
		_Builder.setNegativeButton("Annuler", null);
		_Builder.setPositiveButton("Confirmer", new DialogInterface.OnClickListener() 
		{
			public void onClick(DialogInterface arg0, int arg1) 
			{
				ArrayList<HashMap<String, String>> _list_etudiant=_MySQliteFonction_ChefDepartement.Afficher_Etudiant_INFORMATION(Integer.parseInt(_ID_Etudianttt));
				String _ID_Binomee=_list_etudiant.get(0).get("Id_etudiant1_Etudiant");
				String _Id_travailPfe_offreStage=_list_offreStage.get("cle_travailPfe_OffreStage");
				String _ID_offreStage=_list_offreStage.get("ID_OffreStage");
				String _URL=_Context.getResources().getString(R.string.ChefDepartementActivity_DeleteUpdate_CondidaterOffreStageEtudiant);

				boolean x=_MySQliteFonction_ChefDepartement.DeleteUpdateInTo_Serveur_CondidaterOffreStageEtudiant(_ID_offreStage, _ID_Etudianttt, _ID_Binomee, _URL);
				if (x)
				{
					String _URL0=_Context.getResources().getString(R.string.ChefDepartementActivity_delete_TravailPfeStage);
					boolean y=_MySQliteFonction_ChefDepartement.DeleteInTo_Serveur_TravailPfeStage(_Id_travailPfe_offreStage, _URL0);
					if (y)
					{
						Refresh_Activity();	
					}
				}
			}
		});
		_Builder.show();

	}
	public void Verification_Etudiant(String _ID_Etudiant,Button _Button_Binome)
	{
		ArrayList<HashMap<String, String>> _list_etudiant=_MySQliteFonction_ChefDepartement.Afficher_Etudiant_INFORMATION(Integer.parseInt(_ID_Etudiant));
		_ID_Binome=_list_etudiant.get(0).get("Id_etudiant1_Etudiant");
		if (_ID_Binome==null)
		{
			_Button_Binome.setVisibility(View.GONE);
		}else
		{
			_Button_Binome.setVisibility(View.VISIBLE);
		}
	}
	////////////////////////
	public int getChildrenCount(int groupPosition)
	{
		String _ID_OffreStage=_List_OffreStage.get(groupPosition).get("ID_OffreStage");
		_List_EtudiantEntreprise=_MySQliteFonction_ChefDepartement.Afficher_EtudiantEntreprise_DEMANDESTAGE(Integer.parseInt(_ID_OffreStage));
		return _List_EtudiantEntreprise.size();
	}
	public HashMap<String, String> getGroup(int groupPosition)
	{
		return _List_OffreStage.get(groupPosition);
	}

	public int getGroupCount() 
	{
		return _List_OffreStage.size();
	}
	public long getGroupId(int groupPosition) 
	{
		return groupPosition;
	}

	public View getGroupView(int groupPosition, boolean isLastChild, View contentView, ViewGroup parent)
	{
		HashMap<String, String> _list_offreStage=getGroup(groupPosition);
		if (contentView==null)
		{
			LayoutInflater layoutInflater=(LayoutInflater) _Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			contentView=layoutInflater.inflate(R.layout.activity_chefdepartement_strocture_demandestage_parent, null);
		}
		final TextView _TextView_OffreStage=(TextView) contentView.findViewById(R.id.ActivityChefDepartement_strocture_DemandeStage_Parent_TextView_nom);
		ImageView _ImageView=(ImageView) contentView.findViewById(R.id.ActivityChefDepartement_strocture_DemandeStage_Parent_ImageView);

		_TextView_OffreStage.setText(_list_offreStage.get("nom_OffreStage"));

		if (isLastChild) 
		{ //séléctionnné
			_TextView_OffreStage.setTextColor(_Context.getResources().getColor(R.color.orange));
			_ImageView.setImageDrawable(_Context.getResources().getDrawable(R.drawable.demandeencadrement_ouvert));
		}else 
		{ //non séléctionné
			_TextView_OffreStage.setTextColor(_Context.getResources().getColor(android.R.color.secondary_text_light));
			_ImageView.setImageDrawable(_Context.getResources().getDrawable(R.drawable.demandeencadrement_fermer));
		}

		_TextView_OffreStage.setTag(R.id.position,groupPosition);
		_TextView_OffreStage.setOnClickListener(new OnClickListener()
		{
			public void onClick(View arg0) 
			{
				int groupPosition=(Integer) _TextView_OffreStage.getTag(R.id.position);
				(((Activity_ChefDepartement) _Context)).expandGroup(Activity_ChefDepartement._ExpandableListView_DemandeStage,groupPosition);
			}
		});

		return contentView;
	}
	public boolean hasStableIds()
	{
		return false;
	}
	public boolean isChildSelectable(int arg0, int arg1) 
	{
		return false;
	}

	public void Refresh_Activity()
	{
		((Activity) _Context).finish();
		Intent _Intent_To_ActivityChefDepartement=new Intent(_Context,Activity_ChefDepartement.class);
		_Context.startActivity(_Intent_To_ActivityChefDepartement);
	}

}
