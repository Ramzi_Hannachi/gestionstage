package package_MyAdapter_ChefDepartement;

import java.util.ArrayList;
import java.util.HashMap;

import package_Foction_DataBase.MySQliteFonction_ChefDepartement;
import package_MemoirStage.Activity_ChefDepartement;
import package_MyAdapter_Etudiant.MyDialog_EtudiantInformation;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.projet_gestionstage_test.R;

public class MyAdapter_Spinner extends BaseAdapter implements OnClickListener
{
	private Context _Context;
	private LayoutInflater _LayoutInflater;
	private MySQliteFonction_ChefDepartement _MySQliteFonction_ChefDepartement;

	private ArrayList<HashMap<String, String>> _List_TravailStageEntreprise=new ArrayList<HashMap<String,String>>();
	private String _ID_Enseignant;

	public MyAdapter_Spinner(Context _Context,ArrayList<HashMap<String, String>> _List_TravailStageEntreprise,String _ID_Enseignant) 
	{
		this._Context=_Context;
		this._List_TravailStageEntreprise=_List_TravailStageEntreprise;
		this._LayoutInflater=LayoutInflater.from(_Context);
		this._ID_Enseignant=_ID_Enseignant;
		this._MySQliteFonction_ChefDepartement = new MySQliteFonction_ChefDepartement(_Context);
	}

	public int getCount()
	{
		ArrayList<HashMap<String, String>> _list_TravailStageEntreprise=new ArrayList<HashMap<String,String>>();
		for (int i = 0; i < _List_TravailStageEntreprise.size(); i++) 
		{
			if (_List_TravailStageEntreprise.get(i).get("_Id_enseignant_TravailPfe")==null)
			{
				_list_TravailStageEntreprise.add(_List_TravailStageEntreprise.get(i));
			}
		}
		return _list_TravailStageEntreprise.size();
	}

	public HashMap<String, String> getItem(int position) 
	{
		ArrayList<HashMap<String, String>> _list_TravailStageEntreprise=new ArrayList<HashMap<String,String>>();
		for (int i = 0; i < _List_TravailStageEntreprise.size(); i++) 
		{
			if (_List_TravailStageEntreprise.get(i).get("_Id_enseignant_TravailPfe")==null)
			{
				_list_TravailStageEntreprise.add(_List_TravailStageEntreprise.get(i));
			}
		}
		return _list_TravailStageEntreprise.get(position);
	}

	public long getItemId(int position) 
	{
		return position;
	}
	////////
	private class ViewHolder
	{
		TextView _TextView_NomStage;
		Button _Button_DescriptionStage;
		Button _Button_Etudiant1;
		Button _Button_Etudiant2;
		Button _Button_AffectationAdd;
	}
	public View getView(int position, View convertView, ViewGroup arg2) 
	{
		final ViewHolder _ViewHolder;

		if(convertView==null)
		{
			_ViewHolder=new ViewHolder();

			convertView=_LayoutInflater.inflate(R.layout.activity_chefdepartement_strocture_spinner, null);

			_ViewHolder._TextView_NomStage=(TextView) convertView.findViewById(R.id.ActivityChefDepartement_strocture_Spinner_TextView_NomStage);
			_ViewHolder._Button_DescriptionStage=(Button) convertView.findViewById(R.id.ActivityChefDepartement_strocture_Spinner_Button_Description);
			_ViewHolder._Button_Etudiant1=(Button) convertView.findViewById(R.id.ActivityChefDepartement_strocture_Spinner_Button_Etudiant1);
			_ViewHolder._Button_Etudiant2=(Button) convertView.findViewById(R.id.ActivityChefDepartement_strocture_Spinner_Button_Etudiant2);
			_ViewHolder._Button_AffectationAdd=(Button) convertView.findViewById(R.id.ActivityChefDepartement_strocture_Spinner_Button_AddAffectation);

			convertView.setTag(_ViewHolder);
		}else
		{
			_ViewHolder=(ViewHolder) convertView.getTag();
		}
		_ViewHolder._TextView_NomStage.setText(getItem(position).get("_nom_Stage"));

		_ViewHolder._Button_DescriptionStage.setTag(R.id._HashMap,getItem(position));
		_ViewHolder._Button_DescriptionStage.setOnClickListener(this);

		_ViewHolder._Button_Etudiant1.setTag(R.id.ID_travailPfe,getItem(position).get("_Id_travailPfe_Stage"));
		_ViewHolder._Button_Etudiant1.setOnClickListener(this);

		_ViewHolder._Button_Etudiant2.setTag(R.id.ID_travailPfe,getItem(position).get("_Id_travailPfe_Stage"));
		_ViewHolder._Button_Etudiant2.setOnClickListener(this);

		_ViewHolder._Button_AffectationAdd.setTag(R.id.ID_travailPfe,getItem(position).get("_Id_travailPfe_Stage"));
		_ViewHolder._Button_AffectationAdd.setOnClickListener(this);

		Verification_Etudiant(getItem(position).get("_Id_travailPfe_Stage"), _ViewHolder._Button_Etudiant2);
		return convertView;
	}

	public void Verification_Etudiant(String _ID_TravailPfe,Button _Button_Binome)
	{
		String _ID_Binome=_MySQliteFonction_ChefDepartement.Afficher_Etudiant_AFFECTATIOnENCADREUR(_ID_TravailPfe).get(0).get("Id_etudiant1_Etudiant");
		if (_ID_Binome==null)
		{
			_Button_Binome.setVisibility(View.GONE);
		}else
		{
			_Button_Binome.setVisibility(View.VISIBLE);
		}
	}


	@SuppressWarnings("unchecked")
	public void onClick(View v) 
	{
		switch (v.getId()) 
		{
		case R.id.ActivityChefDepartement_strocture_Spinner_Button_Description:
			HashMap<String, String> _HashMap_Stage=(HashMap<String, String>) v.getTag(R.id._HashMap);
			AlertDialog.Builder box1 = new AlertDialog.Builder(_Context);
			box1.setTitle("Description Stage");
			String SMS1="<br />"+_HashMap_Stage.get("_descriptiona_Stage") +"<br />"+_ID_Enseignant;
			box1.setMessage(Html.fromHtml("<font color=\"#000000 \">" + SMS1 + "</font>",null, null));
			AlertDialog dialog1 = box1.show();
			TextView messageText1 = (TextView)dialog1.findViewById(android.R.id.message);
			messageText1.setGravity(Gravity.CENTER);
			messageText1.setTextSize(14);
			dialog1.show();
			break;

		case R.id.ActivityChefDepartement_strocture_Spinner_Button_Etudiant1:
			String _ID_TravialPfe1= (String) v.getTag(R.id.ID_travailPfe);
			ArrayList<HashMap<String, String>> _list_Etudiant=new ArrayList<HashMap<String,String>>();
			String _ID_Etudiant=_MySQliteFonction_ChefDepartement.Afficher_Etudiant_AFFECTATIOnENCADREUR(_ID_TravialPfe1).get(0).get("ID_Etudiant");

			_list_Etudiant=_MySQliteFonction_ChefDepartement.Afficher_Etudiant_INFORMATION(Integer.parseInt(_ID_Etudiant));
			if (_list_Etudiant.isEmpty()==false) 
			{
				MyDialog_EtudiantInformation _MyDialog_EtudiantInformation1=new MyDialog_EtudiantInformation(_Context, _list_Etudiant);
				_MyDialog_EtudiantInformation1.DialogEudiantInformation();
			}	
			break;

		case R.id.ActivityChefDepartement_strocture_Spinner_Button_Etudiant2:
			String _ID_TravailPfe2= (String) v.getTag(R.id.ID_travailPfe);
			ArrayList<HashMap<String, String>> _list_Binome=new ArrayList<HashMap<String,String>>();
			String _ID_Binome=_MySQliteFonction_ChefDepartement.Afficher_Etudiant_AFFECTATIOnENCADREUR(_ID_TravailPfe2).get(0).get("Id_etudiant1_Etudiant");

			_list_Binome=_MySQliteFonction_ChefDepartement.Afficher_Etudiant_INFORMATION(Integer.parseInt(_ID_Binome));
			if (_list_Binome.isEmpty()==false) 
			{
				MyDialog_EtudiantInformation _MyDialog_EtudiantInformation1=new MyDialog_EtudiantInformation(_Context, _list_Binome);
				_MyDialog_EtudiantInformation1.DialogEudiantInformation();
			}
			break;

		case R.id.ActivityChefDepartement_strocture_Spinner_Button_AddAffectation:
			String _ID_TravailPfe3= (String) v.getTag(R.id.ID_travailPfe);
			
			String _ID_Etudiant0=_MySQliteFonction_ChefDepartement.Afficher_Etudiant_AFFECTATIOnENCADREUR(_ID_TravailPfe3).get(0).get("ID_Etudiant");
			String _ID_Binome0=_MySQliteFonction_ChefDepartement.Afficher_Etudiant_AFFECTATIOnENCADREUR(_ID_TravailPfe3).get(0).get("Id_etudiant1_Etudiant");
			boolean ok=_MySQliteFonction_ChefDepartement.Verifier_StageExisteInDemandeEncadrement(_ID_Etudiant0, _ID_Binome0, _ID_Enseignant);
			if (ok==true)
			{
				AlertDialog.Builder _Builder=new AlertDialog.Builder(_Context);
				_Builder.setTitle("Information");
				_Builder.setMessage("Vous-avez bien reçu une demande d'encadrement depuis cet encadreur académique" +
						" vous pouvez accepter ou annuler l'affectation de ce stage a cet encadreur depuis la  partie Demande Encadrement ");
				_Builder.show();
			}else
			{
				AffectationEncadreurAcademiqueStage(_ID_TravailPfe3);
			}
			break;

		default:
			break;
		}
	}

	public void AffectationEncadreurAcademiqueStage(final String _ID_TravailPfe )
	{
		AlertDialog.Builder _Builder = new AlertDialog.Builder(_Context);
		_Builder.setTitle("Affectation entre Encadreur Académique et Stage");
		_Builder.setMessage("Êtes-vous sûr de vouloir affecter ce stage a cet Encadreur Académique  ?");
		_Builder.setNegativeButton("Annuler", null);
		_Builder.setPositiveButton("Confirmer", new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface arg0, int arg1) 
			{
				String _URL1=_Context.getResources().getString(R.string.ChefDepartementActivity_updatePlus_TravailPfe_ACADEMIQUE);
				boolean y=_MySQliteFonction_ChefDepartement.UpdateInTo_Serveur_TravailPfe_ACADEMIQUE(_ID_Enseignant, _ID_TravailPfe, _URL1);
				if (y) 
				{
					Refresh_Activity();
				}
			}
		});
		_Builder.show();	
	}


	public void Refresh_Activity()
	{
		((Activity) _Context).finish();
		Intent _Intent_To_ActivityChefDepartement=new Intent(_Context,Activity_ChefDepartement.class);
		_Context.startActivity(_Intent_To_ActivityChefDepartement);
	}

}
