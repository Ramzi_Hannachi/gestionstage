package package_MyAdapter_ChefDepartement;

import java.util.ArrayList;
import java.util.HashMap;

import package_Foction_DataBase.MySQliteFonction_ChefDepartement;
import package_MemoirStage.Activity_ChefDepartement;
import package_MyAdapter_Entreprise.MyDialog_Entreprise_InscriptionInformation;
import package_MyAdapter_Etudiant.MyDialog_EtudiantInformation;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Html;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.projet_gestionstage_test.R;

public class MyExpandableListAdapter_ChefDepartement_DepotStage extends BaseExpandableListAdapter implements OnClickListener
{
	MySQliteFonction_ChefDepartement _MySQliteFonction_ChefDepartement ;

	private Context _Context;
	private ArrayList<HashMap<String, String>> _List_Stage=new ArrayList<HashMap<String,String>>();
	private ArrayList<HashMap<String, String>> _List_Entreprise=new ArrayList<HashMap<String,String>>();

	public MyExpandableListAdapter_ChefDepartement_DepotStage(Context _Context,ArrayList<HashMap<String, String>> _List_Stage,ArrayList<HashMap<String, String>> _List_Entreprise)
	{
		_MySQliteFonction_ChefDepartement=new MySQliteFonction_ChefDepartement(_Context);

		this._Context=_Context;
		this._List_Stage=_List_Stage;
		this._List_Entreprise=_List_Entreprise;
	}

	public HashMap<String, String> getChild(int groupPosition, int childPosition) 
	{
		String _ID_Entreprise=_List_Entreprise.get(groupPosition).get("ID_Entreprise");
		ArrayList<HashMap<String, String>> _list_StageEtudiant=new ArrayList<HashMap<String,String>>();
		for (int i = 0; i < _List_Stage.size(); i++)
		{
			if (_List_Stage.get(i).get("_Id_entreprise_Stage").equals(_ID_Entreprise)) 
			{
				_list_StageEtudiant.add(_List_Stage.get(i));
			}
		}
		return _list_StageEtudiant.get(childPosition);
	}

	public long getChildId(int groupPosition, int childPosition)
	{
		return childPosition;
	}
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View contentView,ViewGroup parent) 
	{
		final HashMap<String, String> _list_Stage=getChild(groupPosition, childPosition);
		if (contentView==null)
		{
			LayoutInflater layoutInflater=(LayoutInflater) _Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			contentView=layoutInflater.inflate(R.layout.activity_chefdepartement_strocture_depotstage_child, null);
		}
		String _ID_Stage=_list_Stage.get("_ID_Stage");

		TextView _TextView_SiteWeb_Entreprise=(TextView) contentView.findViewById(R.id.ActivityChefDepartement_strocture_DepotStage_Child_TextView_SiteWebEntreprise);
		TextView _TextView_NumTel_Entreprise=(TextView) contentView.findViewById(R.id.ActivityChefDepartement_strocture_DepotStage_Child_TextView_NumTelEntreprise);
		TextView _TextView_Nom_Stage=(TextView) contentView.findViewById(R.id.ActivityChefDepartement_strocture_DepotStage_Child_TextView_NomStage);
		TextView _TextView_Date_Stage=(TextView) contentView.findViewById(R.id.ActivityChefDepartement_strocture_DepotStage_Child_TextView_DateStage);

		Button _Button_Description_Stage=(Button) contentView.findViewById(R.id.ActivityChefDepartement_strocture_DepotStage_Child_TextView_DescriptionStage);
		Button _Button_Etudiant=(Button) contentView.findViewById(R.id.ActivityChefDepartement_strocture_DepotStage_Child_Button_Etudiant);
		Button _Button_Binome=(Button) contentView.findViewById(R.id.ActivityChefDepartement_strocture_DepotStage_Child_Button_Binome);
		Button _Button_opiton_child=(Button) contentView.findViewById(R.id.ActivityChefDepartement_strocture_DepotStage_Child_Button_Option);
		Button _Button_EncadreurPro=(Button) contentView.findViewById(R.id.ActivityChefDepartement_strocture_DepotStage_Child_Button_EncadreurPro);

		_TextView_SiteWeb_Entreprise.setText(_List_Entreprise.get(groupPosition).get("siteWeb_Entreprise"));
		_TextView_NumTel_Entreprise.setText(_List_Entreprise.get(groupPosition).get("numTel_Entreprise"));
		_TextView_Nom_Stage.setText(_list_Stage.get("_nom_Stage"));
		_TextView_Date_Stage.setText(_list_Stage.get("_date_Stage"));

		_Button_Description_Stage.setTag(R.id._HashMap,_list_Stage);
		_Button_Description_Stage.setOnClickListener(this);

		_Button_Etudiant.setTag(R.id.ID_etudiant,VerifierAndReturn_IDEtudiant(_ID_Stage, _Button_Binome));
		_Button_Etudiant.setOnClickListener(this);

		_Button_Binome.setTag(R.id.ID_etudiant,Return_IDBinome(_ID_Stage));
		_Button_Binome.setOnClickListener(this);

		_Button_opiton_child.setTag(R.id._HashMap,_list_Stage);
		_Button_opiton_child.setOnClickListener(this);

		_Button_EncadreurPro.setTag(R.id.ID_Stage,_list_Stage.get("_ID_Stage"));
		_Button_EncadreurPro.setOnClickListener(this);

		VerifierAndReturn_IDEtudiant(_ID_Stage, _Button_Binome);
		Verifier_EncadreurPro(_Button_EncadreurPro, _list_Stage.get("_Id_encadreurPro_Stage"));
		return contentView;
	}

	public void Verifier_EncadreurPro(Button _Button_EncadreurPro,String _ID_EncadreurPro)
	{
		if (_ID_EncadreurPro==null) 
		{
			_Button_EncadreurPro.setVisibility(View.GONE);
		}else
		{
			_Button_EncadreurPro.setVisibility(View.VISIBLE);
		}
	}

	public String VerifierAndReturn_IDEtudiant(String _ID_Stage,Button _Button_Binome)
	{
		ArrayList<HashMap<String, String>> _list_IDEtudiant=_MySQliteFonction_ChefDepartement.Afficher_IDEtudiant_DEPOTSTAGE(_ID_Stage);
		if (_list_IDEtudiant.size()==1)
		{
			_Button_Binome.setVisibility(View.GONE);
			return _list_IDEtudiant.get(0).get("ID_Etudiant");
		}
		else 
		{
			_Button_Binome.setVisibility(View.VISIBLE);
			return _list_IDEtudiant.get(0).get("ID_Etudiant");
		}
	}

	public String Return_IDBinome(String _ID_Stage)
	{
		ArrayList<HashMap<String, String>> _list_IDEtudiant=_MySQliteFonction_ChefDepartement.Afficher_IDEtudiant_DEPOTSTAGE(_ID_Stage);
		if (_list_IDEtudiant.size()==1)
		{
			return "null";
		}
		else 
		{
			return _list_IDEtudiant.get(1).get("ID_Etudiant");
		}
	}


	public int getChildrenCount(int groupPosition)
	{
		String _ID_Entreprise=_List_Entreprise.get(groupPosition).get("ID_Entreprise");
		ArrayList<HashMap<String, String>> _list_Stage=new ArrayList<HashMap<String,String>>();
		for (int i = 0; i < _List_Stage.size(); i++)
		{
			if (_List_Stage.get(i).get("_Id_entreprise_Stage").equals(_ID_Entreprise)) 
			{
				_list_Stage.add(_List_Stage.get(i));
			}
		}
		return _list_Stage.size();
	}
	//
	public HashMap<String, String> getGroup(int groupPosition)
	{
		return _List_Entreprise.get(groupPosition);
	}
	public int getGroupCount()
	{
		return _List_Entreprise.size();
	}
	public long getGroupId(int groupPosition)
	{
		return groupPosition;
	}

	public View getGroupView(int groupPosition, boolean isLastChild, View contentView, ViewGroup parent)
	{
		final HashMap<String, String> _list_Entreprise=  getGroup(groupPosition);
		if (contentView==null)
		{
			LayoutInflater layoutInflater=(LayoutInflater) _Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			contentView=layoutInflater.inflate(R.layout.activity_chefdepartement_strocture_depotstage_parent, null);
		}
		final TextView _TextView_libelleEntreprise=(TextView) contentView.findViewById(R.id.ActivityChefDepartement_strocture_DepotStage_Parent_TextView_Libelle);
		ImageView _ImageView=(ImageView) contentView.findViewById(R.id.ActivityChefDepartement_strocture_DepotStage_Parent_ImageView);
		Button _Button_Option_Parent=(Button) contentView.findViewById(R.id.ActivityChefDepartement_strocture_DepotStage_Parent_Option);

		if (isLastChild) 
		{ //séléctionnné
			_TextView_libelleEntreprise.setTextColor(_Context.getResources().getColor(R.color.orange));
			_ImageView.setImageDrawable(_Context.getResources().getDrawable(R.drawable.entreprise_ouvert));
		}else 
		{ //non séléctionné
			_TextView_libelleEntreprise.setTextColor(_Context.getResources().getColor(android.R.color.secondary_text_light));
			_ImageView.setImageDrawable(_Context.getResources().getDrawable(R.drawable.entreprise_fermer));
		}

		_TextView_libelleEntreprise.setTag(R.id.position,groupPosition);
		_TextView_libelleEntreprise.setText(_list_Entreprise.get("libelle_Entreprise"));
		_TextView_libelleEntreprise.setOnClickListener(new OnClickListener()
		{
			public void onClick(View arg0) 
			{
				int groupPosition=(Integer) _TextView_libelleEntreprise.getTag(R.id.position);
				(((Activity_ChefDepartement) _Context)).expandGroup(Activity_ChefDepartement._ExpandableListView_Stage_Depot,groupPosition);
			}
		});
		_Button_Option_Parent.setTag(R.id.position,groupPosition);
		_Button_Option_Parent.setOnClickListener(this);

		return contentView;
	}

	@SuppressWarnings("unchecked")
	public void onClick(View v) 
	{
		switch (v.getId()) 
		{
		case R.id.ActivityChefDepartement_strocture_DepotStage_Child_TextView_DescriptionStage :
			HashMap<String, String> _list_StageEtudiant=(HashMap<String, String>) v.getTag(R.id._HashMap);
			String _Description_Stage=_list_StageEtudiant.get("_descriptiona_Stage");

			AlertDialog.Builder box0 = new AlertDialog.Builder(_Context);
			box0.setTitle("Déscription Stage");
			String SMS0="<br />"+_Description_Stage+"<br />";
			box0.setMessage(Html.fromHtml("<font color=\"#000000 \">" + SMS0 + "</font>",null, null));
			AlertDialog dialog0 = box0.show();
			TextView messageText0 = (TextView)dialog0.findViewById(android.R.id.message);
			messageText0.setGravity(Gravity.CENTER);
			messageText0.setTextSize(14);
			dialog0.show();
			break;

		case R.id.ActivityChefDepartement_strocture_DepotStage_Child_Button_Etudiant:
			String _ID_Etudiant=(String) v.getTag(R.id.ID_etudiant);
			ArrayList<HashMap<String, String>> _list_Etudiant=_MySQliteFonction_ChefDepartement.Afficher_Etudiant_INFORMATION(Integer.parseInt(_ID_Etudiant));
			MyDialog_EtudiantInformation _MyDialog_EtudiantInformation = new MyDialog_EtudiantInformation(_Context, _list_Etudiant);
			_MyDialog_EtudiantInformation.DialogEudiantInformation();
			break;

		case R.id.ActivityChefDepartement_strocture_DepotStage_Child_Button_Binome:
			String _ID_Binome=(String) v.getTag(R.id.ID_etudiant);
			ArrayList<HashMap<String, String>> _list_Binome=_MySQliteFonction_ChefDepartement.Afficher_Etudiant_INFORMATION(Integer.parseInt(_ID_Binome));
			MyDialog_EtudiantInformation _MyDialog_BinomeInformation = new MyDialog_EtudiantInformation(_Context, _list_Binome);
			_MyDialog_BinomeInformation.DialogEudiantInformation();
			break;

		case R.id.ActivityChefDepartement_strocture_DepotStage_Child_Button_Option: 
			final HashMap<String, String> _list_Stage=(HashMap<String, String>) v.getTag(R.id._HashMap);
			AlertDialog.Builder _Builder=new AlertDialog.Builder(_Context);
			_Builder.setTitle(" Options ");
			_Builder.setItems(new CharSequence[]
					{ " Modifier le Stage ", " Supprimer le Stage", "Ajouter/Modifier Encadreur Professionnel"},
					new DialogInterface.OnClickListener()
			{
				public void onClick(DialogInterface dialog, int which)
				{
					switch (which)
					{
					case 0:
						// modifier stage
						String _ID_Stage=_list_Stage.get("_ID_Stage");
						ArrayList<HashMap<String, String>> _list_Etudiants=_MySQliteFonction_ChefDepartement.Afficher_IDEtudiant_DEPOTSTAGE(_ID_Stage);
						MyDialog_ChefDepartement_DepotStage_InsertUdate _MyDialog_ChefDepartement_DepotStage_InsertUdate = new MyDialog_ChefDepartement_DepotStage_InsertUdate(_Context, null, _list_Stage,_list_Etudiants);
						_MyDialog_ChefDepartement_DepotStage_InsertUdate.DialogEntrepirse_DepostageInsert();
						break;
					case 1:
						// supprimer stage  travailPfe Stage Etudiant ...... !!!! a terminer pour enseignant encadreurPro
						String _ID_Stagee=_list_Stage.get("_ID_Stage");
						ArrayList<HashMap<String, String>> _list_IDEtudiants=_MySQliteFonction_ChefDepartement.Afficher_IDEtudiant_DEPOTSTAGE(_ID_Stagee);
						Supprimer_Stage(_list_IDEtudiants,_list_Stage);
						break;
					case 2:
						// Ajoute/modifier Encadreur Professionnel
						String _ID_Stageee=_list_Stage.get("_ID_Stage");
						HashMap< String, String> _HashMap_EncadreurPro=new HashMap<String, String>();
						ArrayList<HashMap<String, String>> _list_EncadreurPro=_MySQliteFonction_ChefDepartement.Afficher_EncadreurPro_DEPOTSTAGE(_ID_Stageee);
						if (_list_EncadreurPro.isEmpty()==false)
						{
							_HashMap_EncadreurPro=_list_EncadreurPro.get(0);
							MyDialog_ChefDepartement_EncadreurPro_InsertUpdate _MyDialog_EncadreurPro_InsertUpdate=new MyDialog_ChefDepartement_EncadreurPro_InsertUpdate(_Context, _HashMap_EncadreurPro , _ID_Stageee );
							_MyDialog_EncadreurPro_InsertUpdate.MyDialogEncadreurPro_InsertUpdate();
						}else
						{
							AjouterAffecter_EncadreurProStage(_HashMap_EncadreurPro, _ID_Stageee);
						}
						break;
					}
				}
			});
			_Builder.create().show();
			break;

		case R.id.ActivityChefDepartement_strocture_DepotStage_Parent_Option :
			final int groupPosition_parent=(Integer) v.getTag(R.id.position); 
			final String _ID_Entreprise_parent=_List_Entreprise.get(groupPosition_parent).get("ID_Entreprise");
			AlertDialog.Builder _Builder_parent=new AlertDialog.Builder(_Context);
			_Builder_parent.setTitle(" Options ");
			_Builder_parent.setItems(new CharSequence[]
					{" Modifier l'Entreprise ", " Supprimer l'Entreprise "," Ajouter un Stage "},
					new DialogInterface.OnClickListener()
			{
				public void onClick(DialogInterface dialog, int which)
				{
					switch (which)
					{
					case 0:
						// modification entreprise
						ArrayList<HashMap<String, String>> _ArrayList_EntrepriseInformation= new  ArrayList<HashMap<String,String>>();
						_ArrayList_EntrepriseInformation.add(getGroup(groupPosition_parent));
						MyDialog_Entreprise_InscriptionInformation _MyDialog_Entreprise_InscriptionInformation = new MyDialog_Entreprise_InscriptionInformation(_Context, _ArrayList_EntrepriseInformation,null);
						_MyDialog_Entreprise_InscriptionInformation.DialogEntrepriseInscription();
						break;
					case 1:
						// supprimer entreprise
						Supprimer_Entreprise(_ID_Entreprise_parent);
						break;
					case 2:
						// ajouter stage
						HashMap<String, String> _list_stage_vide=new HashMap<String,String>();
						ArrayList<HashMap<String, String>> _list_etudiants_vide=new ArrayList<HashMap<String,String>>();
						MyDialog_ChefDepartement_DepotStage_InsertUdate _MyDialog_ChefDepartement_DepotStage_InsertUdate = new MyDialog_ChefDepartement_DepotStage_InsertUdate(_Context, _ID_Entreprise_parent, _list_stage_vide,_list_etudiants_vide);
						_MyDialog_ChefDepartement_DepotStage_InsertUdate.DialogEntrepirse_DepostageInsert();
						break;
					}
				}
			});
			_Builder_parent.create().show();
			break;

		case R.id.ActivityChefDepartement_strocture_DepotStage_Child_Button_EncadreurPro:
			final String _ID_Stage=(String) v.getTag(R.id.ID_Stage); 
			final HashMap<String, String> _list_EncadreurPro = _MySQliteFonction_ChefDepartement.Afficher_EncadreurPro_DEPOTSTAGE(_ID_Stage).get(0);
			if (_list_EncadreurPro.isEmpty()==false)
			{
				AlertDialog.Builder box1 = new AlertDialog.Builder(_Context);
				box1.setTitle("Encadreur Professionnel");
				String _cin=_list_EncadreurPro.get("Cin_EncadreurPro");
				String _nom="<br />"+_list_EncadreurPro.get("Nom_EncadreurPro");
				String _prenom="<br />"+_list_EncadreurPro.get("Prenom_EncadreurPro");
				String _num="<br />"+_list_EncadreurPro.get("NumTel_EncadreurPro");
				String _email="<br />"+_list_EncadreurPro.get("Email_EncadreurPro");
				box1.setMessage(Html.fromHtml("<font color=\"#000000 \">" + _cin+_nom+_prenom+_num+_email + "</font>",null, null));
				box1.setPositiveButton("Supprimer l'encadreur professionnel", new DialogInterface.OnClickListener() 
				{
					public void onClick(DialogInterface dialog, int which)
					{
						Suppimer_EncadreurPro(_ID_Stage, _list_EncadreurPro);
					}
				});
				box1.setNegativeButton("Supprimer l'affectation Encadreur/Stage", new DialogInterface.OnClickListener() 
				{
					public void onClick(DialogInterface dialog, int which) 
					{
						Annuler_Affectation_EncadreurStage(_ID_Stage);
					}
				});
				AlertDialog dialog1 = box1.show();
				TextView messageText1 = (TextView)dialog1.findViewById(android.R.id.message);
				messageText1.setGravity(Gravity.CENTER);
				messageText1.setTextSize(14);
				dialog1.show();	
			}
			break;

		default:
			break;
		}
	}

	public void Annuler_Affectation_EncadreurStage( final String _ID_Stage)
	{
		AlertDialog.Builder _Builder =new AlertDialog.Builder(_Context);
		_Builder.setTitle("Annuler l'affectation Encadeur/Stage");
		_Builder.setMessage("êtes-vous sûr de vouloir annuler l'affectation entre cette encadreur et ce stage ? ");
		_Builder.setNegativeButton("Annuler", null);
		_Builder.setPositiveButton("Confirmer", new DialogInterface.OnClickListener() 
		{
			public void onClick(DialogInterface arg0, int arg1) 
			{
				String _URL=_Context.getResources().getString(R.string.ChefDepartementActivity_update_EncadreurProStage_DELETEaFFECTATION);
				boolean x=_MySQliteFonction_ChefDepartement.UpdateInTo_Serveur_EncadreurProStage_DELETEaFFECTATION(_ID_Stage, _URL);
				if (x) 
				{
					Refresh_Activity();
				}
			}
		});
		_Builder.show();
	}

	public void AjouterAffecter_EncadreurProStage(final HashMap<String, String> _HashMap_EncadreurPro ,final String _ID_Stageee)
	{
		AlertDialog.Builder _Builder=new AlertDialog.Builder(_Context);
		_Builder.setTitle("Affectation Encadreur Professionnel Stage");
		_Builder.setMessage(" veuillez entrer le numéro de la carte d'identité " +
				" de l'encadreur proffessionnel que vous voulez affecter a ce stage");
		LinearLayout layout = new LinearLayout(_Context);
		layout.setOrientation(LinearLayout.HORIZONTAL);
		layout.setGravity(Gravity.CENTER);
		final EditText _EditText_CIN = new EditText(_Context);
		_EditText_CIN.setHint(" Numéro CIN :");
		_EditText_CIN.setInputType(InputType.TYPE_CLASS_NUMBER);
		layout.addView(_EditText_CIN);
		_Builder.setView(layout);
		_Builder.setNegativeButton("Confirmer l'affectation", new DialogInterface.OnClickListener() 
		{
			public void onClick(DialogInterface dialog, int which) 
			{
				if (_EditText_CIN.getText().toString().equals("")==false)
				{
					ArrayList<HashMap<String, String>> _list_EncadreurPro=new ArrayList<HashMap<String,String>>();
					_list_EncadreurPro=_MySQliteFonction_ChefDepartement.Recherche_EncadreurPro_DEPOTSTAGE(_EditText_CIN.getText().toString());
					if (_list_EncadreurPro.isEmpty())
					{// num carte invalide 
						AlertDialog.Builder _Builde=new AlertDialog.Builder(_Context);
						_Builde.setTitle("Erreur");
						_Builde.setMessage("numéro de carte invalide ou n'existe pas");
						AlertDialog dialog1 = _Builde.show();
						TextView messageText1 = (TextView)dialog1.findViewById(android.R.id.message);
						messageText1.setGravity(Gravity.CENTER);
						messageText1.setTextSize(15);
						dialog1.show();	
					}else
					{// num carte valide
						AlertDialog.Builder _Builde=new AlertDialog.Builder(_Context);
						_Builde.setTitle("Information");
						String _cin=_list_EncadreurPro.get(0).get("Cin_EncadreurPro");
						String _nom="<br />"+_list_EncadreurPro.get(0).get("Nom_EncadreurPro");
						String _prenom="<br />"+_list_EncadreurPro.get(0).get("Prenom_EncadreurPro");
						String _num="<br />"+_list_EncadreurPro.get(0).get("NumTel_EncadreurPro");
						String _email="<br />"+_list_EncadreurPro.get(0).get("Email_EncadreurPro");
						_Builde.setMessage(Html.fromHtml("<font color=\"#000000 \">" + _cin+_nom+_prenom+_num+_email+ "</font>",null, null));
						_Builde.setNegativeButton("Annuler", null);
						_Builde.setPositiveButton("Confirmer", new DialogInterface.OnClickListener()
						{
							public void onClick(DialogInterface arg0, int arg1)
							{
								String _URL=_Context.getResources().getString(R.string.ChefDepartementActivity_update_EncadreurProStage_ADDaFFECTATION);
								boolean x=_MySQliteFonction_ChefDepartement.UpdateInTo_Serveur_EncadreurProStage_ADDaFFECTATION(_EditText_CIN.getText().toString(), _ID_Stageee, _URL);
								if (x) 
								{
									Refresh_Activity();
								}
							}
						});
						AlertDialog dialog1 = _Builde.show();
						TextView messageText1 = (TextView)dialog1.findViewById(android.R.id.message);
						messageText1.setGravity(Gravity.CENTER);
						messageText1.setTextSize(15);
						dialog1.show();	
					}
				}else
				{// editText vide
					AlertDialog.Builder _Builde=new AlertDialog.Builder(_Context);
					_Builde.setTitle("Erreur");
					_Builde.setMessage("Champs vide");	
					AlertDialog dialog1 = _Builde.show();
					TextView messageText1 = (TextView)dialog1.findViewById(android.R.id.message);
					messageText1.setGravity(Gravity.CENTER);
					messageText1.setTextSize(15);
					dialog1.show();	
				}
			}
		});
		_Builder.setPositiveButton("Ajouter Un nouveau encadreur professionnel ", new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int which)
			{
				MyDialog_ChefDepartement_EncadreurPro_InsertUpdate _MyDialog_EncadreurPro_InsertUpdate=new MyDialog_ChefDepartement_EncadreurPro_InsertUpdate(_Context, _HashMap_EncadreurPro , _ID_Stageee );
				_MyDialog_EncadreurPro_InsertUpdate.MyDialogEncadreurPro_InsertUpdate();

			}
		});
		_Builder.show();
	}



	public void Suppimer_EncadreurPro(final String _ID_Stage ,final HashMap<String, String> _list_EncadreurPro)
	{
		AlertDialog.Builder _Builder =new AlertDialog.Builder(_Context);
		_Builder.setTitle("Suppression");
		_Builder.setMessage("êtes-vous sûr de vouloir supprimer cette Encadreur Professionnel définitivement ? " +
				", vous devez verifier qu'aucun autre stage n'est affecter a cette encadreur.");
		_Builder.setNegativeButton("Annuler", null);
		_Builder.setPositiveButton("Confirmer", new DialogInterface.OnClickListener() 
		{
			public void onClick(DialogInterface arg0, int arg1) 
			{
				String _URl=_Context.getResources().getString(R.string.ChefDepartementActivity_delete_EncadreurPro);
				String _ID_EncadreurPro=_list_EncadreurPro.get("ID_EncadreurPro");
				boolean x=_MySQliteFonction_ChefDepartement.DeleteInTo_Serveur_EncadreurPro(_ID_Stage, _ID_EncadreurPro, _URl);
				if (x)
				{
					Refresh_Activity();
				}
			}
		});
		_Builder.show();
	}

	public void Supprimer_Stage(final ArrayList<HashMap<String , String>> _list_IDEtudiants,final HashMap<String, String> _list_Stage)
	{
		AlertDialog.Builder _Builder =new AlertDialog.Builder(_Context);
		_Builder.setTitle("Suppression");
		_Builder.setMessage("êtes-vous sûr de vouloir supprimer ce Stage ? ");
		_Builder.setNegativeButton("Annuler", null);
		_Builder.setPositiveButton("Confirmer", new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface arg0, int arg1)
			{
				String _URL0=_Context.getResources().getString(R.string.ChefDepartementActivity_update_TravailPfeStageEtudiant);
				String _ID_Stage=null;
				String _ID_TravailPfe=null;
				String _ID_Etudiant_AV=_list_IDEtudiants.get(0).get("ID_Etudiant");
				String _ID_Etudiant_AP=null;
				String _ID_Binome_AV=null;
				if (_list_IDEtudiants.size()==2)
				{  _ID_Binome_AV=_list_IDEtudiants.get(1).get("ID_Etudiant"); }
				String _ID_Binome_AP=null;
				boolean x=_MySQliteFonction_ChefDepartement.UpdateInTo_Serveur_TravailPfeStageEtudiant(_ID_Stage, null, null, null,_ID_Etudiant_AV ,_ID_Etudiant_AP,_ID_Binome_AV,_ID_Binome_AP ,_ID_TravailPfe,_URL0);
				if (x)
				{
					String _URL1=_Context.getResources().getString(R.string.ChefDepartementActivity_delete_TravailPfeStage);
					String _ID_TravaiPfe=_list_Stage.get("_Id_travailPfe_Stage");
					boolean y=_MySQliteFonction_ChefDepartement.DeleteInTo_Serveur_TravailPfeStage(_ID_TravaiPfe, _URL1);
					if (y)
					{
						Refresh_Activity();	
					}
				}
			}
		});
		_Builder.show();
	}
	public void Supprimer_Entreprise(final String _ID_Entreprise)
	{
		AlertDialog.Builder _Builder =new AlertDialog.Builder(_Context);
		_Builder.setTitle("Suppression");
		_Builder.setMessage("êtes-vous sûr de vouloir supprimer cette Entreprise ? , " +
				"veuillez vérifier que toutes les offres de stage et les stages en relation avec cette entreprise en été supprimer");
		_Builder.setNegativeButton("Annuler", null);
		_Builder.setPositiveButton("Confirmer", new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface arg0, int arg1)
			{
				String _URL=_Context.getResources().getString(R.string.ChefDepartementActivity_delete_Entreprise);
				_MySQliteFonction_ChefDepartement= new MySQliteFonction_ChefDepartement(_Context);
				boolean x=_MySQliteFonction_ChefDepartement.DeleteInTo_Serveur_Entreprise(_ID_Entreprise, _URL);
				if (x)
				{
					Refresh_Activity();
				}
			}
		});
		_Builder.show();
	}

	public void Refresh_Activity()
	{
		((Activity) _Context).finish();
		Intent _Intent_To_ActivityChefDepartement=new Intent(_Context,Activity_ChefDepartement.class);
		_Context.startActivity(_Intent_To_ActivityChefDepartement);
	}

	public boolean hasStableIds() 
	{
		return false;
	}

	public boolean isChildSelectable(int arg0, int arg1)
	{
		return false;
	}


}
