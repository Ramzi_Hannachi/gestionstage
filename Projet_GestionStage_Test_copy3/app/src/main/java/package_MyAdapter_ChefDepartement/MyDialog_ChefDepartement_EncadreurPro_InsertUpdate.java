package package_MyAdapter_ChefDepartement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import package_Foction_DataBase.MySQliteFonction_ChefDepartement;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.projet_gestionstage_test.R;

public class MyDialog_ChefDepartement_EncadreurPro_InsertUpdate implements OnClickListener
{
	private MySQliteFonction_ChefDepartement _MySQliteFonction_ChefDepartement;

	private Context _Context;

	EditText _EditText_Cin;
	EditText _EditText_Nom;
	EditText _EditText_Prenom;
	EditText _EditText_NumTel;
	EditText _EditText_Email;

	TextView _TextView_Cin;
	TextView _TextView_Nom;
	TextView _TextView_Prenom;
	TextView _TextView_NumTel;
	TextView _TextView_Email;

	Button _Button_Confirmer;
	Button _Button_Annuler;

	HashMap<String, String> _HashMap_EncadreurPro=new HashMap<String, String>();
	String _ID_Stage;

	public MyDialog_ChefDepartement_EncadreurPro_InsertUpdate(Context _Context ,HashMap<String, String> _HashMap_EncadreurPro , String _ID_Stage)
	{
		this._Context=_Context;
		this._HashMap_EncadreurPro=_HashMap_EncadreurPro;
		this._ID_Stage=_ID_Stage;
	}

	public void MyDialogEncadreurPro_InsertUpdate()
	{
		Dialog alert = new Dialog(_Context);
		alert.requestWindowFeature(Window.FEATURE_NO_TITLE);
		alert.setContentView(R.layout.activity_chefdepartement_dialog_encadreurpro);

		_EditText_Cin=(EditText) alert.findViewById(R.id.Activity_ChefDepartement_Dialog_EncadreurPro_EditText_cin);
		_EditText_Nom=(EditText) alert.findViewById(R.id.Activity_ChefDepartement_Dialog_EncadreurPro_EditText_nom);
		_EditText_Prenom=(EditText) alert.findViewById(R.id.Activity_ChefDepartement_Dialog_EncadreurPro_EditText_prenom);
		_EditText_NumTel=(EditText) alert.findViewById(R.id.Activity_ChefDepartement_Dialog_EncadreurPro_EditText_numTel);
		_EditText_Email=(EditText) alert.findViewById(R.id.Activity_ChefDepartement_Dialog_EncadreurPro_EditText_email);

		_TextView_Cin=(TextView) alert.findViewById(R.id.Activity_ChefDepartement_Dialog_EncadreurPro_TextView_cin);
		_TextView_Nom=(TextView) alert.findViewById(R.id.Activity_ChefDepartement_Dialog_EncadreurPro_TextView_nom);
		_TextView_Prenom=(TextView) alert.findViewById(R.id.Activity_ChefDepartement_Dialog_EncadreurPro_TextView_prenom);
		_TextView_NumTel=(TextView) alert.findViewById(R.id.Activity_ChefDepartement_Dialog_EncadreurPro_TextView_numTel);
		_TextView_Email=(TextView) alert.findViewById(R.id.Activity_ChefDepartement_Dialog_EncadreurPro_TextView_email);

		_Button_Confirmer=(Button) alert.findViewById(R.id.Activity_ChefDepartement_Dialog_EncadreurPro_Button_confirmer);
		_Button_Annuler=(Button) alert.findViewById(R.id.Activity_ChefDepartement_Dialog_EncadreurPro_Button_annuler);

		_Button_Annuler.setOnClickListener(this);
		_Button_Confirmer.setOnClickListener(this);

		if (_HashMap_EncadreurPro.isEmpty()==false) 
		{
			_EditText_Cin.setText(_HashMap_EncadreurPro.get("Cin_EncadreurPro"));
			_EditText_Nom.setText(_HashMap_EncadreurPro.get("Nom_EncadreurPro"));
			_EditText_Prenom.setText(_HashMap_EncadreurPro.get("Prenom_EncadreurPro"));
			_EditText_NumTel.setText(_HashMap_EncadreurPro.get("NumTel_EncadreurPro"));
			_EditText_Email.setText(_HashMap_EncadreurPro.get("Email_EncadreurPro"));
		}

		_EditText_Cin.addTextChangedListener(new TextWatcher()
		{
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {}
			public void afterTextChanged(Editable s)
			{
				Verifier_Cin(_EditText_Cin.getText().toString());
			}
		});
		_EditText_Nom.addTextChangedListener(new TextWatcher()
		{
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {}
			public void afterTextChanged(Editable s)
			{
				Verifier_Nom(_EditText_Nom.getText().toString());
			}
		});
		_EditText_Prenom.addTextChangedListener(new TextWatcher()
		{
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {}
			public void afterTextChanged(Editable s)
			{
				Verifier_Prenom(_EditText_Prenom.getText().toString());
			}
		});
		_EditText_NumTel.addTextChangedListener(new TextWatcher()
		{
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {}
			public void afterTextChanged(Editable s)
			{
				Verifier_NumTel(_EditText_NumTel.getText().toString());
			}
		});
		_EditText_Email.addTextChangedListener(new TextWatcher()
		{
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {}
			public void afterTextChanged(Editable s)
			{
				Verifier_Email(_EditText_Email.getText().toString());
			}
		});

		alert.show();
	}

	public void onClick(View v) 
	{
		switch (v.getId()) 
		{
		case R.id.Activity_ChefDepartement_Dialog_EncadreurPro_Button_confirmer:

			if (!Verifier_Cin(_EditText_Cin.getText().toString()))
			{
				Toast.makeText(_Context, "Numéro Carte Identité invalide! ", Toast.LENGTH_LONG).show();
				return;
			}
			if (!Verifier_Nom(_EditText_Nom.getText().toString()))
			{
				Toast.makeText(_Context, "Nom invalide! ", Toast.LENGTH_LONG).show();
				return;
			}
			if (!Verifier_Prenom(_EditText_Prenom.getText().toString()))
			{
				Toast.makeText(_Context, "Prenom invalide! ", Toast.LENGTH_LONG).show();
				return;
			}
			if (!Verifier_NumTel(_EditText_NumTel.getText().toString()))
			{
				Toast.makeText(_Context, "Numéro de Téléphone invalide! ", Toast.LENGTH_LONG).show();
				return;
			}
			if (!Verifier_Email(_EditText_Email.getText().toString()))
			{
				Toast.makeText(_Context, "Email invalide! ", Toast.LENGTH_LONG).show();
				return;
			}
			if (_HashMap_EncadreurPro.isEmpty())
			{
				// ajouter
				Ajouter_EncadreurPro();
			}else
			{
				// modifier
				Modifier_EncadreurPro();
			}
			break;

		case R.id.Activity_ChefDepartement_Dialog_EncadreurPro_Button_annuler:
			_EditText_Cin.setText("");
			_EditText_Nom.setText("");
			_EditText_Prenom.setText("");
			_EditText_NumTel.setText("");
			_EditText_Email.setText("");
			break;
		default:
			break;
		}
	}

	public void Modifier_EncadreurPro()
	{
		AlertDialog.Builder _Builder=new AlertDialog.Builder(_Context);
		_Builder.setTitle("Modifier Encadreur Professionnel");
		_Builder.setMessage("Êtes-vous sûr de vouloir Modifier cette Encadreur Professionnel");
		_Builder.setNegativeButton("Annuler", null);
		_Builder.setPositiveButton("Confirmer", new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface arg0, int arg1) 
			{
				String _URL=_Context.getResources().getString(R.string.ChefDepartementActivity_update_EncadreurPro);
				_MySQliteFonction_ChefDepartement=new MySQliteFonction_ChefDepartement(_Context);
				// modifier
				String _ID_EncadreurPro=_HashMap_EncadreurPro.get("ID_EncadreurPro");
				String _cin=_EditText_Cin.getText().toString();
				String _nom=_EditText_Nom.getText().toString();
				String _prenom=_EditText_Prenom.getText().toString();
				String _numTel=_EditText_NumTel.getText().toString();
				String _email=_EditText_Email.getText().toString();
				boolean x=_MySQliteFonction_ChefDepartement.UpdateInTo_Serveur_EncadreurPro(_ID_EncadreurPro, _cin, _nom, _prenom, _numTel, _email, _URL);
				if (x)
				{
					Refresh();
				}
			}
		});
		_Builder.show();
	}	

	public void Ajouter_EncadreurPro()
	{
		AlertDialog.Builder _Builder=new AlertDialog.Builder(_Context);
		_Builder.setTitle("Ajouter Encadreur Professionnel");
		_Builder.setMessage("Êtes-vous sûr de vouloir Ajouter cette Encadreur Professionnel a ce Stage");
		_Builder.setNegativeButton("Annuler", null);
		_Builder.setPositiveButton("Confirmer", new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface arg0, int arg1) 
			{
				String _URL=_Context.getResources().getString(R.string.ChefDepartementActivity_insert_EncadreurPro);
				_MySQliteFonction_ChefDepartement=new MySQliteFonction_ChefDepartement(_Context);
				// ajouter
				String _cin=_EditText_Cin.getText().toString();
				String _nom=_EditText_Nom.getText().toString();
				String _prenom=_EditText_Prenom.getText().toString();
				String _numTel=_EditText_NumTel.getText().toString();
				String _email=_EditText_Email.getText().toString();
				boolean x=_MySQliteFonction_ChefDepartement.InsertInTo_Serveur_EncadreurPro(_cin, _nom, _prenom, _numTel, _email, _ID_Stage,_URL);
				if (x) 
				{
					Refresh();
				}
			}
		});
		_Builder.show();
	}

	public void Refresh()
	{
		((Activity) _Context).finish();
		Intent lIntent_To_Activity = new Intent(_Context, ((Activity)_Context).getClass());
		_Context.startActivity(lIntent_To_Activity);

	}











	public boolean Verifier_Cin(String cin)
	{
		if (cin.equals(""))
		{
			_TextView_Cin.setTextColor(_Context.getResources().getColor(R.color.red));
			return false;
		}

		Pattern p = Pattern.compile("^[0-9]{8}");
		Matcher n = p.matcher(cin);
		if ( n.matches())
		{
			if (_HashMap_EncadreurPro.isEmpty())
			{
				ArrayList<HashMap<String, String>> _list_EncadreurPro=new ArrayList<HashMap<String,String>>();
				_MySQliteFonction_ChefDepartement=new MySQliteFonction_ChefDepartement(_Context);
				_list_EncadreurPro=_MySQliteFonction_ChefDepartement.Recherche_EncadreurPro_DEPOTSTAGE(cin);
				if (_list_EncadreurPro.isEmpty())
				{
					_TextView_Cin.setTextColor(_Context.getResources().getColor(android.R.color.black));
					return true;
				}else
				{
					Toast.makeText(_Context, "numéro de la carte d'identité existe", Toast.LENGTH_SHORT).show();
					_TextView_Cin.setTextColor(_Context.getResources().getColor(R.color.red));
					return false;
				}
			}else
			{
				_TextView_Cin.setTextColor(_Context.getResources().getColor(android.R.color.black));
				return true;	
			}
		}
		_TextView_Cin.setTextColor(_Context.getResources().getColor(R.color.red));
		return false;
	}

	public boolean Verifier_Nom(String nom)
	{
		if (nom.equals(""))
		{
			_TextView_Nom.setTextColor(_Context.getResources().getColor(R.color.red));
			return false;
		}

		Pattern p = Pattern.compile("^[a-zA-Z_ _à_À_â_Â_é_è_É_È_ê_Ê_ô_Ô_ù_Ù_Ç_ç_`]{4,20}");
		Matcher n = p.matcher(nom);
		if ( n.matches())
		{
			_TextView_Nom.setTextColor(_Context.getResources().getColor(android.R.color.black));
			return true;
		}
		_TextView_Nom.setTextColor(_Context.getResources().getColor(R.color.red));
		return false;
	}

	public boolean Verifier_Prenom(String nom)
	{
		if (nom.equals(""))
		{
			_TextView_Prenom.setTextColor(_Context.getResources().getColor(R.color.red));
			return false;
		}

		Pattern p = Pattern.compile("^[a-zA-Z_ _à_À_â_Â_é_è_É_È_ê_Ê_ô_Ô_ù_Ù_Ç_ç_`]{4,20}");
		Matcher n = p.matcher(nom);
		if ( n.matches())
		{
			_TextView_Prenom.setTextColor(_Context.getResources().getColor(android.R.color.black));
			return true;
		}
		_TextView_Prenom.setTextColor(_Context.getResources().getColor(R.color.red));
		return false;
	}

	public boolean Verifier_NumTel(String numTel)
	{
		if (numTel.equals(""))
		{
			_TextView_NumTel.setTextColor(_Context.getResources().getColor(R.color.red));
			return false;
		}

		Pattern p = Pattern.compile("^[0-9]{8}");
		Matcher n = p.matcher(numTel);
		if ( n.matches())
		{
			_TextView_NumTel.setTextColor(_Context.getResources().getColor(android.R.color.black));
			return true;
		}
		_TextView_NumTel.setTextColor(_Context.getResources().getColor(R.color.red));
		return false;
	}

	public  boolean Verifier_Email(CharSequence email)
	{
		if (email.equals(""))
		{
			_TextView_Email.setTextColor(_Context.getResources().getColor(R.color.red));
			return false;
		}
		if (android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches())
		{
			_TextView_Email.setTextColor(_Context.getResources().getColor(android.R.color.black));
			return true;
		} 
		_TextView_Email.setTextColor(_Context.getResources().getColor(R.color.red));
		return false;
	}


}

