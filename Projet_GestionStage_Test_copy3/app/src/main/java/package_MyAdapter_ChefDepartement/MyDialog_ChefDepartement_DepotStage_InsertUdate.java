package package_MyAdapter_ChefDepartement;



import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import package_Foction_DataBase.MySQliteFonction_ChefDepartement;
import package_MemoirStage.Activity_ChefDepartement;
import package_MyAdapter_Etudiant.MyDialog_EtudiantInformation;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.projet_gestionstage_test.R;

public class MyDialog_ChefDepartement_DepotStage_InsertUdate implements android.view.View.OnClickListener 
{
	private	ArrayList<HashMap<String, String>> _ArrayList_Etudiant=new ArrayList<HashMap<String,String>>();
	private ArrayList<HashMap<String, String>> _ArrayList_Binome=new ArrayList<HashMap<String,String>>();

	private String  _ID_ETUDIANT="null";
	private String  _ID_BINOME="null";

	/////// Insert Stage !!
	MySQliteFonction_ChefDepartement _MySQliteFonction_ChefDepartement;
	TextView _TextView_Nom;
	TextView _TextView_Description;

	EditText _EditText_Nom;
	EditText _EditText_Description;
	DatePicker	_DatePicker_Date;

	Button _Button_AddEtudiant;
	Button _Button_Etudiant;
	Button _Button_Binome;

	TextView _TextView_Nom_Etudiant;
	TextView _TextView_NumInscrit_Etudiant;
	TextView _TextView_Nom_Binome;
	TextView _TextView_NumInscrit_Binome;

	Button _Button_Confirmer;
	Button _Button_Annuler;

	private Context _Context;
	private String _ID_Entreprise;
	/////// update Stage !!!
	HashMap<String, String> _list_Stage_selection=new HashMap<String, String>();
	ArrayList<HashMap<String, String>> _list_Etudiants_selection=new ArrayList<HashMap<String,String>>();
	public MyDialog_ChefDepartement_DepotStage_InsertUdate(Context _Context,String _ID_Entreprise,HashMap<String, String> _list_Stage_selection,ArrayList<HashMap<String, String>> _list_Etudiants_selection)
	{
		_MySQliteFonction_ChefDepartement=new MySQliteFonction_ChefDepartement(_Context);
		this._Context=_Context;
		this._ID_Entreprise=_ID_Entreprise;
		/// update offreStage 
		this._list_Stage_selection=_list_Stage_selection;
		this._list_Etudiants_selection=_list_Etudiants_selection;
	}

	public void DialogEntrepirse_DepostageInsert()
	{
		Dialog _Dialog = new Dialog(_Context);
		_Dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		_Dialog.setContentView(R.layout.activity_chefdepartement_dialog_depotstage);

		_TextView_Nom=(TextView) _Dialog.findViewById(R.id.Activity_ChefDepartement_Dialog_DepotStage_TextView_nom);
		_TextView_Description=(TextView) _Dialog.findViewById(R.id.Activity_ChefDepartement_Dialog_DepotStage_TextView_description);

		_EditText_Nom=(EditText) _Dialog.findViewById(R.id.Activity_ChefDepartement_Dialog_DepotStage_EditText_nom);
		_EditText_Description=(EditText) _Dialog.findViewById(R.id.Activity_ChefDepartement_Dialog_DepotStage_EditText_description);
		_DatePicker_Date=(DatePicker) _Dialog.findViewById(R.id.Activity_ChefDepartement_Dialog_DepotStage_EditText_date);
		//   Etudiant/Binome 
		_Button_AddEtudiant=(Button) _Dialog.findViewById(R.id.Activity_ChefDepartement_Dialog_DepotStage_Button_ADDEtudiant);

		_Button_Etudiant=(Button) _Dialog.findViewById(R.id.Activity_ChefDepartement_Dialog_DepotStage_Button_Etudiant);
		_TextView_Nom_Etudiant=(TextView) _Dialog.findViewById(R.id.Activity_ChefDepartement_Dialog_DepotStage_TextView_Etudiant_Nom);
		_TextView_NumInscrit_Etudiant=(TextView) _Dialog.findViewById(R.id.Activity_ChefDepartement_Dialog_DepotStage_TextView_Etudiant_NumInscrit);

		_Button_Binome=(Button) _Dialog.findViewById(R.id.Activity_ChefDepartement_Dialog_DepotStage_Button_Binome);
		_TextView_Nom_Binome=(TextView) _Dialog.findViewById(R.id.Activity_ChefDepartement_Dialog_DepotStage_TextView_Binome_Nom);
		_TextView_NumInscrit_Binome=(TextView) _Dialog.findViewById(R.id.Activity_ChefDepartement_Dialog_DepotStage_TextView_Binome_NumInscrit);
		// Confirmation annuler
		_Button_Confirmer=(Button) _Dialog.findViewById(R.id.Activity_ChefDepartement_Dialog_DepotStage_Button_confirmer);
		_Button_Annuler=(Button) _Dialog.findViewById(R.id.Activity_ChefDepartement_Dialog_DepotStage_Button_annuler);

		_Button_AddEtudiant.setOnClickListener(this);
		_Button_Etudiant.setOnClickListener(this);
		_Button_Binome.setOnClickListener(this);

		_Button_Confirmer.setOnClickListener(this);
		_Button_Annuler.setOnClickListener(this);

		RemplirChamps_Update();
		Visibiliter_Binome();

		_Dialog.show();

		_EditText_Nom.addTextChangedListener(new TextWatcher(){
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {}
			public void afterTextChanged(Editable s)
			{
				Verifier_Nom(_EditText_Nom.getText().toString());
			}
		});
		_EditText_Description.addTextChangedListener(new TextWatcher() {
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,int arg3) {}
			public void afterTextChanged(Editable arg0)
			{
				Verifier_Description(_EditText_Description.getText().toString());	
			}
		});
	}

	public void onClick(View v)
	{
		switch (v.getId()) 
		{
		case R.id.Activity_ChefDepartement_Dialog_DepotStage_Button_ADDEtudiant:
			AddEtudiant();
			break;

		case R.id.Activity_ChefDepartement_Dialog_DepotStage_Button_Etudiant:
			Afficher_Etudiant();
			break;

		case R.id.Activity_ChefDepartement_Dialog_DepotStage_Button_Binome: 
			Afficher_Binome();
			break;

		case R.id.Activity_ChefDepartement_Dialog_DepotStage_Button_confirmer:
			InsertUpdate_Stage();
			break;

		case R.id.Activity_ChefDepartement_Dialog_DepotStage_Button_annuler:
			Viderchamps();
			break;
		default:
			break;
		}
	}

	public boolean Verifier_Description(String description)
	{
		if (description.equals(""))
		{
			_TextView_Description.setTextColor(_Context.getResources().getColor(R.color.red));
			return false;
		}

		Pattern p = Pattern.compile("^[a-zA-Z_0-9_ _à_À_â_Â_é_è_É_È_ê_Ê_ô_Ô_ù_Ù_Ç_ç_-_,_;_._:_(_)_`]{4,225}");//^[a-zAn-Z_0-9]{4,20}"
		Matcher d = p.matcher(description);
		if ( d.matches())
		{
			_TextView_Description.setTextColor(_Context.getResources().getColor(android.R.color.black));
			return true;
		}
		_TextView_Description.setTextColor(_Context.getResources().getColor(R.color.red));
		return false;
	}

	public boolean Verifier_Nom(String nom)
	{
		if (nom.equals(""))
		{
			_TextView_Nom.setTextColor(_Context.getResources().getColor(R.color.red));
			return false;
		}

		Pattern p = Pattern.compile("^[a-zA-Z_0-9_ _à_À_â_Â_é_è_É_È_ê_Ê_ô_Ô_ù_Ù_Ç_ç_-_,_;_._:_(_)_`]{4,225}");//^[a-zAn-Z_0-9]{4,20}"
		Matcher n = p.matcher(nom);
		if ( n.matches())
		{
			_TextView_Nom.setTextColor(_Context.getResources().getColor(android.R.color.black));
			return true;
		}
		_TextView_Nom.setTextColor(_Context.getResources().getColor(R.color.red));
		return false;
	}

	public boolean Verifier_TextViewEtudiant(TextView _TextView_nom,TextView _TextView_numInscrit)
	{

		if (_TextView_nom.getText().toString().equals("Nom Etudiant") || _TextView_numInscrit.getText().toString().equals("numéro d`inscription") )
		{
			_TextView_nom.setTextColor(_Context.getResources().getColor(R.color.red));
			_TextView_numInscrit.setTextColor(_Context.getResources().getColor(R.color.red));
			return false;
		}else 
		{
			_TextView_nom.setTextColor(_Context.getResources().getColor(android.R.color.darker_gray));
			_TextView_numInscrit.setTextColor(_Context.getResources().getColor(android.R.color.darker_gray));
			return true;	
		}
	}

	public void Viderchamps()
	{
		_EditText_Description.setText("");
		_EditText_Nom.setText("");
	}

	public void RemplirChamps_Update()
	{
		if (_list_Stage_selection.isEmpty()==false && _list_Etudiants_selection.isEmpty()==false)
		{   // remplir les champs pour la modification !! 
			// Stage
			String nom_Stage=_list_Stage_selection.get("_nom_Stage");
			String description_Stage= _list_Stage_selection.get("_descriptiona_Stage");
			_EditText_Nom.setText(nom_Stage);
			_EditText_Description.setText(description_Stage);
			// Etudiant
			_ID_ETUDIANT=_list_Etudiants_selection.get(0).get("ID_Etudiant");
			Log.d("_ID_ETUDIANT", _ID_ETUDIANT);
			String nom_Etudiant=_list_Etudiants_selection.get(0).get("nom_Etudiant");
			String prenom_Etudiant=_list_Etudiants_selection.get(0).get("prenom_Etudiant");
			String numInscrit_Etudiant=_list_Etudiants_selection.get(0).get("numInscrit_Etudiant");
			_TextView_Nom_Etudiant.setText(nom_Etudiant+" "+prenom_Etudiant);
			_TextView_NumInscrit_Etudiant.setText(numInscrit_Etudiant);
			_ArrayList_Etudiant=_MySQliteFonction_ChefDepartement.Afficher_Etudiant_INFORMATION(Integer.parseInt(_ID_ETUDIANT));

			if (_list_Etudiants_selection.size()==2)
			{
				_ID_BINOME=_list_Etudiants_selection.get(1).get("ID_Etudiant");
				String nom_Binome=_list_Etudiants_selection.get(1).get("nom_Etudiant");
				String prenom_Binome=_list_Etudiants_selection.get(1).get("prenom_Etudiant");
				String numInscrit_Binome=_list_Etudiants_selection.get(1).get("numInscrit_Etudiant");
				_TextView_Nom_Binome.setText(nom_Binome+" "+prenom_Binome);
				_TextView_NumInscrit_Binome.setText(numInscrit_Binome);
				_ArrayList_Binome=_MySQliteFonction_ChefDepartement.Afficher_Etudiant_INFORMATION(Integer.parseInt(_ID_BINOME));
			}
		}
	}

	public void AddEtudiant()
	{
		AlertDialog.Builder _Builder1=new AlertDialog.Builder(_Context);
		_Builder1.setTitle("Ajouter Un Etudiant");
		_Builder1.setMessage(" Veuillez entrer le numéro d'inscription de l'étudiant à ajouter à ce Stage ");
		LinearLayout layout = new LinearLayout(_Context);
		layout.setOrientation(LinearLayout.HORIZONTAL);
		layout.setGravity(Gravity.CENTER);
		final EditText _EditText_Etudiant = new EditText(_Context);
		_EditText_Etudiant.setHint(" Num Inscription ");
		_EditText_Etudiant.setInputType(InputType.TYPE_CLASS_NUMBER);
		layout.addView(_EditText_Etudiant);
		_Builder1.setView(layout);
		_Builder1.setNegativeButton("Annuler", null);
		_Builder1.setPositiveButton("Confirmer",new OnClickListener()
		{
			public void onClick(DialogInterface arg0, int arg1)
			{
				String _URL=_Context.getResources().getString(R.string.ChefDepartementActivity_display_Etudiant);
				String _ID_Departement=Activity_ChefDepartement.ID_DEPARTEMENT;
				String _NumInscrit_Etudiant=_EditText_Etudiant.getText().toString();
				_ArrayList_Etudiant=new ArrayList<HashMap<String,String>>();
				_ArrayList_Etudiant=_MySQliteFonction_ChefDepartement.Watch_Serveur_Etudiant(_NumInscrit_Etudiant, null,_ID_Departement , _URL);
				if (_ArrayList_Etudiant.isEmpty()==false)
				{
					_ID_ETUDIANT=_ArrayList_Etudiant.get(0).get("_ID_Etudiant");
					_ID_BINOME=_ArrayList_Etudiant.get(0).get("_Id_etudiant1_Etudiant");
					String nom_Etudiant=_ArrayList_Etudiant.get(0).get("nom_Etudiant");
					String prenom_Etudiant=_ArrayList_Etudiant.get(0).get("prenom_Etudiant");
					String numInscrit_Etudiant=_ArrayList_Etudiant.get(0).get("numInscrit_Etudiant");
					_TextView_Nom_Etudiant.setText(prenom_Etudiant+" "+nom_Etudiant);
					_TextView_NumInscrit_Etudiant.setText(numInscrit_Etudiant);
					if (_ID_BINOME.equals("null")==false)
					{
						_ArrayList_Binome=new ArrayList<HashMap<String,String>>();
						_ArrayList_Binome=_MySQliteFonction_ChefDepartement.Watch_Serveur_Etudiant(null, _ID_BINOME,_ID_Departement , _URL);
						if (_ArrayList_Binome.isEmpty()==false)
						{
							String nom_Binome=_ArrayList_Binome.get(0).get("nom_Etudiant");
							String prenom_Binome=_ArrayList_Binome.get(0).get("prenom_Etudiant");
							String numInscrit_Binome=_ArrayList_Binome.get(0).get("numInscrit_Etudiant");

							_TextView_Nom_Binome.setText(prenom_Binome+" "+nom_Binome);
							_TextView_NumInscrit_Binome.setText(numInscrit_Binome);
						}

					}
					Visibiliter_Binome();
					Verifier_TextViewEtudiant(_TextView_Nom_Etudiant, _TextView_NumInscrit_Etudiant);
				}else
				{
					AlertDialog.Builder _Builder=new AlertDialog.Builder(_Context);
					_Builder.setTitle("Erreur");
					_Builder.setMessage("Cette étudiant n'est pas valide , il possède déjà" +
							" un stage ou il n'existe pas  ");
					_Builder.show();
				}
			}
		});
		_Builder1.show();
	}
	public void Visibiliter_Binome()
	{
		if (_ID_BINOME.equals("null"))
		{
			_Button_Binome.setVisibility(View.GONE);
			_TextView_Nom_Binome.setVisibility(View.GONE);
			_TextView_NumInscrit_Binome.setVisibility(View.GONE);
		}else 
		{
			_Button_Binome.setVisibility(View.VISIBLE);
			_TextView_Nom_Binome.setVisibility(View.VISIBLE);
			_TextView_NumInscrit_Binome.setVisibility(View.VISIBLE);
		}

	}
	public void Afficher_Etudiant() 
	{
		if (_ID_ETUDIANT.equals("null")==false)
		{
			MyDialog_EtudiantInformation _MyDialog_EtudiantInformation = new MyDialog_EtudiantInformation(_Context, _ArrayList_Etudiant);
			_MyDialog_EtudiantInformation.DialogEudiantInformation();
		}else
		{
			AlertDialog.Builder _Builder=new AlertDialog.Builder(_Context);
			_Builder.setTitle("Erreur");
			_Builder.setMessage(" Avant d'afficher un étudiant vous devez l'ajouter ");
			_Builder.show();
		}
	}
	public void Afficher_Binome() 
	{
		if (_ID_BINOME.equals("null")==false)
		{
			MyDialog_EtudiantInformation _MyDialog_EtudiantInformation = new MyDialog_EtudiantInformation(_Context, _ArrayList_Binome);
			_MyDialog_EtudiantInformation.DialogEudiantInformation();
		}
	}
	public void InsertUpdate_Stage()
	{   
		String _description=_EditText_Description.getText().toString();
		String _nom=_EditText_Nom.getText().toString();
		boolean x=Verifier_Description(_description);
		boolean y=Verifier_Nom(_nom);
		boolean z=Verifier_TextViewEtudiant(_TextView_Nom_Etudiant, _TextView_NumInscrit_Etudiant);
		if ( x && y && z)
		{
			AlertDialog.Builder _Builder=new AlertDialog.Builder(_Context);
			if (_list_Stage_selection.isEmpty() && _list_Etudiants_selection.isEmpty())
			{
				_Builder.setTitle("Êtes-vous sûr de vouloir Ajouter ce stage");
			}else
			{
				_Builder.setTitle("Êtes-vous sûr de vouloir Modifier ce stage");
			}
			_Builder.setNegativeButton("Annuler", null);
			_Builder.setPositiveButton("Confirmer", new OnClickListener()
			{
				@SuppressLint("ShowToast")
				public void onClick(DialogInterface dialog, int which)
				{
					String jour_stage=String.valueOf(_DatePicker_Date.getDayOfMonth());
					String mois_stage=String.valueOf(_DatePicker_Date.getMonth());
					String annee_stage=String.valueOf(_DatePicker_Date.getYear());
					String _date_stage=jour_stage+"/"+mois_stage+"/"+annee_stage;
					String _nom_stage=_EditText_Nom.getText().toString();
					String _description_stage=_EditText_Description.getText().toString();
					if (_list_Stage_selection.isEmpty() && _list_Etudiants_selection.isEmpty())
					{// Insert Stage !!!
						boolean x0=_MySQliteFonction_ChefDepartement.InsertInTo_Serveur_TravailPfe(_nom_stage, _Context.getResources().getString(R.string.ChefDepartementActivity_insert_TravailPfe));
						if (x0)
						{
							ArrayList<HashMap<String, String>> _list_TravailPfe=new ArrayList<HashMap<String,String>>();
							_list_TravailPfe=_MySQliteFonction_ChefDepartement.Watch_Serveur_TravailPfe(_Context.getResources().getString(R.string.ChefDepartementActivity_display_TravailPfe));
							String _ID_TravailPfe=_list_TravailPfe.get(0).get("_ID_TravailPfe");
							boolean x1=_MySQliteFonction_ChefDepartement.InsertInTo_Serveur_Stage(_nom_stage, _date_stage, _ID_TravailPfe, "null", _description_stage,_ID_Entreprise, _Context.getResources().getString(R.string.ChefDepartementActivity_insert_Stage_CHEF));
							if (x1)
							{
								//update etudiant binome 
								boolean x2=_MySQliteFonction_ChefDepartement.UpdateInTo_Serveur_OffreStageEtudiant(null, _ID_TravailPfe, null, _ID_ETUDIANT,_ID_BINOME, _Context.getResources().getString(R.string.ChefDepartementActivity_update_OffreStageEtudiant));
								if (x2)
								{
									((Activity) _Context).finish();
									Intent _Intent_To_ActivityChefDepartement=new Intent(_Context,Activity_ChefDepartement.class);
									_Context.startActivity(_Intent_To_ActivityChefDepartement);
								}
							}
						}
					}else
					{
						// modification
						String _URL=_Context.getResources().getString(R.string.ChefDepartementActivity_update_TravailPfeStageEtudiant);
						String _ID_Stage=_list_Stage_selection.get("_ID_Stage");
						String _ID_TravailPfe=_list_Stage_selection.get("_Id_travailPfe_Stage");
						String _ID_Etudiant_AV=_list_Etudiants_selection.get(0).get("ID_Etudiant");
						String _ID_Etudiant_AP=_ID_ETUDIANT;
						String _ID_Binome_AV=null;
						if (_list_Etudiants_selection.size()==2)
						{  _ID_Binome_AV=_list_Etudiants_selection.get(1).get("ID_Etudiant"); }
						String _ID_Binome_AP=_ID_BINOME;
						boolean x4=_MySQliteFonction_ChefDepartement.UpdateInTo_Serveur_TravailPfeStageEtudiant(_ID_Stage, _nom_stage, _date_stage, _description_stage,_ID_Etudiant_AV ,_ID_Etudiant_AP,_ID_Binome_AV,_ID_Binome_AP ,_ID_TravailPfe,_URL);
						if (x4)
						{
							((Activity) _Context).finish();
							Intent lIntent_To_Activity = new Intent(_Context, ((Activity)_Context).getClass());
							_Context.startActivity(lIntent_To_Activity);
						}
					}
				}
			});
			_Builder.show();
		}
	}
}
