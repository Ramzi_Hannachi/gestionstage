package package_MyAdapter_ChefDepartement;

import java.util.ArrayList;
import java.util.HashMap;

import package_Foction_DataBase.MySQliteFonction_ChefDepartement;
import package_MemoirStage.Activity_ChefDepartement;
import package_MyAdapter_Etudiant.MyDialog_EtudiantInformation;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.projet_gestionstage_test.R;

public class MyExpandableListAdapter_ChefDepartement_DemandeEncadrement extends BaseExpandableListAdapter implements OnClickListener 
{
	MySQliteFonction_ChefDepartement _MySQliteFonction_ChefDepartement;

	private Context _Context;
	private ArrayList<HashMap<String, String>> _List_EnseignantDepartement=new ArrayList<HashMap<String,String>>();

	public MyExpandableListAdapter_ChefDepartement_DemandeEncadrement(Context _Context,ArrayList<HashMap<String, String>> _List_EnseignantDepartement )
	{
		_MySQliteFonction_ChefDepartement=new MySQliteFonction_ChefDepartement(_Context);

		this._Context=_Context;
		this._List_EnseignantDepartement=_List_EnseignantDepartement;
	}

	public HashMap<String, String> getChild(int groupPosition, int childPosition) 
	{
		String _ID_Enseignant=_List_EnseignantDepartement.get(groupPosition).get("ID_Enseignant");
		ArrayList<HashMap<String, String>> _list_DemandeEtudiantTravailStageEntreprise=new ArrayList<HashMap<String,String>>();
		_list_DemandeEtudiantTravailStageEntreprise=_MySQliteFonction_ChefDepartement.Afficher_DemandeEtudiantTravailStageEntreprise_DEMANDeENCADREMENT(_ID_Enseignant);
		if (!_list_DemandeEtudiantTravailStageEntreprise.isEmpty())
		{
			return _list_DemandeEtudiantTravailStageEntreprise.get(childPosition);
		}else
		{
			return null;
		}

	}
	public long getChildId(int groupPosition, int childPosition)
	{
		return childPosition;
	}
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View contentView,ViewGroup parent)
	{
		HashMap<String, String> _list_DemandeEtudiantTravailStageEntreprise=getChild(groupPosition, childPosition);	
		if (contentView==null)
		{
			LayoutInflater layoutInflater=(LayoutInflater) _Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			contentView=layoutInflater.inflate(R.layout.activity_chefdepartement_strocture_demandeencadrement_child, null);
		}
		TextView _TextView_NomStage=(TextView) contentView.findViewById(R.id.ActivityChefDepartement_strocture_DemandeEncadrement_Child_TextView_NomStage);
		Button _Button_Entreprise=(Button) contentView.findViewById(R.id.ActivityChefDepartement_strocture_DemandeEncadrement_Child_Button_Entreprise);
		Button _Button_Description=(Button) contentView.findViewById(R.id.ActivityChefDepartement_strocture_DemandeEncadrement_Child_Button_Description);
		Button _Button_Etudiant1=(Button) contentView.findViewById(R.id.ActivityChefDepartement_strocture_DemandeEncadrement_Child_Button_Etudiant1);
		Button _Button_Etudiant2=(Button) contentView.findViewById(R.id.ActivityChefDepartement_strocture_DemandeEncadrement_Child_Button_Etudiant2);
		Button _Button_EncadrementAdd=(Button) contentView.findViewById(R.id.ActivityChefDepartement_strocture_DemandeEncadrement_Child_Button_EncadrementAdd);
		Button _Button_EncadrementRemove=(Button) contentView.findViewById(R.id.ActivityChefDepartement_strocture_DemandeEncadrement_Child_Button_EncadrementRemove);

		_TextView_NomStage.setText(_list_DemandeEtudiantTravailStageEntreprise.get("_nom_Stage"));

		_Button_Entreprise.setTag(R.id._HashMap,_list_DemandeEtudiantTravailStageEntreprise);
		_Button_Entreprise.setOnClickListener(this);

		_Button_Description.setTag(R.id._HashMap,_list_DemandeEtudiantTravailStageEntreprise);
		_Button_Description.setOnClickListener(this);

		_Button_Etudiant1.setTag(R.id.ID_etudiant,_list_DemandeEtudiantTravailStageEntreprise.get("Id_etudiant_Demande"));
		_Button_Etudiant1.setOnClickListener(this);

		_Button_Etudiant2.setTag(R.id.ID_binome,_list_DemandeEtudiantTravailStageEntreprise.get("Id_etudiant1_Etudiant"));
		_Button_Etudiant2.setOnClickListener(this);

		_Button_EncadrementAdd.setTag(R.id._HashMap,_list_DemandeEtudiantTravailStageEntreprise);
		_Button_EncadrementAdd.setOnClickListener(this);

		_Button_EncadrementRemove.setTag(R.id._HashMap,_list_DemandeEtudiantTravailStageEntreprise);
		_Button_EncadrementRemove.setOnClickListener(this);

		Verification_Etudiant(_list_DemandeEtudiantTravailStageEntreprise.get("Id_etudiant_Demande"), _Button_Etudiant2);
		Verification_DemandeEncadrement(_list_DemandeEtudiantTravailStageEntreprise.get("confirmation_Demande"), _Button_EncadrementAdd, _Button_EncadrementRemove);
		return contentView;
	}

	public void Verification_DemandeEncadrement (String _confirmation_Demande,Button _Button_EncadrementAdd,Button _Button_EncadrementRemove)
	{
		if (_confirmation_Demande.equals("1")) 
		{
			_Button_EncadrementAdd.setBackgroundResource(R.drawable.ajouter_confirmation);
			_Button_EncadrementAdd.setEnabled(true);

			_Button_EncadrementRemove.setBackgroundResource(R.drawable.supprimer_confirmation);
			_Button_EncadrementRemove.setEnabled(true);
		}

		if (_confirmation_Demande.equals("2")) 
		{
			_Button_EncadrementAdd.setBackgroundResource(R.drawable.button_signer_stage_vert);
			_Button_EncadrementAdd.setEnabled(true);

			_Button_EncadrementRemove.setEnabled(false);
			_Button_EncadrementRemove.setVisibility(View.GONE);
		}
	}

	@SuppressWarnings("unchecked")
	public void onClick(View v) 
	{
		switch (v.getId())
		{
		case R.id.ActivityChefDepartement_strocture_DemandeEncadrement_Child_Button_Entreprise:
			HashMap<String, String> _list_Entrepirse=(HashMap<String, String>) v.getTag(R.id._HashMap);
			String _entreprise_Libelle=_list_Entrepirse.get("libelle_Entreprise");
			String _entreprise_Raison=_list_Entrepirse.get("raison_Entreprise");
			String _entreprise_Email=_list_Entrepirse.get("email_Entreprise");
			String _entreprise_SiteWeb=_list_Entrepirse.get("siteWeb_Entreprise");
			AlertDialog.Builder box0 = new AlertDialog.Builder(_Context);
			box0.setTitle("Entreprise");
			String SMS0="<br />"+_entreprise_Libelle+"<br />"+_entreprise_Raison+"<br />"+_entreprise_Email+"<br />"+_entreprise_SiteWeb;
			box0.setMessage(Html.fromHtml("<font color=\"#000000 \">" + SMS0 + "</font>",null, null));
			AlertDialog dialog0 = box0.show();
			TextView messageText0 = (TextView)dialog0.findViewById(android.R.id.message);
			messageText0.setGravity(Gravity.CENTER);
			messageText0.setTextSize(14);
			dialog0.show();
			break;

		case R.id.ActivityChefDepartement_strocture_DemandeEncadrement_Child_Button_Description:
			HashMap<String, String> _list_Stage=(HashMap<String, String>) v.getTag(R.id._HashMap);
			AlertDialog.Builder box1 = new AlertDialog.Builder(_Context);
			box1.setTitle("Description Stage");
			String SMS1="<br />"+_list_Stage.get("_descriptiona_Stage") +"<br />";
			box1.setMessage(Html.fromHtml("<font color=\"#000000 \">" + SMS1 + "</font>",null, null));
			AlertDialog dialog1 = box1.show();
			TextView messageText1 = (TextView)dialog1.findViewById(android.R.id.message);
			messageText1.setGravity(Gravity.CENTER);
			messageText1.setTextSize(14);
			dialog1.show();
			break;

		case R.id.ActivityChefDepartement_strocture_DemandeEncadrement_Child_Button_Etudiant1:
			String _ID_Etudiant= (String) v.getTag(R.id.ID_etudiant);
			ArrayList<HashMap<String, String>> _list_etudiant=new ArrayList<HashMap<String,String>>();
			_list_etudiant=_MySQliteFonction_ChefDepartement.Afficher_Etudiant_INFORMATION(Integer.parseInt(_ID_Etudiant));
			MyDialog_EtudiantInformation _MyDialog_EtudiantInformation=new MyDialog_EtudiantInformation(_Context, _list_etudiant);
			_MyDialog_EtudiantInformation.DialogEudiantInformation();
			break;

		case R.id.ActivityChefDepartement_strocture_DemandeEncadrement_Child_Button_Etudiant2:
			String _ID_Binome= (String) v.getTag(R.id.ID_binome);
			ArrayList<HashMap<String, String>> _list_Binome =new ArrayList<HashMap<String,String>>();

			_list_Binome=_MySQliteFonction_ChefDepartement.Afficher_Etudiant_INFORMATION(Integer.parseInt(_ID_Binome));
			if (_list_Binome.isEmpty()==false) 
			{
				MyDialog_EtudiantInformation _MyDialog_EtudiantInformation1=new MyDialog_EtudiantInformation(_Context, _list_Binome);
				_MyDialog_EtudiantInformation1.DialogEudiantInformation();
			}
			break;

		case R.id.ActivityChefDepartement_strocture_DemandeEncadrement_Child_Button_EncadrementAdd:
			HashMap<String, String> _list_DemandeStage0=(HashMap<String, String>) v.getTag(R.id._HashMap);
			Accepter_DemandeEncadrement(_list_DemandeStage0);
			break;

		case R.id.ActivityChefDepartement_strocture_DemandeEncadrement_Child_Button_EncadrementRemove:
			HashMap<String, String> _list_DemandeStage1=(HashMap<String, String>) v.getTag(R.id._HashMap);
			Refuser_DemandeEncadrement(_list_DemandeStage1);
			break;
		default:
			break;
		}
	}
	public void Refuser_DemandeEncadrement(final HashMap<String, String> _list_DemandeStage)
	{
		String _confirmation_Demande=_list_DemandeStage.get("confirmation_Demande");
		if (_confirmation_Demande.equals("1"))
		{
			AlertDialog.Builder _Builder = new AlertDialog.Builder(_Context);
			_Builder.setTitle("Refuser Encadreur Académique");
			_Builder.setMessage("Êtes-vous sûr de vouloir refuser cette demande d'encadrement ?");
			_Builder.setNegativeButton("Annuler", null);
			_Builder.setPositiveButton("Confirmer", new DialogInterface.OnClickListener()
			{
				public void onClick(DialogInterface arg0, int arg1) 
				{
					String _URL0=_Context.getResources().getString(R.string.ChefDepartementActivity_update_Demande);

					String _ID_etudiant_Demande=_list_DemandeStage.get("Id_etudiant_Demande");
					String _ID_enseignant_Demande=_list_DemandeStage.get("Id_enseignant_Demande");
					boolean x=_MySQliteFonction_ChefDepartement.UpdateInTo_Serveur_Demande("0", _ID_etudiant_Demande, _ID_enseignant_Demande, _URL0);
					if (x) 
					{
						Refresh_Activity();
					}
				}
			});
			_Builder.show();

		}
	}

	public void Accepter_DemandeEncadrement(final HashMap<String, String> _list_DemandeStage)
	{
		String _confirmation_Demande=_list_DemandeStage.get("confirmation_Demande");
		if (_confirmation_Demande.equals("1"))
		{
			AlertDialog.Builder _Builder = new AlertDialog.Builder(_Context);
			_Builder.setTitle("Ajouter Encadreur Académique");
			_Builder.setMessage("Êtes-vous sûr de vouloir accepter cette demande d'encadrement ?");
			_Builder.setNegativeButton("Annuler", null);
			_Builder.setPositiveButton("Confirmer", new DialogInterface.OnClickListener()
			{
				public void onClick(DialogInterface arg0, int arg1) 
				{
					String _ID_etudiant_Demande=_list_DemandeStage.get("Id_etudiant_Demande");
					String _ID_enseignant_Demande=_list_DemandeStage.get("Id_enseignant_Demande");
					String _ID_travailPfe_Stage=_list_DemandeStage.get("_Id_travailPfe_Stage");

					String _URL0=_Context.getResources().getString(R.string.ChefDepartementActivity_update_Demande);
					String _URL1=_Context.getResources().getString(R.string.ChefDepartementActivity_updatePlus_TravailPfe_ACADEMIQUE);

					boolean x=_MySQliteFonction_ChefDepartement.UpdateInTo_Serveur_Demande("2", _ID_etudiant_Demande, _ID_enseignant_Demande, _URL0);
					boolean y=_MySQliteFonction_ChefDepartement.UpdateInTo_Serveur_TravailPfe_ACADEMIQUE(_ID_enseignant_Demande, _ID_travailPfe_Stage, _URL1);
					if (x && y) 
					{
						Refresh_Activity();
					}
				}
			});
			_Builder.show();

		}else if(_confirmation_Demande.equals("2"))
		{
			AlertDialog.Builder _Builder0=new AlertDialog.Builder(_Context);
			_Builder0.setTitle("Suppression");
			_Builder0.setMessage("Vous avez accepté cette demande d'encadrement" +
					" , Voulez-vous la supprimer ? , Opération irréversible ! ");
			_Builder0.setNegativeButton("Annuler", null);
			_Builder0.setPositiveButton("Confirmer", new DialogInterface.OnClickListener()
			{
				public void onClick(DialogInterface arg0, int arg1) 
				{
					String _ID_etudiant_Demande=_list_DemandeStage.get("Id_etudiant_Demande");
					String _ID_enseignant_Demande=_list_DemandeStage.get("Id_enseignant_Demande");
					String _ID_travailPfe_Stage=_list_DemandeStage.get("_Id_travailPfe_Stage");

					String _URL0=_Context.getResources().getString(R.string.ChefDepartementActivity_update_Demande);
					String _URL1=_Context.getResources().getString(R.string.ChefDepartementActivity_updateMoin_TravailPfe_ACADEMIQUE);

					boolean y=_MySQliteFonction_ChefDepartement.UpdateInTo_Serveur_TravailPfe_ACADEMIQUE("null", _ID_travailPfe_Stage, _URL1);
					boolean x=_MySQliteFonction_ChefDepartement.UpdateInTo_Serveur_Demande("0", _ID_etudiant_Demande, _ID_enseignant_Demande, _URL0);
					if (y && x) 
					{
						Refresh_Activity();
					}	
				}
			});
			_Builder0.show();
		}

	}
	public void Verification_Etudiant(String _ID_Etudiant,Button _Button_Binome)
	{
		ArrayList<HashMap<String, String>> _list_etudiant=_MySQliteFonction_ChefDepartement.Afficher_Etudiant_INFORMATION(Integer.parseInt(_ID_Etudiant));
		String _ID_Binome=_list_etudiant.get(0).get("Id_etudiant1_Etudiant");
		if (_ID_Binome==null)
		{
			_Button_Binome.setVisibility(View.GONE);
		}else
		{
			_Button_Binome.setVisibility(View.VISIBLE);
		}
	}
	////////////////////////
	public int getChildrenCount(int groupPosition)
	{
		String _ID_Enseignant=_List_EnseignantDepartement.get(groupPosition).get("ID_Enseignant");
		ArrayList<HashMap<String, String>> _list_DemandeEtudiantTravailStageEntreprise=new ArrayList<HashMap<String,String>>();
		_list_DemandeEtudiantTravailStageEntreprise=_MySQliteFonction_ChefDepartement.Afficher_DemandeEtudiantTravailStageEntreprise_DEMANDeENCADREMENT(_ID_Enseignant);
		if (!_list_DemandeEtudiantTravailStageEntreprise.isEmpty())
		{
			return _list_DemandeEtudiantTravailStageEntreprise.size();
		}else
		{
			return 0;
		}
	}

	public HashMap<String, String> getGroup(int groupPosition)
	{
		return _List_EnseignantDepartement.get(groupPosition);
	}

	public int getGroupCount() 
	{
		return _List_EnseignantDepartement.size();
	}
	public long getGroupId(int groupPosition) 
	{
		return groupPosition;
	}

	public View getGroupView(int groupPosition, boolean isLastChild, View contentView, ViewGroup parent)
	{
		HashMap<String, String> _list_offreStage=getGroup(groupPosition);
		if (contentView==null)
		{
			LayoutInflater layoutInflater=(LayoutInflater) _Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			contentView=layoutInflater.inflate(R.layout.activity_chefdepartement_strocture_demandeencadrement_parent, null);
		}
		final TextView _TextView_Enseignant=(TextView) contentView.findViewById(R.id.ActivityChefDepartement_strocture_DemandeEncadrement_Parent_TextView_nom);
		ImageView _ImageView=(ImageView) contentView.findViewById(R.id.ActivityChefDepartement_strocture_DemandeEncadrement_Parent_ImageView);

		String _nom_Enseignant=_list_offreStage.get("nom_Enseignant");
		String _prenom_Enseignant=_list_offreStage.get("prenom_Enseignant");
		_TextView_Enseignant.setText(_prenom_Enseignant+" "+_nom_Enseignant);

		if (isLastChild) 
		{ //séléctionnné
			_TextView_Enseignant.setTextColor(_Context.getResources().getColor(R.color.orange));
			_ImageView.setImageDrawable(_Context.getResources().getDrawable(R.drawable.boitereception_ouvert));
		}else 
		{ //non séléctionné
			_TextView_Enseignant.setTextColor(_Context.getResources().getColor(android.R.color.secondary_text_light));
			_ImageView.setImageDrawable(_Context.getResources().getDrawable(R.drawable.boitereception_fermer));
		}

		_TextView_Enseignant.setTag(R.id.position,groupPosition);
		_TextView_Enseignant.setOnClickListener(new OnClickListener()
		{
			public void onClick(View arg0) 
			{
				int groupPosition=(Integer) _TextView_Enseignant.getTag(R.id.position);
				(((Activity_ChefDepartement) _Context)).expandGroup(Activity_ChefDepartement._ExpandableListView_Stage_DemandeEncadrement,groupPosition);
			}
		});

		return contentView;
	}




	public boolean hasStableIds()
	{
		return false;
	}
	public boolean isChildSelectable(int arg0, int arg1) 
	{
		return false;
	}

	public void Refresh_Activity()
	{
		((Activity) _Context).finish();
		Intent _Intent_To_ActivityChefDepartement=new Intent(_Context,Activity_ChefDepartement.class);
		_Context.startActivity(_Intent_To_ActivityChefDepartement);
	}

}
