package package_MyAdapter_ChefDepartement;

import java.util.ArrayList;
import java.util.HashMap;

import package_Foction_DataBase.MySQliteFonction_ChefDepartement;
import package_MemoirStage.Activity_ChefDepartement;
import package_MyAdapter_Etudiant.MyDialog_EtudiantInformation;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.projet_gestionstage_test.R;

public class MyExpandableListAdapter_ChefDepartement_AffectationEncadreur extends BaseExpandableListAdapter implements OnClickListener 
{
	MySQliteFonction_ChefDepartement _MySQliteFonction_ChefDepartement;

	private Context _Context;
	private ArrayList<HashMap<String, String>> _List_EnseignantDepartement=new ArrayList<HashMap<String,String>>();
	private ArrayList<HashMap<String, String>> _List_TravailStageEntreprise = new ArrayList<HashMap<String,String>>();

	public MyExpandableListAdapter_ChefDepartement_AffectationEncadreur(Context _Context,ArrayList<HashMap<String, String>> _List_EnseignantDepartement ,ArrayList<HashMap<String, String>> _List_TravailStageEntreprise )
	{
		_MySQliteFonction_ChefDepartement=new MySQliteFonction_ChefDepartement(_Context);

		this._Context=_Context;
		this._List_EnseignantDepartement=_List_EnseignantDepartement;
		this._List_TravailStageEntreprise=_List_TravailStageEntreprise;
	}

	public HashMap<String, String> getChild(int groupPosition, int childPosition) 
	{
		String _ID_Enseignant=_List_EnseignantDepartement.get(groupPosition).get("ID_Enseignant");
		ArrayList<HashMap<String, String>> _list_TravailStageEntreprise=new ArrayList<HashMap<String,String>>();
		for (int i = 0; i < _List_TravailStageEntreprise.size(); i++) 
		{
			if (_List_TravailStageEntreprise.get(i).get("_Id_enseignant_TravailPfe")!=null)
			{
				if (_List_TravailStageEntreprise.get(i).get("_Id_enseignant_TravailPfe").equals(_ID_Enseignant))
				{
					_list_TravailStageEntreprise.add(_List_TravailStageEntreprise.get(i));
				}
			}
		}
		return _list_TravailStageEntreprise.get(childPosition);
	}

	public long getChildId(int groupPosition, int childPosition)
	{
		return childPosition;
	}
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View contentView,ViewGroup parent)
	{
		HashMap<String, String> _list_EtudiantTravailStageEntreprise=getChild(groupPosition, childPosition);	
		if (contentView==null)
		{
			LayoutInflater layoutInflater=(LayoutInflater) _Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			contentView=layoutInflater.inflate(R.layout.activity_chefdepartement_strocture_affectationencadrement_child, null);
		}
		TextView _TextView_NomStage=(TextView) contentView.findViewById(R.id.ActivityChefDepartement_strocture_AffectationEncadrement_Child_TextView_NomStage);
		Button _Button_Entreprise=(Button) contentView.findViewById(R.id.ActivityChefDepartement_strocture_AffectationEncadrement_Child_Button_Entreprise);
		Button _Button_Description=(Button) contentView.findViewById(R.id.ActivityChefDepartement_strocture_AffectationEncadrement_Child_Button_Description);
		Button _Button_Etudiant1=(Button) contentView.findViewById(R.id.ActivityChefDepartement_strocture_AffectationEncadrement_Child_Button_Etudiant1);
		Button _Button_Etudiant2=(Button) contentView.findViewById(R.id.ActivityChefDepartement_strocture_AffectationEncadrement_Child_Button_Etudiant2);
		Button _Button_AffectationDelete=(Button) contentView.findViewById(R.id.ActivityChefDepartement_strocture_AffectationEncadrement_Child_Button_AffectationDelete);

		_TextView_NomStage.setText(_list_EtudiantTravailStageEntreprise.get("_nom_Stage"));

		_Button_Entreprise.setTag(R.id._HashMap,_list_EtudiantTravailStageEntreprise);
		_Button_Entreprise.setOnClickListener(this);

		_Button_Description.setTag(R.id._HashMap,_list_EtudiantTravailStageEntreprise);
		_Button_Description.setOnClickListener(this);

		_Button_Etudiant1.setTag(R.id.ID_travailPfe,_list_EtudiantTravailStageEntreprise.get("_Id_travailPfe_Stage"));
		_Button_Etudiant1.setOnClickListener(this);

		_Button_Etudiant2.setTag(R.id.ID_travailPfe,_list_EtudiantTravailStageEntreprise.get("_Id_travailPfe_Stage"));
		_Button_Etudiant2.setOnClickListener(this);

		_Button_AffectationDelete.setTag(R.id.position,groupPosition);
		_Button_AffectationDelete.setTag(R.id.ID_travailPfe,_list_EtudiantTravailStageEntreprise.get("_Id_travailPfe_Stage"));
		_Button_AffectationDelete.setOnClickListener(this);

		Verification_Etudiant(_list_EtudiantTravailStageEntreprise.get("_Id_travailPfe_Stage"), _Button_Etudiant2);
		return contentView;
	}



	@SuppressWarnings("unchecked")
	public void onClick(View v) 
	{
		switch (v.getId())
		{
		case R.id.ActivityChefDepartement_strocture_AffectationEncadrement_Child_Button_AffectationDelete:
			int groupPosition=(Integer) v.getTag(R.id.position);
			final String _ID_TravialPfe= (String) v.getTag(R.id.ID_travailPfe);
			final String _ID_enseignant=getGroup(groupPosition).get("ID_Enseignant");
			final String _ID_Etudiantt=_MySQliteFonction_ChefDepartement.Afficher_Etudiant_AFFECTATIOnENCADREUR(_ID_TravialPfe).get(0).get("ID_Etudiant");
			AlertDialog.Builder _Builder0=new AlertDialog.Builder(_Context);
			_Builder0.setTitle("Annulation de l'affectation Encadreur Stage");
			_Builder0.setMessage(" Voulez-vous annuler l'affectation entre cette encadreur académique et ce stage  ? ");
			_Builder0.setNegativeButton("Annuler", null);
			_Builder0.setPositiveButton("Confirmer", new DialogInterface.OnClickListener()
			{
				public void onClick(DialogInterface arg0, int arg1) 
				{
					String _URL0=_Context.getResources().getString(R.string.ChefDepartementActivity_update_Demande);
					String _URL1=_Context.getResources().getString(R.string.ChefDepartementActivity_updateMoin_TravailPfe_ACADEMIQUE);

					boolean y=_MySQliteFonction_ChefDepartement.UpdateInTo_Serveur_TravailPfe_ACADEMIQUE("null", _ID_TravialPfe, _URL1);
					boolean x=_MySQliteFonction_ChefDepartement.UpdateInTo_Serveur_Demande("0", _ID_Etudiantt, _ID_enseignant, _URL0);
					if (y && x) 
					{
						Refresh_Activity();
					}	
				}
			});
			_Builder0.show();
			break;

		case R.id.ActivityChefDepartement_strocture_AffectationEncadrement_Child_Button_Entreprise:
			HashMap<String, String> _list_Entrepirse=(HashMap<String, String>) v.getTag(R.id._HashMap);
			String _entreprise_Libelle=_list_Entrepirse.get("libelle_Entreprise");
			String _entreprise_Raison=_list_Entrepirse.get("raison_Entreprise");
			String _entreprise_Email=_list_Entrepirse.get("email_Entreprise");
			String _entreprise_SiteWeb=_list_Entrepirse.get("siteWeb_Entreprise");
			AlertDialog.Builder box0 = new AlertDialog.Builder(_Context);
			box0.setTitle("Entreprise");
			String SMS0="<br />"+_entreprise_Libelle+"<br />"+_entreprise_Raison+"<br />"+_entreprise_Email+"<br />"+_entreprise_SiteWeb;
			box0.setMessage(Html.fromHtml("<font color=\"#000000 \">" + SMS0 + "</font>",null, null));
			AlertDialog dialog0 = box0.show();
			TextView messageText0 = (TextView)dialog0.findViewById(android.R.id.message);
			messageText0.setGravity(Gravity.CENTER);
			messageText0.setTextSize(14);
			dialog0.show();
			break;

		case R.id.ActivityChefDepartement_strocture_AffectationEncadrement_Child_Button_Description:
			HashMap<String, String> _list_Stage=(HashMap<String, String>) v.getTag(R.id._HashMap);
			AlertDialog.Builder box1 = new AlertDialog.Builder(_Context);
			box1.setTitle("Description Stage");
			String SMS1="<br />"+_list_Stage.get("_descriptiona_Stage") +"<br />";
			box1.setMessage(Html.fromHtml("<font color=\"#000000 \">" + SMS1 + "</font>",null, null));
			AlertDialog dialog1 = box1.show();
			TextView messageText1 = (TextView)dialog1.findViewById(android.R.id.message);
			messageText1.setGravity(Gravity.CENTER);
			messageText1.setTextSize(14);
			dialog1.show();
			break;

		case R.id.ActivityChefDepartement_strocture_AffectationEncadrement_Child_Button_Etudiant1:
			String _ID_TravialPfe1= (String) v.getTag(R.id.ID_travailPfe);
			ArrayList<HashMap<String, String>> _list_Etudiant=new ArrayList<HashMap<String,String>>();
			String _ID_Etudiant=_MySQliteFonction_ChefDepartement.Afficher_Etudiant_AFFECTATIOnENCADREUR(_ID_TravialPfe1).get(0).get("ID_Etudiant");

			_list_Etudiant=_MySQliteFonction_ChefDepartement.Afficher_Etudiant_INFORMATION(Integer.parseInt(_ID_Etudiant));
			if (_list_Etudiant.isEmpty()==false) 
			{
				MyDialog_EtudiantInformation _MyDialog_EtudiantInformation1=new MyDialog_EtudiantInformation(_Context, _list_Etudiant);
				_MyDialog_EtudiantInformation1.DialogEudiantInformation();
			}	
			break;

		case R.id.ActivityChefDepartement_strocture_AffectationEncadrement_Child_Button_Etudiant2:
			String _ID_TravailPfe2= (String) v.getTag(R.id.ID_travailPfe);
			ArrayList<HashMap<String, String>> _list_Binome=new ArrayList<HashMap<String,String>>();
			String _ID_Binome=_MySQliteFonction_ChefDepartement.Afficher_Etudiant_AFFECTATIOnENCADREUR(_ID_TravailPfe2).get(0).get("Id_etudiant1_Etudiant");

			_list_Binome=_MySQliteFonction_ChefDepartement.Afficher_Etudiant_INFORMATION(Integer.parseInt(_ID_Binome));
			if (_list_Binome.isEmpty()==false) 
			{
				MyDialog_EtudiantInformation _MyDialog_EtudiantInformation1=new MyDialog_EtudiantInformation(_Context, _list_Binome);
				_MyDialog_EtudiantInformation1.DialogEudiantInformation();
			}
			break;

		default:
			break;
		}
	}

	public void Verification_Etudiant(String _ID_TravailPfe,Button _Button_Binome)
	{
		String _ID_Binome=_MySQliteFonction_ChefDepartement.Afficher_Etudiant_AFFECTATIOnENCADREUR(_ID_TravailPfe).get(0).get("Id_etudiant1_Etudiant");
		if (_ID_Binome==null)
		{
			_Button_Binome.setVisibility(View.GONE);
		}else
		{
			_Button_Binome.setVisibility(View.VISIBLE);
		}
	}

	////////////////////////
	public int getChildrenCount(int groupPosition)
	{
		String _ID_Enseignant=_List_EnseignantDepartement.get(groupPosition).get("ID_Enseignant");
		ArrayList<HashMap<String, String>> _list_TravailStageEntreprise=new ArrayList<HashMap<String,String>>();
		for (int i = 0; i < _List_TravailStageEntreprise.size(); i++) 
		{
			if (_List_TravailStageEntreprise.get(i).get("_Id_enseignant_TravailPfe")!=null)
			{
				if (_List_TravailStageEntreprise.get(i).get("_Id_enseignant_TravailPfe").equals(_ID_Enseignant))
				{
					_list_TravailStageEntreprise.add(_List_TravailStageEntreprise.get(i));
				}
			}
		}
		return _list_TravailStageEntreprise.size();

	}

	public HashMap<String, String> getGroup(int groupPosition)
	{
		return _List_EnseignantDepartement.get(groupPosition);
	}

	public int getGroupCount() 
	{
		return _List_EnseignantDepartement.size();
	}
	public long getGroupId(int groupPosition) 
	{
		return groupPosition;
	}

	public View getGroupView(int groupPosition, boolean isLastChild, View contentView, ViewGroup parent)
	{
		HashMap<String, String> _list_offreStage=getGroup(groupPosition);
		if (contentView==null)
		{
			LayoutInflater layoutInflater=(LayoutInflater) _Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			contentView=layoutInflater.inflate(R.layout.activity_chefdepartement_strocture_affectationencadrement_parent, null);
		}
		final TextView _TextView_Enseignant=(TextView) contentView.findViewById(R.id.ActivityChefDepartement_strocture_AffectationEncadrement_Parent_TextView_nom);
		ImageView _ImageView=(ImageView) contentView.findViewById(R.id.ActivityChefDepartement_strocture_AffectationEncadrement_Parent_ImageView);
		Spinner _Spinner_Stage=(Spinner) contentView.findViewById(R.id.ActivityChefDepartement_strocture_AffectationEncadrement_Parent_Spinner);

		String _nom_Enseignant=_list_offreStage.get("nom_Enseignant");
		String _prenom_Enseignant=_list_offreStage.get("prenom_Enseignant");
		_TextView_Enseignant.setText(_prenom_Enseignant+" "+_nom_Enseignant);

		MyAdapter_Spinner _MyAdapter_Spinner=new MyAdapter_Spinner(_Context, _List_TravailStageEntreprise,getGroup(groupPosition).get("ID_Enseignant"));
		_Spinner_Stage.setAdapter(_MyAdapter_Spinner);

		if (isLastChild) 
		{ //séléctionnné
			_TextView_Enseignant.setTextColor(_Context.getResources().getColor(R.color.orange)); 
			_ImageView.setImageDrawable(_Context.getResources().getDrawable(R.drawable.encadreur_acad_ouvert));
		}else 
		{ //non séléctionné
			_TextView_Enseignant.setTextColor(_Context.getResources().getColor(android.R.color.secondary_text_light));
			_ImageView.setImageDrawable(_Context.getResources().getDrawable(R.drawable.encadreur_acad_fermer));
		}

		_TextView_Enseignant.setTag(R.id.position,groupPosition);
		_TextView_Enseignant.setOnClickListener(new OnClickListener()
		{
			public void onClick(View arg0) 
			{
				int groupPosition=(Integer) _TextView_Enseignant.getTag(R.id.position);
				(((Activity_ChefDepartement) _Context)).expandGroup(Activity_ChefDepartement._ExpandableListView_AffectationEncadreur,groupPosition);
			}
		});

		return contentView;
	}




	public boolean hasStableIds()
	{
		return false;
	}
	public boolean isChildSelectable(int arg0, int arg1) 
	{
		return false;
	}

	public void Refresh_Activity()
	{
		((Activity) _Context).finish();
		Intent _Intent_To_ActivityChefDepartement=new Intent(_Context,Activity_ChefDepartement.class);
		_Context.startActivity(_Intent_To_ActivityChefDepartement);
	}

}
