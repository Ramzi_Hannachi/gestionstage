package package_MyAdapter_Entreprise;



import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import package_Foction_DataBase.MySQliteFonction_ChefDepartement;
import package_Foction_DataBase.MySQliteFonction_Entreprise;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.projet_gestionstage_test.R;

public class MyDialog_Entreprise_DepotOffreStage_InsertUdate implements android.view.View.OnClickListener 
{
	String _ID_Entreprise;
	/////// update offreStage !!!
	HashMap<String, String> _list_offreStage_selection=new HashMap<String, String>();// offreStage du champs a modifier
	/////// Insert offreStage !!
	MySQliteFonction_Entreprise _MySQliteFonction_Entreprise;
	ArrayList<HashMap<String, String>> _ArrayList_filiere;
	TextView _TextView_Nom;
	TextView _TextView_Description;
	String _ID_filiere;

	EditText _EditText_Nom;
	EditText _EditText_Description;
	DatePicker	_DatePicker_Date;
	Spinner _Spinner_Filiere;
	Button _Button_Confirmer;
	Button _Button_Annuler;

	private Context _Context;

	public MyDialog_Entreprise_DepotOffreStage_InsertUdate(Context _Context,HashMap<String, String> _list_offreStage_selection,String _ID_Entreprise)
	{
		this._ID_Entreprise=_ID_Entreprise;
		this._Context=_Context;
		/// update offreStage 
		this._list_offreStage_selection=_list_offreStage_selection;
	}

	public void DialogEntrepirse_DepostageInsert()
	{
		Dialog _Dialog = new Dialog(_Context);
		_Dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		_Dialog.setContentView(R.layout.activity_entreprise_dialog_depotoffrestage);

		_TextView_Nom=(TextView) _Dialog.findViewById(R.id.Activity_Entreprise_Dialog_DepotOffreStage_TextView_nom);
		_TextView_Description=(TextView) _Dialog.findViewById(R.id.Activity_Entreprise_Dialog_DepotOffreStage_TextView_description);

		_EditText_Nom=(EditText) _Dialog.findViewById(R.id.Activity_Entreprise_Dialog_DepotOffreStage_EditText_nom);
		_EditText_Description=(EditText) _Dialog.findViewById(R.id.Activity_Entreprise_Dialog_DepotOffreStage_EditText_description);
		_DatePicker_Date=(DatePicker) _Dialog.findViewById(R.id.Activity_Entreprise_Dialog_DepotOffreStage_EditText_date);
		_Spinner_Filiere=(Spinner) _Dialog.findViewById(R.id.Activity_Entreprise_Dialog_DepotOffreStage_Spinner_filiere);

		_Button_Confirmer=(Button) _Dialog.findViewById(R.id.Activity_Entreprise_Dialog_DepotOffreStage_Button_confirmer);
		_Button_Annuler=(Button) _Dialog.findViewById(R.id.Activity_Entreprise_Dialog_DepotOffreStage_Button_annuler);
		Spinner_Filiere();

		_Button_Confirmer.setOnClickListener(this);
		_Button_Annuler.setOnClickListener(this);
		_Dialog.show();
		if (_list_offreStage_selection!=null)
		{ /// remplir les champs pour la modification !! 
			String nom_offreStage=_list_offreStage_selection.get("nom_OffreStage");
			String description_offreStage= _list_offreStage_selection.get("description_OffreStage");
			String Id_filiere=_list_offreStage_selection.get("cle_filiere_OffreStage");

			_EditText_Nom.setText(nom_offreStage);
			_EditText_Description.setText(description_offreStage);	
			//_Spinner_Filiere.setSelection(Integer.parseInt(Id_filiere)-1); 
			//  l'utilisateur voit la date actuelle d'aujourd'hui
		}

		_EditText_Nom.addTextChangedListener(new TextWatcher(){
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {}
			public void afterTextChanged(Editable s)
			{
				Verifier_Nom(_EditText_Nom.getText().toString());
			}
		});
		_EditText_Description.addTextChangedListener(new TextWatcher() {
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,int arg3) {}
			public void afterTextChanged(Editable arg0)
			{
				Verifier_Description(_EditText_Description.getText().toString());	
			}
		});
	}

	public void onClick(View v)
	{
		switch (v.getId()) 
		{
		case R.id.Activity_Entreprise_Dialog_DepotOffreStage_Button_confirmer:
			InsertUpdate_OffreStage();
			break;
		case R.id.Activity_Entreprise_Dialog_DepotOffreStage_Button_annuler:
			Viderchamps();
			break;
		default:
			break;
		}
	}

	public boolean Verifier_Description(String description)
	{
		if (description.equals(""))
		{
			_TextView_Description.setTextColor(_Context.getResources().getColor(R.color.red));
			return false;
		}

		Pattern p = Pattern.compile("^[a-zA-Z_0-9_ _à_À_â_Â_é_è_É_È_ê_Ê_ô_Ô_ù_Ù_Ç_ç_-_,_;_._:_(_)_`]{4,225}");//^[a-zAn-Z_0-9]{4,20}"
		Matcher d = p.matcher(description);
		if ( d.matches())
		{
			_TextView_Description.setTextColor(_Context.getResources().getColor(android.R.color.black));
			return true;
		}
		_TextView_Description.setTextColor(_Context.getResources().getColor(R.color.red));
		return false;
	}

	public boolean Verifier_Nom(String nom)
	{
		if (nom.equals(""))
		{
			_TextView_Nom.setTextColor(_Context.getResources().getColor(R.color.red));
			return false;
		}

		Pattern p = Pattern.compile("^[a-zA-Z_0-9_ _à_À_â_Â_é_è_É_È_ê_Ê_ô_Ô_ù_Ù_Ç_ç_-_,_;_._:_(_)_`]{4,225}");//^[a-zAn-Z_0-9]{4,20}"
		Matcher n = p.matcher(nom);
		if ( n.matches())
		{
			_TextView_Nom.setTextColor(_Context.getResources().getColor(android.R.color.black));
			return true;
		}
		_TextView_Nom.setTextColor(_Context.getResources().getColor(R.color.red));
		return false;
	}

	public void Spinner_Filiere()
	{
		_MySQliteFonction_Entreprise=new MySQliteFonction_Entreprise(_Context);
		_ArrayList_filiere=new ArrayList<HashMap<String,String>>();
		_ArrayList_filiere=_MySQliteFonction_Entreprise.Afficher_Filiere();
		ArrayList<String> _ListFiliere=new ArrayList<String>();
		for (int i = 0; i < _ArrayList_filiere.size(); i++)
		{
			_ListFiliere.add(_ArrayList_filiere.get(i).get("designation_Filiere"));
		}
		ArrayAdapter<String> _Adapter = new ArrayAdapter<String>(_Context, R.layout.activity_entreprise_strocture_textview_filiere, _ListFiliere);
		_Adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		_Spinner_Filiere.setAdapter(_Adapter);
		_Spinner_Filiere.setOnItemSelectedListener(new OnItemSelectedListener()
		{
			public void onItemSelected(AdapterView<?> arg0, View arg1,int arg2, long arg3)
			{
				String _spinnerText=_Spinner_Filiere.getSelectedItem().toString();
				for (int i = 0; i < _ArrayList_filiere.size(); i++) 
				{
					if (_ArrayList_filiere.get(i).get("designation_Filiere")==_spinnerText)
					{
						_ID_filiere=_ArrayList_filiere.get(i).get("ID_Filiere");
					}
				}

			}
			public void onNothingSelected(AdapterView<?> arg0) {}
		});
	}

	public void Viderchamps()
	{
		_EditText_Description.setText("");
		_EditText_Nom.setText("");
	}



	public void InsertUpdate_OffreStage()
	{   
		String _description=_EditText_Description.getText().toString();
		String _nom=_EditText_Nom.getText().toString();
		boolean x=Verifier_Description(_description);
		boolean y=Verifier_Nom(_nom);
		if ( x && y )
		{
			AlertDialog.Builder _Builder=new AlertDialog.Builder(_Context);
			if (_list_offreStage_selection==null)
			{
				_Builder.setTitle("Êtes-vous sûr de vouloir Ajouter cette offre stage");
			}else
			{
				_Builder.setTitle("Êtes-vous sûr de vouloir Modifier cette offre stage");
			}
			_Builder.setNegativeButton("Annuler", null);
			_Builder.setPositiveButton("Confirmer", new OnClickListener()
			{
				public void onClick(DialogInterface dialog, int which)
				{
					String jour=String.valueOf(_DatePicker_Date.getDayOfMonth());
					String mois=String.valueOf(_DatePicker_Date.getMonth());
					String annee=String.valueOf(_DatePicker_Date.getYear());
					String _date=jour+"/"+mois+"/"+annee;
					String _nom=_EditText_Nom.getText().toString();
					String _description=_EditText_Description.getText().toString();
					int _cle_Entreprise=Integer.parseInt(_ID_Entreprise);
					int _cle_Filiere=Integer.parseInt(_ID_filiere);
					boolean _Retour=false;
					if (_list_offreStage_selection==null)
					{// Insert OffreStage !!!
						String _URL=_Context.getResources().getString(R.string.EntrepriseActivity_insert_OffreStage);
						_Retour=_MySQliteFonction_Entreprise.InsertInTo_Serveur_OffreStage(_nom, _description, _date, _cle_Entreprise, _cle_Filiere, _URL);
					}else
					{// Update OffreStage !!!
						int _ID_offrestage=Integer.parseInt(_list_offreStage_selection.get("ID_OffreStage"));
						String _URL=_Context.getResources().getString(R.string.EntrepriseActivity_update_OffreStage);
						_MySQliteFonction_Entreprise=new MySQliteFonction_Entreprise(_Context);
						_Retour=_MySQliteFonction_Entreprise.UpdateInTo_Serveur_OffreStage(_ID_offrestage, _nom, _description, _date, _cle_Filiere, _URL);

						String _ID_Stage=_list_offreStage_selection.get("cle_stage_OffreStage");
						String _ID_travailPfe_Stage=_list_offreStage_selection.get("cle_travailPfe_OffreStage");

						if (_Retour && _ID_Stage!=null && _ID_travailPfe_Stage!=null)
						{ // update travailPfe , stage 
							String _nom_Stage=_nom;
							String _date_Stage=_list_offreStage_selection.get("date_OffreStage"); 
							String _description_Stage=_description;
							String _URL1=_Context.getResources().getString(R.string.ChefDepartementActivity_update_TravailPfeStageEtudiant);
							MySQliteFonction_ChefDepartement _MySQliteFonction_ChefDepartement=new MySQliteFonction_ChefDepartement(_Context);
							_Retour=_MySQliteFonction_ChefDepartement.UpdateInTo_Serveur_TravailPfeStageEtudiant(_ID_Stage, _nom_Stage, _date_Stage, _description_Stage, null, null, null, null, _ID_travailPfe_Stage, _URL1);
						}
					}
					if (_Retour)
					{
						((Activity) _Context).finish();
						Intent lIntent_To_Activity = new Intent(_Context, ((Activity)_Context).getClass());
						_Context.startActivity(lIntent_To_Activity);
					}
				}
			});
			_Builder.show();
		}
	}
}
