package package_MyAdapter_Entreprise;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import package_Foction_DataBase.MySQliteFonction_ChefDepartement;
import package_Foction_DataBase.MySQliteFonction_Entreprise;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.projet_gestionstage_test.R;

public class MyDialog_Entreprise_InscriptionInformation implements android.view.View.OnClickListener
{
	private String _ID_Entreprise;
	private MySQliteFonction_Entreprise _MySQliteFonction_Entreprise;

	TextView _TextView_libelle;
	TextView _TextView_raison;
	TextView _TextView_numTel;
	TextView _TextView_email;
	TextView _TextView_siteWeb;
	TextView _TextView_login;
	TextView _TextView_password;
	RadioButton _RadioButton_http;
	RadioButton _RadioButton_https;

	EditText _EditText_libelle;
	EditText _EditText_raison;
	EditText _EditText_numTel;
	EditText _EditText_email;
	EditText _EditText_siteWeb;
	EditText _EditText_login;
	EditText _EditText_password;

	Button _Button_confirmer;
	Button _Button_annuler;

	String libelle_EditText;
	String raison_EditText;
	String numTel_EditText;
	String email_EditText;
	String login_EditText;
	String password_EditText;
	String siteWeb_EditText;
	String logo_EditText;

	private  Context _Context;
	private ArrayList<HashMap<String, String>> _ArrayList_EntrepriseInformation = new ArrayList<HashMap<String,String>>();
	private String _Id_enseignant_Entreprise;
	public MyDialog_Entreprise_InscriptionInformation(Context _Context,ArrayList<HashMap<String, String>> _ArrayList_EntrepriseInformation,String _Id_enseignant_Entreprise)
	{
		this._Context=_Context;
		this._ArrayList_EntrepriseInformation=_ArrayList_EntrepriseInformation;
		this._Id_enseignant_Entreprise=_Id_enseignant_Entreprise;
	}

	public void DialogEntrepriseInscription()
	{
		_MySQliteFonction_Entreprise=new MySQliteFonction_Entreprise(_Context);

		Dialog _Dialog = new Dialog(_Context);
		_Dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		_Dialog.setContentView(R.layout.activity_entreprise_dialog_inscriptioninformation);

		_TextView_libelle=(TextView) _Dialog.findViewById(R.id.Activity_Entreprise_Dialog_InscriptionInformation_TextView_libelle);
		_TextView_raison=(TextView) _Dialog.findViewById(R.id.Activity_Entreprise_Dialog_InscriptionInformation_TextView_raison);
		_TextView_numTel=(TextView) _Dialog.findViewById(R.id.Activity_Entreprise_Dialog_InscriptionInformation_TextView_numTel);
		_TextView_email=(TextView) _Dialog.findViewById(R.id.Activity_Entreprise_Dialog_InscriptionInformation_TextView_email);
		_TextView_siteWeb=(TextView) _Dialog.findViewById(R.id.Activity_Entreprise_Dialog_InscriptionInformation_TextView_siteWeb);
		_TextView_login=(TextView) _Dialog.findViewById(R.id.Activity_Entreprise_Dialog_InscriptionInformation_TextView_Login);
		_TextView_password=(TextView) _Dialog.findViewById(R.id.Activity_Entreprise_Dialog_InscriptionInformation_TextView_password);
		_RadioButton_http=(RadioButton) _Dialog.findViewById(R.id.Activity_Entreprise_Dialog_InscriptionInformation_RadioButton_http);
		_RadioButton_https=(RadioButton) _Dialog.findViewById(R.id.Activity_Entreprise_Dialog_InscriptionInformation_RadioButton_https);

		_EditText_libelle=(EditText) _Dialog.findViewById(R.id.Activity_Entreprise_Dialog_InscriptionInformation_EditText_libelle);
		_EditText_raison=(EditText) _Dialog.findViewById(R.id.Activity_Entreprise_Dialog_InscriptionInformation_EditText_raison);
		_EditText_numTel=(EditText) _Dialog.findViewById(R.id.Activity_Entreprise_Dialog_InscriptionInformation_EditText_numTel);
		_EditText_email=(EditText) _Dialog.findViewById(R.id.Activity_Entreprise_Dialog_InscriptionInformation_EditText_email);
		_EditText_siteWeb=(EditText) _Dialog.findViewById(R.id.Activity_Entreprise_Dialog_InscriptionInformation_EditText_siteWeb);
		_EditText_login=(EditText) _Dialog.findViewById(R.id.Activity_Entreprise_Dialog_InscriptionInformation_EditText_Login);
		_EditText_password=(EditText) _Dialog.findViewById(R.id.Activity_Entreprise_Dialog_InscriptionInformation_EditText_password);

		if (_ArrayList_EntrepriseInformation.isEmpty()==false)
		{
			_ID_Entreprise=_ArrayList_EntrepriseInformation.get(0).get("ID_Entreprise");
			_EditText_libelle.setText(_ArrayList_EntrepriseInformation.get(0).get("libelle_Entreprise"));
			_EditText_raison.setText(_ArrayList_EntrepriseInformation.get(0).get("raison_Entreprise"));
			_EditText_numTel.setText(_ArrayList_EntrepriseInformation.get(0).get("numTel_Entreprise"));
			_EditText_email.setText(_ArrayList_EntrepriseInformation.get(0).get("email_Entreprise"));
			_EditText_siteWeb.setText(_ArrayList_EntrepriseInformation.get(0).get("siteWeb_Entreprise"));
			_EditText_login.setText(_ArrayList_EntrepriseInformation.get(0).get("login_Entreprise"));
			_EditText_password.setText(_ArrayList_EntrepriseInformation.get(0).get("password_Entreprise"));
			//_ArrayList_EntrepriseInformation.get(0).get("logo_Entreprise");
		}
		_Button_confirmer=(Button) _Dialog.findViewById(R.id.Activity_Entreprise_Dialog_InscriptionInformation_Button_confirmer);
		_Button_annuler=(Button) _Dialog.findViewById(R.id.Activity_Entreprise_Dialog_InscriptionInformation_Button_annuler);

		_Button_annuler.setOnClickListener(new View.OnClickListener() 
		{
			public void onClick(View arg0) 
			{
				_EditText_libelle.setText("");
				_EditText_raison.setText("");
				_EditText_numTel.setText("");
				_EditText_email.setText("");
				_EditText_siteWeb.setText("");
				_EditText_login.setText("");
				_EditText_password.setText("");
			}
		});
		_Button_confirmer.setOnClickListener(new View.OnClickListener()
		{
			public void onClick(View v) 
			{
				libelle_EditText=_EditText_libelle.getText().toString();
				raison_EditText=_EditText_raison.getText().toString();
				numTel_EditText=_EditText_numTel.getText().toString();
				email_EditText=_EditText_email.getText().toString();
				siteWeb_EditText=_EditText_siteWeb.getText().toString();
				login_EditText=_EditText_login.getText().toString();
				password_EditText=_EditText_password.getText().toString();

				VerifierEmail(email_EditText);
				VerifierLibelle(libelle_EditText);
				VerifierLogin(login_EditText);
				VerifierNumTelephone(numTel_EditText);
				VerifierPassword(password_EditText);
				VerifierRaison(raison_EditText);
				if (siteWeb_EditText.equals(""))
				{
					_TextView_siteWeb.setTextColor(_Context.getResources().getColor(R.color.red));
				}
				/// Libelle
				if (VerifierLibelle(libelle_EditText)==false)
				{
					Toast.makeText(_Context, "libellé invalide! ", Toast.LENGTH_LONG).show();
					return;
				}
				///// Raison Sociale
				if (VerifierRaison(raison_EditText)==false) 
				{
					Toast.makeText(_Context, "Raison Sociale invalide! ", Toast.LENGTH_LONG).show();
					return;
				}
				///// Num Tel
				if (VerifierNumTelephone(numTel_EditText)==false)
				{
					Toast.makeText(_Context, "Num Téléphone invalide! ", Toast.LENGTH_LONG).show();
					return;
				}
				///// Email
				if (VerifierEmail(email_EditText)==false) 
				{
					Toast.makeText(_Context, "Email invalide! ", Toast.LENGTH_LONG).show();
					return;
				}
				/////Site Web
				if (VerifierSiteWeb(siteWeb_EditText)==false)
				{
					Toast.makeText(_Context, "site web invalide! ", Toast.LENGTH_LONG).show();
					return;
				}
				///// Login
				if (VerifierLogin(login_EditText)==false) 
				{
					Toast.makeText(_Context, "Login invalide! ", Toast.LENGTH_LONG).show();
					return;
				}
				///// Password
				if (VerifierPassword(password_EditText)==false) 
				{
					Toast.makeText(_Context, "Password invalide! ", Toast.LENGTH_LONG).show();
					return;
				}
				////// Verifier si il sagit d'une Inscription(Insert) ou Update
				if (_ArrayList_EntrepriseInformation.isEmpty()) 
				{
					InscriptionEntreprise();
				}else
				{
					UpdateEntreprise();
				}
			}
		});

		_Dialog.show();

		//// EditText Libelle
		_EditText_libelle.addTextChangedListener(new TextWatcher()
		{
			public void onTextChanged(CharSequence s, int start, int before, int count){}
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {}
			public void afterTextChanged(Editable s)
			{
				libelle_EditText=_EditText_libelle.getText().toString();
				VerifierLibelle(libelle_EditText);
			}
		});
		//// EditText Raison
		_EditText_raison.addTextChangedListener(new TextWatcher() 
		{
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {}
			public void afterTextChanged(Editable s)
			{
				raison_EditText=_EditText_raison.getText().toString();
				VerifierRaison(raison_EditText);
			}
		});
		//// EditText numTel
		_EditText_numTel.addTextChangedListener(new TextWatcher()
		{
			public void onTextChanged(CharSequence s, int start, int before, int count){}
			public void beforeTextChanged(CharSequence s, int start, int count,int after){}
			public void afterTextChanged(Editable arg0)
			{
				numTel_EditText=_EditText_numTel.getText().toString();
				VerifierNumTelephone(numTel_EditText);
			}
		});
		//// EditText Email
		_EditText_email.addTextChangedListener(new TextWatcher() 
		{
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {}
			public void afterTextChanged(Editable s)
			{
				email_EditText=_EditText_email.getText().toString();
				VerifierEmail(email_EditText);
			}
		});
		//// EditText SiteWeb
		_EditText_siteWeb.addTextChangedListener(new TextWatcher() 
		{
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,int arg3) {}
			public void afterTextChanged(Editable arg0)
			{
				siteWeb_EditText=_EditText_siteWeb.getText().toString();
				if (siteWeb_EditText.equals(""))
				{
					_TextView_siteWeb.setTextColor(_Context.getResources().getColor(R.color.red));
				}else 
				{
					_TextView_siteWeb.setTextColor(_Context.getResources().getColor(android.R.color.black));
				}
			}
		});

		//// EditText Login
		_EditText_login.addTextChangedListener(new TextWatcher() 
		{
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {}
			public void afterTextChanged(Editable s)
			{
				login_EditText=_EditText_login.getText().toString();
				VerifierLogin(login_EditText);
			}
		});
		//// EditTExt Password
		_EditText_password.addTextChangedListener(new TextWatcher()
		{
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {}
			public void afterTextChanged(Editable s) 
			{
				password_EditText=_EditText_password.getText().toString();
				VerifierPassword(password_EditText);
			}
		});

		_TextView_libelle.setOnClickListener(this);
		_TextView_raison.setOnClickListener(this);
		_TextView_numTel.setOnClickListener(this);
		_TextView_email.setOnClickListener(this);
		_TextView_siteWeb.setOnClickListener(this);
		_TextView_login.setOnClickListener(this);
		_TextView_password.setOnClickListener(this);

	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId()) 
		{
		case R.id.Activity_Entreprise_Dialog_InscriptionInformation_TextView_libelle:
			Toast.makeText(_Context, "", Toast.LENGTH_LONG).show();
			break;
		case R.id.Activity_Entreprise_Dialog_InscriptionInformation_TextView_raison :
			Toast.makeText(_Context, "", Toast.LENGTH_LONG).show();
			break;
		case R.id.Activity_Entreprise_Dialog_InscriptionInformation_TextView_numTel:
			Toast.makeText(_Context, "", Toast.LENGTH_LONG).show();
			break;
		case R.id.Activity_Entreprise_Dialog_InscriptionInformation_TextView_email:
			Toast.makeText(_Context, "", Toast.LENGTH_LONG).show();
			break;
		case R.id.Activity_Entreprise_Dialog_InscriptionInformation_TextView_siteWeb:
			Toast.makeText(_Context, "", Toast.LENGTH_LONG).show();
			break;
		case R.id.Activity_Entreprise_Dialog_InscriptionInformation_TextView_Login:
			Toast.makeText(_Context, "", Toast.LENGTH_LONG).show();
			break;
		case R.id.Activity_Entreprise_Dialog_InscriptionInformation_TextView_password:
			Toast.makeText(_Context, "", Toast.LENGTH_LONG).show();
			break;
		default:
			break;
		}
	}


	///// VERIFICATION !!
	public boolean VerifierLibelle(String libelle)
	{
		if (libelle.equals(""))
		{
			_TextView_libelle.setTextColor(_Context.getResources().getColor(R.color.red));
			return false;
		}
		Pattern p1 = Pattern.compile("^[a-zA-Z_0-9_ _à_À_â_Â_é_è_É_È_ê_Ê_ô_Ô_ù_Ù_Ç_ç_-_`]{3,20}");//^[a-zA-Z_0-9]{4,20}"
		Matcher m = p1.matcher(libelle);
		if ( m.matches())
		{
			_TextView_libelle.setTextColor(_Context.getResources().getColor(android.R.color.black));
			return true;
		}
		_TextView_libelle.setTextColor(_Context.getResources().getColor(R.color.red));
		return false;
	}
	public boolean VerifierRaison(String raison)
	{
		if (raison.equals(""))
		{
			_TextView_raison.setTextColor(_Context.getResources().getColor(android.R.color.black));
			return true;
		}
		Pattern p1 = Pattern.compile("^[a-zA-Z_0-9_ _à_À_â_Â_é_è_É_È_ê_Ê_ô_Ô_ù_Ù_Ç_ç_-_`]{3,20}");//^[a-zA-Z_0-9]{4,20}"
		Matcher m = p1.matcher(raison);
		if ( m.matches())
		{
			_TextView_raison.setTextColor(_Context.getResources().getColor(android.R.color.black));
			return true;
		}
		_TextView_raison.setTextColor(_Context.getResources().getColor(R.color.red));
		return false;
	}
	public boolean VerifierNumTelephone(String numTel)
	{
		if (numTel.equals(""))
		{
			_TextView_numTel.setTextColor(_Context.getResources().getColor(android.R.color.black));
			return true;
		}
		Pattern p1 = Pattern.compile("((?=.*\\d).{8})");
		Matcher m = p1.matcher(numTel);
		if ( m.matches())
		{
			_TextView_numTel.setTextColor(_Context.getResources().getColor(android.R.color.black));
			return true;
		}
		_TextView_numTel.setTextColor(_Context.getResources().getColor(R.color.red));
		return false;
	}
	public boolean VerifierPassword(String password)
	{
		if (password.equals(""))
		{
			_TextView_password.setTextColor(_Context.getResources().getColor(R.color.red));
			return false;
		}
		Pattern p = Pattern.compile("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{5,20})");//^[a-zA-Z_0-9]{4,20}"
		Matcher m = p.matcher(password);
		if ( m.matches())
		{
			_TextView_password.setTextColor(_Context.getResources().getColor(android.R.color.black));
			return true;
		}
		_TextView_password.setTextColor(_Context.getResources().getColor(R.color.red));
		return false;
	}
	public boolean VerifierLogin(String login)
	{
		if (login.equals(""))
		{
			_TextView_login.setTextColor(_Context.getResources().getColor(R.color.red));
			return false;
		}
		Pattern p = Pattern.compile("^[a-zA-Z_0-9]{4,15}");//^[a-zAn-Z_0-9]{4,20}"
		Matcher m = p.matcher(login);
		if ( m.matches())
		{
			_TextView_login.setTextColor(_Context.getResources().getColor(android.R.color.black));
			return true;
		}
		_TextView_login.setTextColor(_Context.getResources().getColor(R.color.red));
		return false;
	}
	public  boolean VerifierEmail(CharSequence email)
	{
		if (email.equals(""))
		{
			_TextView_email.setTextColor(_Context.getResources().getColor(R.color.red));
			return false;
		}
		if (android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches())
		{
			_TextView_email.setTextColor(_Context.getResources().getColor(android.R.color.black));
			return true;
		} 
		_TextView_email.setTextColor(_Context.getResources().getColor(R.color.red));
		return false;
	}
	public boolean VerifierSiteWeb(String siteWeb)
	{
		if (siteWeb.equals(""))
		{
			_TextView_siteWeb.setTextColor(_Context.getResources().getColor(R.color.red));
			return false;
		}
		String web="";
		if (_RadioButton_http.isChecked())
		{
			web="http://";
		}if (_RadioButton_https.isChecked())
		{
			web="https://";
		}
		String adresseWeb=web+siteWeb;
		try {
			URL u = new URL (adresseWeb);
			HttpURLConnection huc =  (HttpURLConnection)  u.openConnection(); 
			huc.setRequestMethod("GET"); 
			huc.connect(); 
			int code=huc.getResponseCode();
			if (code==200)
			{
				_TextView_siteWeb.setTextColor(_Context.getResources().getColor(android.R.color.black));
				return true;
			}else
			{
				_TextView_siteWeb.setTextColor(_Context.getResources().getColor(R.color.red));
				return false;
			}

		} catch (MalformedURLException e)
		{
			_TextView_siteWeb.setTextColor(_Context.getResources().getColor(R.color.red));
			e.printStackTrace();
			return false;
		} catch (IOException e) 
		{
			_TextView_siteWeb.setTextColor(_Context.getResources().getColor(R.color.red));
			e.printStackTrace();
			return false;
		} 

	}
	public void UpdateEntreprise()
	{
		// Update Entreprise
		boolean _Retour=_MySQliteFonction_Entreprise.UpdateInTo_Serveur_Entreprise(_ID_Entreprise,libelle_EditText, raison_EditText, numTel_EditText, email_EditText, siteWeb_EditText, login_EditText, password_EditText, _Context.getResources().getString(R.string.EntrepriseActivity_update_Entreprise));
		if (_Retour)
		{
			//	Mettre A jour la BD selon les nouvelle modification					
			String lLogin_apresUpdate=_EditText_login.getText().toString();
			String lPassword_apresUpdate=_EditText_password.getText().toString();
			_MySQliteFonction_Entreprise.ClearAll_Entreprise();
			boolean sms_insertion=_MySQliteFonction_Entreprise.Synchroniser_Serveur_Entreprise(lLogin_apresUpdate,lPassword_apresUpdate,null,_Context.getResources().getString(R.string.EntrepriseActivity_import_Entreprise));
			if (sms_insertion)
			{
				Refresh();
			}
		}
	}

	public void InscriptionEntreprise()
	{
		// Inscription(Insert  Entreprise)  par l'Entreprise
		if (_Id_enseignant_Entreprise.equals("null")) 
		{ 
			boolean _Retour=_MySQliteFonction_Entreprise.InsertInTo_Serveur_Entreprise(libelle_EditText, raison_EditText, numTel_EditText, email_EditText, siteWeb_EditText, login_EditText, password_EditText, _Context.getResources().getString(R.string.EntrepriseActivity_insert_Entreprise));
			if (_Retour)
			{
				Refresh();
			}
		}
		// Inscription(Insert  Entreprise)  par le chef de departement
		if (!_Id_enseignant_Entreprise.equals("null"))
		{
			MySQliteFonction_ChefDepartement _MySQliteFonction_ChefDepartement = new MySQliteFonction_ChefDepartement(_Context);
			boolean _Retour=_MySQliteFonction_ChefDepartement.InsertInTo_Serveur_Entreprise(libelle_EditText, raison_EditText, numTel_EditText, email_EditText, siteWeb_EditText, login_EditText, password_EditText,_Id_enseignant_Entreprise, _Context.getResources().getString(R.string.ChefDepartementActivity_insert_Entreprise));
			if (_Retour)
			{
				Refresh();
			}
		}
	}

	public void Refresh()
	{
		((Activity)_Context).finish();
		Intent lIntent_To_Activity = new Intent(_Context,((Activity) _Context).getClass());
		_Context.startActivity(lIntent_To_Activity);
	}

}
