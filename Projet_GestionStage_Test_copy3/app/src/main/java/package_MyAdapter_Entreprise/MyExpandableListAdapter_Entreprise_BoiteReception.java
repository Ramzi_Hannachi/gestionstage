package package_MyAdapter_Entreprise;

import java.util.ArrayList;
import java.util.HashMap;

import package_Foction_DataBase.MySQliteFonction_ChefDepartement;
import package_Foction_DataBase.MySQliteFonction_Entreprise;
import package_MemoirStage.Activity_Entreprise;
import package_MyAdapter_ChefDepartement.MyDialog_ChefDepartement_EncadreurPro_InsertUpdate;
import package_MyAdapter_Etudiant.MyDialog_EtudiantInformation;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Html;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.projet_gestionstage_test.R;


public class MyExpandableListAdapter_Entreprise_BoiteReception extends BaseExpandableListAdapter  implements OnClickListener
{
	private MySQliteFonction_Entreprise _MySQliteFonction_Entreprise;
	private MySQliteFonction_ChefDepartement _MySQliteFonction_ChefDepartement;
	private Context _Context;

	private ArrayList<HashMap<String, String>> _List_OffreStage=new ArrayList<HashMap<String,String>>();
	private ArrayList<HashMap<String, String>> _List_Condidater=new ArrayList<HashMap<String,String>>();

	public MyExpandableListAdapter_Entreprise_BoiteReception(Context _Context,ArrayList<HashMap<String, String>> _List_OffreStage,ArrayList<HashMap<String, String>> _List_Condidater)
	{
		this._Context=_Context;
		this._List_OffreStage=_List_OffreStage;
		this._List_Condidater=_List_Condidater;
		_MySQliteFonction_ChefDepartement=new MySQliteFonction_ChefDepartement(_Context);
	}
	/* si l'entreprise a signer un contrat avec un étudiant il l'affiche seulement si
	 * non il affiche tout les demande recue
	 */
	public HashMap<String, String> getChild(int groupPosition, int childPosition)
	{
		boolean ok =false;
		String _ID_OffreStage=_List_OffreStage.get(groupPosition).get("ID_OffreStage");
		ArrayList<HashMap<String, String>> _list_condidater=new ArrayList<HashMap<String,String>>();
		for (int i = 0; i < _List_Condidater.size(); i++) 
		{
			if (_List_Condidater.get(i).get("Id_offreStage_Condidater").equals(_ID_OffreStage))
			{
				if (_List_Condidater.get(i).get("confirmation_Condidater").equals("2"))
				{
					_list_condidater.add(_List_Condidater.get(i));
					ok=true;
					break;
				}
			}
		}
		for (int i = 0; i < _List_Condidater.size(); i++) 
		{
			if (_List_Condidater.get(i).get("Id_offreStage_Condidater").equals(_ID_OffreStage) && ok==false)
			{
				_list_condidater.add(_List_Condidater.get(i));
			}
		}

		return _list_condidater.get(childPosition);
	}

	public long getChildId(int groupPosition, int childPosition)
	{
		return childPosition;
	}
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View contentView,ViewGroup parent)
	{
		HashMap<String, String> _list_condidater=getChild(groupPosition, childPosition);
		if (contentView==null)
		{
			LayoutInflater layoutInflater=(LayoutInflater) _Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			contentView=layoutInflater.inflate(R.layout.activity_entreprise_strocture_boitereception_child, null);
		}
		///
		_MySQliteFonction_Entreprise=new MySQliteFonction_Entreprise(_Context);
		///
		Button _Button_Confirmation_Add=(Button) contentView.findViewById(R.id.ActivityEntreprise_strocture_BoiteReception_Child_Button_Confirmation_Add);
		Button _Button_Confirmation_Remove=(Button) contentView.findViewById(R.id.ActivityEntreprise_strocture_BoiteReception_Child_Button_Confirmation_Remove);
		TextView _TextView_DateReception=(TextView) contentView.findViewById(R.id.ActivityEntreprise_strocture_BoiteReception_Child_TextView_Date);
		Button _Button_Etudiant=(Button) contentView.findViewById(R.id.ActivityEntreprise_strocture_BoiteReception_Child_Button_Etudiant1);
		Button _Button_Binome=(Button) contentView.findViewById(R.id.ActivityEntreprise_strocture_BoiteReception_Child_Button_Etudiant2);
		Button _Button_StageADD=(Button) contentView.findViewById(R.id.ActivityEntreprise_strocture_BoiteReception_Child_Button_StageAdd);
		Button _Button_Option=(Button) contentView.findViewById(R.id.ActivityEntreprise_strocture_BoiteReception_Child_Button_Option);
		Button _Button_EncadreurPro=(Button) contentView.findViewById(R.id.ActivityEntreprise_strocture_BoiteReception_Child_Button_EncadreurPro);

		_TextView_DateReception.setText(_list_condidater.get("date_Condidater"));

		_Button_Confirmation_Add.setTag(R.id.id_etudiant_condidater,_list_condidater.get("Id_etudiant_Condidater"));
		_Button_Confirmation_Add.setTag(R.id.id_offreStage_condidater,_list_condidater.get("Id_offreStage_Condidater"));
		_Button_Confirmation_Add.setOnClickListener( this);

		_Button_Confirmation_Remove.setTag(R.id.id_etudiant_condidater,_list_condidater.get("Id_etudiant_Condidater"));
		_Button_Confirmation_Remove.setTag(R.id.id_offreStage_condidater,_list_condidater.get("Id_offreStage_Condidater"));
		_Button_Confirmation_Remove.setOnClickListener( this);

		_Button_StageADD.setTag(R.id.confirmation_Condidater,_list_condidater.get("confirmation_Condidater"));
		_Button_StageADD.setTag(R.id.id_etudiant_condidater,_list_condidater.get("Id_etudiant_Condidater"));
		_Button_StageADD.setTag(R.id.id_offreStage_condidater,_list_condidater.get("Id_offreStage_Condidater"));
		_Button_StageADD.setTag(R.id.position,groupPosition);
		_Button_StageADD.setTag(R.id.Button,_Button_StageADD);
		_Button_StageADD.setOnClickListener(this);

		_Button_Etudiant.setTag(_list_condidater.get("Id_etudiant_Condidater"));
		_Button_Etudiant.setOnClickListener(this);
		_Button_Binome.setTag(_list_condidater.get("Id_etudiant_Condidater"));
		_Button_Binome.setOnClickListener(this);

		_Button_Option.setTag(R.id.position,groupPosition);
		_Button_Option.setOnClickListener(this);

		_Button_EncadreurPro.setTag(R.id.position,groupPosition);
		_Button_EncadreurPro.setOnClickListener(this);

		Verification_Etudiant(_list_condidater.get("Id_etudiant_Condidater"),_Button_Binome);
		Verifier_Confirmation(_list_condidater, _Button_Confirmation_Add, _Button_Confirmation_Remove);
		Verifier_OffreStage_IS_Stage(groupPosition, _Button_StageADD);
		Verifier_Option(groupPosition, _Button_Option);
		Verifier_EncadreurPro(_List_OffreStage.get(groupPosition).get("cle_stage_OffreStage"), _Button_EncadreurPro);
		return contentView;
	}

	public void Verifier_EncadreurPro(String _ID_Stage,Button _Button_EncadreurPro)
	{
		ArrayList<HashMap<String, String>> _list_EncadreurPro=new ArrayList<HashMap<String,String>>();
		_list_EncadreurPro=_MySQliteFonction_Entreprise.Afficher_EncadreurPro_BoiteReception(_ID_Stage);
		if (_list_EncadreurPro.isEmpty()) 
		{
			_Button_EncadreurPro.setVisibility(View.GONE);
		}else
		{
			_Button_EncadreurPro.setVisibility(View.VISIBLE);
		}
	}

	public void Verifier_Option(int groupPosition,Button _Button_Option )
	{
		String _Id_stage_OffreStage=_List_OffreStage.get(groupPosition).get("cle_stage_OffreStage");
		String _Id_travailPfe_OffreStage=_List_OffreStage.get(groupPosition).get("cle_travailPfe_OffreStage");
		if (_Id_stage_OffreStage!=null && _Id_travailPfe_OffreStage!=null) 
		{
			_Button_Option.setVisibility(View.VISIBLE);
		}else
		{
			_Button_Option.setVisibility(View.GONE);
		}
	}

	public boolean Verifier_OffreStage_IS_Stage(int groupPosition,Button _Button_StageADD)
	{
		String _Id_stage_OffreStage=_List_OffreStage.get(groupPosition).get("cle_stage_OffreStage");
		String _Id_travailPfe_OffreStage=_List_OffreStage.get(groupPosition).get("cle_travailPfe_OffreStage");
		if (_Id_stage_OffreStage!=null && _Id_travailPfe_OffreStage!=null) 
		{
			_Button_StageADD.setBackgroundResource(R.drawable.button_signer_stage_vert);
			return true;
		}else
		{
			_Button_StageADD.setBackgroundResource(R.drawable.button_signer_stage);
			return false;
		}
	}

	public void Verifier_Confirmation(HashMap<String, String> _list_condidater,Button _Button_Confirmation_Add,Button _Button_Confirmation_Remove)
	{
		String confirmation_Condidater=_list_condidater.get("confirmation_Condidater");
		if (confirmation_Condidater.equals("null"))
		{
			_Button_Confirmation_Add.setVisibility(View.VISIBLE);
			_Button_Confirmation_Remove.setVisibility(View.VISIBLE);
		}if (confirmation_Condidater.equals("0"))
		{
			_Button_Confirmation_Add.setVisibility(View.VISIBLE);
			_Button_Confirmation_Remove.setVisibility(View.GONE);	
		}if (confirmation_Condidater.equals("1"))
		{
			_Button_Confirmation_Add.setVisibility(View.GONE);
			_Button_Confirmation_Remove.setVisibility(View.VISIBLE);	
		}if (confirmation_Condidater.equals("2"))
		{
			_Button_Confirmation_Add.setVisibility(View.GONE);
			_Button_Confirmation_Remove.setVisibility(View.GONE);	
		}
	}

	public void Verification_Etudiant(String _ID_Etudiant,Button _Button_Binome)
	{
		ArrayList<HashMap<String, String>> _list_etudiant=_MySQliteFonction_Entreprise.Afficher_Etudiant(Integer.parseInt(_ID_Etudiant));
		String _ID_Binome=_list_etudiant.get(0).get("Id_etudiant1_Etudiant");
		if (_ID_Binome==null)
		{
			_Button_Binome.setVisibility(View.GONE);
		}else
		{
			_Button_Binome.setVisibility(View.VISIBLE);
		}
	}

	public void onClick(View v)
	{
		switch (v.getId()) 
		{
		case R.id.ActivityEntreprise_strocture_BoiteReception_Child_Button_EncadreurPro:
			int groupPosition=(Integer) v.getTag(R.id.position);
			final String _ID_Stage=_List_OffreStage.get(groupPosition).get("cle_stage_OffreStage");
			final HashMap<String, String> _list_EncadreurPro=_MySQliteFonction_Entreprise.Afficher_EncadreurPro_BoiteReception(_ID_Stage).get(0);
			if (_list_EncadreurPro.isEmpty()==false)
			{
				AlertDialog.Builder box1 = new AlertDialog.Builder(_Context);
				box1.setTitle("Encadreur Professionnel");
				String _cin=_list_EncadreurPro.get("Cin_EncadreurPro");
				String _nom="<br />"+_list_EncadreurPro.get("Nom_EncadreurPro");
				String _prenom="<br />"+_list_EncadreurPro.get("Prenom_EncadreurPro");
				String _num="<br />"+_list_EncadreurPro.get("NumTel_EncadreurPro");
				String _email="<br />"+_list_EncadreurPro.get("Email_EncadreurPro");
				box1.setMessage(Html.fromHtml("<font color=\"#000000 \">" + _cin+_nom+_prenom+_num+_email + "</font>",null, null));
				box1.setPositiveButton("Supprimer l'encadreur professionnel", new DialogInterface.OnClickListener() 
				{
					public void onClick(DialogInterface dialog, int which)
					{
						Suppimer_EncadreurPro(_ID_Stage, _list_EncadreurPro);
					}
				});
				box1.setNegativeButton("Supprimer l'affectation Encadreur/Stage", new DialogInterface.OnClickListener() 
				{
					public void onClick(DialogInterface dialog, int which) 
					{
						Annuler_Affectation_EncadreurStage(_ID_Stage);
					}
				});
				AlertDialog dialog1 = box1.show();
				TextView messageText1 = (TextView)dialog1.findViewById(android.R.id.message);
				messageText1.setGravity(Gravity.CENTER);
				messageText1.setTextSize(14);
				dialog1.show();	
			}
			break;

		case R.id.ActivityEntreprise_strocture_BoiteReception_Child_Button_Option:
			final int groupPositionn=(Integer) v.getTag(R.id.position);
			AlertDialog.Builder _BuilderR=new AlertDialog.Builder(_Context);
			_BuilderR.setTitle(" Options ");
			_BuilderR.setItems (new CharSequence[]
					{ "Ajouter/Modifier Encadreur Professionnel"},
					new DialogInterface.OnClickListener()
			{
				public void onClick(DialogInterface dialog, int which)
				{
					switch (which)
					{
					case 0:
						String _ID_Stage=_List_OffreStage.get(groupPositionn).get("cle_stage_OffreStage");
						HashMap< String, String> _HashMap_EncadreurPro=new HashMap<String, String>();
						ArrayList<HashMap<String, String>> _list_EncadreurPro=_MySQliteFonction_Entreprise.Afficher_EncadreurPro_BoiteReception(_ID_Stage);
						if (_list_EncadreurPro.isEmpty()==false)
						{
							_HashMap_EncadreurPro=_list_EncadreurPro.get(0);
							MyDialog_ChefDepartement_EncadreurPro_InsertUpdate _MyDialog_EncadreurPro_InsertUpdate=new MyDialog_ChefDepartement_EncadreurPro_InsertUpdate(_Context, _HashMap_EncadreurPro , _ID_Stage );
							_MyDialog_EncadreurPro_InsertUpdate.MyDialogEncadreurPro_InsertUpdate();
						}else
						{
							AjouterAffecter_EncadreurProStage(_HashMap_EncadreurPro, _ID_Stage);
						}
					}
				}
			});
			_BuilderR.create().show();
			break;

		case R.id.ActivityEntreprise_strocture_BoiteReception_Child_Button_Confirmation_Add:
			final String _Id_etudiant= (String) v.getTag(R.id.id_etudiant_condidater);
			final String _Id_offreStage=(String) v.getTag(R.id.id_offreStage_condidater);

			AlertDialog.Builder _Builder =new AlertDialog.Builder(_Context);
			_Builder.setTitle("Entretien");
			_Builder.setMessage("Êtes-vous sûr de vouloir confirmer la permission pour un entretien ?");
			_Builder.setNegativeButton("Annuler", null);
			_Builder.setPositiveButton("Confirmer", new DialogInterface.OnClickListener()
			{
				public void onClick(DialogInterface dialog, int which)
				{
					String _URL=_Context.getResources().getString(R.string.EntrepriseActivity_update_Condidater);
					boolean x=_MySQliteFonction_Entreprise.UpdateInTo_Serveur_Condidater("1", Integer.parseInt(_Id_etudiant), Integer.parseInt(_Id_offreStage), _URL);
					if (x)
					{
						Refresh_Activity();
					}
				}
			});
			_Builder.show();
			break;
		case R.id.ActivityEntreprise_strocture_BoiteReception_Child_Button_Confirmation_Remove :
			final String _Id_etudiant1= (String) v.getTag(R.id.id_etudiant_condidater);
			final String _Id_offreStage1=(String) v.getTag(R.id.id_offreStage_condidater);
			AlertDialog.Builder _Builder0 =new AlertDialog.Builder(_Context);
			_Builder0.setTitle("Entretien");
			_Builder0.setMessage("Êtes-vous sûr de vouloir réfuter la permission pour un entretien ?");
			_Builder0.setNegativeButton("Annuler", null);
			_Builder0.setPositiveButton("Confirmer", new DialogInterface.OnClickListener()
			{
				public void onClick(DialogInterface dialog, int which)
				{
					String _URL=_Context.getResources().getString(R.string.EntrepriseActivity_update_Condidater);
					boolean x=_MySQliteFonction_Entreprise.UpdateInTo_Serveur_Condidater("0", Integer.parseInt(_Id_etudiant1), Integer.parseInt(_Id_offreStage1), _URL);
					if (x)
					{
						Refresh_Activity();	
					}
				}
			});
			_Builder0.show();
			break;
		case R.id.ActivityEntreprise_strocture_BoiteReception_Child_Button_Etudiant1:
			String _ID_Etudiant= (String) v.getTag();
			ArrayList<HashMap<String, String>> _list_etudiant=new ArrayList<HashMap<String,String>>();
			_list_etudiant=_MySQliteFonction_Entreprise.Afficher_Etudiant(Integer.parseInt(_ID_Etudiant));
			MyDialog_EtudiantInformation _MyDialog_EtudiantInformation=new MyDialog_EtudiantInformation(_Context, _list_etudiant);
			_MyDialog_EtudiantInformation.DialogEudiantInformation();
			break;
		case R.id.ActivityEntreprise_strocture_BoiteReception_Child_Button_Etudiant2:
			String _ID_Etudiant1= (String) v.getTag();
			ArrayList<HashMap<String, String>> _list_Etudiant1=new ArrayList<HashMap<String,String>>();
			ArrayList<HashMap<String, String>> _list_Binome =new ArrayList<HashMap<String,String>>();

			_list_Etudiant1=_MySQliteFonction_Entreprise.Afficher_Etudiant(Integer.parseInt(_ID_Etudiant1));
			String _ID_Binome=_list_Etudiant1.get(0).get("Id_etudiant1_Etudiant");
			if (_ID_Binome.equals("null")==false)
			{
				_list_Binome=_MySQliteFonction_Entreprise.Afficher_Etudiant(Integer.parseInt(_ID_Binome));
				MyDialog_EtudiantInformation _MyDialog_EtudiantInformation1=new MyDialog_EtudiantInformation(_Context, _list_Binome);
				_MyDialog_EtudiantInformation1.DialogEudiantInformation();
			}
			break;
		case R.id.ActivityEntreprise_strocture_BoiteReception_Child_Button_StageAdd:
			int position=(Integer) v.getTag(R.id.position);
			Button _Button_StageADD=(Button) v.getTag(R.id.Button);
			String confirmation_Condidater=(String) v.getTag(R.id.confirmation_Condidater);
			if (confirmation_Condidater.equals("1"))
			{
				final String _Id_etudiant2= (String) v.getTag(R.id.id_etudiant_condidater);
				final String _Id_offreStage2=(String) v.getTag(R.id.id_offreStage_condidater);

				/* verifier si l'entreprise a signé une autre demande de 
				 stage avec le meme étudiant ou binome */
				ArrayList<HashMap<String, String>> _list_etudian=_MySQliteFonction_Entreprise.Afficher_Etudiant(Integer.parseInt(_Id_etudiant2));
				String _Id_binome2=_list_etudian.get(0).get("Id_etudiant1_Etudiant");
				String _URL=_Context.getResources().getString(R.string.EntrepriseActivity_display_Condidater);
				boolean ok=_MySQliteFonction_Entreprise.Watch_Serveur_Condidater(_Id_etudiant2,_Id_binome2,_URL);
				if (ok==false)
				{
					SigneDemandeStage(_Id_etudiant2, _Id_offreStage2);
				}else
				{
					AlertDialog.Builder _Builder5 = new AlertDialog.Builder(_Context);
					_Builder5.setTitle("Information");
					_Builder5.setMessage("  une autre demande de Stage a été signé avec cette etudiant ou son  collègue, " +
							" vous n'avez pas le droit de signé plus d'une demande de stage avec le même étudiant ou son collègue ");
					_Builder5.show();
				}
			}else if (confirmation_Condidater.equals("2") && Verifier_OffreStage_IS_Stage(position, _Button_StageADD)==false)
			{
				AlertDialog.Builder _Builder3 = new AlertDialog.Builder(_Context);
				_Builder3.setMessage(" Vous Avez Signé Cette Demande de Stage , En Cours d'évaluation par le Chef de département ");
				_Builder3.show();
			}else if (confirmation_Condidater.equals("2") && Verifier_OffreStage_IS_Stage(position, _Button_StageADD)==true)
			{
				AlertDialog.Builder _Builder3 = new AlertDialog.Builder(_Context);
				_Builder3.setMessage(" Vous Avez Signé Cette Demande de Stage , Elle a été accepté par le chef de département  ");
				_Builder3.show();
			}
			else 
			{
				AlertDialog.Builder _Builder4 = new AlertDialog.Builder(_Context);
				_Builder4.setMessage(" avant de signer la demande de stage vous devez accepter un l'entretien avec cet étudiant ");
				_Builder4.show();
			}
			break;

		default:
			break;
		}
	}

	public void Annuler_Affectation_EncadreurStage( final String _ID_Stage)
	{
		AlertDialog.Builder _Builder =new AlertDialog.Builder(_Context);
		_Builder.setTitle("Annuler l'affectation Encadeur/Stage");
		_Builder.setMessage("êtes-vous sûr de vouloir annuler l'affectation entre cette encadreur et ce stage ? ");
		_Builder.setNegativeButton("Annuler", null);
		_Builder.setPositiveButton("Confirmer", new DialogInterface.OnClickListener() 
		{
			public void onClick(DialogInterface arg0, int arg1) 
			{
				String _URL=_Context.getResources().getString(R.string.ChefDepartementActivity_update_EncadreurProStage_DELETEaFFECTATION);
				boolean x=_MySQliteFonction_ChefDepartement.UpdateInTo_Serveur_EncadreurProStage_DELETEaFFECTATION(_ID_Stage, _URL);
				if (x) 
				{
					Refresh_Activity();
				}
			}
		});
		_Builder.show();
	}

	public void Suppimer_EncadreurPro(final String _ID_Stage ,final HashMap<String, String> _list_EncadreurPro)
	{
		AlertDialog.Builder _Builder =new AlertDialog.Builder(_Context);
		_Builder.setTitle("Suppression");
		_Builder.setMessage("êtes-vous sûr de vouloir supprimer cette Encadreur Professionnel définitivement ? " +
				", vous devez verifier qu'aucun autre stage n'est affecter a cette encadreur.");
		_Builder.setNegativeButton("Annuler", null);
		_Builder.setPositiveButton("Confirmer", new DialogInterface.OnClickListener() 
		{
			public void onClick(DialogInterface arg0, int arg1) 
			{
				String _URl=_Context.getResources().getString(R.string.ChefDepartementActivity_delete_EncadreurPro);
				String _ID_EncadreurPro=_list_EncadreurPro.get("ID_EncadreurPro");
				boolean x=_MySQliteFonction_ChefDepartement.DeleteInTo_Serveur_EncadreurPro(_ID_Stage, _ID_EncadreurPro, _URl);
				if (x)
				{
					Refresh_Activity();
				}
			}
		});
		_Builder.show();
	}

	public void AjouterAffecter_EncadreurProStage(final HashMap<String, String> _HashMap_EncadreurPro ,final String _ID_Stageee)
	{
		AlertDialog.Builder _Builder=new AlertDialog.Builder(_Context);
		_Builder.setTitle("Affectation Encadreur Professionnel Stage");
		_Builder.setMessage(" veuillez entrer le numéro de la carte d'identité " +
				" de l'encadreur proffessionnel que vous voulez affecter a ce stage");
		LinearLayout layout = new LinearLayout(_Context);
		layout.setOrientation(LinearLayout.HORIZONTAL);
		layout.setGravity(Gravity.CENTER);
		final EditText _EditText_CIN = new EditText(_Context);
		_EditText_CIN.setHint(" Numéro CIN :");
		_EditText_CIN.setInputType(InputType.TYPE_CLASS_NUMBER);
		layout.addView(_EditText_CIN);
		_Builder.setView(layout);
		_Builder.setNegativeButton("Confirmer l'affectation", new DialogInterface.OnClickListener() 
		{
			public void onClick(DialogInterface dialog, int which) 
			{
				if (_EditText_CIN.getText().toString().equals("")==false)
				{
					ArrayList<HashMap<String, String>> _list_EncadreurPro=new ArrayList<HashMap<String,String>>();
					_list_EncadreurPro=_MySQliteFonction_Entreprise.Recherche_EncadreurPro_BoiteReception(_EditText_CIN.getText().toString());
					if (_list_EncadreurPro.isEmpty())
					{// num carte invalide 
						AlertDialog.Builder _Builde=new AlertDialog.Builder(_Context);
						_Builde.setTitle("Erreur");
						_Builde.setMessage("numéro de carte invalide ou n'existe pas");
						AlertDialog dialog1 = _Builde.show();
						TextView messageText1 = (TextView)dialog1.findViewById(android.R.id.message);
						messageText1.setGravity(Gravity.CENTER);
						messageText1.setTextSize(15);
						dialog1.show();	
					}else
					{// num carte valide
						AlertDialog.Builder _Builde=new AlertDialog.Builder(_Context);
						_Builde.setTitle("Information");
						String _cin=_list_EncadreurPro.get(0).get("Cin_EncadreurPro");
						String _nom="<br />"+_list_EncadreurPro.get(0).get("Nom_EncadreurPro");
						String _prenom="<br />"+_list_EncadreurPro.get(0).get("Prenom_EncadreurPro");
						String _num="<br />"+_list_EncadreurPro.get(0).get("NumTel_EncadreurPro");
						String _email="<br />"+_list_EncadreurPro.get(0).get("Email_EncadreurPro");
						_Builde.setMessage(Html.fromHtml("<font color=\"#000000 \">" + _cin+_nom+_prenom+_num+_email+ "</font>",null, null));
						_Builde.setNegativeButton("Annuler", null);
						_Builde.setPositiveButton("Confirmer", new DialogInterface.OnClickListener()
						{
							public void onClick(DialogInterface arg0, int arg1)
							{
								String _URL=_Context.getResources().getString(R.string.ChefDepartementActivity_update_EncadreurProStage_ADDaFFECTATION);
								boolean x=_MySQliteFonction_ChefDepartement.UpdateInTo_Serveur_EncadreurProStage_ADDaFFECTATION(_EditText_CIN.getText().toString(), _ID_Stageee, _URL);
								if (x) 
								{
									Refresh_Activity();
								}
							}
						});
						AlertDialog dialog1 = _Builde.show();
						TextView messageText1 = (TextView)dialog1.findViewById(android.R.id.message);
						messageText1.setGravity(Gravity.CENTER);
						messageText1.setTextSize(15);
						dialog1.show();	
					}
				}else
				{// editText vide
					AlertDialog.Builder _Builde=new AlertDialog.Builder(_Context);
					_Builde.setTitle("Erreur");
					_Builde.setMessage("Champs vide");	
					AlertDialog dialog1 = _Builde.show();
					TextView messageText1 = (TextView)dialog1.findViewById(android.R.id.message);
					messageText1.setGravity(Gravity.CENTER);
					messageText1.setTextSize(15);
					dialog1.show();	
				}
			}
		});
		_Builder.setPositiveButton("Ajouter Un nouveau encadreur professionnel ", new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int which)
			{
				MyDialog_ChefDepartement_EncadreurPro_InsertUpdate _MyDialog_EncadreurPro_InsertUpdate=new MyDialog_ChefDepartement_EncadreurPro_InsertUpdate(_Context, _HashMap_EncadreurPro , _ID_Stageee );
				_MyDialog_EncadreurPro_InsertUpdate.MyDialogEncadreurPro_InsertUpdate();

			}
		});
		_Builder.show();
	}


	public void SigneDemandeStage(final String _ID_etudiant ,final String _ID_offreStage)
	{
		AlertDialog.Builder _Builder1 =new AlertDialog.Builder(_Context);
		_Builder1.setTitle("Stage");
		_Builder1.setMessage("Êtes-vous sûr de vouloir signer une demande de Stage avec cet étudiant ? , La signature sur un support papier est obligatoire");
		_Builder1.setNegativeButton("Annuler", null);
		_Builder1.setPositiveButton("Confirmer", new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface arg0, int arg1) 
			{
				String _URL=_Context.getResources().getString(R.string.EntrepriseActivity_update_CondidaterOffreStage);
				boolean x=_MySQliteFonction_Entreprise.UpdateInTo_Serveur_CondidaterOffreStage("2", Integer.parseInt(_ID_etudiant), Integer.parseInt(_ID_offreStage), _URL);
				if (x)
				{
					Refresh_Activity();	
				}
			}
		});
		_Builder1.show();
	}
	/////////
	public int getChildrenCount(int groupPosition) 
	{
		boolean ok =false;
		String _ID_OffreStage=_List_OffreStage.get(groupPosition).get("ID_OffreStage");
		ArrayList<HashMap<String, String>> _list_condidater=new ArrayList<HashMap<String,String>>();
		for (int i = 0; i < _List_Condidater.size(); i++) 
		{
			if (_List_Condidater.get(i).get("Id_offreStage_Condidater").equals(_ID_OffreStage))
			{
				if (_List_Condidater.get(i).get("confirmation_Condidater").equals("2"))
				{
					_list_condidater.add(_List_Condidater.get(i));
					ok=true;
					break;
				}
			}
		}
		for (int i = 0; i < _List_Condidater.size(); i++) 
		{
			if (_List_Condidater.get(i).get("Id_offreStage_Condidater").equals(_ID_OffreStage) && ok==false)
			{
				_list_condidater.add(_List_Condidater.get(i));
			}
		}

		return _list_condidater.size();
	}

	public HashMap<String, String> getGroup(int groupPosition) 
	{
		return _List_OffreStage.get(groupPosition);
	}

	public int getGroupCount() 
	{
		return _List_OffreStage.size();
	}

	public long getGroupId(int groupPosition) 
	{
		return groupPosition;
	}


	public View getGroupView(int groupPosition, boolean isLastChild, View contentView, ViewGroup parent)
	{
		HashMap<String, String> _list_offreStage=getGroup(groupPosition);
		if (contentView==null)
		{
			LayoutInflater layoutInflater=(LayoutInflater) _Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			contentView=layoutInflater.inflate(R.layout.activity_entreprise_strocture_boitereception_parent, null);
		}
		final TextView _TextView_OffreStage_Nom=(TextView) contentView.findViewById(R.id.ActivityEntreprise_strocture_BoiteReception_Parent_TextView_nom);
		ImageView _ImageView=(ImageView) contentView.findViewById(R.id.ActivityEntreprise_strocture_BoiteReception_Parent_ImageView);

		if (isLastChild) 
		{ //séléctionnné
			_TextView_OffreStage_Nom.setTextColor(_Context.getResources().getColor(R.color.orange));
			_ImageView.setImageDrawable(_Context.getResources().getDrawable(R.drawable.boitereception_ouvert));
		}else 
		{ //non séléctionné
			_TextView_OffreStage_Nom.setTextColor(_Context.getResources().getColor(android.R.color.secondary_text_light));
			_ImageView.setImageDrawable(_Context.getResources().getDrawable(R.drawable.boitereception_fermer));
		}
		_TextView_OffreStage_Nom.setText(_list_offreStage.get("nom_OffreStage"));

		_TextView_OffreStage_Nom.setTag(R.id.position,groupPosition);
		_TextView_OffreStage_Nom.setOnClickListener(new View.OnClickListener()
		{
			public void onClick(View arg0) 
			{
				int groupPosition=(Integer) _TextView_OffreStage_Nom.getTag(R.id.position);
				(((Activity_Entreprise) _Context)).expandGroup(Activity_Entreprise._ExpandableListView_BoiteReception,groupPosition);
			}
		});

		return contentView;
	}



	public boolean hasStableIds()
	{
		return false;
	}

	public boolean isChildSelectable(int arg0, int arg1) 
	{
		return false;
	}

	public void Refresh_Activity()
	{
		((Activity) _Context).finish();
		Intent _Intent_To_ActivityEntreprise=new Intent(_Context,Activity_Entreprise.class);
		_Context.startActivity(_Intent_To_ActivityEntreprise);
	}

}
