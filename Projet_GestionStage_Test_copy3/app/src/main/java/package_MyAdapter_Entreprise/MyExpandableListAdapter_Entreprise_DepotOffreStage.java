package package_MyAdapter_Entreprise;



import java.util.ArrayList;
import java.util.HashMap;

import package_Foction_DataBase.MySQliteFonction_Entreprise;
import package_MemoirStage.Activity_Entreprise;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.projet_gestionstage_test.R;

public class MyExpandableListAdapter_Entreprise_DepotOffreStage extends BaseExpandableListAdapter
{
	private MySQliteFonction_Entreprise _MySQliteFonction_Entreprise;
	private  MyDialog_Entreprise_DepotOffreStage_InsertUdate _MyDialog_Entreprise_DepotStageInsert;

	private Context _Context;
	private ArrayList<HashMap<String, String>> _List_Filiere=new ArrayList<HashMap<String,String>>();
	private ArrayList<HashMap<String, String>> _List_OffreStage=new ArrayList<HashMap<String,String>>();

	public MyExpandableListAdapter_Entreprise_DepotOffreStage(Context _Context,ArrayList<HashMap<String, String>> _List_Filiere,ArrayList<HashMap<String, String>> _List_OffreStage)
	{
		this._Context=_Context;
		this._List_Filiere=_List_Filiere;
		this._List_OffreStage=_List_OffreStage;
	}

	public HashMap<String, String> getChild(int groupPosition, int childPosition) 
	{
		String _ID_Filiere=_List_Filiere.get(groupPosition).get("ID_Filiere");
		ArrayList<HashMap<String, String>> _list_offreStage=new ArrayList<HashMap<String,String>>();
		for (int i = 0; i < _List_OffreStage.size(); i++)
		{
			if (_List_OffreStage.get(i).get("cle_filiere_OffreStage").equals(_ID_Filiere)) 
			{
				_list_offreStage.add(_List_OffreStage.get(i));
			}
		}
		return _list_offreStage.get(childPosition);
	}

	public long getChildId(int groupPosition, int childPosition)
	{
		return childPosition;
	}

	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View contentView,ViewGroup parent)
	{
		final HashMap<String, String> _list_offreStage = getChild(groupPosition, childPosition); 
		if (contentView==null)
		{
			LayoutInflater layoutInflater=(LayoutInflater) _Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			contentView=layoutInflater.inflate(R.layout.activity_entreprise_strocture_depotoffrestage_child, null);
		}
		TextView _TextView_Nom_offreStage=(TextView) contentView.findViewById(R.id.ActivityEntreprise_strocture_DepotOffrestage_Child_TextView_nomStage);
		TextView _TextView_Date_offreStage=(TextView) contentView.findViewById(R.id.ActivityEntreprise_strocture_DepotOffrestage_Child_TextView_dateStage);
		Button _Button_Description_offreStage=(Button) contentView.findViewById(R.id.ActivityEntreprise_strocture_DepotOffrestage_Child_Button_DescriptionStage);
		final ImageView _ImageView_Confirmation_offreStage=(ImageView) contentView.findViewById(R.id.ActivityEntreprise_strocture_DepotOffrestage_Child_ImageView_ConfirmationStage);
		Button _Button_SupprimerModifier_offreStage=(Button) contentView.findViewById(R.id.ActivityEntreprise_strocture_DepotOffrestage_Child_Button_SupprimerModifier);

		_TextView_Nom_offreStage.setText(_list_offreStage.get("nom_OffreStage").trim());
		_TextView_Date_offreStage.setText(_list_offreStage.get("date_OffreStage").trim());

		VerifierConfirmation_OffreStage(_list_offreStage, _ImageView_Confirmation_offreStage);
		_Button_Description_offreStage.setOnClickListener(new View.OnClickListener()
		{
			public void onClick(View v)
			{
				Description_OffreStage(_list_offreStage);
			}
		});
		////////
		_Button_SupprimerModifier_offreStage.setOnClickListener(new View.OnClickListener()
		{
			public void onClick(View arg0)
			{
				String x=VerifierConfirmation_OffreStage(_list_offreStage, _ImageView_Confirmation_offreStage);
				if (x.equals("null"))
				{// supprimer ou modifier !!
					AlertDialog.Builder _Builder = new AlertDialog.Builder(_Context);
					_Builder.setTitle("Que voulez-vous faire ?");
					_Builder.setNegativeButton("Modifier", new OnClickListener()
					{
						public void onClick(DialogInterface arg0, int arg1)
						{
							Modifier_OffreStage(_list_offreStage);
						}
					});
					_Builder.setPositiveButton("Supprimer", new OnClickListener()
					{
						public void onClick(DialogInterface dialog, int which)
						{
							Supprimer_OffreStage(_list_offreStage);
						}
					});
					_Builder.show();
				}else if(x.equals("0"))
				{// supprimer !!
					AlertDialog.Builder _Builder1 = new AlertDialog.Builder(_Context);
					_Builder1.setTitle("Que voulez-vous faire ?");
					_Builder1.setPositiveButton("Supprimer", new OnClickListener()
					{
						public void onClick(DialogInterface dialog, int which)
						{
							Supprimer_OffreStage(_list_offreStage);
						}
					});
					_Builder1.show();
				}else
				{
					AlertDialog.Builder _Builder2 = new AlertDialog.Builder(_Context);
					_Builder2.setTitle(" Interdiction ");
					_Builder2.setMessage(" vous n'avez pas le droit de supprimer ou modifier une offre de stage" +
							" qui a été accepter par le chef de département ");
					_Builder2.show();
				}
			}
		});
		///////
		_ImageView_Confirmation_offreStage.setOnClickListener(new View.OnClickListener()
		{
			public void onClick(View v)
			{
				String x=VerifierConfirmation_OffreStage(_list_offreStage, _ImageView_Confirmation_offreStage);
				AlertDialog.Builder _Builder = new AlertDialog.Builder(_Context);
				_Builder.setTitle("Offre Stage");
				if (x.equals("1"))
				{
					_Builder.setMessage("l'Offre de Stage a été Acceptée par le chef de Département");
				}if (x.equals("0"))
				{
					_Builder.setMessage("l'Offre de Stage a été Refusée par le chef de Département");
				}if (x.equals("null")) 
				{
					_Builder.setMessage("n'a pas encore été évaluée par le chef de Département");
				}if (x.equals("2false"))
				{
					_Builder.setMessage("Vous avez signé une demande de stage pour cette Offre de Stage ,          en cours d'évaluation .....");
				}if (x.equals("2true"))
				{
					_Builder.setMessage("Vous avez signé une demande de stage pour cette Offre de Stage , Elle a été accepté par le chef du département");
				}
				_Builder.show();
			}
		});
		return contentView;
	}


	public int getChildrenCount(int groupPosition)
	{
		String _ID_Filiere=_List_Filiere.get(groupPosition).get("ID_Filiere");
		ArrayList<HashMap<String, String>> _list_offreStage=new ArrayList<HashMap<String,String>>();
		for (int i = 0; i < _List_OffreStage.size(); i++)
		{
			if (_List_OffreStage.get(i).get("cle_filiere_OffreStage").equals(_ID_Filiere)) 
			{
				_list_offreStage.add(_List_OffreStage.get(i));
			}
		}
		return _list_offreStage.size();
	}

	public HashMap<String, String> getGroup(int groupPosition)
	{
		return _List_Filiere.get(groupPosition);
	}

	public int getGroupCount() 
	{
		return _List_Filiere.size();
	}

	public long getGroupId(int groupPosition)
	{
		return groupPosition;
	}

	public View getGroupView(int groupPosition, boolean isLastChild, View contentView, ViewGroup parent)
	{
		HashMap<String, String> _list_filiere=  getGroup(groupPosition);
		if (contentView==null)
		{
			LayoutInflater layoutInflater=(LayoutInflater) _Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			contentView=layoutInflater.inflate(R.layout.activity_entreprise_strocture_depotoffrestage_parent, null);
		}

		final TextView _TextView_Nom_Filiere=(TextView) contentView.findViewById(R.id.ActivityEntreprise_strocture_DepotOffrestage_Parent_TextView_NomFiliere);
		ImageView _ImageView=(ImageView) contentView.findViewById(R.id.ActivityEntreprise_strocture_DepotOffrestage_Parent_ImageView);

		_TextView_Nom_Filiere.setPadding(45, 5, 0, 5);
		_TextView_Nom_Filiere.setText(_list_filiere.get("designation_Filiere").trim());

		if (isLastChild) 
		{ //séléctionnné
			_TextView_Nom_Filiere.setTextColor(_Context.getResources().getColor(R.color.orange));
			_ImageView.setImageDrawable(_Context.getResources().getDrawable(R.drawable.depotoffrestage_ouvert) );
		}else 
		{ //non séléctionné
			_TextView_Nom_Filiere.setTextColor(_Context.getResources().getColor(android.R.color.secondary_text_light));
			_ImageView.setImageDrawable(_Context.getResources().getDrawable(R.drawable.depotoffrestage_fermer) );
		}

		_TextView_Nom_Filiere.setTag(R.id.position,groupPosition);
		_TextView_Nom_Filiere.setOnClickListener(new View.OnClickListener()
		{
			public void onClick(View arg0) 
			{
				int groupPosition=(Integer) _TextView_Nom_Filiere.getTag(R.id.position);
				(((Activity_Entreprise) _Context)).expandGroup(Activity_Entreprise._ExpandableListView_DepotOffreStage,groupPosition);
			}
		});

		return contentView;
	}
	public boolean hasStableIds()
	{
		return false;
	}
	public boolean isChildSelectable(int arg0, int arg1)
	{
		return false;
	}

	public String VerifierConfirmation_OffreStage(HashMap<String, String> _list_offreStage,ImageView _ImageView_Confirmation_offreStage)
	{
		String _confirmaiton=_list_offreStage.get("confirmation_OffreStage");
		String _Id_stage_OffreStage=_list_offreStage.get("cle_stage_OffreStage");
		String _Id_travailPfe_OffreStage=_list_offreStage.get("cle_travailPfe_OffreStage");

		if (_confirmaiton.equals("1"))
		{
			_ImageView_Confirmation_offreStage.setImageResource(R.drawable.button_ajouter_offre_accepter);
			return "1";
		}if (_confirmaiton.equals("0"))
		{
			_ImageView_Confirmation_offreStage.setImageResource(R.drawable.button_ajouter_offre_refuser);
			return "0";
		}if (_confirmaiton.equals("null")) 
		{
			_ImageView_Confirmation_offreStage.setImageResource(R.drawable.button_ajouter_offre_noir);
			return "null";
		}if (_confirmaiton.equals("2") && _Id_stage_OffreStage==null && _Id_travailPfe_OffreStage==null )
		{
			_ImageView_Confirmation_offreStage.setImageResource(R.drawable.stage_evaluation);
			return "2false";
		}if (_confirmaiton.equals("2") && _Id_stage_OffreStage!=null && _Id_travailPfe_OffreStage!=null )
		{
			_ImageView_Confirmation_offreStage.setImageResource(R.drawable.button_signer_stage_vert);
			return "2true";
		}
		return "";
	}
	public void Description_OffreStage(HashMap<String, String> _list_offreStage)
	{
		AlertDialog.Builder _Builder = new AlertDialog.Builder(_Context);
		TextView description = new TextView(_Context);
		description.setText(_list_offreStage.get("description_OffreStage"));
		description.setGravity(Gravity.CENTER_HORIZONTAL);
		description.setTextSize(16);
		description.setTextColor(_Context.getResources().getColor(android.R.color.white));
		_Builder.setTitle("Description Offre Stage");
		_Builder.setView(description);
		_Builder.show();
	}

	public void Supprimer_OffreStage(final HashMap<String, String> _list_offreStage)
	{
		AlertDialog.Builder _Builder =new AlertDialog.Builder(_Context);
		_Builder.setTitle("Suppression");
		_Builder.setMessage("êtes-vous sûr de vouloir supprimer cette offre de stage ?");
		_Builder.setNegativeButton("Annuler", null);
		_Builder.setPositiveButton("Confirmer", new OnClickListener()
		{
			public void onClick(DialogInterface arg0, int arg1)
			{
				_MySQliteFonction_Entreprise=new MySQliteFonction_Entreprise(_Context);
				boolean x=_MySQliteFonction_Entreprise.DeleteInTo_Serveur_OffreStage(_list_offreStage.get("ID_OffreStage"), _Context.getResources().getString(R.string.EntrepriseActivity_delete_OffreStage));
				if (x)
				{   
					((Activity) _Context).finish();
					Intent lIntent_To_ActivityEntreprise = new Intent(_Context,Activity_Entreprise.class);
					_Context.startActivity(lIntent_To_ActivityEntreprise);
				}
			}
		});
		_Builder.show();
	}
	public void Modifier_OffreStage(HashMap<String, String> _list_offreStage)
	{
		_MyDialog_Entreprise_DepotStageInsert=new MyDialog_Entreprise_DepotOffreStage_InsertUdate(_Context,_list_offreStage,Activity_Entreprise.ID_ENTREPRISE);
		_MyDialog_Entreprise_DepotStageInsert.DialogEntrepirse_DepostageInsert();
	}
}
