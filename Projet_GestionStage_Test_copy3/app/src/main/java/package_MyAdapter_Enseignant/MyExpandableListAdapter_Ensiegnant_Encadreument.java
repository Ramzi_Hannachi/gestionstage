package package_MyAdapter_Enseignant;

import java.util.ArrayList;
import java.util.HashMap;

import package_Foction_DataBase.MySQliteFonction_Enseignant;
import package_MemoirStage.Activity_Enseignant;
import package_MyAdapter_Etudiant.MyDialog_EtudiantInformation;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.projet_gestionstage_test.R;

public class MyExpandableListAdapter_Ensiegnant_Encadreument extends BaseExpandableListAdapter implements OnClickListener 
{
	MySQliteFonction_Enseignant  _MySQliteFonction_Enseignant;

	private Context _Context;
	private ArrayList<HashMap<String, String>> _List_Stage=new ArrayList<HashMap<String,String>>();
	private ArrayList<HashMap<String, String>> _List_EtudiantEntreprise=new ArrayList<HashMap<String,String>>();

	public MyExpandableListAdapter_Ensiegnant_Encadreument(Context _Context,ArrayList<HashMap<String, String>> _List_Stage ,ArrayList<HashMap<String, String>> _List_EtudiantEntreprise)
	{
		this._Context=_Context;
		this._List_Stage=_List_Stage;
		this._List_EtudiantEntreprise=_List_EtudiantEntreprise;

		_MySQliteFonction_Enseignant=new MySQliteFonction_Enseignant(_Context);
	}

	public HashMap<String, String> getChild(int groupPosition, int childPosition) 
	{
		String _Id_travailPfe_Stage=_List_Stage.get(groupPosition).get("_Id_travailPfe_Stage");
		ArrayList<HashMap<String, String>>  _list_EtudiantEntreprise=new ArrayList<HashMap<String,String>>();
		for (int i = 0; i < _List_EtudiantEntreprise.size(); i++) 
		{
			String _Id_travailPfe_Etudiant=_List_EtudiantEntreprise.get(i).get("Id_travailPfe_Etudiant");
			if (_Id_travailPfe_Etudiant.equals(_Id_travailPfe_Stage) )
			{
				_list_EtudiantEntreprise.add(_List_EtudiantEntreprise.get(i));
				break;
			}
		}
		return _list_EtudiantEntreprise.get(childPosition); // just mais a verifier
	}

	public long getChildId(int groupPosition, int childPosition)
	{
		return childPosition;
	}

	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View contentView,ViewGroup parent)
	{
		HashMap<String, String> _list_EtudiantEntrepirse=getChild(groupPosition, childPosition);	
		if (contentView==null)
		{
			LayoutInflater layoutInflater=(LayoutInflater) _Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			contentView=layoutInflater.inflate(R.layout.activity_enseignant_strocture_encadrement_child, null);
		}
		Button _Button_Description=(Button) contentView.findViewById(R.id.ActivityEnseignant_strocture_Encadreument_Child_Button_Description);
		Button _Button_Entreprise=(Button) contentView.findViewById(R.id.ActivityEnseignant_strocture_Encadreument_Child_Button_Entreprise);
		Button _Button_EncadreurPro=(Button) contentView.findViewById(R.id.ActivityEnseignant_strocture_Encadreument_Child_Button_EncadreurPro);
		Button _Button_Etudiant1=(Button) contentView.findViewById(R.id.ActivityEnseignant_strocture_Encadreument_Child_Button_Etudiant1);
		Button _Button_Etudiant2=(Button) contentView.findViewById(R.id.ActivityEnseignant_strocture_Encadreument_Child_Button_Etudiant2);
		Button _Button_Calendrier=(Button) contentView.findViewById(R.id.ActivityEnseignant_strocture_Encadreument_Child_Button_Calendrier);

		_Button_Entreprise.setTag(R.id._HashMap,_list_EtudiantEntrepirse);
		_Button_Entreprise.setOnClickListener(this);

		_Button_Description.setTag(R.id.position,groupPosition);
		_Button_Description.setOnClickListener(this);

		_Button_EncadreurPro.setTag(R.id.ID_EncadreurPro,getGroup(groupPosition).get("_Id_encadreurPro_Stage"));
		_Button_EncadreurPro.setOnClickListener(this);

		_Button_Etudiant1.setTag(R.id.ID_etudiant,_list_EtudiantEntrepirse.get("ID_Etudiant"));
		_Button_Etudiant1.setOnClickListener(this);

		_Button_Etudiant2.setTag(R.id.ID_binome,_list_EtudiantEntrepirse.get("Id_etudiant1_Etudiant"));
		_Button_Etudiant2.setOnClickListener(this);

		Verifier_EtudiantBinome(_list_EtudiantEntrepirse.get("Id_etudiant1_Etudiant"), _Button_Etudiant2);
		return contentView;
	}

	public void Verifier_EtudiantBinome( String _ID_Binome,Button _Button_Etudiant2)
	{
		if (_ID_Binome==null) 
		{
			_Button_Etudiant2.setEnabled(false);
			_Button_Etudiant2.setVisibility(View.GONE);
		}else
		{
			_Button_Etudiant2.setEnabled(true);
			_Button_Etudiant2.setVisibility(View.VISIBLE);
		}
	}

	@SuppressWarnings("unchecked")
	public void onClick(View v) 
	{
		switch (v.getId())
		{
		case R.id.ActivityEnseignant_strocture_Encadreument_Child_Button_Description:
			int groupPosition=(Integer) v.getTag(R.id.position);
			Description_Stage(groupPosition);
			break;
		case R.id.ActivityEnseignant_strocture_Encadreument_Child_Button_Entreprise:
			HashMap<String, String> _HashMap_list_Stage=(HashMap<String, String>) v.getTag(R.id._HashMap);
			Afficher_Entreprise(_HashMap_list_Stage);
			break;
		case R.id.ActivityEnseignant_strocture_Encadreument_Child_Button_Etudiant1:
			String _ID_Etudiant=  (String) v.getTag(R.id.ID_etudiant);
			Afficher_Etudiant(Integer.valueOf(_ID_Etudiant));
			break;
		case R.id.ActivityEnseignant_strocture_Encadreument_Child_Button_Etudiant2:
			String _ID_Binome= (String) v.getTag(R.id.ID_binome);
			Afficher_Etudiant(Integer.valueOf(_ID_Binome));
			break;
		case R.id.ActivityEnseignant_strocture_Encadreument_Child_Button_EncadreurPro:
			String _ID_Encadreurpro= (String) v.getTag(R.id.ID_EncadreurPro);
			Afficher_EncadreurPro(_ID_Encadreurpro);
			break;
		default:
			break;
		}
	}

	public void Afficher_EncadreurPro(String _ID_Encadreurpro)
	{
		if (_ID_Encadreurpro!=null)
		{
			ArrayList<HashMap<String, String>> _list_EncdreurPro=new ArrayList<HashMap<String,String>>();
			_list_EncdreurPro=_MySQliteFonction_Enseignant.Afficher_EncadreurPro_ENCADREMENT(Integer.valueOf(_ID_Encadreurpro));

			String _nom_EncadreurPro= _list_EncdreurPro.get(0).get("nom_EncadreurPro");
			String _prenom_EncadreurPro= _list_EncdreurPro.get(0).get("prenom_EncadreurPro");
			String _numTel_EncadreurPro= _list_EncdreurPro.get(0).get("numTel_EncadreurPro");
			String _email_EncadreurPro= _list_EncdreurPro.get(0).get("email_EncadreurPro");

			AlertDialog.Builder box0 = new AlertDialog.Builder(_Context);
			box0.setTitle("Encadreur Professionnel");
			String SMS0="<br />"+_nom_EncadreurPro+"<br />"+"<br />"+_prenom_EncadreurPro+"<br />"+"<br />"
					+_numTel_EncadreurPro+"<br />"+"<br />"+_email_EncadreurPro+"<br />";
			box0.setMessage(Html.fromHtml("<font color=\"#000000 \">" + SMS0 + "</font>",null, null));
			AlertDialog dialog0 = box0.show();
			TextView messageText0 = (TextView)dialog0.findViewById(android.R.id.message);
			messageText0.setGravity(Gravity.CENTER);
			messageText0.setTextSize(14);
			dialog0.show();
		}else
		{
			AlertDialog.Builder _Builder=new AlertDialog.Builder(_Context);
			_Builder.setTitle("Information");
			_Builder.setMessage("Aucun Encadreur Professionnel n'a été " +
					"encore affecté pour ce stage ");
			_Builder.show();
		}
	}


	public void Afficher_Etudiant(int _ID_Etudiant)
	{
		ArrayList<HashMap<String, String>> _list_Etudiant=new ArrayList<HashMap<String,String>>();
		_list_Etudiant=_MySQliteFonction_Enseignant.Afficher_Etudiant_INFORMATION(_ID_Etudiant);
		MyDialog_EtudiantInformation _MyDialog_EtudiantInformation =new MyDialog_EtudiantInformation(_Context, _list_Etudiant);
		_MyDialog_EtudiantInformation.DialogEudiantInformation();
	}

	public void Description_Stage(int groupPosition)
	{
		String description_Stage=getGroup(groupPosition).get("_description_Stage");
		AlertDialog.Builder _Builder=new AlertDialog.Builder(_Context);
		_Builder.setTitle("Description Stage");
		_Builder.setMessage(description_Stage);
		_Builder.show();
	}

	public void Afficher_Entreprise(HashMap<String, String> _HashMap_list_Stage)
	{
		String _nom_Entreprise= _HashMap_list_Stage.get("libelle_Entreprise");
		String _numTel_Entreprise= _HashMap_list_Stage.get("numTel_Entreprise");
		String _email_Entreprise= _HashMap_list_Stage.get("email_Entreprise");
		String _siteWeb_Entreprise= _HashMap_list_Stage.get("siteWeb_Entreprise");
		AlertDialog.Builder box0 = new AlertDialog.Builder(_Context);
		box0.setTitle("Entreprise");
		String SMS0="<br />"+_nom_Entreprise+"<br />"+"<br />"+_numTel_Entreprise+"<br />"+"<br />"
				+_email_Entreprise+"<br />"+"<br />"+_siteWeb_Entreprise+"<br />";
		box0.setMessage(Html.fromHtml("<font color=\"#000000 \">" + SMS0 + "</font>",null, null));
		AlertDialog dialog0 = box0.show();
		TextView messageText0 = (TextView)dialog0.findViewById(android.R.id.message);
		messageText0.setGravity(Gravity.CENTER);
		messageText0.setTextSize(14);
		dialog0.show();
	}

	public int getChildrenCount(int groupPosition)
	{
		String _Id_travailPfe_Stage=_List_Stage.get(groupPosition).get("_Id_travailPfe_Stage");
		ArrayList<HashMap<String, String>>  _list_DemandeEtudiantEntreprise=new ArrayList<HashMap<String,String>>();
		for (int i = 0; i < _List_EtudiantEntreprise.size(); i++) 
		{
			String _Id_travailPfe_Etudiant=_List_EtudiantEntreprise.get(i).get("Id_travailPfe_Etudiant");
			if (_Id_travailPfe_Etudiant.equals(_Id_travailPfe_Stage) )
			{
				_list_DemandeEtudiantEntreprise.add(_List_EtudiantEntreprise.get(i));
				break;
			}
		}
		return _list_DemandeEtudiantEntreprise.size();
	}

	//////////////////////
	public HashMap<String, String> getGroup(int groupPosition)
	{
		return _List_Stage.get(groupPosition);
	}

	public int getGroupCount() 
	{
		return _List_Stage.size();
	}

	public long getGroupId(int groupPosition) 
	{
		return groupPosition;
	}



	public View getGroupView(int groupPosition, boolean isLastChild, View contentView, ViewGroup parent)
	{
		HashMap<String, String> _list_Stage=getGroup(groupPosition);
		if (contentView==null)
		{
			LayoutInflater layoutInflater=(LayoutInflater) _Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			contentView=layoutInflater.inflate(R.layout.activity_enseignant_strocture_encadrement_parent, null);
		}
		final TextView _TextView_OffreStage=(TextView) contentView.findViewById(R.id.ActivityEnseignant_strocture_Encadreument_Parent_TextView_nom);
		ImageView _ImageView=(ImageView) contentView.findViewById(R.id.ActivityEnseignant_strocture_Encadreument_Parent_ImgaView);

		_TextView_OffreStage.setText(_list_Stage.get("_nom_Stage"));

		if (isLastChild) 
		{ //séléctionnné
			_TextView_OffreStage.setTextColor(_Context.getResources().getColor(R.color.orange));
			_ImageView.setImageDrawable(_Context.getResources().getDrawable(R.drawable.encadreument_ouvert));
		}else 
		{ //non séléctionné
			_TextView_OffreStage.setTextColor(_Context.getResources().getColor(android.R.color.secondary_text_light));
			_ImageView.setImageDrawable(_Context.getResources().getDrawable(R.drawable.encadreument_fermer));
		}

		_TextView_OffreStage.setTag(R.id.position,groupPosition);
		_TextView_OffreStage.setOnClickListener(new OnClickListener()
		{
			public void onClick(View arg0) 
			{
				int groupPosition=(Integer) _TextView_OffreStage.getTag(R.id.position);
				(((Activity_Enseignant) _Context)).expandGroup(Activity_Enseignant._ExpandableListView_Encadreument,groupPosition);
			}
		});

		return contentView;
	}



	public boolean hasStableIds()
	{
		return false;
	}
	public boolean isChildSelectable(int arg0, int arg1) 
	{
		return false;
	}



	public void Refresh_Activity()
	{
		((Activity) _Context).finish();
		Intent _Intent_To_ActivityChefDepartement=new Intent(_Context,Activity_Enseignant.class);
		_Context.startActivity(_Intent_To_ActivityChefDepartement);
	}

}
