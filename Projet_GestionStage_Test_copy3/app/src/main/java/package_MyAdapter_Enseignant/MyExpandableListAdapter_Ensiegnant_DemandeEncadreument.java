package package_MyAdapter_Enseignant;

import java.util.ArrayList;
import java.util.HashMap;

import package_Foction_DataBase.MySQliteFonction_Enseignant;
import package_MemoirStage.Activity_Enseignant;
import package_MyAdapter_Etudiant.MyDialog_EtudiantInformation;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.projet_gestionstage_test.R;

public class MyExpandableListAdapter_Ensiegnant_DemandeEncadreument extends BaseExpandableListAdapter implements OnClickListener 
{
	MySQliteFonction_Enseignant  _MySQliteFonction_Enseignant;

	private Context _Context;
	private ArrayList<HashMap<String, String>> _List_Stage=new ArrayList<HashMap<String,String>>();
	private ArrayList<HashMap<String, String>> _List_DemandeEtudiantEntreprise=new ArrayList<HashMap<String,String>>();

	public MyExpandableListAdapter_Ensiegnant_DemandeEncadreument(Context _Context,ArrayList<HashMap<String, String>> _List_Stage ,ArrayList<HashMap<String, String>> _List_DemandeEtudiantEntreprise)
	{
		_MySQliteFonction_Enseignant=new MySQliteFonction_Enseignant(_Context);
		this._Context=_Context;
		this._List_Stage=_List_Stage;
		this._List_DemandeEtudiantEntreprise=_List_DemandeEtudiantEntreprise;
	}

	public HashMap<String, String> getChild(int groupPosition, int childPosition) 
	{
		String _Id_travailPfe_Stage=_List_Stage.get(groupPosition).get("_Id_travailPfe_Stage");
		ArrayList<HashMap<String, String>>  _list_DemandeEtudiantEntreprise=new ArrayList<HashMap<String,String>>();
		for (int i = 0; i < _List_DemandeEtudiantEntreprise.size(); i++) 
		{
			String _Id_travailPfe_Etudiant=_List_DemandeEtudiantEntreprise.get(i).get("Id_travailPfe_Etudiant");
			if (_Id_travailPfe_Etudiant.equals(_Id_travailPfe_Stage) )
			{
				_list_DemandeEtudiantEntreprise.add(_List_DemandeEtudiantEntreprise.get(i));
			}
		}
		return _list_DemandeEtudiantEntreprise.get(childPosition);
	}

	public long getChildId(int groupPosition, int childPosition)
	{
		return childPosition;
	}

	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View contentView,ViewGroup parent)
	{
		HashMap<String, String> _list_DemandeEtudiantEntrepirse=getChild(groupPosition, childPosition);	
		if (contentView==null)
		{
			LayoutInflater layoutInflater=(LayoutInflater) _Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			contentView=layoutInflater.inflate(R.layout.activity_enseignant_strocture_demandeencadreument_child, null);
		}
		Button _Button_Entreprise=(Button) contentView.findViewById(R.id.ActivityEnseignant_strocture_DemandeEncadreument_Child_Button_Entreprise);
		Button _Button_Description=(Button) contentView.findViewById(R.id.ActivityEnseignant_strocture_DemandeEncadreument_Child_Button_Description);
		Button _Button_Etudiant1=(Button) contentView.findViewById(R.id.ActivityEnseignant_strocture_DemandeEncadreument_Child_Button_Etudiant1);
		Button _Button_Etudiant2=(Button) contentView.findViewById(R.id.ActivityEnseignant_strocture_DemandeEncadreument_Child_Button_Etudiant2);
		Button _Button_StageAdd=(Button) contentView.findViewById(R.id.ActivityEnseignant_strocture_DemandeEncadreument_Child_Button_Add);
		Button _Button_StageRemove=(Button) contentView.findViewById(R.id.ActivityEnseignant_strocture_DemandeEncadreument_Child_Button_Remove);

		TextView _TextView_dateDemande=(TextView) contentView.findViewById(R.id.ActivityEnseignant_strocture_DemandeEncadreument_Child_Button_DateDemande);
		_TextView_dateDemande.setText(_list_DemandeEtudiantEntrepirse.get("date_Demande"));

		_Button_Entreprise.setTag(R.id._HashMap,_list_DemandeEtudiantEntrepirse);
		_Button_Entreprise.setOnClickListener(this);

		_Button_Description.setTag(R.id.position,groupPosition);
		_Button_Description.setOnClickListener(this);

		_Button_Etudiant1.setTag(R.id.ID_etudiant,_list_DemandeEtudiantEntrepirse.get("ID_Etudiant"));
		_Button_Etudiant1.setOnClickListener(this);

		_Button_Etudiant2.setTag(R.id.ID_binome,_list_DemandeEtudiantEntrepirse.get("Id_etudiant1_Etudiant"));
		_Button_Etudiant2.setOnClickListener(this);

		_Button_StageAdd.setTag(R.id._HashMap,_list_DemandeEtudiantEntrepirse);
		_Button_StageAdd.setOnClickListener(this);

		_Button_StageRemove.setTag(R.id._HashMap,_list_DemandeEtudiantEntrepirse);
		_Button_StageRemove.setOnClickListener(this);

		Verifier_ConfirmationDemande(_list_DemandeEtudiantEntrepirse, _Button_StageAdd, _Button_StageRemove);
		Verifier_EtudiantBinome(_list_DemandeEtudiantEntrepirse.get("Id_etudiant1_Etudiant"), _Button_Etudiant2);
		return contentView;
	}

	public String Verifier_ConfirmationDemande(HashMap<String, String> _list_Demande,Button _Button_Add,Button _Button_Remove)
	{
		String ok="";
		String confirmation_Demande=_list_Demande.get("confirmation_Demande");
		if (confirmation_Demande.equals("null"))
		{
			_Button_Add.setEnabled(true);
			_Button_Add.setVisibility(View.VISIBLE);
			_Button_Add.setBackgroundResource(R.drawable.ajouter_confirmation);

			_Button_Remove.setEnabled(true);
			_Button_Remove.setVisibility(View.VISIBLE);
			_Button_Remove.setBackgroundResource(R.drawable.supprimer_confirmation);
			ok="null";
		}
		if (confirmation_Demande.equals("0"))
		{
			_Button_Add.setEnabled(true);
			_Button_Add.setVisibility(View.VISIBLE);
			_Button_Add.setBackgroundResource(R.drawable.supprimer_confirmation_rouge);

			_Button_Remove.setEnabled(false);
			_Button_Remove.setVisibility(View.GONE);
			ok="0";
		}
		if (confirmation_Demande.equals("1"))
		{
			_Button_Remove.setEnabled(true);
			_Button_Remove.setVisibility(View.VISIBLE);
			_Button_Remove.setBackgroundResource(R.drawable.ajouter_confirmation_vert);

			_Button_Add.setEnabled(false);
			_Button_Add.setVisibility(View.GONE);
			ok="1";
		}
		if (confirmation_Demande.equals("2"))
		{
			_Button_Remove.setEnabled(true);
			_Button_Remove.setVisibility(View.VISIBLE);
			_Button_Remove.setBackgroundResource(R.drawable.button_signer_stage_vert);

			_Button_Add.setEnabled(false);
			_Button_Add.setVisibility(View.GONE);
			ok="2";
		}
		return ok;
	}

	public void Verifier_EtudiantBinome( String _ID_Binome,Button _Button_Etudiant2)
	{
		if (_ID_Binome==null) 
		{
			_Button_Etudiant2.setEnabled(false);
			_Button_Etudiant2.setVisibility(View.GONE);
		}else
		{
			_Button_Etudiant2.setEnabled(true);
			_Button_Etudiant2.setVisibility(View.VISIBLE);
		}
	}

	@SuppressWarnings("unchecked")
	public void onClick(View v) 
	{
		switch (v.getId())
		{
		case R.id.ActivityEnseignant_strocture_DemandeEncadreument_Child_Button_Description:
			int groupPosition=(Integer) v.getTag(R.id.position);
			Description_Stage(groupPosition);
			break;
		case R.id.ActivityEnseignant_strocture_DemandeEncadreument_Child_Button_Entreprise:
			HashMap<String, String> _HashMap_list_Stage=(HashMap<String, String>) v.getTag(R.id._HashMap);
			Afficher_Entreprise(_HashMap_list_Stage);
			break;
		case R.id.ActivityEnseignant_strocture_DemandeEncadreument_Child_Button_Etudiant1:
			String _ID_Etudiant=  (String) v.getTag(R.id.ID_etudiant);
			Afficher_Etudiant(Integer.valueOf(_ID_Etudiant));
			break;
		case R.id.ActivityEnseignant_strocture_DemandeEncadreument_Child_Button_Etudiant2:
			String _ID_Binome= (String) v.getTag(R.id.ID_binome);
			Afficher_Etudiant(Integer.valueOf(_ID_Binome));
			break;
		case  R.id.ActivityEnseignant_strocture_DemandeEncadreument_Child_Button_Remove:
			HashMap<String, String> _list_Demande0=(HashMap<String, String>) v.getTag(R.id._HashMap);
			Modifier_ConfirmationRemove_Demande( _list_Demande0);
			break;
		case R.id.ActivityEnseignant_strocture_DemandeEncadreument_Child_Button_Add:
			HashMap<String, String> _list_Demande1=(HashMap<String, String>) v.getTag(R.id._HashMap);
			Modifier_ConfirmationAdd_Demande( _list_Demande1);
			break;
		default:
			break;
		}
	}

	public void Modifier_ConfirmationRemove_Demande(final HashMap<String, String> _list_Demande)
	{
		if (_list_Demande.get("confirmation_Demande").equals("null"))
		{
			AlertDialog.Builder _Builder=new AlertDialog.Builder(_Context);
			_Builder.setTitle("Réfutation");
			_Builder.setMessage("Êtes-vous sûr de vouloir  Refuser la permission d'encadrement" +
					" pour cet étudiant ");
			_Builder.setNegativeButton("Annuler", null);
			_Builder.setPositiveButton("Confirmer", new DialogInterface.OnClickListener() 
			{
				public void onClick(DialogInterface arg0, int arg1) 
				{
					String _URL=_Context.getResources().getString(R.string.EnseignantActivity_Update_Demande);
					String _ID_Enseignant=_list_Demande.get("Id_enseignant_Demande");
					String _ID_Etudiant=_list_Demande.get("Id_etudiant_Demande");
					_MySQliteFonction_Enseignant.UpdateInTo_Serveur_Demande("0" , _ID_Enseignant, _ID_Etudiant, _URL);
					Refresh_Activity();
				}
			});
			_Builder.show();
		}
		if (_list_Demande.get("confirmation_Demande").equals("1"))
		{
			AlertDialog.Builder _Builder=new AlertDialog.Builder(_Context);
			_Builder.setTitle("Réfutation");
			_Builder.setMessage(" Vous Avez Accepté cette permission d'encadrement    " +
					" Vous n'avez pas le droit de la supprimer");
			_Builder.show();
		}
		if (_list_Demande.get("confirmation_Demande").equals("2"))
		{
			AlertDialog.Builder _Builder=new AlertDialog.Builder(_Context);
			_Builder.setTitle("Information");
			_Builder.setMessage("Le Chef de Département a accepté la demande d'encadrement");
			_Builder.show();
		}
	}

	public void Modifier_ConfirmationAdd_Demande(final HashMap<String, String> _list_Demande)
	{
		AlertDialog.Builder _Builder=new AlertDialog.Builder(_Context);
		_Builder.setTitle("Acceptation");
		_Builder.setMessage("Êtes-vous sûr de vouloir accorder une confirmation " +
				"d'encadrement pour cet étudiant , Opération irréversible " +
				" elle doit être évaluée par le Chef de département ");
		_Builder.setNegativeButton("Annuler", null);
		_Builder.setPositiveButton("Confirmer", new DialogInterface.OnClickListener() 
		{
			public void onClick(DialogInterface arg0, int arg1) 
			{
				String _ID_Etudiant=_list_Demande.get("Id_etudiant_Demande");
				String _ID_Binome=_list_Demande.get("Id_etudiant1_Etudiant");
				String _URL0=_Context.getResources().getString(R.string.EnseignantActivity_display_Demande);
				boolean ok=_MySQliteFonction_Enseignant.Watch_Serveur_Demande(_ID_Etudiant, _ID_Binome, _URL0);
				if (ok==false) // verifier si l'etudiant a été acceptée par un autre enseignant ou pas
				{
					String _URL1=_Context.getResources().getString(R.string.EnseignantActivity_Update_Demande);
					String _ID_Enseignant=_list_Demande.get("Id_enseignant_Demande");
					boolean x=_MySQliteFonction_Enseignant.UpdateInTo_Serveur_Demande("1" , _ID_Enseignant, _ID_Etudiant, _URL1);
					if (x)
					{Refresh_Activity();}
				}else
				{
					AlertDialog.Builder _Builder =new AlertDialog.Builder(_Context);
					_Builder.setTitle(" Information ");
					_Builder.setMessage(" Cet étudiant a été acceptée par un autre enseignant ");
					_Builder.show();
				}
			}
		});
		_Builder.show();
	}

	public void Afficher_Etudiant(int _ID_Etudiant)
	{
		ArrayList<HashMap<String, String>> _list_Etudiant=new ArrayList<HashMap<String,String>>();
		_list_Etudiant=_MySQliteFonction_Enseignant.Afficher_Etudiant_INFORMATION(_ID_Etudiant);
		Log.d("_list_Etudiant",String.valueOf(_list_Etudiant));
		MyDialog_EtudiantInformation _MyDialog_EtudiantInformation =new MyDialog_EtudiantInformation(_Context, _list_Etudiant);
		_MyDialog_EtudiantInformation.DialogEudiantInformation();
	}

	public void Description_Stage(int groupPosition)
	{
		String description_Stage=getGroup(groupPosition).get("_description_Stage");
		AlertDialog.Builder _Builder=new AlertDialog.Builder(_Context);
		_Builder.setTitle("Description Stage");
		_Builder.setMessage(description_Stage);
		_Builder.show();
	}

	public void Afficher_Entreprise(HashMap<String, String> _HashMap_list_Stage)
	{
		String _nom_Entreprise= _HashMap_list_Stage.get("libelle_Entreprise");
		String _numTel_Entreprise= _HashMap_list_Stage.get("numTel_Entreprise");
		String _email_Entreprise= _HashMap_list_Stage.get("email_Entreprise");
		String _siteWeb_Entreprise= _HashMap_list_Stage.get("siteWeb_Entreprise");
		AlertDialog.Builder box0 = new AlertDialog.Builder(_Context);
		box0.setTitle("Entreprise");
		String SMS0="<br />"+_nom_Entreprise+"<br />"+"<br />"+_numTel_Entreprise+"<br />"+"<br />"
				+_email_Entreprise+"<br />"+"<br />"+_siteWeb_Entreprise+"<br />";
		box0.setMessage(Html.fromHtml("<font color=\"#000000 \">" + SMS0 + "</font>",null, null));
		AlertDialog dialog0 = box0.show();
		TextView messageText0 = (TextView)dialog0.findViewById(android.R.id.message);
		messageText0.setGravity(Gravity.CENTER);
		messageText0.setTextSize(14);
		dialog0.show();
	}

	public int getChildrenCount(int groupPosition)
	{
		String _Id_travailPfe_Stage=_List_Stage.get(groupPosition).get("_Id_travailPfe_Stage");
		ArrayList<HashMap<String, String>>  _list_DemandeEtudiantEntreprise=new ArrayList<HashMap<String,String>>();
		for (int i = 0; i < _List_DemandeEtudiantEntreprise.size(); i++) 
		{
			String _Id_travailPfe_Etudiant=_List_DemandeEtudiantEntreprise.get(i).get("Id_travailPfe_Etudiant");
			if (_Id_travailPfe_Etudiant.equals(_Id_travailPfe_Stage) )
			{
				_list_DemandeEtudiantEntreprise.add(_List_DemandeEtudiantEntreprise.get(i));
			}
		}
		return _list_DemandeEtudiantEntreprise.size();
	}

	//////////////////////
	public HashMap<String, String> getGroup(int groupPosition)
	{
		return _List_Stage.get(groupPosition);
	}

	public int getGroupCount() 
	{
		return _List_Stage.size();
	}

	public long getGroupId(int groupPosition) 
	{
		return groupPosition;
	}



	public View getGroupView(int groupPosition, boolean isLastChild, View contentView, ViewGroup parent)
	{
		HashMap<String, String> _list_Stage=getGroup(groupPosition);
		if (contentView==null)
		{
			LayoutInflater layoutInflater=(LayoutInflater) _Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			contentView=layoutInflater.inflate(R.layout.activity_enseignant_strocture_demandeencadreument_parent, null);
		}
		final TextView _TextView_OffreStage=(TextView) contentView.findViewById(R.id.ActivityEnseignant_strocture_DemandeEncadreument_Parent_TextView_nom);
		ImageView _ImageView=(ImageView) contentView.findViewById(R.id.ActivityEnseignant_strocture_DemandeEncadreument_Parent_ImageView);
		_TextView_OffreStage.setText(_list_Stage.get("_nom_Stage"));

		if (isLastChild) 
		{ //séléctionnné
			_TextView_OffreStage.setTextColor(_Context.getResources().getColor(R.color.orange));
			_ImageView.setImageDrawable(_Context.getResources().getDrawable(R.drawable.demandeencadrement_ouvert));
		}else 
		{ //non séléctionné
			_TextView_OffreStage.setTextColor(_Context.getResources().getColor(android.R.color.secondary_text_light));
			_ImageView.setImageDrawable(_Context.getResources().getDrawable(R.drawable.demandeencadrement_fermer));
		}

		_TextView_OffreStage.setTag(R.id.position,groupPosition);
		_TextView_OffreStage.setOnClickListener(new OnClickListener()
		{
			public void onClick(View arg0) 
			{
				int groupPosition=(Integer) _TextView_OffreStage.getTag(R.id.position);
				(((Activity_Enseignant) _Context)).expandGroup(Activity_Enseignant._ExpandableListView_DemandeEncadreument,groupPosition);
			}
		});

		return contentView;
	}



	public boolean hasStableIds()
	{
		return false;
	}
	public boolean isChildSelectable(int arg0, int arg1) 
	{
		return false;
	}



	public void Refresh_Activity()
	{
		((Activity) _Context).finish();
		Intent _Intent_To_ActivityChefDepartement=new Intent(_Context,Activity_Enseignant.class);
		_Context.startActivity(_Intent_To_ActivityChefDepartement);
	}

}
