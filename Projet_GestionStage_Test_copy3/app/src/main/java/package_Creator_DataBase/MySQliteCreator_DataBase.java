package package_Creator_DataBase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class MySQliteCreator_DataBase extends SQLiteOpenHelper 
{
	public MySQliteCreator_DataBase(Context context, String name,
			CursorFactory factory, int version) {
		super(context, name, factory, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db)
	{
		String entreprise = "CREATE TABLE IF NOT EXISTS entreprise("
				+ " Id_entreprise        INTEGER NOT NULL ,"
				+ " libelle_entreprise   TEXT NOT NULL ,"
				+ " raison_entreprise    TEXT ,"
				+ " numTel_entreprise    TEXT ,"
				+ " email_entreprise     TEXT NOT NULL ,"
				+ " siteWeb_entreprise   TEXT ,"
				+ " login_entreprise     TEXT NOT NULL ,"
				+ " password_entreprise  TEXT NOT NULL ,"
				+ " logo_entreprise      TEXT ,"
				+ " idensei_entreprise   INTEGER ,"
				+ " PRIMARY KEY (Id_entreprise) ,"
				+ " FOREIGN KEY (idensei_entreprise) REFERENCES enseignant(Idensei)) ;";

		String encadreurPro = "CREATE TABLE IF NOT EXISTS encadreur_pro("
				+ " Id_encadreurPro      INTEGER  NOT NULL ,"
				+ " ncin_encadreurPro    TEXT NOT NULL ,"
				+ " nom_encadreurPro     TEXT NOT NULL ,"
				+ " prenom_encadreurPro  TEXT NOT NULL ,"
				+ " numTel_encadreurPro  TEXT ,"
				+ " email_encadreurPro   TEXT ,"
				+ " PRIMARY KEY (Id_encadreurPro));";

		String offreStage = "CREATE TABLE IF NOT EXISTS offre_stage("
				+ " Id_offreStage            INTEGER  NOT NULL ,"
				+ " nom_offreStage           TEXT NOT NULL ,"
				+ " description_offreStage   TEXT NOT NULL ,"
				+ " confirmation_offreStage  TEXT ,"
				+ " date_offreStage          TEXT NOT NULL ,"
				+ " id_entreprise_offreStage INTEGER NOT NULL ,"
				+ " id_stage_offreStage      INTEGER  ,"
				+ " id_travailPfe_offreStage INTEGER  ,"
				+ " id_filiere_offreStage    INTEGER NOT NULL ,"
				+ " PRIMARY KEY (Id_offreStage) ,"
				+ " FOREIGN KEY (id_entreprise_offreStage) REFERENCES entreprise(Id_entreprise),"
				+ " FOREIGN KEY (id_stage_offreStage) REFERENCES stage(Id_stage),"
				+ " FOREIGN KEY (id_travailPfe_offreStage) REFERENCES travail_pfe(Id_travailPfe),"
				+ " FOREIGN KEY (id_filiere_offreStage) REFERENCES filiere(Id_filiere));";

		String etudiant = "CREATE TABLE IF NOT EXISTS etudiant("
				+ " Id_etudiant          INTEGER NOT NULL ,"
				+ " numInscrit_etudiant  TEXT NOT NULL ,"
				+ " cin_etudiant         TEXT NOT NULL ,"
				+ " nom_etudiant         TEXT NOT NULL ,"
				+ " prenom_etudiant      TEXT NOT NULL ,"
				+ " sexe_etudiant        TEXT NOT NULL ,"
				+ " dateNaiss_etudiant   TEXT ,"
				+ " lieuNaiss_etudiant   TEXT ,"
				+ " numTel_etudiant      TEXT ,"
				+ " email_etudiant       TEXT ,"
				+ " adresse_etudiant     TEXT ,"
				+ " codePostal_etudiant  TEXT ,"
				+ " image_etudiant       TEXT ,"
				+ " id_departement_etudiant INTEGER NOT NULL ,"
				+ " id_travailPfe_etudiant  INTEGER  ,"
				+ " id_etudiant_1        INTEGER  ,"
				+ " id_filiere_etudiant  INTEGER NOT NULL ,"
				+ " id_document_etudiant INTEGER  ,"
				+ " PRIMARY KEY (Id_etudiant) ,"
				+ " FOREIGN KEY (id_departement_etudiant) REFERENCES departement(Id_departement),"
				+ " FOREIGN KEY (id_travailPfe_etudiant) REFERENCES travail_pfe(Id_travailPfe),"
				+ " FOREIGN KEY (id_etudiant_1) REFERENCES etudiant(Id_etudiant),"
				+ " FOREIGN KEY (id_filiere_etudiant) REFERENCES filiere(Id_filiere),"
				+ " FOREIGN KEY (id_document_etudiant) REFERENCES document(Id_document));";

		String filiere = "CREATE TABLE IF NOT EXISTS filiere("
				+ "	Id_filiere           INTEGER NOT NULL ,"
				+ "	designation_filiere  TEXT NOT NULL ,"
				+ "	id_cycle_filiere     INTEGER NOT NULL ,"
				+ "	PRIMARY KEY (Id_filiere) ,"
				+ "	FOREIGN KEY (id_cycle_filiere) REFERENCES cycle(Id_cycle));";

		String niveau = "CREATE TABLE IF NOT EXISTS niveau("
				+ "	Id_niveau           INTEGER NOT NULL ,"
				+ "	num_niveau          TEXT NOT NULL ,"
				+ "	designation_niveau  TEXT ," + "	nbModule_niveau     TEXT ,"
				+ "	nbSection_niveau    TEXT ,"
				+ "	id_filiere_niveau   INTEGER NOT NULL ,"
				+ "	PRIMARY KEY (Id_niveau) ,"
				+ "	FOREIGN KEY (id_filiere_niveau) REFERENCES filiere(Id_filiere));";

		String enseignant = "CREATE TABLE IF NOT EXISTS enseignant("
				+ " Idensei         INTEGER  NOT NULL ,"
				+ " ncin            TEXT NOT NULL ,"
				+ " nom             TEXT NOT NULL ,"
				+ " prenom          TEXT NOT NULL ,"
				+ " sexe            TEXT NOT NULL ,"
				+ " dateNaiss       TEXT ,"
				+ " lieuNaiss       TEXT ,"
				+ " numTel          TEXT ,"
				+ " email           TEXT ,"
				+ " adresse         TEXT ,"
				+ " ville           TEXT ,"
				+ " codePost        TEXT ,"
				+ " grade           TEXT NOT NULL ,"
				+ " diplome         TEXT ,"
				+ " lieuDiplome     TEXT ,"
				+ " situationCivil  TEXT ,"
				+ " titulaire       TEXT ,"
				+ " factAdmin       TEXT ,"
				+ " encadrement     TEXT NOT NULL ,"
				+ " annee_diplome   TEXT ,"
				+ " enDetachement   TEXT ,"
				+ " matricule       TEXT NOT NULL ,"
				+ " specialite      TEXT ,"
				+ " id_departement_enseignant INTEGER NOT NULL ,"
				+ " PRIMARY KEY (Idensei) ,"
				+ " FOREIGN KEY (id_departement_enseignant) REFERENCES departement(Id_departement));";

		String departement = "CREATE TABLE IF NOT EXISTS departement("
				+ "	Id_departement           INTEGER  NOT NULL ,"
				+ "	libelle_departement      TEXT NOT NULL ,"
				+ "	abreviation_departement  TEXT ,"
				+ "	ordre_departement        TEXT ,"
				+ "	PRIMARY KEY (Id_departement));";

		String soutenance = "CREATE TABLE IF NOT EXISTS soutenance("
				+ "	Id_soutenance        INTEGER  NOT NULL ,"
				+ "	type_soutenance      TEXT NOT NULL ,"
				+ "	heureDeb_soutenance  TEXT NOT NULL ,"
				+ "	heureFin_soutenance  TEXT NOT NULL ,"
				+ "	jour_soutenance      TEXT NOT NULL ,"
				+ "	id_salle_soutenance  INTEGER NOT NULL ,"
				+ "	id_travailPfe_soutenance INTEGER NOT NULL ,"
				+ "	PRIMARY KEY (Id_soutenance) ,"
				+ "	FOREIGN KEY (id_salle_soutenance) REFERENCES salle(Id_salle),"
				+ "	FOREIGN KEY (id_travailPfe_soutenance) REFERENCES travail_pfe(Id_travailPfe));";

		String salle = "CREATE TABLE IF NOT EXISTS salle("
				+ "	Id_salle            INTEGER NOT NULL ,"
				+ "	bloc_salle          TEXT NOT NULL ,"
				+ "	etage_salle         TEXT NOT NULL ,"
				+ "	abreviation_salle   TEXT ," + "	type_salle          TEXT ,"
				+ "	capacite_salle      TEXT ," + "	etat_salle          TEXT ,"
				+ "	videoProject_salle  TEXT ," + "	informatique_salle  TEXT ,"
				+ "	PRIMARY KEY (Id_salle));";

		String travailPfe = "CREATE TABLE IF NOT EXISTS travail_pfe("
				+ "	Id_travailPfe       INTEGER NOT NULL ,"
				+ "	libelle_travailPfe  TEXT NOT NULL ,"
				+ "	idensei_travailPfe  INTEGER ,"
				+ "	id_soutenance_travailPfe INTEGER ,"
				+ "	PRIMARY KEY (Id_travailPfe) ,"
				+ "	FOREIGN KEY (idensei_travailPfe) REFERENCES enseignant(Idensei),"
				+ "	FOREIGN KEY (id_soutenance_travailPfe) REFERENCES soutenance(Id_soutenance));";

		String plantAffaire = "CREATE TABLE IF NOT EXISTS plant_affaire("
				+ "	Id_plantAffaire  INTEGER NOT NULL ,"
				+ "	id_travailPfe_plantAffaire    INTEGER NOT NULL ,"
				+ "	PRIMARY KEY (Id_plantAffaire,id_travailPfe_plantAffaire) ,"
				+ "	FOREIGN KEY (id_travailPfe_plantAffaire) REFERENCES travail_pfe(Id_travailPfe));";

		String projet = "CREATE TABLE IF NOT EXISTS projet("
				+ "	Id_projet      INTEGER NOT NULL ,"
				+ "	id_travailPfe_projet  INTEGER NOT NULL ,"
				+ "	PRIMARY KEY (Id_projet,id_travailPfe_projet) ,"
				+ "	FOREIGN KEY (id_travailPfe_projet) REFERENCES travail_pfe(Id_travailPfe));";

		String stage = "CREATE TABLE IF NOT EXISTS stage("
				+ " Id_stage           INTEGER  NOT NULL ,"
				+ " nom_stage          TEXT ,"
				+ " description_stage  TEXT ,"
				+ " date_stage         TEXT NOT NULL ,"
				+ " id_travailPfe_stage      INTEGER NOT NULL ,"
				+ " id_encadreurPro_stage    INTEGER ,"
				+ " id_offreStage_stage      INTEGER  ,"
				+ " id_entreprise_stage      INTEGER NOT NULL ,"
				+ " PRIMARY KEY (Id_stage,id_travailPfe_stage) ,"
				+ " FOREIGN KEY (id_travailPfe_stage) REFERENCES travail_pfe(Id_travailPfe),"
				+ " FOREIGN KEY (id_encadreurPro_stage) REFERENCES encadreur_pro(Id_encadreurPro),"
				+ " FOREIGN KEY (id_offreStage_stage) REFERENCES offre_stage(Id_offreStage),"
				+ " FOREIGN KEY (id_entreprise_stage) REFERENCES entreprise(Id_entreprise));";

		String cycle = "CREATE TABLE IF NOT EXISTS cycle("
				+ "	Id_cycle           INTEGER NOT NULL ,"
				+ "	num_cycle          TEXT NOT NULL ,"
				+ "	designation_cycle  TEXT NOT NULL ,"
				+ "	nbrSem_cycle       TEXT ," +
				"	nbrfiliere_cycle   TEXT ,"
				+ "	PRIMARY KEY (Id_cycle));";

		String document = "CREATE TABLE IF NOT EXISTS document("
				+ " Id_document       INTEGER  NOT NULL ,"
				+ " cv_document       TEXT NOT NULL ,"
				+ " lettreM_document  TEXT NOT NULL ,"
				+ " id_etudiant_document       INTEGER NOT NULL ,"
				+ " PRIMARY KEY (Id_document) ,"
				+ " FOREIGN KEY (id_etudiant_document) REFERENCES etudiant(Id_etudiant));";

		String condidater = "CREATE TABLE IF NOT EXISTS condidater("
				+ " confirmation_condidater  TEXT ,"
				+ " date_condidater          TEXT NOT NULL ,"
				+ " id_etudiant_condidater   INTEGER NOT NULL ,"
				+ " id_offreStage_condidater INTEGER NOT NULL ,"
				+ " PRIMARY KEY (id_etudiant_condidater,id_offreStage_condidater) ,"
				+ " FOREIGN KEY (id_etudiant_condidater) REFERENCES etudiant(Id_etudiant),"
				+ " FOREIGN KEY (id_offreStage_condidater) REFERENCES offre_stage(Id_offreStage));";

		String deriger = "CREATE TABLE IF NOT EXISTS deriger("
				+ "	type_deriger   TEXT NOT NULL ,"
				+ "	idensei_deriger        INTEGER NOT NULL ,"
				+ "	id_soutenance_deriger  INTEGER NOT NULL ,"
				+ "	PRIMARY KEY (idensei_deriger,id_soutenance_deriger) ,"
				+ "	FOREIGN KEY (idensei_deriger) REFERENCES enseignant(Idensei),"
				+ "	FOREIGN KEY (id_soutenance_deriger) REFERENCES soutenance(Id_soutenance));";

		String demande = "CREATE TABLE IF NOT EXISTS demande("
				+ " confirmation_demande  TEXT ,"
				+ " date_demande          TEXT NOT NULL ,"
				+ " idensei_demande       INTEGER NOT NULL ,"
				+ " id_etudiant_demande   INTEGER NOT NULL ,"
				+ " PRIMARY KEY (idensei_demande,id_etudiant_demande) ,"
				+ " FOREIGN KEY (idensei_demande) REFERENCES enseignant(Idensei),"
				+ " FOREIGN KEY (id_etudiant_demande) REFERENCES etudiant(Id_etudiant));";

		db.execSQL(entreprise);
		db.execSQL(encadreurPro);
		db.execSQL(offreStage);
		db.execSQL(etudiant);
		db.execSQL(filiere);
		db.execSQL(niveau);
		db.execSQL(enseignant);
		db.execSQL(departement);
		db.execSQL(soutenance);
		db.execSQL(salle);
		db.execSQL(travailPfe);
		db.execSQL(plantAffaire);
		db.execSQL(projet);
		db.execSQL(stage);
		db.execSQL(cycle);
		db.execSQL(document);
		db.execSQL(condidater);
		db.execSQL(deriger);
		db.execSQL(demande);
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {

	}

}
