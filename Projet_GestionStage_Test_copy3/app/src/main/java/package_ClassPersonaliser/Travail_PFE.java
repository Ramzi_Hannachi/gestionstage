package package_ClassPersonaliser;

public class Travail_PFE 
{

	private int ID_TravailPFE;
	private String libelle_TravailPFE;
	private Integer cle_enseignant_TravailPFE;
	private Integer cle_soutenance_TravailPFE;

	public Travail_PFE(int iD_TravailPFE, String libelle_TravailPFE,
			Integer cle_enseignant_TravailPFE, Integer cle_soutenance_TravailPFE) {
		super();
		ID_TravailPFE = iD_TravailPFE;
		this.libelle_TravailPFE = libelle_TravailPFE;
		this.cle_enseignant_TravailPFE = cle_enseignant_TravailPFE;
		this.cle_soutenance_TravailPFE = cle_soutenance_TravailPFE;
	}


	public int getID_TravailPFE() {
		return ID_TravailPFE;
	}

	public String getLibelle_TravailPFE() {
		return libelle_TravailPFE;
	}

	public Integer getCle_enseignant_TravailPFE() {
		return cle_enseignant_TravailPFE;
	}

	public Integer getCle_soutenance_TravailPFE() {
		return cle_soutenance_TravailPFE;
	}

	public void setID_TravailPFE(int iD_TravailPFE) {
		ID_TravailPFE = iD_TravailPFE;
	}

	public void setLibelle_TravailPFE(String libelle_TravailPFE) {
		this.libelle_TravailPFE = libelle_TravailPFE;
	}

	public void setCle_enseignant_TravailPFE(Integer cle_enseignant_TravailPFE) {
		this.cle_enseignant_TravailPFE = cle_enseignant_TravailPFE;
	}

	public void setCle_soutenance_TravailPFE(Integer cle_soutenance_TravailPFE) {
		this.cle_soutenance_TravailPFE = cle_soutenance_TravailPFE;
	}



}
