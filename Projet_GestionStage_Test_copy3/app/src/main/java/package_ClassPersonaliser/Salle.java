package package_ClassPersonaliser;

public class Salle
{
	int Id_Salle;
	String bloc_Salle;
	String etage_Salle;
	String abreviation_Salle;
	String type_Salle;
	String capacite_Salle;
	String etat_Salle;
	String videoProject_Salle;
	String informatique_Salle;

	public Salle(int id_Salle, String bloc_Salle, String etage_Salle,
			String abreviation_Salle, String type_Salle, String capacite_Salle,
			String etat_Salle, String videoProject_Salle,
			String informatique_Salle) 
	{
		super();
		Id_Salle = id_Salle;
		this.bloc_Salle = bloc_Salle;
		this.etage_Salle = etage_Salle;
		this.abreviation_Salle = abreviation_Salle;
		this.type_Salle = type_Salle;
		this.capacite_Salle = capacite_Salle;
		this.etat_Salle = etat_Salle;
		this.videoProject_Salle = videoProject_Salle;
		this.informatique_Salle = informatique_Salle;
	}

	public int getId_Salle() {
		return Id_Salle;
	}

	public String getBloc_Salle() {
		return bloc_Salle;
	}

	public String getEtage_Salle() {
		return etage_Salle;
	}

	public String getAbreviation_Salle() {
		return abreviation_Salle;
	}

	public String getType_Salle() {
		return type_Salle;
	}

	public String getCapacite_Salle() {
		return capacite_Salle;
	}

	public String getEtat_Salle() {
		return etat_Salle;
	}

	public String getVideoProject_Salle() {
		return videoProject_Salle;
	}

	public String getInformatique_Salle() {
		return informatique_Salle;
	}

	public void setId_Salle(int id_Salle) {
		Id_Salle = id_Salle;
	}

	public void setBloc_Salle(String bloc_Salle) {
		this.bloc_Salle = bloc_Salle;
	}

	public void setEtage_Salle(String etage_Salle) {
		this.etage_Salle = etage_Salle;
	}

	public void setAbreviation_Salle(String abreviation_Salle) {
		this.abreviation_Salle = abreviation_Salle;
	}

	public void setType_Salle(String type_Salle) {
		this.type_Salle = type_Salle;
	}

	public void setCapacite_Salle(String capacite_Salle) {
		this.capacite_Salle = capacite_Salle;
	}

	public void setEtat_Salle(String etat_Salle) {
		this.etat_Salle = etat_Salle;
	}

	public void setVideoProject_Salle(String videoProject_Salle) {
		this.videoProject_Salle = videoProject_Salle;
	}

	public void setInformatique_Salle(String informatique_Salle) {
		this.informatique_Salle = informatique_Salle;
	}




}
