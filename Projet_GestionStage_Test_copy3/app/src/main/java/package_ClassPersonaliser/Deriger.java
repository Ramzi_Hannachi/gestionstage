package package_ClassPersonaliser;

public class Deriger 
{
	String type_deriger;
	String IdEnseignant_deriger;
	String IdSoutenance_deriger;
	
	
	public Deriger(String type_deriger, String idEnseignant_deriger,
			String idSoutenance_deriger) 
	{
		super();
		this.type_deriger = type_deriger;
		IdEnseignant_deriger = idEnseignant_deriger;
		IdSoutenance_deriger = idSoutenance_deriger;
	}


	public String getType_deriger() {
		return type_deriger;
	}


	public String getIdEnseignant_deriger() {
		return IdEnseignant_deriger;
	}


	public String getIdSoutenance_deriger() {
		return IdSoutenance_deriger;
	}


	public void setType_deriger(String type_deriger) {
		this.type_deriger = type_deriger;
	}


	public void setIdEnseignant_deriger(String idEnseignant_deriger) {
		IdEnseignant_deriger = idEnseignant_deriger;
	}


	public void setIdSoutenance_deriger(String idSoutenance_deriger) {
		IdSoutenance_deriger = idSoutenance_deriger;
	}
	
	
}
