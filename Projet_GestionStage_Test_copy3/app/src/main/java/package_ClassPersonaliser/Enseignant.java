package package_ClassPersonaliser;


public class Enseignant 
{
	private	int id_enseignant;
	private String cin_enseignant;
	private String nom_enseignant;
	private String prenom_enseignant;
	private String sexe_enseignant;
	private String dateNaiss_enseignant;
	private String lieuNaiss_enseignant;
	private String numTel_enseignant;
	private String email_enseignant;
	private String adresse_enseignant;
	private String ville;
	private String codePostale;
	private String grade;
	private String diplome;
	private String lieu_diplome;
	private String situation_civil;
	private String titulaire;
	private String factAdmin;
	private String encadrement;
	private String annee_diplome;
	private String enDetachement;
	private String matricule;
	private String specialite;
	private int id_departement;

	public Enseignant(int id_enseignant, String cin_enseignant,
			String nom_enseignant, String prenom_enseignant,
			String sexe_enseignant, String dateNaiss_enseignant,
			String lieuNaiss_enseignant, String numTel_enseignant,
			String email_enseignant, String adresse_enseignant, String ville,
			String codePostale, String grade, String diplome,
			String lieu_diplome, String situation_civil, String titulaire,
			String factAdmin, String encadrement, String annee_diplome,
			String enDetachement, String matricule, String specialite,
			int id_departement) {
		super();
		this.id_enseignant = id_enseignant;
		this.cin_enseignant = cin_enseignant;
		this.nom_enseignant = nom_enseignant;
		this.prenom_enseignant = prenom_enseignant;
		this.sexe_enseignant = sexe_enseignant;
		this.dateNaiss_enseignant = dateNaiss_enseignant;
		this.lieuNaiss_enseignant = lieuNaiss_enseignant;
		this.numTel_enseignant = numTel_enseignant;
		this.email_enseignant = email_enseignant;
		this.adresse_enseignant = adresse_enseignant;
		this.ville = ville;
		this.codePostale = codePostale;
		this.grade = grade;
		this.diplome = diplome;
		this.lieu_diplome = lieu_diplome;
		this.situation_civil = situation_civil;
		this.titulaire = titulaire;
		this.factAdmin = factAdmin;
		this.encadrement = encadrement;
		this.annee_diplome = annee_diplome;
		this.enDetachement = enDetachement;
		this.matricule = matricule;
		this.specialite = specialite;
		this.id_departement = id_departement;
	}

	public int getId_enseignant() {
		return id_enseignant;
	}

	public String getCin_enseignant() {
		return cin_enseignant;
	}

	public String getNom_enseignant() {
		return nom_enseignant;
	}

	public String getPrenom_enseignant() {
		return prenom_enseignant;
	}

	public String getSexe_enseignant() {
		return sexe_enseignant;
	}

	public String getDateNaiss_enseignant() {
		return dateNaiss_enseignant;
	}

	public String getLieuNaiss_enseignant() {
		return lieuNaiss_enseignant;
	}

	public String getNumTel_enseignant() {
		return numTel_enseignant;
	}

	public String getEmail_enseignant() {
		return email_enseignant;
	}

	public String getAdresse_enseignant() {
		return adresse_enseignant;
	}

	public String getVille() {
		return ville;
	}

	public String getCodePostale() {
		return codePostale;
	}

	public String getGrade() {
		return grade;
	}

	public String getDiplome() {
		return diplome;
	}

	public String getLieu_diplome() {
		return lieu_diplome;
	}

	public String getSituation_civil() {
		return situation_civil;
	}

	public String getTitulaire() {
		return titulaire;
	}

	public String getFactAdmin() {
		return factAdmin;
	}

	public String getEncadrement() {
		return encadrement;
	}

	public String getAnnee_diplome() {
		return annee_diplome;
	}

	public String getEnDetachement() {
		return enDetachement;
	}

	public String getMatricule() {
		return matricule;
	}

	public String getSpecialite() {
		return specialite;
	}

	public int getId_departement() {
		return id_departement;
	}

	public void setId_enseignant(int id_enseignant) {
		this.id_enseignant = id_enseignant;
	}

	public void setCin_enseignant(String cin_enseignant) {
		this.cin_enseignant = cin_enseignant;
	}

	public void setNom_enseignant(String nom_enseignant) {
		this.nom_enseignant = nom_enseignant;
	}

	public void setPrenom_enseignant(String prenom_enseignant) {
		this.prenom_enseignant = prenom_enseignant;
	}

	public void setSexe_enseignant(String sexe_enseignant) {
		this.sexe_enseignant = sexe_enseignant;
	}

	public void setDateNaiss_enseignant(String dateNaiss_enseignant) {
		this.dateNaiss_enseignant = dateNaiss_enseignant;
	}

	public void setLieuNaiss_enseignant(String lieuNaiss_enseignant) {
		this.lieuNaiss_enseignant = lieuNaiss_enseignant;
	}

	public void setNumTel_enseignant(String numTel_enseignant) {
		this.numTel_enseignant = numTel_enseignant;
	}

	public void setEmail_enseignant(String email_enseignant) {
		this.email_enseignant = email_enseignant;
	}

	public void setAdresse_enseignant(String adresse_enseignant) {
		this.adresse_enseignant = adresse_enseignant;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public void setCodePostale(String codePostale) {
		this.codePostale = codePostale;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public void setDiplome(String diplome) {
		this.diplome = diplome;
	}

	public void setLieu_diplome(String lieu_diplome) {
		this.lieu_diplome = lieu_diplome;
	}

	public void setSituation_civil(String situation_civil) {
		this.situation_civil = situation_civil;
	}

	public void setTitulaire(String titulaire) {
		this.titulaire = titulaire;
	}

	public void setFactAdmin(String factAdmin) {
		this.factAdmin = factAdmin;
	}

	public void setEncadrement(String encadrement) {
		this.encadrement = encadrement;
	}

	public void setAnnee_diplome(String annee_diplome) {
		this.annee_diplome = annee_diplome;
	}

	public void setEnDetachement(String enDetachement) {
		this.enDetachement = enDetachement;
	}

	public void setMatricule(String matricule) {
		this.matricule = matricule;
	}

	public void setSpecialite(String specialite) {
		this.specialite = specialite;
	}

	public void setId_departement(int id_departement) {
		this.id_departement = id_departement;
	}



}
