package package_ClassPersonaliser;

public class Soutenance
{
	int _ID_Soutenance;
	String _Type_Soutenance;
	String _HeureDeb_Soutenance;
	String _HeureFin_Soutenance;
	String _Date_Soutenance;
	int _IdSalle_Soutenance;
	int _IdTravailPfe_Soutenance;

	public Soutenance(int _ID_Soutenance, String _Type_Soutenance,
			String _HeureDeb_Soutenance, String _HeureFin_Soutenance,
			String _Date_Soutenance, int _IdSalle_Soutenance,
			int _IdTravailPfe_Soutenance)
	{
		super();
		this._ID_Soutenance = _ID_Soutenance;
		this._Type_Soutenance = _Type_Soutenance;
		this._HeureDeb_Soutenance = _HeureDeb_Soutenance;
		this._HeureFin_Soutenance = _HeureFin_Soutenance;
		this._Date_Soutenance = _Date_Soutenance;
		this._IdSalle_Soutenance = _IdSalle_Soutenance;
		this._IdTravailPfe_Soutenance = _IdTravailPfe_Soutenance;
	}

	public int get_ID_Soutenance() {
		return _ID_Soutenance;
	}

	public String get_Type_Soutenance() {
		return _Type_Soutenance;
	}

	public String get_HeureDeb_Soutenance() {
		return _HeureDeb_Soutenance;
	}

	public String get_HeureFin_Soutenance() {
		return _HeureFin_Soutenance;
	}

	public String get_Date_Soutenance() {
		return _Date_Soutenance;
	}

	public int get_IdSalle_Soutenance() {
		return _IdSalle_Soutenance;
	}

	public int get_IdTravailPfe_Soutenance() {
		return _IdTravailPfe_Soutenance;
	}

	public void set_ID_Soutenance(int _ID_Soutenance) {
		this._ID_Soutenance = _ID_Soutenance;
	}

	public void set_Type_Soutenance(String _Type_Soutenance) {
		this._Type_Soutenance = _Type_Soutenance;
	}

	public void set_HeureDeb_Soutenance(String _HeureDeb_Soutenance) {
		this._HeureDeb_Soutenance = _HeureDeb_Soutenance;
	}

	public void set_HeureFin_Soutenance(String _HeureFin_Soutenance) {
		this._HeureFin_Soutenance = _HeureFin_Soutenance;
	}

	public void set_Date_Soutenance(String _Date_Soutenance) {
		this._Date_Soutenance = _Date_Soutenance;
	}

	public void set_IdSalle_Soutenance(int _IdSalle_Soutenance) {
		this._IdSalle_Soutenance = _IdSalle_Soutenance;
	}

	public void set_IdTravailPfe_Soutenance(int _IdTravailPfe_Soutenance) {
		this._IdTravailPfe_Soutenance = _IdTravailPfe_Soutenance;
	}



}
