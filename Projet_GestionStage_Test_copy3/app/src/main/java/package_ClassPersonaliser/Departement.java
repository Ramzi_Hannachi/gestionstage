package package_ClassPersonaliser;

public class Departement 
{
	private int Id_departement;
	private String libelle_Departement;
	private String abreviation_Departement;
	private String ordre_Departement;
	
	public Departement(int id_departement, String libelle_Departement,
			String abreviation_Departement, String ordre_Departement) {
		super();
		Id_departement = id_departement;
		this.libelle_Departement = libelle_Departement;
		this.abreviation_Departement = abreviation_Departement;
		this.ordre_Departement = ordre_Departement;
	}

	public int getId_departement() {
		return Id_departement;
	}

	public String getLibelle_Departement() {
		return libelle_Departement;
	}

	public String getAbreviation_Departement() {
		return abreviation_Departement;
	}

	public String getOrdre_Departement() {
		return ordre_Departement;
	}

	public void setId_departement(int id_departement) {
		Id_departement = id_departement;
	}

	public void setLibelle_Departement(String libelle_Departement) {
		this.libelle_Departement = libelle_Departement;
	}

	public void setAbreviation_Departement(String abreviation_Departement) {
		this.abreviation_Departement = abreviation_Departement;
	}

	public void setOrdre_Departement(String ordre_Departement) {
		this.ordre_Departement = ordre_Departement;
	}

	
	
	
}
