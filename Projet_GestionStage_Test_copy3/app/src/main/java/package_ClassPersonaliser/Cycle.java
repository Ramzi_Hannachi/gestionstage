package package_ClassPersonaliser;

public class Cycle
{
	int Id_cycle;
	String num_cycle;
	String designation_cycle;
	String nbrSem_cycle;
	String nbrFiliere_cycle;

	public Cycle(int id_cycle, String num_cycle, String designation_cycle,
			String nbrSem_cycle, String nbrFiliere_cycle) {
		super();
		Id_cycle = id_cycle;
		this.num_cycle = num_cycle;
		this.designation_cycle = designation_cycle;
		this.nbrSem_cycle = nbrSem_cycle;
		this.nbrFiliere_cycle = nbrFiliere_cycle;
	}

	public int getId_cycle() {
		return Id_cycle;
	}

	public String getNum_cycle() {
		return num_cycle;
	}

	public String getDesignation_cycle() {
		return designation_cycle;
	}

	public String getNbrSem_cycle() {
		return nbrSem_cycle;
	}

	public String getNbrFiliere_cycle() {
		return nbrFiliere_cycle;
	}

	public void setId_cycle(int id_cycle) {
		Id_cycle = id_cycle;
	}

	public void setNum_cycle(String num_cycle) {
		this.num_cycle = num_cycle;
	}

	public void setDesignation_cycle(String designation_cycle) {
		this.designation_cycle = designation_cycle;
	}

	public void setNbrSem_cycle(String nbrSem_cycle) {
		this.nbrSem_cycle = nbrSem_cycle;
	}

	public void setNbrFiliere_cycle(String nbrFiliere_cycle) {
		this.nbrFiliere_cycle = nbrFiliere_cycle;
	}

}
