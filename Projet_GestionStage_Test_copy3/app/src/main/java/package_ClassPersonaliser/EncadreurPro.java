package package_ClassPersonaliser;

public class EncadreurPro
{
	private int id_encadreurPro;
	private String cin_encadreurPro;
	private String nom_encadreurPro;
	private String prenom_encadreurPro;
	private String numTel_encadreurPro;
	private String email_encadreurPro;
	public EncadreurPro(int id_encadreurPro, String cin_encadreurPro,
			String nom_encadreurPro, String prenom_encadreurPro,
			String numTel_encadreurPro, String email_encadreurPro) {
		super();
		this.id_encadreurPro = id_encadreurPro;
		this.cin_encadreurPro = cin_encadreurPro;
		this.nom_encadreurPro = nom_encadreurPro;
		this.prenom_encadreurPro = prenom_encadreurPro;
		this.numTel_encadreurPro = numTel_encadreurPro;
		this.email_encadreurPro = email_encadreurPro;
	}
	public int getId_encadreurPro() {
		return id_encadreurPro;
	}
	public String getCin_encadreurPro() {
		return cin_encadreurPro;
	}
	public String getNom_encadreurPro() {
		return nom_encadreurPro;
	}
	public String getPrenom_encadreurPro() {
		return prenom_encadreurPro;
	}
	public String getNumTel_encadreurPro() {
		return numTel_encadreurPro;
	}
	public String getEmail_encadreurPro() {
		return email_encadreurPro;
	}
	public void setId_encadreurPro(int id_encadreurPro) {
		this.id_encadreurPro = id_encadreurPro;
	}
	public void setCin_encadreurPro(String cin_encadreurPro) {
		this.cin_encadreurPro = cin_encadreurPro;
	}
	public void setNom_encadreurPro(String nom_encadreurPro) {
		this.nom_encadreurPro = nom_encadreurPro;
	}
	public void setPrenom_encadreurPro(String prenom_encadreurPro) {
		this.prenom_encadreurPro = prenom_encadreurPro;
	}
	public void setNumTel_encadreurPro(String numTel_encadreurPro) {
		this.numTel_encadreurPro = numTel_encadreurPro;
	}
	public void setEmail_encadreurPro(String email_encadreurPro) {
		this.email_encadreurPro = email_encadreurPro;
	}
	
	

}
