package package_ClassPersonaliser;


public class OffreStage
{
	private int id_offreStage;
	private String nom_offreStage;
	private String description_offreStage;
	private String confirmation_offreStage;
	private String date_offreStage;
	private Integer cle_entreprise_offreStage;
	private Integer  cle_stage_offreStage;
	private Integer cle_travailPfe_offreStage;
	private Integer cle_filiere_offreStage;
	public OffreStage(int id_offreStage, String nom_offreStage,
			String description_offreStage, String confirmation_offreStage,
			String date_offreStage, Integer cle_entreprise_offreStage,
			Integer cle_stage_offreStage, Integer cle_travailPfe_offreStage,
			Integer cle_filiere_offreStage) {
		super();
		this.id_offreStage = id_offreStage;
		this.nom_offreStage = nom_offreStage;
		this.description_offreStage = description_offreStage;
		this.confirmation_offreStage = confirmation_offreStage;
		this.date_offreStage = date_offreStage;
		this.cle_entreprise_offreStage = cle_entreprise_offreStage;
		this.cle_stage_offreStage = cle_stage_offreStage;
		this.cle_travailPfe_offreStage = cle_travailPfe_offreStage;
		this.cle_filiere_offreStage = cle_filiere_offreStage;
	}
	public int getId_offreStage() {
		return id_offreStage;
	}
	public String getNom_offreStage() {
		return nom_offreStage;
	}
	public String getDescription_offreStage() {
		return description_offreStage;
	}
	public String getConfirmation_offreStage() {
		return confirmation_offreStage;
	}
	public String getDate_offreStage() {
		return date_offreStage;
	}
	public Integer getCle_entreprise_offreStage() {
		return cle_entreprise_offreStage;
	}
	public Integer getCle_stage_offreStage() {
		return cle_stage_offreStage;
	}
	public Integer getCle_travailPfe_offreStage() {
		return cle_travailPfe_offreStage;
	}
	public Integer getCle_filiere_offreStage() {
		return cle_filiere_offreStage;
	}
	public void setId_offreStage(int id_offreStage) {
		this.id_offreStage = id_offreStage;
	}
	public void setNom_offreStage(String nom_offreStage) {
		this.nom_offreStage = nom_offreStage;
	}
	public void setDescription_offreStage(String description_offreStage) {
		this.description_offreStage = description_offreStage;
	}
	public void setConfirmation_offreStage(String confirmation_offreStage) {
		this.confirmation_offreStage = confirmation_offreStage;
	}
	public void setDate_offreStage(String date_offreStage) {
		this.date_offreStage = date_offreStage;
	}
	public void setCle_entreprise_offreStage(Integer cle_entreprise_offreStage) {
		this.cle_entreprise_offreStage = cle_entreprise_offreStage;
	}
	public void setCle_stage_offreStage(Integer cle_stage_offreStage) {
		this.cle_stage_offreStage = cle_stage_offreStage;
	}
	public void setCle_travailPfe_offreStage(Integer cle_travailPfe_offreStage) {
		this.cle_travailPfe_offreStage = cle_travailPfe_offreStage;
	}
	public void setCle_filiere_offreStage(Integer cle_filiere_offreStage) {
		this.cle_filiere_offreStage = cle_filiere_offreStage;
	}
	
	
	
}
