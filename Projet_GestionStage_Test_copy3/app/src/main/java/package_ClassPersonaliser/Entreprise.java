package package_ClassPersonaliser;

public class Entreprise
{
	private int id_entreprise;
	private String libelle_entreprise;
	private String raison_entreprise;
	private String numTel_entreprise;
	private String email_entreprise;
	private String siteWeb_entreprise;
	private String login_entreprise;
	private String password_entreprise;
	private String logo_entreprise;
	private Integer cle_enseignant_entreprise;

	public Entreprise(int id_entreprise, String libelle_entreprise,
			String raison_entreprise, String numTel_entreprise,
			String email_entreprise, String siteWeb_entreprise,
			String login_entreprise, String password_entreprise,
			String logo_entreprise, Integer cle_enseignant_entreprise) {
		super();
		this.id_entreprise = id_entreprise;
		this.libelle_entreprise = libelle_entreprise;
		this.raison_entreprise = raison_entreprise;
		this.numTel_entreprise = numTel_entreprise;
		this.email_entreprise = email_entreprise;
		this.siteWeb_entreprise = siteWeb_entreprise;
		this.login_entreprise = login_entreprise;
		this.password_entreprise = password_entreprise;
		this.logo_entreprise = logo_entreprise;
		this.cle_enseignant_entreprise = cle_enseignant_entreprise;
	}
	public int getId_entreprise() {
		return id_entreprise;
	}
	public String getLibelle_entreprise() {
		return libelle_entreprise;
	}
	public String getRaison_entreprise() {
		return raison_entreprise;
	}
	public String getNumTel_entreprise() {
		return numTel_entreprise;
	}
	public String getEmail_entreprise() {
		return email_entreprise;
	}
	public String getSiteWeb_entreprise() {
		return siteWeb_entreprise;
	}
	public String getLogin_entreprise() {
		return login_entreprise;
	}
	public String getPassword_entreprise() {
		return password_entreprise;
	}
	public String getLogo_entreprise() {
		return logo_entreprise;
	}
	public Integer getCle_enseignant_entreprise() {
		return cle_enseignant_entreprise;
	}
	public void setId_entreprise(int id_entreprise) {
		this.id_entreprise = id_entreprise;
	}
	public void setLibelle_entreprise(String libelle_entreprise) {
		this.libelle_entreprise = libelle_entreprise;
	}
	public void setRaison_entreprise(String raison_entreprise) {
		this.raison_entreprise = raison_entreprise;
	}
	public void setNumTel_entreprise(String numTel_entreprise) {
		this.numTel_entreprise = numTel_entreprise;
	}
	public void setEmail_entreprise(String email_entreprise) {
		this.email_entreprise = email_entreprise;
	}
	public void setSiteWeb_entreprise(String siteWeb_entreprise) {
		this.siteWeb_entreprise = siteWeb_entreprise;
	}
	public void setLogin_entreprise(String login_entreprise) {
		this.login_entreprise = login_entreprise;
	}
	public void setPassword_entreprise(String password_entreprise) {
		this.password_entreprise = password_entreprise;
	}
	public void setLogo_entreprise(String logo_entreprise) {
		this.logo_entreprise = logo_entreprise;
	}
	public void setCle_enseignant_entreprise(Integer cle_enseignant_entreprise) {
		this.cle_enseignant_entreprise = cle_enseignant_entreprise;
	}


}
