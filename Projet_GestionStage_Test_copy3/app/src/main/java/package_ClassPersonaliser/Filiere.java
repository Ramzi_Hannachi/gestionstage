package package_ClassPersonaliser;

public class Filiere 
{
	 private int id_filiere;
	 private String designation_filiere;
	 private Integer cle_cycle_filiere;
	 
	public Filiere(int id_filiere, String designation_filiere,
			Integer cle_cycle_filiere) {
		super();
		this.id_filiere = id_filiere;
		this.designation_filiere = designation_filiere;
		this.cle_cycle_filiere = cle_cycle_filiere;
	}
	public int getId_filiere() {
		return id_filiere;
	}
	public String getDesignation_filiere() {
		return designation_filiere;
	}
	public Integer getCle_cycle_filiere() {
		return cle_cycle_filiere;
	}
	public void setId_filiere(int id_filiere) {
		this.id_filiere = id_filiere;
	}
	public void setDesignation_filiere(String designation_filiere) {
		this.designation_filiere = designation_filiere;
	}
	public void setCle_cycle_filiere(Integer cle_cycle_filiere) {
		this.cle_cycle_filiere = cle_cycle_filiere;
	}
	
	
}
