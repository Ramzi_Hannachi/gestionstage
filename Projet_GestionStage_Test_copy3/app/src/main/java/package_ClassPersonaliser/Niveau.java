package package_ClassPersonaliser;

public class Niveau 
{
	private int id_niveau;
	private String num_niveau;
	private String designation_niveau;
	private String nbModule_niveau;
	private String nbSection_niveau;
	private Integer cle_filiere_niveau;
	
	public Niveau(int id_niveau, String num_niveau,
			String designation_niveau, String nbModule_niveau,
			String nbSection_niveau, Integer cle_filiere_niveau) {
		super();
		this.id_niveau = id_niveau;
		this.num_niveau = num_niveau;
		this.designation_niveau = designation_niveau;
		this.nbModule_niveau = nbModule_niveau;
		this.nbSection_niveau = nbSection_niveau;
		this.cle_filiere_niveau = cle_filiere_niveau;
	}

	public int getId_niveau() {
		return id_niveau;
	}

	public String getNum_niveau() {
		return num_niveau;
	}

	public String getDesignation_niveau() {
		return designation_niveau;
	}

	public String getNbModule_niveau() {
		return nbModule_niveau;
	}

	public String getNbSection_niveau() {
		return nbSection_niveau;
	}

	public Integer getCle_filiere_niveau() {
		return cle_filiere_niveau;
	}

	public void setId_niveau(int id_niveau) {
		this.id_niveau = id_niveau;
	}

	public void setNum_niveau(String num_niveau) {
		this.num_niveau = num_niveau;
	}

	public void setDesignation_niveau(String designation_niveau) {
		this.designation_niveau = designation_niveau;
	}

	public void setNbModule_niveau(String nbModule_niveau) {
		this.nbModule_niveau = nbModule_niveau;
	}

	public void setNbSection_niveau(String nbSection_niveau) {
		this.nbSection_niveau = nbSection_niveau;
	}

	public void setCle_filiere_niveau(Integer cle_filiere_niveau) {
		this.cle_filiere_niveau = cle_filiere_niveau;
	}
	
	
	
	
}
