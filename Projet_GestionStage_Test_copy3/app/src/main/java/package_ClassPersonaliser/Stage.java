package package_ClassPersonaliser;


public class Stage 
{
	private int id_stage;
	private String nom_stage;
	private String description_stage;
	private String date_stage;
	private Integer cle_travailPfe_stage;
	private Integer cle_encadreurPro_stage;
	private Integer cle_offreStage_stage;
	private Integer cle_entreprise_stage;
	
	public Stage(int id_stage, String nom_stage, String description_stage,
			String date_stage, Integer cle_travailPfe_stage,
			Integer cle_encadreurPro_stage, Integer cle_offreStage_stage,
			Integer cle_entreprise_stage)
	{
		super();
		this.id_stage = id_stage;
		this.nom_stage = nom_stage;
		this.description_stage = description_stage;
		this.date_stage = date_stage;
		this.cle_travailPfe_stage = cle_travailPfe_stage;
		this.cle_encadreurPro_stage = cle_encadreurPro_stage;
		this.cle_offreStage_stage = cle_offreStage_stage;
		this.cle_entreprise_stage = cle_entreprise_stage;
	}

	public int getId_stage() {
		return id_stage;
	}

	public String getNom_stage() {
		return nom_stage;
	}

	public String getDescription_stage() {
		return description_stage;
	}

	public String getDate_stage() {
		return date_stage;
	}

	public Integer getCle_travailPfe_stage() {
		return cle_travailPfe_stage;
	}

	public Integer getCle_encadreurPro_stage() {
		return cle_encadreurPro_stage;
	}

	public Integer getCle_offreStage_stage() {
		return cle_offreStage_stage;
	}

	public Integer getCle_entreprise_stage() {
		return cle_entreprise_stage;
	}

	public void setId_stage(int id_stage) {
		this.id_stage = id_stage;
	}

	public void setNom_stage(String nom_stage) {
		this.nom_stage = nom_stage;
	}

	public void setDescription_stage(String description_stage) {
		this.description_stage = description_stage;
	}

	public void setDate_stage(String date_stage) {
		this.date_stage = date_stage;
	}

	public void setCle_travailPfe_stage(Integer cle_travailPfe_stage) {
		this.cle_travailPfe_stage = cle_travailPfe_stage;
	}

	public void setCle_encadreurPro_stage(Integer cle_encadreurPro_stage) {
		this.cle_encadreurPro_stage = cle_encadreurPro_stage;
	}

	public void setCle_offreStage_stage(Integer cle_offreStage_stage) {
		this.cle_offreStage_stage = cle_offreStage_stage;
	}

	public void setCle_entreprise_stage(Integer cle_entreprise_stage) {
		this.cle_entreprise_stage = cle_entreprise_stage;
	}
	
	
	
}
