package package_ClassPersonaliser;


public class Etudiant 
{
	private int id_etudiant;
	private String num_inscrit;
	private String cin_etudiant;
	private String nom_etudiant;
	private String prenom_etudiant;
	private String sexe_etudiant;
	private String dateNaiss_etudiant;
	private String lieuNaiss_etudiant;
	private String numTel_etudiant;
	private String email_etudiant;
	private String adresse_etudiant;
	private String codePostal_etudiant;
	private String image_etudiant;
	private Integer cle_departement_etudiant;
	private Integer cle_travailPfe_etudiant;
	private Integer cle_etudiant1_etudiant;
	private Integer cle_filiere_etudiant;
	private Integer cle_document_etudiant;
	public Etudiant(int id_etudiant, String num_inscrit, String cin_etudiant,
			String nom_etudiant, String prenom_etudiant, String sexe_etudiant,
			String dateNaiss_etudiant, String lieuNaiss_etudiant,
			String numTel_etudiant, String email_etudiant,
			String adresse_etudiant, String codePostal_etudiant,
			String image_etudiant, Integer cle_departement_etudiant,
			Integer cle_travailPfe_etudiant, Integer cle_etudiant1_etudiant,
			Integer cle_filiere_etudiant, Integer cle_document_etudiant) {
		super();
		this.id_etudiant = id_etudiant;
		this.num_inscrit = num_inscrit;
		this.cin_etudiant = cin_etudiant;
		this.nom_etudiant = nom_etudiant;
		this.prenom_etudiant = prenom_etudiant;
		this.sexe_etudiant = sexe_etudiant;
		this.dateNaiss_etudiant = dateNaiss_etudiant;
		this.lieuNaiss_etudiant = lieuNaiss_etudiant;
		this.numTel_etudiant = numTel_etudiant;
		this.email_etudiant = email_etudiant;
		this.adresse_etudiant = adresse_etudiant;
		this.codePostal_etudiant = codePostal_etudiant;
		this.image_etudiant = image_etudiant;
		this.cle_departement_etudiant = cle_departement_etudiant;
		this.cle_travailPfe_etudiant = cle_travailPfe_etudiant;
		this.cle_etudiant1_etudiant = cle_etudiant1_etudiant;
		this.cle_filiere_etudiant = cle_filiere_etudiant;
		this.cle_document_etudiant = cle_document_etudiant;
	}
	public int getId_etudiant() {
		return id_etudiant;
	}
	public String getNum_inscrit() {
		return num_inscrit;
	}
	public String getCin_etudiant() {
		return cin_etudiant;
	}
	public String getNom_etudiant() {
		return nom_etudiant;
	}
	public String getPrenom_etudiant() {
		return prenom_etudiant;
	}
	public String getSexe_etudiant() {
		return sexe_etudiant;
	}
	public String getDateNaiss_etudiant() {
		return dateNaiss_etudiant;
	}
	public String getLieuNaiss_etudiant() {
		return lieuNaiss_etudiant;
	}
	public String getNumTel_etudiant() {
		return numTel_etudiant;
	}
	public String getEmail_etudiant() {
		return email_etudiant;
	}
	public String getAdresse_etudiant() {
		return adresse_etudiant;
	}
	public String getCodePostal_etudiant() {
		return codePostal_etudiant;
	}
	public String getImage_etudiant() {
		return image_etudiant;
	}
	public Integer getCle_departement_etudiant() {
		return cle_departement_etudiant;
	}
	public Integer getCle_travailPfe_etudiant() {
		return cle_travailPfe_etudiant;
	}
	public Integer getCle_etudiant1_etudiant() {
		return cle_etudiant1_etudiant;
	}
	public Integer getCle_filiere_etudiant() {
		return cle_filiere_etudiant;
	}
	public Integer getCle_document_etudiant() {
		return cle_document_etudiant;
	}
	public void setId_etudiant(int id_etudiant) {
		this.id_etudiant = id_etudiant;
	}
	public void setNum_inscrit(String num_inscrit) {
		this.num_inscrit = num_inscrit;
	}
	public void setCin_etudiant(String cin_etudiant) {
		this.cin_etudiant = cin_etudiant;
	}
	public void setNom_etudiant(String nom_etudiant) {
		this.nom_etudiant = nom_etudiant;
	}
	public void setPrenom_etudiant(String prenom_etudiant) {
		this.prenom_etudiant = prenom_etudiant;
	}
	public void setSexe_etudiant(String sexe_etudiant) {
		this.sexe_etudiant = sexe_etudiant;
	}
	public void setDateNaiss_etudiant(String dateNaiss_etudiant) {
		this.dateNaiss_etudiant = dateNaiss_etudiant;
	}
	public void setLieuNaiss_etudiant(String lieuNaiss_etudiant) {
		this.lieuNaiss_etudiant = lieuNaiss_etudiant;
	}
	public void setNumTel_etudiant(String numTel_etudiant) {
		this.numTel_etudiant = numTel_etudiant;
	}
	public void setEmail_etudiant(String email_etudiant) {
		this.email_etudiant = email_etudiant;
	}
	public void setAdresse_etudiant(String adresse_etudiant) {
		this.adresse_etudiant = adresse_etudiant;
	}
	public void setCodePostal_etudiant(String codePostal_etudiant) {
		this.codePostal_etudiant = codePostal_etudiant;
	}
	public void setImage_etudiant(String image_etudiant) {
		this.image_etudiant = image_etudiant;
	}
	public void setCle_departement_etudiant(Integer cle_departement_etudiant) {
		this.cle_departement_etudiant = cle_departement_etudiant;
	}
	public void setCle_travailPfe_etudiant(Integer cle_travailPfe_etudiant) {
		this.cle_travailPfe_etudiant = cle_travailPfe_etudiant;
	}
	public void setCle_etudiant1_etudiant(Integer cle_etudiant1_etudiant) {
		this.cle_etudiant1_etudiant = cle_etudiant1_etudiant;
	}
	public void setCle_filiere_etudiant(Integer cle_filiere_etudiant) {
		this.cle_filiere_etudiant = cle_filiere_etudiant;
	}
	public void setCle_document_etudiant(Integer cle_document_etudiant) {
		this.cle_document_etudiant = cle_document_etudiant;
	}




}
