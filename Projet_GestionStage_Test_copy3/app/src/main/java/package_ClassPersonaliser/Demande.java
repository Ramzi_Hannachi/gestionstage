package package_ClassPersonaliser;

public class Demande 
{
	private String confirmaiton_demande;
	private String  date_demande;
	private int id_enseignant_demande;
	private int id_etudiant_demande;

	public Demande(String confirmaiton_demande, String date_demande,
			int id_enseignant_demande, int id_etudiant_demande)
	{
		super();
		this.confirmaiton_demande = confirmaiton_demande;
		this.date_demande = date_demande;
		this.id_enseignant_demande = id_enseignant_demande;
		this.id_etudiant_demande = id_etudiant_demande;
	}

	public String getConfirmaiton_demande() {
		return confirmaiton_demande;
	}

	public String getDate_demande() {
		return date_demande;
	}

	public int getId_enseignant_demande() {
		return id_enseignant_demande;
	}

	public int getId_etudiant_demande() {
		return id_etudiant_demande;
	}

	public void setConfirmaiton_demande(String confirmaiton_demande) {
		this.confirmaiton_demande = confirmaiton_demande;
	}

	public void setDate_demande(String date_demande) {
		this.date_demande = date_demande;
	}

	public void setId_enseignant_demande(int id_enseignant_demande) {
		this.id_enseignant_demande = id_enseignant_demande;
	}

	public void setId_etudiant_demande(int id_etudiant_demande) {
		this.id_etudiant_demande = id_etudiant_demande;
	}



}
